/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_PROJ_H
#define GEOSTACK_PROJ_H

#include <string>

#include "json11.hpp"

#include "gs_solver.h"

// Link to external resources
extern const char R_PROJ4_datum_csv[];
extern const uint32_t R_PROJ4_datum_csv_size;

extern const char R_PROJ4_ellipsoid_csv[];
extern const uint32_t R_PROJ4_ellipsoid_csv_size;

extern const char R_cl_projection_head_c[];
extern const uint32_t R_cl_projection_head_c_size;

extern const char R_cl_projection_c[];
extern const uint32_t R_cl_projection_c_size;

namespace Geostack
{
    // Forward declarations
    template <typename T>
    class Coordinate;

    // Forward declarations
    template <typename T>
    class VertexCollection;

    /**
    * %Projection parameter structure
    */
    template <typename CTYPE>
    struct alignas(8) ProjectionParameters {

        uint8_t type;    // Projection type:
                         //    1. Projection coordinate system
                         //    2. Geographic latitude-longitude system
        uint8_t cttype;  // Coordinate transformation type:
                         //    1. Transverse Mercator
                         //    7. Mercator
                         //    8. Lambert conformal conic (2 point)
                         //   11. Albers equal area 
        uint8_t stype;   // Sampling type:
                         //    0. Nearest neighbour
                         //    1. Bilinear
        CTYPE a;         // Equatorial radius
        CTYPE f;         // Flattening
        CTYPE x0;        // Central meridian longitude
        CTYPE k0;        // Central meridian scale factor
        CTYPE fe;        // False easting
        CTYPE fn;        // False northing
        CTYPE phi_0;     // Origin latitude
        CTYPE phi_1;     // 1st standard latitude
        CTYPE phi_2;     // 2nd standard latitude
    };

    /**
    * %ProjectionTable class
    */
    class ProjectionTable {
    
    public:
          
        // Get singleton instance of %ProjectionTable.
        static ProjectionTable &instance();
        
        // Get datum
        bool getDatum(std::string datumName, std::string &ellipsoidName);

        // Get ellipsoid
        bool getEllipsoid(std::string ellipsoidName, double &equatorialRadius, double &inverseFlattening);

        // Get ellipsoid name
        template <typename CTYPE>
        std::string getEllipsoidName(CTYPE equatorialRadius, CTYPE inverseFlattening);

    private:

        void initialise();
        volatile bool initialised;

        // Projection tables
        std::map<std::string, std::string > datumTable;
        std::map<std::string, std::vector<double> > ellipsoidTable;
    };

    /**
    * %ProjectionOperator class %Projection OpenCL.
    */
    class ProjectionOperator {
    
    public:

        // Get singleton instance of %ProjectionOperator.
        static ProjectionOperator &instance();

        // Get kernel
        cl::Kernel getKernel(std::string kernel);        

    private:

        void initialise(Solver &);
        volatile bool initialised;
        std::size_t clProjectionHash;
    };
    
    /**
    * %Projection class for geospatial projections.
    */
    template <typename CTYPE>
    class Projection {

    public:

        // Parse PROJ4 string to and from projection object
        static ProjectionParameters<CTYPE> parsePROJ4(const std::string PROJ4);
        static std::string toPROJ4(const ProjectionParameters<CTYPE> proj);

        // Parse OGC WKT string into projection object
        static ProjectionParameters<CTYPE> parseWKT(const std::string WKT);

        // Project coordinates
        static bool convert(Coordinate<CTYPE> &c, ProjectionParameters<CTYPE> to, ProjectionParameters<CTYPE> from);
        static bool convert(std::vector<Coordinate<CTYPE> > &c, ProjectionParameters<CTYPE> to, ProjectionParameters<CTYPE> from);
        static bool convert(VertexCollection<CTYPE> &v, ProjectionParameters<CTYPE> to, ProjectionParameters<CTYPE> from);

        // Check projection
        static bool checkProjection(const ProjectionParameters<CTYPE> &proj);
    };

    // Comparison operators
    template <typename CTYPE>
    bool operator==(const ProjectionParameters<CTYPE> &l, const ProjectionParameters<CTYPE> &r);

    template <typename CTYPE>
    bool operator!=(const ProjectionParameters<CTYPE> &l, const ProjectionParameters<CTYPE> &r);
}

#endif