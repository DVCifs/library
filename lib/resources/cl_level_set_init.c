/**
* Level set initialisation
*/
__kernel void init(
parameters _p,
__global REAL *_vars,
/*__ARGS__*/
) {

    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {
    
        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;

/*__VARS__*/

        // Set initial classification
        _class = 3; // Default value of 3 for 10b (class 1) | 01b (state 1)

        // Cell state is given by lower bit of classification buffer 
        uint _class_state = 1; 

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (_class&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = _class>>24; 

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------

        // Overwrite state if class is zero
        if (_class_lo == 0) _class_state = 0;

        // Set class
        _class = ((_class_sub_lo&0xFF)<<24)|((_class_lo&0x7FFFFF)<<1)|(_class_state&0x1); 

/*__POST__*/

    }

}