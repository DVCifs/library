# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import sys
import requests
from .. import core
import warnings

try:
    import httplib
except:
    import http.client as httplib

__all__ = ["get_wkt_from_epsg_code", "get_epsg", "proj4_from_wkt",
    "have_internet"]

def have_internet():
    conn = httplib.HTTPConnection("www.google.com", timeout=5)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except:
        conn.close()
        return False

def get_wkt_for_epsg_code(epsg_code, timeout=10):
    try:
        out = requests.get("http://epsg.io/%d.wkt" % epsg_code, timeout=timeout)
        if out.status_code == 200:
            wkt_string = out.content.decode()
        else:
            raise RuntimeError("Unable to get the projection for epsg code")
        return wkt_string
    except requests.ConnectionError:
        raise RuntimeError("No internet connection")


def get_epsg(epsg_code):
    '''Get WKT string for a given EPSG code.

    This function uses gets the wkt string for the given epsg code from epsg.io

    Parameters
    ----------
    epsg_code : integer

    Returns
    --------
    out = Json11 object

    Examples
    --------
    >>> out = get_epsg(4326)
    '''
    if not isinstance(epsg_code, int):
        raise TypeError("EPSG code should be of integer type")
    return core.ProjectionParameters.from_wkt(get_wkt_for_epsg_code(epsg_code))


def proj4_from_wkt(inp_str):
    """Convert a WKT string to a proj4 string

    Parameters
    ----------
    inp_str : str
        A WKT representation of a coordinate reference system
    
    Returns
    -------
    out_str : str
        A proj4 representation of a coordinate reference system

    Example
    -------
    >>> inp_str = get_wkt_for_epsg_code(3577)
    >>> out_str = proj4_from_wkt(inp_str)
    """
    has_pyproj = False
    try:
        import pyproj
        has_pyproj = True
    except ImportError:
        warnings.warn("Unable to import pyproj", ImportWarning)

    if has_pyproj:
        out_str = pyproj.CRS(projparams=inp_str).to_proj4()
    else:
        out_str = inp_str
    return out_str

if __name__ == "__main__":
    out = get_epsg(4326)
