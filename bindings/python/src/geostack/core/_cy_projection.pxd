# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3

from libcpp.vector cimport vector
from libcpp.string cimport string
from libc.stdint cimport uint32_t, int32_t, uint64_t
from libcpp cimport bool
from ..vector._cy_vector cimport _Coordinate_d, _Coordinate_f

cdef extern from "gs_vector.h" namespace "Geostack":
    cdef cppclass Coordinate[C]:
        pass

cdef extern from "gs_projection.h" namespace "Geostack":

    cdef cppclass ProjectionParameters[C]:
        uint32_t type   # instance data type
        uint32_t cttype # coordinate transformation type
        uint32_t stype  # sampling type
        C a
        C f
        C x0
        C k0
        C fe
        C fn
        C phi_0
        C phi_1
        C phi_2

    cdef cppclass Projection[C]:

        @staticmethod
        ProjectionParameters[C] parsePROJ4(string PROJ4)

        @staticmethod
        ProjectionParameters[C] parseWKT(string PROJ4)

        @staticmethod
        bool convert "convert"(Coordinate[C] &c, ProjectionParameters[C] &to_,
            ProjectionParameters[C] &from_)

        @staticmethod
        bool convertVector "convert"(vector[Coordinate[C]] &c, ProjectionParameters[C] &to_,
            ProjectionParameters[C] &from_)

    bool operator==[C](ProjectionParameters[C] &l, ProjectionParameters[C] &r)
    bool operator!=[C](ProjectionParameters[C] &l, ProjectionParameters[C] &r)


cdef class _ProjectionParameters_f:
    cdef ProjectionParameters[float] *thisptr
    cdef void c_copy(self, ProjectionParameters[float] inp)

cdef class _ProjectionParameters_d:
    cdef ProjectionParameters[double] *thisptr
    cdef void c_copy(self, ProjectionParameters[double] inp)

cpdef bool _convert_f(_Coordinate_f inp, _ProjectionParameters_f proj_to,
    _ProjectionParameters_f proj_from)
cpdef bool _convert_d(_Coordinate_d inp, _ProjectionParameters_d proj_to,
    _ProjectionParameters_d proj_from)
cpdef float[:, :] _convert_points_f(float[:, :] inp, _ProjectionParameters_f proj_to,
    _ProjectionParameters_f proj_from)
cpdef double[:, :] _convert_points_d(double[:, :] inp, _ProjectionParameters_d proj_to,
    _ProjectionParameters_d proj_from)

cdef _ProjectionParameters_d _parsePROJ4_d(string proj4)
cdef _ProjectionParameters_f _parsePROJ4_f(string proj4)
cdef _ProjectionParameters_d _parseWKT_d(string proj4)
cdef _ProjectionParameters_f _parseWKT_f(string proj4)