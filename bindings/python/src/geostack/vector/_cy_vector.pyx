# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref, preincrement as preinc
from libcpp cimport nullptr_t, nullptr
from libcpp.memory cimport shared_ptr, make_shared
from libcpp.string cimport string
from libcpp.map cimport map as cpp_map
from libcpp.pair cimport pair as cpp_pair
from libcpp.vector cimport vector
from libc.math cimport ceil
from libc.stdio cimport printf
from libc.string cimport memcpy
import numpy as np
cimport numpy as np
from cpython cimport PyObject
from ..raster._cy_raster cimport Raster, GeometryType
from libc.stdint cimport uint32_t, uint16_t, uint64_t

np.import_array()

cdef class IndexList:
    def __cinit__(self):
        self.thisptr = new vector[cl_uint]()
    
    cdef void c_copy(self, _index_list other):
        deref(self.thisptr).clear()
        self.thisptr = new vector[cl_uint](other)

    @property
    def size(self):
        return deref(self.thisptr).size()

    @staticmethod
    cdef IndexList from_index_list(_index_list this):
        out = IndexList()
        out.c_copy(this)
        return out

    def as_array(self):
        cdef uint64_t[:] out
        out = np.zeros(shape=(self.size,), dtype=np.uint64)
        memcpy(&out[0], deref(self.thisptr).data(),
            self.size * sizeof(size_t))
        return out

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        cdef int out
        if self.index < self.size:
            out = deref(self.thisptr)[self.index]
            self.index += 1
            return out
        else:
            self.index = 0
            raise StopIteration

    def __getitem__(self, size_t index):
        cdef int out
        if index >= 0 and index < self.size:
            out = deref(self.thisptr)[index]
            return out
        else:
            raise IndexError(f"{index} is out of range")

    def __len__(self):
        return self.size

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        deref(self.thisptr).clear()
        del self.thisptr

include "_cy_vector.pxi"

cdef class _Vector_d:
    def __cinit__(self):
        self.thisptr.reset(new Vector[double]())

    cdef void c_copy(self, Vector[double] v):
        self.thisptr.reset()
        self.thisptr = move(make_shared[Vector[double]](v))

    cdef size_t _add_point(self, Coordinate[double] c_):
        cdef size_t ret = 0
        ret = deref(self.thisptr).addPoint(c_)
        return ret

    def addPoint(self, _Coordinate_d c):
        cdef size_t out
        out = self._add_point(deref(c.thisptr))
        return <int> out

    cdef size_t _add_line_string(self, vector[Coordinate[double]] cs_):
        cdef size_t ret = 0
        ret = deref(self.thisptr).addLineString(cs_)
        return ret

    def addLineString(self, double[:, :] cs):
        cdef int i, nx
        cdef vector[Coordinate[double]] _list_of_coords
        cdef size_t ret
        nx = <int> cs.shape[0]
        if cs.shape[1] != 4:
            raise TypeError("numpy array should be of dimensions (npoints, 4)")
        for i in range(nx):
            c_ = _Coordinate_d(cs[i, 0], cs[i, 1],
                lev=cs[i, 3], tstep=cs[i, 3])
            _list_of_coords.push_back(deref(c_.thisptr))
        ret = self._add_line_string(_list_of_coords)
        return ret

    cdef size_t _add_polygon(self, vector[vector[Coordinate[double]]] pcs_):
        cdef size_t ret = 0
        ret = deref(self.thisptr).addPolygon(pcs_)
        return ret

    def addPolygon(self, list pcs):
        cdef int i, j, npolys, ncoords
        cdef vector[vector[Coordinate[double]]] pcs_
        cdef vector[Coordinate[double]] cs_
        cdef size_t out
        npolys = <int> len(pcs)
        for j in range(npolys):
            if not isinstance(pcs[j], np.ndarray):
                raise TypeError("only numpy arrays are acceptable")
            if <int> pcs[j].shape[1] != 4:
                raise ValueError("numpy array of coordinates should be (npoints,4)")
            ncoords = <int> pcs[j].shape[0]
            for i in range(ncoords):
                c_ = _Coordinate_d(<double> pcs[j][i, 0], <double> pcs[j][i, 1],
                    lev=<double> pcs[j][i, 2], tstep=<double> pcs[j][i, 3])
                cs_.push_back(deref(c_.thisptr))
            pcs_.push_back(cs_)
        out = self._add_polygon(pcs_)
        return <int> out

    cpdef void updatePointIndex(self, size_t index):
        deref(self.thisptr).updatePointIndex(index)

    cpdef void updateLineStringIndex(self, size_t index):
        deref(self.thisptr).updateLineStringIndex(index)

    cpdef void updatePolygonIndex(self, size_t index):
        deref(self.thisptr).updatePolygonIndex(index)

    cpdef void clear(self):
        deref(self.thisptr).clear()

    cpdef IndexList getGeometryIndexes(self):
        cdef _index_list _geom_indices
        with nogil:
            _geom_indices = deref(self.thisptr).getGeometryIndexes()
        out = IndexList.from_index_list(_geom_indices)
        return out

    cpdef IndexList getPointIndexes(self):
        cdef _index_list _point_indices
        with nogil:
            _point_indices = deref(self.thisptr).getPointIndexes()
        out = IndexList.from_index_list(_point_indices)
        return out

    cpdef IndexList getLineStringIndexes(self):
        cdef _index_list _linestr_indices
        with nogil:
            _linestr_indices = deref(self.thisptr).getLineStringIndexes()
        out = IndexList.from_index_list(_linestr_indices)
        return out

    cpdef IndexList getPolygonIndexes(self):
        cdef _index_list _polygon_indices
        with nogil:
            _polygon_indices = deref(self.thisptr).getPolygonIndexes()
        out = IndexList.from_index_list(_polygon_indices)
        return out

    cpdef _Coordinate_d getPointCoordinate(self, size_t index):
        cdef int i
        cdef _coordinate_d _out
        _out = deref(self.thisptr).getPointCoordinate(index)
        out = _Coordinate_d(0.0, 0.0)
        out.c_copy(_out)
        return out

    cpdef double[:, :] getLineStringCoordinates(self, size_t index):
        cdef int i
        cdef vector[_coordinate_d] _out
        cdef double[:, :] out
        _out = deref(self.thisptr).getLineStringCoordinates(index)

        out = np.zeros(shape=(_out.size(), 4), dtype=np.float64)
        for i in range(<int>_out.size()):
            out[i, 0] = _out[i].p
            out[i, 1] = _out[i].q
            out[i, 2] = _out[i].r
            out[i, 3] = _out[i].s
        return out

    cpdef double[:, :] getPolygonCoordinates(self, size_t index):
        cdef int i
        cdef vector[_coordinate_d] _out
        cdef double[:, :] out
        _out = deref(self.thisptr).getPolygonCoordinates(index)
        out = np.zeros(shape=(_out.size(), 4), dtype=np.float64)
        for i in range(<int>_out.size()):
            out[i, 0] = _out[i].p
            out[i, 1] = _out[i].q
            out[i, 2] = _out[i].r
            out[i, 3] = _out[i].s
        return out

    cpdef IndexList getPolygonSubIndexes(self, size_t index):
        cdef _index_list _polygon_indices
        with nogil:
            _polygon_indices = deref(self.thisptr).getPolygonSubIndexes(index)
        out = IndexList.from_index_list(_polygon_indices)
        return out

    def getPolygonSubBounds(self, int index):
        cdef _bbox_list_d _polygon_bounds
        with nogil:
            _bbox_list_d = deref(self.thisptr).getPolygonSubBounds(<size_t> index)

    def setProperty(self, int index, bytes name, other):

        if isinstance(other, (str, bytes)):
            if isinstance(other, str):
                deref(self.thisptr).setProperty[string](<size_t> index,
                    <string> name, <string> other.encode('UTF-8'))
            else:
                deref(self.thisptr).setProperty[string](<size_t> index,
                    <string> name, <string> other)
        elif isinstance(other, int):
                deref(self.thisptr).setProperty[int](<size_t> index,
                    <string> name, <int> other)
        elif isinstance(other, float):
                deref(self.thisptr).setProperty[double](<size_t> index,
                    <string> name, <double> other)
        else:
            raise TypeError("other can be of type str, int or float")

    def getProperty(self, int idx, bytes prop_name, prop_type):
        cdef int _out_i
        cdef double _out_d
        cdef string _out_s

        if prop_type == str:
            _out_s = deref(self.thisptr).getProperty[string](<size_t> idx,
                <string>prop_name)
            return _out_s
        elif prop_type == int:
            _out_i = deref(self.thisptr).getProperty[int](<size_t> idx,
                <string>prop_name)
            return _out_i
        elif prop_type == float:
            _out_d = deref(self.thisptr).getProperty[double](<size_t> idx,
                <string>prop_name)
            return _out_d

    def deleteProperty(self, int idx, bytes prop_name):
        deref(self.thisptr).deleteProperty(<size_t>idx, <string> prop_name)

    cpdef void setProjectionParameters(self, _ProjectionParameters_d proj_):
        deref(self.thisptr).setProjectionParameters(deref(proj_.thisptr))

    cpdef _ProjectionParameters_d getProjectionParameters(self):
        out = _ProjectionParameters_d()
        out.c_copy(deref(self.thisptr).getProjectionParameters())
        return out

    cpdef _Vector_d convert(self, _ProjectionParameters_d this):
        out = _Vector_d()
        out.c_copy(deref(self.thisptr).convert(deref(this.thisptr)))
        return out

    # def getPoints(self):
    #     cdef double[:, :] points_out
    #     cdef int i, nx
    #     cdef Coordinate[double] c
    #     cdef _PointPtr_d p
    #     with nogil:
    #         if self._vec_points.empty():
    #             self._vec_points = self._get_points()
    #         nx = <int> self._vec_points.size()
    #     return _ListOfPoints_d.c_copy(self._vec_points)

    # def getLineStrings(self):
    #     cdef double[:, :] _line
    #     cdef vector[Coordinate[double]] _list_of_coords
    #     cdef Coordinate[double] c
    #     cdef int i, j, nlines, nx

    #     lines_out = []
    #     with nogil:
    #         if self._vec_lines.empty():
    #             self._vec_lines = self._get_line_strings()
    #         nlines = <int> self._vec_lines.size()
    #     return _ListOfLines_d.c_copy(self._vec_lines)

    # def getPolygons(self):
    #     cdef double[:, :] _poly
    #     cdef vector[Coordinate[double]] _list_of_coords
    #     cdef Coordinate[double] c
    #     cdef int i, j, npolys, nx

    #     polys_out = []
    #     with nogil:
    #         if self._vec_polygons.empty():
    #             self._vec_polygons = self._get_polygons()
    #         npolys = <int> self._vec_polygons.size()
    #     return _ListOfPolygons_d.c_copy(self._vec_polygons)

    # def getPolygonBounds(self, int idx):
    #     _out = _BoundingBox_d()
    #     if self._vec_polygons.empty():
    #         self.getPolygons()
    #     if not (idx >= 0 and idx < <int> self._vec_polygons.size()):
    #         raise IndexError("index %d is out of bounds" % idx)
    #     _out.c_copy(deref(self._vec_polygons[idx]).getBounds())
    #     return _out

    cpdef _Vector_d region(self, _BoundingBox_d other, size_t parameter):
        out = _Vector_d()
        out.c_copy(deref(self.thisptr).region(deref(other.thisptr), parameter))
        return out

    cpdef _Vector_d nearest(self, _BoundingBox_d other, size_t parameter):
        out = _Vector_d()
        out.c_copy(deref(self.thisptr).nearest(deref(other.thisptr), parameter))
        return out

    cpdef _Vector_d attached(self, _Coordinate_d other, size_t parameter):
        cdef vector[shared_ptr[GeometryBase[double]]] geom_base_ptr
        out = _Vector_d()
        geom_base_ptr = deref(self.thisptr).attached(deref(other.thisptr), parameter)
        return out

    def deduplicateVertices(self):
        deref(self.thisptr).deduplicateVertices()

    cpdef _cyRaster_d mapDistance(self, double resolution, size_t geom_type,
        _BoundingBox_d bounds) except+:
        cdef Raster[double, double] _out
        out = _cyRaster_d()
        with nogil:
            _out = deref(self.thisptr).mapDistance[double](resolution,
                geom_type, deref(bounds.thisptr))
        out.c_rastercopy(_out)
        return out

    cpdef _cyRaster_d rasterise(self, double resolution, string script,
        size_t geom_type, _BoundingBox_d bounds) except+:
        cdef Raster[double, double] _out
        out = _cyRaster_d()
        with nogil:
            _out = deref(self.thisptr).rasterise[double](resolution, script,
                geom_type, deref(bounds.thisptr))
        out.c_rastercopy(_out)
        return out

    cpdef bool pointSample(self, _cyRaster_d r):
        cdef bool out = deref(self.thisptr).pointSample(deref(r.derivedptr))
        return out

    cpdef _BoundingBox_d getBounds(self):
        _out = _BoundingBox_d()
        _out.c_copy(deref(self.thisptr).getBounds())
        return _out

    def hasData(self):
        cdef bool out
        out = deref(self.thisptr).hasData()
        return out

    def __eq__(self, _Vector_d other):
        cdef bool out
        out = self.thisptr == other.thisptr
        return out

    def __ne__(self, _Vector_d other):
        cdef bool out
        out = self.thisptr != other.thisptr
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        self.thisptr.reset()

cdef class _Vector_f:
    def __cinit__(self):
        self.thisptr.reset(new Vector[float]())

    cdef void c_copy(self, Vector[float] v):
        self.thisptr.reset()
        self.thisptr = move(make_shared[Vector[float]](v))

    cdef size_t _add_point(self, Coordinate[float] c_):
        cdef size_t ret = 0
        ret = deref(self.thisptr).addPoint(c_)
        return ret

    def addPoint(self, _Coordinate_f c):
        cdef size_t out
        out = self._add_point(deref(c.thisptr))
        return <int> out

    cdef size_t _add_line_string(self, vector[Coordinate[float]] cs_):
        cdef size_t ret = 0
        ret = deref(self.thisptr).addLineString(cs_)
        return ret

    def addLineString(self, float[:, :] cs_):
        cdef int i, nx
        cdef vector[Coordinate[float]] _list_of_coords
        cdef size_t out
        nx = <int> cs_.shape[0]

        if cs_.shape[1] != 4:
            raise TypeError("numpy array should be of dimensions (npoints, 4)")
        for i in range(nx):
            c_ = _Coordinate_f(cs_[i, 0], cs_[i, 1],
                lev=cs_[i, 3], tstep=cs_[i, 3])
            _list_of_coords.push_back(deref(c_.thisptr))
        out = self._add_line_string(_list_of_coords)
        return out

    cdef size_t _add_polygon(self, vector[vector[Coordinate[float]]] pcs_):
        cdef size_t ret = 0
        ret = deref(self.thisptr).addPolygon(pcs_)
        return ret

    def addPolygon(self, list pcs):
        cdef int i, j, npolys, ncoords
        cdef vector[vector[Coordinate[float]]] pcs_
        cdef vector[Coordinate[float]] cs_
        cdef size_t out
        npolys = <int> len(pcs)
        for j in range(npolys):
            if not isinstance(pcs[j], np.ndarray):
                raise TypeError("only numpy arrays are acceptable")
            if <int> pcs[j].shape[1] != 4:
                raise ValueError("numpy array of coordinates should be (npoints,4)")
            ncoords = <int> pcs[j].shape[0]
            for i in range(ncoords):
                c_ = _Coordinate_f(<float> pcs[j][i, 0], <float> pcs[j][i, 1],
                    lev=<float> pcs[j][i, 3], tstep=<float> pcs[j][i, 3])
                cs_.push_back(deref(c_.thisptr))
            pcs_.push_back(cs_)
        out = self._add_polygon(pcs_)
        return <int> out

    cpdef void updatePointIndex(self, size_t index):
        deref(self.thisptr).updatePointIndex(index)

    cpdef void updateLineStringIndex(self, size_t index):
        deref(self.thisptr).updateLineStringIndex(index)

    cpdef void updatePolygonIndex(self, size_t index):
        deref(self.thisptr).updatePolygonIndex(index)

    cpdef void clear(self):
        deref(self.thisptr).clear()

    cpdef IndexList getGeometryIndexes(self):
        cdef _index_list _geom_indices
        with nogil:
            _geom_indices = deref(self.thisptr).getGeometryIndexes()
        out = IndexList.from_index_list(_geom_indices)
        return out

    cpdef IndexList getPointIndexes(self):
        cdef _index_list _point_indices
        with nogil:
            _point_indices = deref(self.thisptr).getPointIndexes()
        out = IndexList.from_index_list(_point_indices)
        return out

    cpdef IndexList getLineStringIndexes(self):
        cdef _index_list _linestr_indices
        with nogil:
            _linestr_indices = deref(self.thisptr).getLineStringIndexes()
        out = IndexList.from_index_list(_linestr_indices)
        return out

    cpdef IndexList getPolygonIndexes(self):
        cdef _index_list _polygon_indices
        with nogil:
            _polygon_indices = deref(self.thisptr).getPolygonIndexes()
        out = IndexList.from_index_list(_polygon_indices)
        return out

    cpdef _Coordinate_f getPointCoordinate(self, size_t index):
        cdef int i
        cdef _coordinate_f _out
        _out = deref(self.thisptr).getPointCoordinate(index)
        out = _Coordinate_f(0.0, 0.0)
        out.c_copy(_out)
        return out

    cpdef float[:, :] getLineStringCoordinates(self, size_t index):
        cdef int i
        cdef vector[_coordinate_f] _out
        cdef float[:, :] out
        _out = deref(self.thisptr).getLineStringCoordinates(<size_t> index)
        out = np.zeros(shape=(_out.size(), 4), dtype=np.float32)
        for i in range(<int>_out.size()):
            out[i, 0] = _out[i].p
            out[i, 1] = _out[i].q
            out[i, 2] = _out[i].r
            out[i, 3] = _out[i].s
        return out

    cpdef float[:, :] getPolygonCoordinates(self, size_t index):
        cdef int i
        cdef vector[Coordinate[float]] _out
        cdef float[:, :] out
        _out = deref(self.thisptr).getPolygonCoordinates(<int> index)
        out = np.zeros(shape=(_out.size(), 4), dtype=np.float32)
        for i in range(<int>_out.size()):
            out[i, 0] = _out[i].p
            out[i, 1] = _out[i].q
            out[i, 2] = _out[i].r
            out[i, 3] = _out[i].s
        return out

    cpdef IndexList getPolygonSubIndexes(self, size_t index):
        cdef _index_list _polygon_indices
        with nogil:
            _polygon_indices = deref(self.thisptr).getPolygonSubIndexes(index)
        out = IndexList.from_index_list(_polygon_indices)
        return out

    def getPolygonSubBounds(self, int index):
        cdef _bbox_list_d _polygon_bounds
        with nogil:
            _bbox_list_d = deref(self.thisptr).getPolygonSubBounds(<size_t> index)

    def setProperty(self, int index, bytes name, other):

        if isinstance(other, (str, bytes)):
            if isinstance(other, str):
                deref(self.thisptr).setProperty[string](<size_t> index,
                    <string> name, <string> other.encode('UTF-8'))
            else:
                deref(self.thisptr).setProperty[string](<size_t> index,
                    <string> name, <string> other)
        elif isinstance(other, int):
                deref(self.thisptr).setProperty[int](<size_t> index,
                    <string> name, <int> other)
        elif isinstance(other, float):
                deref(self.thisptr).setProperty[double](<size_t> index,
                    <string> name, <double> other)
        else:
            raise TypeError("other can be of type str, int or float")

    def getProperty(self, int idx, bytes prop_name, prop_type):
        cdef int _out_i
        cdef double _out_d
        cdef string _out_s

        if prop_type == str:
            _out_s = deref(self.thisptr).getProperty[string](<size_t> idx,
                <string>prop_name)
            return _out_s
        elif prop_type == int:
            _out_i = deref(self.thisptr).getProperty[int](<size_t> idx,
                <string>prop_name)
            return _out_i
        elif prop_type == float:
            _out_d = deref(self.thisptr).getProperty[double](<size_t> idx,
                <string>prop_name)
            return _out_d

    def deleteProperty(self, int idx, bytes prop_name):
        deref(self.thisptr).deleteProperty(<size_t> idx, <string>prop_name)

    cpdef void setProjectionParameters(self, _ProjectionParameters_f proj_):
        deref(self.thisptr).setProjectionParameters(deref(proj_.thisptr))

    cpdef _ProjectionParameters_f getProjectionParameters(self):
        out = _ProjectionParameters_f()
        out.c_copy(deref(self.thisptr).getProjectionParameters())
        return out

    cpdef _Vector_f convert(self, _ProjectionParameters_f this):
        out = _Vector_f()
        out.c_copy(deref(self.thisptr).convert(deref(this.thisptr)))
        return out

    # def getPoints(self):
    #     cdef float[:, :] points_out
    #     cdef int i, nx
    #     cdef Coordinate[float] c
    #     cdef _PointPtr_f p
    #     with nogil:
    #         if self._vec_points.empty():
    #             self._vec_points = self._get_points()
    #         nx = <int> self._vec_points.size()
    #     return _ListOfPoints_f.c_copy(self._vec_points)

    # def getLineStrings(self):
    #     cdef float[:, :] _line
    #     cdef vector[Coordinate[float]] _list_of_coords
    #     cdef Coordinate[float] c
    #     cdef int i, j, nlines, nx

    #     lines_out = []
    #     with nogil:
    #         if self._vec_lines.empty():
    #             self._vec_lines = self._get_line_strings()
    #         nlines = <int> self._vec_lines.size()
    #     return _ListOfLines_f.c_copy(self._vec_lines)

    # def getPolygons(self):
    #     cdef float[:, :] _poly
    #     cdef vector[Coordinate[float]] _list_of_coords
    #     cdef Coordinate[float] c
    #     cdef int i, j, npolys, nx

    #     polys_out = []
    #     with nogil:
    #         if self._vec_polygons.empty():
    #             self._vec_polygons = self._get_polygons()
    #         npolys = <int> self._vec_polygons.size()
    #     return _ListOfPolygons_f.c_copy(self._vec_polygons)

    # def getPolygonBounds(self, int idx):
    #     _out = _BoundingBox_f()
    #     if self._vec_polygons.empty():
    #         self.getPolygons()
    #     if not (idx >= 0 and idx < <int> self._vec_polygons.size()):
    #         raise IndexError("index %d is out of bounds" % idx)
    #     _out.c_copy(deref(self._vec_polygons[idx]).getBounds())
    #     return _out

    cpdef _Vector_f region(self, _BoundingBox_f other, size_t parameter):
        out = _Vector_f()
        out.c_copy(deref(self.thisptr).region(deref(other.thisptr), parameter))
        return out

    cpdef _Vector_f nearest(self, _BoundingBox_f other, size_t parameter):
        out = _Vector_f()
        out.c_copy(deref(self.thisptr).nearest(deref(other.thisptr), parameter))
        return out

    cpdef _Vector_f attached(self, _Coordinate_f other, size_t parameter):
        cdef vector[shared_ptr[GeometryBase[float]]] geom_base_ptr
        out = _Vector_f()
        geom_base_ptr = deref(self.thisptr).attached(deref(other.thisptr), parameter)
        return out

    def deduplicateVertices(self):
        deref(self.thisptr).deduplicateVertices()

    cpdef _cyRaster_f mapDistance(self, float resolution, size_t geom_type,
        _BoundingBox_f bounds) except+:
        cdef Raster[float, float] _out
        out = _cyRaster_f()
        with nogil:
            _out = deref(self.thisptr).mapDistance[float](resolution, geom_type,
                deref(bounds.thisptr))
        out.c_rastercopy(_out)
        return out

    cpdef _cyRaster_f rasterise(self, float resolution, string script,
        size_t geom_type, _BoundingBox_f bounds) except+:
        cdef Raster[float, float] _out
        out = _cyRaster_f()
        with nogil:
            _out = deref(self.thisptr).rasterise[float](resolution, script,
                geom_type, deref(bounds.thisptr))
        out.c_rastercopy(_out)
        return out

    cpdef bool pointSample(self, _cyRaster_f r):
        cdef bool out = deref(self.thisptr).pointSample(deref(r.derivedptr))
        return out

    cpdef _BoundingBox_f getBounds(self):
        _out = _BoundingBox_f()
        _out.c_copy(deref(self.thisptr).getBounds())
        return _out

    def hasData(self):
        cdef bool out
        out = deref(self.thisptr).hasData()
        return out

    def __eq__(self, _Vector_f other):
        cdef bool out
        out = self.thisptr == other.thisptr
        return out

    def __ne__(self, _Vector_f other):
        cdef bool out
        out = self.thisptr != other.thisptr
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        self.thisptr.reset()