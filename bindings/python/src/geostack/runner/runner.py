# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import ctypes
import numbers
from typing import Union
import numpy as np
from ..raster import raster, _cy_raster
from .. import gs_enums
from ..runner._cy_runner import (
    test_solver,
    _run_script_noout_f,
    _run_script_noout_d,
    _run_script_f,
    _run_script_d,
    _run_script_d_i, 
    _run_script_f_i,
    _run_areascript_f, 
    _run_areascript_d,
    _run_areascript_d_i, 
    _run_areascript_f_i)

__all__ = ["runScript", "getSolver", "runAreaScript"]

def getSolver(verbosity=False):
    '''Get OpenCL instance.

    A function to invoke OpenCL solver when unable to aquire one during
    runScript execution.

    Parameters
    ----------
    verbosity: bool

    Returns
    -------
    Nil

    Examples
    --------
    >>> import geostack
    >>> from geostack.runner import getSolver
    >>> getSolver()
    '''
    
    # Check for solver
    if not test_solver(verbosity):
        raise RuntimeError("Unable to get solver")


def runScript(script, inputRasters,
              parameter: Union[numbers.Integral,
                               gs_enums.RasterCombinationType,
                               gs_enums.RasterResolutionType,
                               gs_enums.RasterNullValueType,
                               gs_enums.RasterInterpolationType,
                               gs_enums.ReductionType]=0,
              output_type=np.float32):
    '''Run script on a list of input raster over GPU

    Parameters
    ----------
    script : str
        Script to build kernel for running on GPU.
    inputRasters : list/tuple/RasterBaseList/RasterPtrList
        A list of input rasters
    parameter : Union[numbers.Integral,
                      gs_enums.RasterCombinationType,
                      gs_enums.RasterResolutionType,
                      gs_enums.RasterNullValueType,
                      gs_enums.RasterInterpolationType]
        parameter for controlling script processing, default 0
    output_type : np.float32/np.float64/np.uint32
        Data type for the output raster object from runScript

    Returns
    -------
    out : Raster object
        Raster output from the runScript

    Examples
    --------
    >>> out = runScript("out = 100 * testRasterA", [testRasterA])
    '''

    # Check script and encode as necessary
    if isinstance(script, (str, bytes)):
        if isinstance(script, str):
            _script = script.encode('UTF-8')
        else:
            _script = script
    else:
        raise TypeError("script can be only string or bytes")

    # check parameter
    if not isinstance(parameter, (numbers.Integral,
                                  gs_enums.RasterCombinationType,
                                  gs_enums.RasterResolutionType,
                                  gs_enums.RasterNullValueType,
                                  gs_enums.RasterInterpolationType,
                                  gs_enums.ReductionType)):
        raise TypeError("parameter should be int/ gs_enums")
    elif isinstance(parameter, numbers.Integral):
        # assign raw parameter
        _parameter = parameter
    else:
        # assign value from enum.Enum
        _parameter = parameter.value

    # Run script for list of rasters
    if isinstance(inputRasters, list):
    
        # Ensure list has entries
        if not(len(inputRasters) > 0):
            raise RuntimeError("length of inputRasters should be greater than 0")
            
        # Ensure list entires are Rasters or DataFileHandlers
        for i, item in enumerate(inputRasters, 0):
            if not isinstance(item, (raster.Raster, raster.RasterFile)):
                raise TypeError("item at %d in inputRasters is not of Raster/RasterFile type" % i)
        
        # Set base type to first raster type
        base_type = inputRasters[0].base_type
        
        # Check all raster types are the same as the base type
        for i, item in enumerate(inputRasters, 0):
            if item.base_type != base_type:
                raise TypeError("Basetype of input raster at %d is different. All the raster in list should be of same basetype" % i)

        # Create vector of raster bases
        vec = raster.RasterBaseList(dtype=base_type)
        for item in inputRasters:
            vec.append(item)

        if output_type is None:
        
            # Create script function based on types
            if base_type == np.float32:
                run_script_function = _run_script_noout_f
            elif base_type == np.float64:
                run_script_function = _run_script_noout_d
                          
            # Run script
            rc = run_script_function(_script, vec._handle, _parameter)
                
            if not rc:
                raise RuntimeError("Unable to run script")
            # Return first raster
            return inputRasters[0]
            
        else:
        
            # Check for valid types
            if output_type not in [np.uint32, np.float32, np.float64]:
                raise ValueError("Output type should np.float32/np.float64/np.uint32")
            
            # Create script function based on types
            if base_type == np.float32:
                if output_type == np.float32:
                    run_script_function = _run_script_f
                elif output_type == np.uint32:
                    run_script_function = _run_script_f_i
            elif base_type == np.float64:
                if output_type == np.float64:
                    run_script_function = _run_script_d
                elif output_type == np.uint32:
                    run_script_function = _run_script_d_i
            
            # Run script
            _out = run_script_function(_script, vec._handle, _parameter)

            # Convert from Cython to Python Raster
            out = raster.Raster.copy("output", _out)
            del(_out)
            
            # Return output raster
            return out
        
    # Run script for list of raster bases
    elif isinstance(inputRasters, raster.RasterBaseList):

        if output_type is None:
        
            # Create script function based on types
            if inputRasters._dtype == np.float32:
                run_script_function = _run_script_noout_f
            elif inputRasters._dtype == np.float64:
                run_script_function = _run_script_noout_d

            # Run script
            rc = run_script_function(_script, inputRasters._handle, _parameter)
            
            if not rc:
                raise RuntimeError("Unable to run script")
                
            # Return first raster
            return vec[0]
            
        else:
        
            # Create script function based on types
            if inputRasters._dtype == np.float32:
                if output_type == np.uint32:
                    run_script_function = _run_script_f_i
                else:
                    run_script_function = _run_script_f
            elif inputRasters._dtype == np.float64:
                if output_type == np.uint32:
                    run_script_function = _run_script_d_i
                else:
                    run_script_function = _run_script_d
            
            # Run script
            _out = run_script_function(_script, inputRasters._handle, _parameter)
        
            # Convert from Cython to Python Raster
            out = raster.Raster.copy("output", _out)
            del(_out)
            
            # Return output raster
            return out
        
    else:
    
        # Throw exception if input is not a valid list
        raise TypeError("inputRasters should be of type list")


def runAreaScript(script, inputRaster,
                  width: numbers.Integral=1,
                  output_type=np.float32):
    '''Run areascript on an input raster over GPU

    Parameters
    ----------
    script : str
        Script to build kernel for running on GPU.
    inputRasters : Raster/DataFileHandler
        An input raster.RasterFile object
    width : integer
        The width of the area window in cells, default 1
    output_type : np.float32/np.float64/np.uint32
        Data type for the output raster object from runScript

    Returns
    -------
    out : Raster object
        Raster output from the runScript

    Examples
    --------
    >>> from geostack import raster
    >>> script = """// Average
    ... if (isValid_REAL(testRasterA)) { 
    ... output += testRasterA; 
    ... sum++;
    ... }
    ... """
    >>> testRasterA = Raster(name="testRasterA", base_type=np.float32,
    ... data_type=np.float32)
    >>> testRasterA.init(21, 1.0, ny=21, hy=1.0)
    >>> testRasterA.setAllCellValues(0.0)
    >>> testRasterA.setCellValue(99.9, 10, 10)
    >>> out = runAreaScript(script, testRasterA, 3)
    '''

    # Check for valid types
    if output_type is None:
        raise ValueError("Output type should be provided")
    if output_type not in [np.uint32, np.float32, np.float64]:
        raise ValueError("Output type should np.float32/np.float64/np.uint32")

    # Check script and encode as necessary
    if isinstance(script, (str, bytes)):
        if isinstance(script, str):
            _script = script.encode('UTF-8')
        else:
            _script = script
    else:
        raise TypeError("script can be only string or bytes")

    if isinstance(width, numbers.Integral):
        _width = width
    else:
        raise TypeError("width should be integer")

    # Run script for list of rasters
    if isinstance(inputRaster, (raster.Raster, raster.RasterFile)):
    
        # Set base type
        base_type = inputRaster.base_type

        # Check base type
        if base_type == np.float32:
        
            # Create script function based on types
            if output_type == np.float32:
                run_script_function = _run_areascript_f
            elif output_type == np.uint32:
                run_script_function = _run_areascript_f_i
                
            # Run script
            _out = run_script_function(_script, inputRaster._handle, _width)
        
        elif base_type == np.float64:
        
            # Create script function based on types
            if output_type == np.float64:
                run_script_function = _run_areascript_d
            elif output_type == np.uint32:
                run_script_function = _run_areascript_d_i
                
            # Run script
            _out = run_script_function(_script, inputRaster._handle, _width)
    
    # Convert from Cython to Python Raster
    out = raster.Raster.copy("output", _out)
    del(_out)
    
    # Return output raster
    return out