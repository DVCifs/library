# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from .get_projection import (get_epsg, get_wkt_for_epsg_code, proj4_from_wkt,
    have_internet)