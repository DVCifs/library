/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

 #include "gs_property.h"

namespace Geostack
{

    /**
    * Default %Property constructor
    */
    Property::Property():
        propertyType(PropertyType::Undefined) { };

    /**
    * String %Property constructor
    * @param v @Property value.
    */
    Property::Property(std::string v):
        propertyType(PropertyType::String) {

        // Create string
        new (&stringValue) std::string(v);
    }

    /**
    * Integer %Property constructor
    * @param v @Property value.
    */
    Property::Property(int v):
        propertyType(PropertyType::Integer) {

        // Create integer
        integerValue = v;
    }

    /**
    * Double %Property constructor
    * @param v @Property value.
    */
    Property::Property(double v):
        propertyType(PropertyType::Double) {

        // Create integer
        doubleValue = v;
    }

    /**
    * Index %Property constructor
    * @param v @Property value.
    */
    Property::Property(cl_uint v):
        propertyType(PropertyType::Index) {

        // Create integer
        indexValue = v;
    }

    /**
    * %Property destructor
    */
    Property::~Property() {
        clear();
    }

    /**
    * %Property copy
    * @param r @Property to copy.
    */
    Property::Property(const Property &r) {
        copy(r);
    }

    /**
    * %Property assignment
    * @param r @Property to assign from.
    */
    Property &Property::operator=(const Property &r) {
        if (&r != this) {
            clear();
            copy(r);
        }
        return *this;
    }

    /**
    * Get string from %Property, performs conversion if required
    * @return String value of property
    */
    template <>
    std::string Property::get<std::string>() const {

        switch (propertyType) {

            case(PropertyType::String):

                // Return value
                return stringValue;
                break;

            case(PropertyType::Integer):

                // Convert to string
                return std::to_string(integerValue);
                break;

            case(PropertyType::Double):

                // Convert to string
                return std::to_string(doubleValue);
                break;

            case(PropertyType::Index):

                // Convert to string
                return std::to_string(indexValue);
                break;
        }

        return std::string();
    }

    /**
    * Get integer from %Property, performs conversion if required
    * @return Integer value of property
    */
    template <>
    int Property::get<int>() const {

        switch (propertyType) {

            case(PropertyType::String): {

                // Convert from string
                int v = 0;
                try {
                    v = std::stoi(stringValue);
                } catch(std::invalid_argument) {
                    v = 0; // TODO handle exception
                }
                return v;
                } break;

            case(PropertyType::Integer):

                // Return value
                return integerValue;
                break;

            case(PropertyType::Double):

                // Convert double to integer
                return (int)doubleValue;
                break;

            case(PropertyType::Index):

                // Convert to integer
                 // TODO handle underflow
                return (int)indexValue;
                break;
        }

        return 0;
    }

    /**
    * Get double from %Property, performs conversion if required
    * @return Double value of property
    */
    template <>
    double Property::get<double>() const {

        switch (propertyType) {

            case(PropertyType::String): {

                // Convert from string
                double v = 0.0;
                try {
                    v = std::stod(stringValue);
                } catch(std::invalid_argument) {
                    v = 0.0; // TODO handle exception
                }
                return v;
                } break;

            case(PropertyType::Integer):

                // Convert integer to double
                return (double)integerValue;
                break;

            case(PropertyType::Double):

                // Return value
                return doubleValue;
                break;

            case(PropertyType::Index):
                
                // Convert integer to double
                return (double)indexValue;
                break;
        }

        return 0.0;
    }

    /**
    * Get index from %Property, performs conversion if required
    * @return Index value of property
    */
    template <>
    cl_uint Property::get<cl_uint>() const {

        switch (propertyType) {
            
            case(PropertyType::String): {

                // Convert from string
                cl_uint v = 0;
                try {
                    v = (cl_uint)std::stoi(stringValue);
                } catch(std::invalid_argument) {
                    v = 0; // TODO handle exception
                }
                return v;
                } break;

            case(PropertyType::Integer):
                
                // Convert from integer
                return (cl_uint)integerValue;
                break;

            case(PropertyType::Double):

                // Convert double to index
                return (cl_uint)doubleValue;
                break;

            case(PropertyType::Index):

                // Return value
                return indexValue;
                break;
        }

        return 0;
    }

    /**
    * Set string for %Property
    * @param v @Property value.
    */
    template <>
    void Property::set<std::string>(const std::string v) {

        // Clear data and assign value
        clear();
        propertyType = PropertyType::String;
        new (&stringValue) std::string(v);
    }

    /**
    * Set integer for %Property
    * @param v @Property value.
    */
    template <>
    void Property::set<int>(const int v) {

        // Clear data and assign value
        clear();
        propertyType = PropertyType::Integer;
        integerValue = v;
    }

    /**
    * Set double for %Property
    * @param v @Property value.
    */
    template <>
    void Property::set<double>(const double v) {

        // Clear data and assign value
        clear();
        propertyType = PropertyType::Double;
        doubleValue = v;
    }

    /**
    * Set index for %Property
    * @param v @Property value.
    */
    template <>
    void Property::set<cl_uint>(const cl_uint v) {

        // Clear data and assign value
        clear();
        propertyType = PropertyType::Index;
        indexValue = v;
    }

    /**
    * Clear %Property data
    */
    void Property::clear() {

        switch (propertyType) {

            case(PropertyType::String): {

                // Delete string
                using std::string; // For clang bug
                stringValue.~string();
                //stringType.std::string::~string();
                } break;

            case(PropertyType::Integer):

                // No destruction required for integer
                break;

            case(PropertyType::Double):

                // No destruction required for double
                break;

            case(PropertyType::Index):

                // No destruction required for index
                break;
        }

        // Set type to none
        propertyType = PropertyType::Undefined;
    }

    /**
    * Copy %Property data
    * @param r @Property to copy.
    */
    void Property::copy(const Property &r) {

        // Set type
        propertyType = r.propertyType;

        switch (propertyType) {

            case(PropertyType::String):

                // Copy string
                new (&stringValue) std::string(r.stringValue);
                break;

            case(PropertyType::Integer):

                // Copy integer
                integerValue = r.integerValue;
                break;

            case(PropertyType::Double):

                // Copy double
                doubleValue = r.doubleValue;
                break;

            case(PropertyType::Index):

                // Copy double
                indexValue = r.indexValue;
                break;
        }
    }

    /**
    * @PropertyHandler for strings.
    * @param name @Property name.
    * @param v @Property string value.
    */
    void PropertyHandler::setProperty(std::string name, std::string v) const {

        // Search for property
        auto it = properties.find(name);
        if (it == properties.end()) {

            // Add property
            PropertyPtr p = std::make_shared<Property>(v);
            properties[name] = p;

        } else {

            // Update property
            it->second->set<std::string>(v);
        }
    }

    /**
    * @PropertyHandler for strings.
    * @param name @Property name.
    * @param v @Property integer value.
    */
    void PropertyHandler::setProperty(std::string name, int v) const {

        // Search for property
        auto it = properties.find(name);
        if (it == properties.end()) {

            // Add property
            PropertyPtr p = std::make_shared<Property>(v);
            properties[name] = p;

        } else {

            // Update property
            it->second->set<int>(v);
        }
    }

    /**
    * @PropertyHandler for strings.
    * @param name @Property name.
    * @param v @Property double value.
    */
    void PropertyHandler::setProperty(std::string name, double v) const {

        // Search for property
        auto it = properties.find(name);
        if (it == properties.end()) {

            // Add property
            PropertyPtr p = std::make_shared<Property>(v);
            properties[name] = p;

        } else {

            // Update property
            it->second->set<double>(v);
        }
    }

    /**
    * @PropertyHandler for strings.
    * @param name @Property name.
    * @param v @Property index value.
    */
    void PropertyHandler::setProperty(std::string name, cl_uint v) const {

        // Search for property
        auto it = properties.find(name);
        if (it == properties.end()) {

            // Add property
            PropertyPtr p = std::make_shared<Property>(v);
            properties[name] = p;

        } else {

            // Update property
            it->second->set<cl_uint>(v);
        }
    }

    /**
    * Get all properties of string type.
    * @return reference to @Property map.
    */
    template <>
    std::map<std::string, std::string> PropertyHandler::getProperties() const {

        // Create map
        std::map<std::string, std::string> p;
        for (auto r : properties) {
            if (r.second->getType() == PropertyType::String) {
                p[r.first] = r.second->get<std::string>();
            }
        }

        return p;
    }

    /**
    * Get all properties of integer type.
    * @return reference to @Property map.
    */
    template <>
    std::map<std::string, int> PropertyHandler::getProperties() const {

        // Create map
        std::map<std::string, int> p;
        for (auto r : properties) {
            if (r.second->getType() == PropertyType::Integer) {
                p[r.first] = r.second->get<int>();
            }
        }

        return p;
    }

    /**
    * Get all properties of double type.
    * @return reference to @Property map.
    */
    template <>
    std::map<std::string, double> PropertyHandler::getProperties() const {

        // Create map
        std::map<std::string, double> p;
        for (auto r : properties) {
            if (r.second->getType() == PropertyType::Double) {
                p[r.first] = r.second->get<double>();
            }
        }

        return p;
    }

    /**
    * Get all properties of index type.
    * @return reference to @Property map.
    */
    template <>
    std::map<std::string, cl_uint> PropertyHandler::getProperties() const {

        // Create map
        std::map<std::string, cl_uint> p;
        for (auto r : properties) {
            if (r.second->getType() == PropertyType::Index) {
                p[r.first] = r.second->get<cl_uint>();
            }
        }

        return p;
    }

    /**
    * Get string property.
    * @return string value.
    */
    template <>
    std::string PropertyHandler::getProperty(std::string name) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {
            return it->second->get<std::string>();
        }

        return std::string();
    }

    /**
    * Get integer property.
    * @return integer value.
    */
    template <>
    int PropertyHandler::getProperty(std::string name) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {
            return it->second->get<int>();
        }

        return 0;
    }

    /**
    * Get double property.
    * @return integer value.
    */
    template <>
    double PropertyHandler::getProperty(std::string name) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {
            return it->second->get<double>();
        }

        return 0.0;
    }

    /**
    * Get index property.
    * @return index value.
    */
    template <>
    cl_uint PropertyHandler::getProperty(std::string name) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {
            return it->second->get<cl_uint>();
        }

        return 0;
    }
    
    /**
    * Get index property.
    * @return index value.
    */
    void PropertyHandler::removeProperty(std::string name) {
        properties.erase(name);
    }
}