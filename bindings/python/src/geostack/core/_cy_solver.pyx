# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode


from cython.operator cimport dereference as deref
from libcpp cimport bool
from libcpp.string cimport string
from libcpp cimport nullptr
import numpy as np
cimport numpy as np

np.import_array()


cdef class cySolver:
    def __cinit__(self):
        self.thisptr.reset(new Solver())    

    def __dealloc__(self):
        if self.thisptr != nullptr:
            if type(self) is cySolver:
                self.this.reset()

    def __repr__(self):
        return self.__class__.__name__

    def __exit__(self):
        self.__dealloc__()