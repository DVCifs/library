# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import ctypes
import json
from enum import Enum, unique
from collections import OrderedDict
import numpy as np
from .. import core
from ._cy_raster import (_RasterBaseList_d, _RasterBaseList_f,
    equalSpatialMetrics_f, equalSpatialMetrics_d)
from ._cy_raster import (_cyRaster_d, _cyRaster_f, _cyRaster_d_i,
    _cyRaster_f_i)
from ._cy_raster import _RasterPtrList_d, _RasterPtrList_f
from ._cy_raster import _RasterDimensions_d, _RasterDimensions_f
from ._cy_raster import _Dimensions_f, _Dimensions_d
from ._cy_raster import _Variables_f, _Variables_d
from ._cy_raster import (DataFileHandler_f, DataFileHandler_d,
    DataFileHandler_d_i, DataFileHandler_f_i)
from ._cy_raster import TileSpecifications
from ._cy_raster import getNullValue_dbl, getNullValue_flt, getNullValue_uint32
from ..vector import vector, _cy_vector
from ..runner import runner
from ..readers.rasterReaders import NC_Handler, GDAL_Handler, DataHandler
from ..readers.rasterReaders import XR_Handler, RIO_Handler
from ..dataset import HAS_GDAL, HAS_NCDF, HAS_PYDAP, HAS_XARRAY, HAS_RASTERIO
from . import _base
from .. import gs_enums
import numbers

if HAS_PYDAP:
    from ..readers.ncutils import Pydap2NC

if HAS_GDAL:
    from osgeo import gdal

if HAS_RASTERIO:
    import rasterio as rio

if HAS_XARRAY:
    import xarray as xr

if HAS_NCDF:
    import netCDF4 as nc


__all__ = ["Raster", "equalSpatialMetrics", "RasterDimensions",
    "Dimensions", "TileDimensions", "TileSpecifications",
    "RasterFile", "RasterBaseList", "RasterPtrList",
    "Variables", "getNullValue"]

class TileDimensions:
    def __init__(self, dtype=np.float32):
        self._dtype = None
        self._handle = None

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__

def getNullValue(dtype):
    out = np.nan
    if dtype == np.float32:
        out = getNullValue_flt()
    elif dtype == np.float64:
        out = getNullValue_dbl()
    elif dtype == np.uint32:
        out = getNullValue_uint32()
    return out

class Dimensions:
    def __init__(self, dtype=np.float32):
        if dtype is not None:
            if dtype != np.float32 and dtype != np.float64:
                raise ValueError("dtype should be either np.float32 or np.float64")
        self._dtype = dtype
        self._handle = None

    @classmethod
    def copy(cls, other):
        if not isinstance(other, (cls, _Dimensions_d, _Dimensions_f)):
            raise TypeError("Input argument should be an instance of Dimensions")
        if isinstance(other, cls):
            if other._handle is not None:
                out = cls.copy(other._handle)
            else:
                raise RuntimeError("Input dimensions is not yet initialized")
        elif isinstance(other, _Dimensions_d):
            out = cls(dtype=np.float64)
            out._handle = _Dimensions_d.copy(other)
        elif isinstance(other, _Dimensions_f):
            out = cls(dtype=np.float32)
            out._handle = _Dimensions_f.copy(other)
        return out

    @classmethod
    def from_dict(cls, other, dtype=np.float32):
        if not isinstance(other, (dict, OrderedDict)):
            raise TypeError("Input argument should be a dictionary")
        missing_args = ""
        for item in ['nx','ny','nz','hx','hy','hz','ox','oy','mx','my']:
            if item not in other:
                missing_args += "%s," % item
        if len(missing_args) > 0:
            raise KeyError("%s not present in the input dictionary" % missing_args[:-1])

        if dtype == np.float32:
            out = cls.copy(_Dimensions_f.from_dict(other))
        elif dtype == np.float64:
            out = cls.copy(_Dimensions_d.from_dict(other))
        return out

    def to_dict(self):
        if self._handle is not None:
            return self._handle.to_dict()
        else:
            raise RuntimeError("Dimensions is not yet initialized")

    def __eq__(self, other):
        assert isinstance(other, Dimensions)
        return self._handle == other._handle

    def __ne__(self, other):
        assert isinstance(other, Dimensions)
        return self._handle != other._handle

    def __getattr__(self, other):
        if self._handle is None:
            raise RuntimeError("Dimensions is not yet initialized")
        if other in ['nx','ny','nz','hx','hy','hz','ox','oy','mx','my']:
            return getattr(getattr(self, "_handle"), other)
        else:
            raise AttributeError("%s is not a recognized attribute" % other)

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__


class RasterDimensions:
    def __init__(self, dtype=np.float32):
        self._dtype = None
        self._handle = None
        if dtype is not None:
            if dtype in [np.float64, ctypes.c_double]:
                self._handle = _RasterDimensions_d()
                self._dtype = np.float64
            elif dtype in [float, np.float32, ctypes.c_float]:
                self._handle = _RasterDimensions_f()
                self._dtype = np.float32
            else:
                raise TypeError("dtype should be np.float32 or np.float64")

    @classmethod
    def copy(cls, other):
        if isinstance(other, (cls, _RasterDimensions_d,
            _RasterDimensions_f)):
            if isinstance(other, cls):
                if other._handle is not None:
                    out = cls.copy(other._handle)
                else:
                    raise RuntimeError("Input instance of RasterDimensions is not initialized")
            else:
                out = cls(dtype=None)
                if isinstance(other, _RasterDimensions_d):
                    out._dtype = np.float64
                    out._handle = other
                elif isinstance(other, _RasterDimensions_f):
                    out._dtype = np.float32
                    out._handle = other
            return out
        else:
            raise TypeError("copy argument should be an instance of RasterDimensions")

    @classmethod
    def from_dict(cls, other, dtype=np.float32):
        if not isinstance(other, (dict, OrderedDict)):
            raise TypeError("Input argument should be a dictionary")
        missing_args = ""
        for item in ['dim', 'ex', 'ey', 'ez', 'tx', 'ty']:
            if item not in other:
                missing_args += "%s," % item
            if len(missing_args) > 0:
                raise KeyError("%s not present in the input dictionary" % missing_args[:-1])

        if not isinstance(other['dim'], (dict, OrderedDict)):
            raise TypeError("value of 'dim' key in the input dictionary should be a dictionary")

        missing_args = ""
        for item in ['nx','ny','nz','hx','hy','hz','ox','oy','mx','my']:
            if item not in other['dim']:
                missing_args += "%s," % item
        if len(missing_args) > 0:
            raise KeyError("%s not present in the 'dim' key in the input dictionary" % missing_args[:-1])

        out = cls(dtype=None)
        if dtype == np.float32:
            out._dtype = np.float32
            out._handle = _RasterDimensions_f(other)
        elif dtype == np.float64:
            out._dtype = np.float32
            out._handle = _RasterDimensions_d(other)
        return out

    def to_dict(self):
        if self._handle is not None:
            return self._handle.to_dict()
        else:
            raise RuntimeError("RasterDimensions is not yet initialized")

    def __getattr__(self, other):
        if self._handle is None:
            raise RuntimeError("RasterDimensions is not yet initialized")
        if other in ['nx','ny','nz','hx','hy','hz','ox','oy','oz',
                'mx','my','ex','ey','ez','tx','ty']:
            return getattr(getattr(self, "_handle"), other)
        else:
            raise AttributeError("%s is not a recognized attribute" % other)

    def __eq__(self, other):
        assert isinstance(other, RasterDimensions)
        return self._handle == other._handle

    def __ne__(self, other):
        assert isinstance(other, RasterDimensions)
        return self._handle != other._handle

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__


class Variables:
    def __init__(self, dtype=np.float32):
        self._handle = None
        if dtype == np.float32:
            self._handle = _Variables_f()
            self._dtype = np.float32
        elif dtype == np.float64:
            self._handle = _Variables_d()
            self._dtype = dtype

    def set(self, var_name, var_value):
        assert self._handle is not None, "Variables object is not initialized"
        return self._handle.set(var_name, var_value)
        
    def get(self, var_name):
        assert self._handle is not None, "Variables object is not initialized"
        return self._handle.get(var_name)
        
    def getIndexes(self):
        assert self._handle is not None, "Variable object is not initialised"
        return self._handle.getIndexes()

    def __repr__(self):
        return self.__repr__()

    def __str__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__


class Raster(_base._RasterBase):
    """Raster wrapper class for cython wrapper of c++ Raster class.
    """
    def __init__(self, *args, **kwargs):

        if len(args) > 0:
            _inp_args = self._parse_args(args)
        if kwargs:
            _inp_args = self._parse_kwargs(kwargs)

        if not len(args) > 0 and not kwargs:
            _inp_args = ["", np.float32, np.float32]

        super().__init__(*_inp_args[1:])
        _name = _inp_args[0]

        if self.base_type == np.float64:
            if self.data_type == np.float64:
                self._handle = _cyRaster_d(_name.encode('UTF-8'))
            elif self.data_type == np.uint32:
                self._handle = _cyRaster_d_i(_name.encode('UTF-8'))
        elif self.base_type == np.float32:
            if self.data_type == np.float32:
                self._handle = _cyRaster_f(_name.encode('UTF-8'))
            elif self.data_type == np.uint32:
                self._handle = _cyRaster_f_i(_name.encode('UTF-8'))

    def _parse_args(self, args):
        # set default values
        out = ["", np.float32, np.float32]

        if len(args) > 3:
            raise RuntimeError("Only three Parameters at most should be provided")
        for i, arg in enumerate(args, 0):
            if i == 0:
                if isinstance(arg, str):
                    out[i] = arg
                else:
                    raise ValueError("first argument, name of Raster should be string type")
            elif i == 1:
                if arg in [np.float64, ctypes.c_double]:
                    out[i] = np.float64
                elif arg != np.float32:
                    raise ValueError("Second argument, base type should be np.float32/np.float64")
            elif i == 2:
                if arg in [ctypes.c_double, np.float64]:
                    out[i] = np.float64
                elif arg in [np.uint32, np.int32, np.int]:
                    out[i] = np.uint32
                elif arg != np.float32:
                    raise ValueError("Third argument, data type should be np.uint32/np.float32/np.float64")
            else:
                raise TypeError("Unable to understand provided argument")
        return out

    def _parse_kwargs(self, kwargs):
        # set default values
        out = ["", np.float32, np.float32]

        if len(kwargs) > 3:
            raise RuntimeError("Only three keyword Parameters at most should be provided")
        for arg in kwargs:
            if arg == "name":
                if isinstance(kwargs[arg], str):
                    out[0] = kwargs[arg]
                else:
                    raise TypeError("Incorrect type for 'name' keyword argument")
            elif arg == "base_type":
                if kwargs[arg] in [np.float64, ctypes.c_double]:
                    out[1] = np.float64
                elif kwargs[arg] != np.float32:
                    raise ValueError("base_type can be np.float32/np.float64")
            elif arg == "data_type":
                if kwargs[arg] in [ctypes.c_double, np.float64]:
                    out[2] = np.float64
                elif kwargs[arg] in [np.int32, np.uint32]:
                    out[2] = np.uint32
                elif kwargs[arg] != np.float32:
                    ValueError("data_type can be of type np.float32, np.float64, np.uint32")
            else:
                raise TypeError("Unable to understand provided argument")
        return out
    
    @property
    def data(self):
        return self.get_full_data()

    @data.setter
    def data(self, inpData):
        self.set_full_data(inpData)

    def set_full_data(self, inpData):
    
        # Check raster
        if not hasattr(self, "raster_dim"):
            self.raster_dim = self.getDimensions()
        
        if not isinstance(inpData, np.ndarray):
            raise TypeError("Input argument should be a numpy array")

        assert self._handle is not None, "Raster is not instantiated"
        
        # Get raster kind
        raster_kind = self._handle.getRasterKind()
            
        # Check data
        if raster_kind == _base.RasterKind.Raster2D:
            if inpData.shape != (self.raster_dim.ny, self.raster_dim.nx):
                raise ValueError("Shape mismatch between Raster dimensions and input data")
        elif raster_kind == _base.RasterKind.Raster3D:
            if inpData.shape != (self.raster_dim.nz, self.raster_dim.ny, self.raster_dim.nx):
                raise ValueError("Shape mismatch between Raster dimensions and input data")
        else:
            raise NotImplementedError("Only handling of 2D or 3D arrays have been added")

        # Set data
        numTiles = self.raster_dim.tx * self.raster_dim.ty
        for idx in range(numTiles):
            ti = idx % self.raster_dim.tx
            tj = idx // self.raster_dim.tx
            idx_s, idx_e, jdx_s, jdx_e = self.get_tile_idx_bounds(idx)
            if raster_kind == _base.RasterKind.Raster2D:
                self.readData(inpData[jdx_s:jdx_e,idx_s:idx_e].astype(self.data_type), ti=ti, tj=tj)
            elif raster_kind == _base.RasterKind.Raster3D:
                self.readData(inpData[:, jdx_s:jdx_e,idx_s:idx_e].astype(self.data_type), ti=ti, tj=tj)

    @classmethod
    def copy(cls, name, other):
        """Create a copy of raster from input raster.

        Parameters
        ----------
        name : str
            Name of the output raster.
        other : Raster
            Input raster to be used for copying.

        Returns
        -------
        out : Raster
            Output copy of raster object

        Examples
        --------
        >>> import numpy as np
        >>> testRasterA = Raster("testRasterA", np.float32)
        >>> testRasterA.init(nx = 5, ny = 5, hx = 1.0, hy = 1.0)
        >>> testRasterC = Raster.copy("testRasterC", testRasterA)
        """
        if not isinstance(name, str):
            raise TypeError("name should be string type")

        if isinstance(other, cls):
            out = cls(name=name)
            if out.base_type == np.float32:
                if out.data_type == out.base_type:
                    out._handle = _cyRaster_f.rastercopy(other._handle)
                else:
                    out._handle = _cyRaster_f_i.rastercopy(other._handle)
            elif out.base_type == np.float64:
                if out.data_type == out.base_type:
                    out._handle = _cyRaster_d.rastercopy(other._handle)
                else:
                    out._handle = _cyRaster_d_i.rastercopy(other._handle)
        elif isinstance(other, _cyRaster_d):
            out = cls(data_type=np.float64, base_type=np.float64)
            out._handle = _cyRaster_d.rastercopy(other)
        elif isinstance(other, _cyRaster_f):
            out = cls(data_type=np.float32, base_type=np.float32)
            out._handle = _cyRaster_f.rastercopy(other)
        elif isinstance(other, _cyRaster_f_i):
            out = cls(data_type=np.uint32, base_type=np.float32)
            out._handle = _cyRaster_f_i.rastercopy(other)
        elif isinstance(other, _cyRaster_d_i):
            out = cls(data_type=np.uint32, base_type=np.float64)
            out._handle = _cyRaster_d_i.rastercopy(other)
        out.name = name
        return out

    def init_with_bbox(self, bbox, resolution):
        if not isinstance(resolution, numbers.Real):
            raise TypeError("resolution should be a real number")
        if not isinstance(bbox, (vector.BoundingBox,
                                _cy_vector._BoundingBox_d,
                                _cy_vector._BoundingBox_f)):
            raise TypeError("input bounding box should be an instance of BoundingBox")

        assert self._handle is not None, "Raster is not instantiated"

        if isinstance(bbox, vector.BoundingBox):
            assert self.base_type == bbox._dtype, "datatype mismatch"
            self._handle.init(bbox._handle, resolution=resolution)
        else:
            if isinstance(bbox, _cy_vector._BoundingBox_d):
                assert self.base_type == np.float64, "datatype mismatch"
            elif isinstance(bbox, _cy_vector._BoundingBox_f):
                assert self.base_type == np.float32, "datatype mismatch"
            self._handle.init(bbox, resolution=resolution)

    def init_with_dims(self, dims):
        if not isinstance(dims, (RasterDimensions,
                                _RasterDimensions_d,
                                _RasterDimensions_f)):
            raise TypeError("input raster dimensions should be an instance of RasterDimensions")
        assert self._handle is not None, "Raster is not instantiated"
        if isinstance(dims, RasterDimensions):
            assert self.base_type == dims._dtype, "datatype mismatch"
            self._handle.init(dims._handle)
        else:
            if isinstance(dims, _RasterDimensions_d):
                assert self.base_type == np.float64, "datatype mismatch"
            elif isinstance(dims, _RasterDimensions_f):
                assert self.base_type == np.float32, "datatype mismatch"
            self._handle.init(dims)

    def init(self, nx, hx, ny=None, nz=None, hy=None, hz=None,
            ox=0.0, oy=0.0, oz=0.0):
        assert self._handle is not None, "Raster is not instantiated"
        if (ny is None or ny == 0 ) and (nz is None or nz == 0):
            self._handle.init1D(nx, hx, ox)
        elif ny > 0 and (nz is None or nz == 0):
            if hy is not None:
                self._handle.init2D(nx, ny, hx, hy, ox, oy)
            else:
                raise ValueError("for 2d array, hy cannot be None")
        elif ny > 0 and nz > 0:
            if hy is not None and hz is not None:
                self._handle.init3D(nx, ny, nz, hx, hy, hz, ox, oy, oz)
            else:
                raise ValueError("hy and hz cannot be None for 3d array")

    def setCellValue(self, c, i, j=0, k=0):
        """Set a value to a cell in the raster object.

        Parameters
        ----------
        c : int/float
            The value to be assigned to a the cells in the raster object.
        i : int
            Cell index along x-axis
        j : int (optional)
            Cell index along y-axis.
        k : int (optional)
            Cell index along z-axis

        Returns
        -------
        Nil
        """
        assert self._handle is not None, "Raster is not instantiated"
        if isinstance(c, numbers.Real):
            return self._handle.setCellValue(c, i, j=j, k=k)
        else:
            raise TypeError("%s is not supported" % type(c).__name__)

    def setAllCellValues(self, c):
        """Set a constant value for all the cells in the raster object.

        Parameters
        ----------
        c : int/float
            A constant value to be assigned to all the cells in the raster object.

        Returns
        -------
        Nil
        """
        assert self._handle is not None, "Raster is not instantiated"
        if isinstance(c, numbers.Real):
            return self._handle.setAllCellValues(c)
        else:
            raise TypeError("%s is not supported" % type(c).__name__)

    def __pad_data_array(self, inparray, nz=None):
    
        # Check data
        assert self._handle is not None, "Raster is not instantiated"
        
        # Get raster kind
        raster_kind = self._handle.getRasterKind()
            
        # Pad data
        nx = ny = TileSpecifications().tileSize
        if raster_kind == _base.RasterKind.Raster1D:
            _buf = np.full((nx,), self.nullValue,)
            _buf[:inparray.size] = inparray[:]
        elif raster_kind == _base.RasterKind.Raster2D:
            _buf = np.full((ny, nx,), self.nullValue)
            inp_ny, inp_nx = inparray.shape
            _buf[:inp_ny, :inp_nx] = inparray
        elif raster_kind == _base.RasterKind.Raster3D:
            if nz is None:
                raise ValueError("nz should not be None for 3D array")
            _buf = np.full((nz, ny, nx,), self.nullValue)
            inp_nz, inp_ny, inp_nx = inparray.shape
            _buf[:inp_nz, :inp_ny, :inp_nx] = inparray

        return _buf.astype(self.data_type)


    def readData(self, inp, ti=0, tj=0):
        """Set tile data for the raster object.

        Parameters
        ---------
        ti : int
            Tile index in x-direction.
        tj : int
            Tile index in y-direction.

        Returns
        ------
        Nil

        Examples
        --------
        >>> import numpy as np
        >>> testRasterA = Raster(name="testRasterA", base_type=np.float32,
        ... data_type=np.float32)
        >>> testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
        >>> testRasterA.readData(np.arange(25).reshape((5,5)), ti=0, tj=0)
        """
        if isinstance(inp, np.ndarray):
        
            # Check data
            assert inp.dtype == self.data_type, "datatype mismatch"
            assert self._handle is not None, "Raster is not instantiated"
            
            # Get raster kind
            raster_kind = self._handle.getRasterKind()
            
            # Set data
            assert len(inp.shape) <= 3, "Up to three dimensions are currently supported"
            if len(inp.shape) == 1:
                assert raster_kind == _base.RasterKind.Raster1D, "Raster object is not 1D"
                _buf = self.__pad_data_array(inp)
                self._handle.set1D(_buf, ti=ti, tj=tj)
            elif len(inp.shape) == 2:
                assert raster_kind == _base.RasterKind.Raster2D, "Raster object is not 2D"
                _buf = self.__pad_data_array(inp)
                self._handle.set2D(_buf, ti=ti, tj=tj)
            elif len(inp.shape) == 3:
                assert raster_kind == _base.RasterKind.Raster3D, "Raster object is not 3D"
                _buf = self.__pad_data_array(inp, nz=self.raster_dim.nz)
                self._handle.set3D(_buf, ti=ti, tj=tj)
        else:
            raise TypeError("input can only be numpy array")

    def writeData(self, ti=0, tj=0):
        """Get tile data from the raster object.

        Parameters
        ----------
        ti : int
            Tile index in x-direction.
        tj : int
            Tile index in y-direction.

        Returns
        -------
        out : np.ndarray
            Tile data from the Raster object.

        Examples
        --------
        >>> import numpy as np
        >>> testRasterA = Raster(name="testRasterA", base_type=np.float32,
        ... data_type=np.float32)
        >>> testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
        >>> testRasterA.setAllCellValues(2.0)
        >>> testRasterA.writeData()
        """
        assert self._handle is not None, "Raster is not instantiated"
        return self._handle.getData(ti, tj)

    def read(self, fileName):
        """Read a file into the raster object.

        Parameters
        ----------
        fileName : str
            Path of the file to be read into the Raster object.

        Returns
        -------
        Nil
        """
        if not pth.exists(fileName):
            raise FileNotFoundError("file %s doesnt exist, check path" % fileName)

        if isinstance(fileName, str):
            out = self._handle.read(fileName.encode('UTF-8'))
        elif isinstance(fileName, bytes):
            out = self._handle.read(fileName)

        if out:
            raster_out = Raster.copy(self.name, self)
            self._handle = raster_out._handle
            self._properties = raster_out._properties
            self.data_type = raster_out.data_type
            self.base_type = raster_out.base_type
        else:
            raise RuntimeError("unable to read file %s" % fileName)

    def write(self, fileName, jsonConfig=None):
        """Write raster data to a output file.

        Parameters
        ----------
        fileName : str
            Path of the file to write raster data.
        jsonConfig : str/dict
            A string or dictionary containing configuration for the output file.

        Returns
        -------
        out : bool
            Return true if raster data was written to the file else false.
        """
        if jsonConfig is None:
            _json_config = "".encode("UTF-8")
        else:
            if isinstance(jsonConfig, str):
                _json_config = jsonConfig.encode("UTF-8")
            elif isinstance(jsonConfig, bytes):
                _json_config = jsonConfig

        if isinstance(fileName, str):
            out = self._handle.write(fileName.encode('UTF-8'),
                _json_config)
        elif isinstance(fileName, bytes):
            out = self._handle.write(fileName, _json_config)
        return out

    @property
    def bounds(self):
        return self.getBounds()

    @property
    def dimensions(self):
        return self.getDimensions()
        
    @property
    def reduceVal(self):
        return self._handle.reduceVal()

    def __iter__(self):
        self.tileNum = 0
        return self

    def __next__(self):
        return self.get_next_tile()

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__


class RasterPtrList(_base._Raster_list):
    def __init__(self, *args, dtype=np.float32):
        if dtype is not None:
            if dtype in [np.float64, ctypes.c_double]:
                super().__init__(np.float64, handle=_RasterPtrList_d())
            elif dtype in [float, np.float32, ctypes.c_float]:
                super().__init__(np.float32, handle=_RasterPtrList_f())
            else:
                raise TypeError("dtype should be np.float32 or np.float64")
        if args:
            if dtype is None:
                raise ValueError("dtype must be given when instantiating from iterable")
            if len(args) > 1:
                raise ValueError("Only one argument should be provided")
            if not isinstance(args[0], (list, tuple)):
                raise ValueError("Input argument should be a list or tuple")
            else:
                self._from_iterable(args[0])

    def from_tuple(self, *args):
        """Instantiate RasterPtrList from tuple of Raster/ RasterFile.

        Parameters
        ----------
        arg1 : tuple
            A tuple of Raster or RasterFile object.

        Returns
        -------
        Nil
        """
        return self._from_tuple(*args)

    def from_list(self, *args):
        """Instantiate RasterPtrList from list of Raster/ RasterFile.

        Parameters
        ----------
        arg1 : list
            A list of Raster or RasterFile objects.

        Returns
        -------
        Nil
        """
        return self._from_list(*args)

    @property
    def size(self):
        """Get size of the RasterPtrList.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : int
            Length of the RasterPtrList.
        """
        return self._size

    def append(self, *args):
        """Append a Raster/RasterFile object to RasterPtrList.

        Parameters
        ----------
        arg1 : Raster/ RasterFile object.
            A Raster/ RasterFile object to append to RasterPtrList.

        Returns
        -------
        Nil
        """
        return self._append(*args)

    def add_data_handler(self, *args):
        """Add a RasterFile object to the RasterPtrList.

        Parameters
        ----------
        arg1 : RasterFile object
            A RasterFile object to be added to RasterPtrList.

        Returns
        -------
        Nil
        """
        return self._add_data_handler(*args)

    def add_raster(self, *args):
        """Add a Raster object to the RasterPtrList.

        Parameters
        ----------
        arg1 : Raster object
            A Raster object to be added to RasterPtrList.

        Returns
        -------
        Nil
        """
        return self._add_raster(*args)

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__


class RasterBaseList(_base._Raster_list):
    def __init__(self, *args, dtype=np.float32):
        if dtype is not None:
            if dtype in [np.float64, ctypes.c_double]:
                super().__init__(np.float64, handle=_RasterBaseList_d())
            elif dtype in [float, np.float32, ctypes.c_float]:
                super().__init__(np.float32, handle=_RasterBaseList_f())
            else:
                raise TypeError("dtype should be np.float32 or np.float64")
        if args:
            if dtype is None:
                raise ValueError("dtype must be given when instantiating from iterable")
            if len(args) > 1:
                raise ValueError("Only one argument should be provided")
            if not isinstance(args[0], (list, tuple)):
                raise ValueError("Input argument should be a list or tuple")
            else:
                self._from_iterable(args[0])

    def from_tuple(self, *args):
        """Instantiate RasterBaseList from tuple of Raster/ RasterFile.

        Parameters
        ---------
        arg1 : tuple
            A tuple of Raster or RasterFile object.

        Returns
        -------
        Nil
        """
        return self._from_tuple(*args)

    def from_list(self, *args):
        """Instantiate RasterBaseList from list of Raster/ RasterFile.

        Parameters
        ---------
        arg1 : list
            A list of Raster or RasterFile objects.

        Returns
        -------
        Nil
        """
        return self._from_list(*args)

    @property
    def size(self):
        """Get size of the RasterBaseList.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : int
            Length of the RasterBaseList.
        """
        return self._size

    def append(self, *args):
        """Append a Raster/RasterFile object to RasterBaseList.

        Parameters
        ---------
        arg1 : Raster/ RasterFile object.
            A Raster/ RasterFile object to append to RasterBaseList.

        Returns
        -------
        Nil
        """
        return self._append(*args)

    def add_data_handler(self, *args):
        """Add a RasterFile object to the RasterBaseList.

        Parameters
        ---------
        arg1 : RasterFile object
            A RasterFile object to be added to RasterBaseList.

        Returns
        -------
        Nil
        """
        return self._add_data_handler(*args)

    def add_raster(self, *args):
        """Add a Raster object to the RasterbaseList.

        Parameters
        ----------
        arg1 : Raster object
            A Raster object to be added to RasterBaseList.

        Returns
        -------
        Nil
        """
        return self._add_raster(*args)

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__


class RasterFile(_base._RasterBase):
    """Data file reader with file input through Python libraries.

    Parameters
    ----------
    base_type : numpy.dtype
        base type for Raster object
    data_type : numpy.dtype
        data type for Raster object
    file_handler : DataHandler object
        User defined file reader/ writer object e.g. NC_Handler, GDAL_Handler
    file_path : str/ gdal.Dataset/ nc.Dataset
        Path of the file to be read or an instance of opened file using IO libraries. 

    Attributes
    ----------
    base_type : numpy.dtype
        base type used for the Raster object
    data_type : numpy.dtype
        data type used for the Raster object
    _handle : _DataFileHandler_f/ _DataFileHandler_d object
        An instance of the RasterFile cython object

    Methods
    -------
    update_time(time_idx)
        Update time or raster band index in the input file.
    read(thredds=bool)
        Read file and initialised raster using dimensions from the input file.
    write(fileName, jsonConfig)
        Write file to path 'fileName' and set configuration given in 'jsonConfig'.
    setFileInputHandler()
        Set file input handler in the raster object.
    setFileOutputHandler()
        Set file output handler in the raster object.
    getProperty(prop)
        Get the value of property 'prop' from the raster object.
    hasProperty(prop)
        Check whether property 'prop' is defined for the raster object.
    setProperty(prop, propValue, propType=(str/float,int))
        Set a value of property 'prop' to a value 'propValue' and data type 'propType'.
    getRasterDimensions()
        Get dimensions for the raster object.
    getData(ti, tj, tidx)
        Get raster data for tile index (ti, tj) and time index 'tidx'. Here time index is the third
        dimension like raster band, vertical levels, time.
    """
    def __init__(self, filePath=None, backend=None,
            file_handler=None, base_type=np.float32,
            data_type=np.float32, name=None):

        if name is None:
            _var_name = ""
        else:
            _var_name = name.encode('utf-8')

        _supported_backends = {'xarray': XR_Handler,
                               "netcdf": NC_Handler,
                               "rasterio": RIO_Handler,
                               "gdal": GDAL_Handler}

        if file_handler is None and backend is None:
            raise ValueError("value of file_handler and backend cannot be None")
        if file_handler is not None and backend is not None:
            raise ValueError("Only file_handler or backend should be specified")

        if backend is not None:
            if isinstance(backend, str):
                if backend in _supported_backends:
                    _file_handler = _supported_backends[backend]
                else:
                    raise ValueError(f"{backend} is not recognised")
            else:
                raise TypeError("Value of backend should be of string type")
        elif file_handler is not None:
            _file_handler = file_handler

        if not issubclass(_file_handler, DataHandler):
            raise TypeError("file_handler should be an instance of DataHandler")

        if filePath is None:
            file_path = ''
        else:
            file_path = filePath

        if issubclass(_file_handler, GDAL_Handler) and HAS_GDAL:
            if not isinstance(file_path, gdal.Dataset):
                if not isinstance(file_path, str):
                    raise TypeError("file_path should be a string")
        elif issubclass(_file_handler, RIO_Handler) and HAS_RASTERIO:
            if not isinstance(file_path, rio.DatasetReader):
                if not isinstance(file_path, str):
                    raise TypeError("file_path should be a string")
        elif issubclass(_file_handler, XR_Handler) and HAS_XARRAY:
            if not isinstance(file_path, xr.Dataset):
                if not isinstance(file_path, str):
                    raise TypeError("file_path should be a string")
        elif issubclass(_file_handler, NC_Handler) and HAS_NCDF:
            if not isinstance(file_path, nc.Dataset):
                if HAS_PYDAP:
                    if not isinstance(file_path, Pydap2NC):
                        if not isinstance(file_path, str):
                            raise TypeError("file_path should be a string")
                else:
                    if not isinstance(file_path, str):
                        raise TypeError("file_path should be a string")
        else:
            if not isinstance(file_path, str):
                raise TypeError("file_path should be a string")

        if isinstance(filePath, str):
            if len(filePath) > 0 and 'dodsC' not in filePath:
                if not pth.exists(filePath):
                    raise FileNotFoundError(f"file {filePath} not present")

        if base_type is not None:
            if base_type in [np.float64, ctypes.c_double]:
                if data_type is None or data_type == base_type:
                    super().__init__(np.float64, np.float64)
                    self._handle = DataFileHandler_d(_var_name, _file_handler, file_path)
                elif data_type == np.uint32:
                    super().__init__(np.float64, np.uint32)
                    self._handle = DataFileHandler_d_i(_var_name, _file_handler, file_path)
            elif base_type in [float, np.float32, ctypes.c_float]:
                if data_type is None or data_type == base_type:
                    super().__init__(np.float32, np.float32)
                    self._handle = DataFileHandler_f(_var_name, _file_handler, file_path)
                elif data_type == np.uint32:
                    super().__init__(np.float32, np.uint32)
                    self._handle = DataFileHandler_f_i(_var_name, _file_handler, file_path)
            else:
                raise TypeError("base_type should be np.float32 or np.float64")
        else:
            super().__init__(np.float32, np.float32)
            self._handle = DataFileHandler_f(_var_name, _file_handler, file_path)
        self._backend = backend


    def getTime(self):
        """Get time based on current time index in the input file.

        Parameters
        ----------
        Nil

        Returns
        -------
        time : double
            Time associated with time index.
        """
    
        assert self._handle is not None, "RasterFile is not instantiated"
        
        return self._handle.time
    
    def setTimeIndex(self, time_idx):
        """Update time index in the input file.

        Parameters
        ----------
        time_idx : int
            Time index or raster Band count.

        Returns
        -------
        Nil
        """

        # Check index
        if not isinstance(time_idx, numbers.Integral):
            raise TypeError("time_idx should be of type int")

        # Update time
        self._handle.update_time(time_idx)

        # Get handle
        if hasattr(self, '_handle'):
            cy_obj = "_handle"
            if hasattr(getattr(self, "_handle"), "cy_raster_obj"):
                cy_obj = "_handle.cy_raster_obj"
        else:
            raise AttributeError("Raster or Vector has not been created")
            
        # Clear all raster data
        self._custom_getattr(cy_obj).deleteRasterData()

    def read(self, thredds=False, use_pydap=False):
        """Read input file and initialise Raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        Nil
        """
        assert self._handle is not None, "RasterFile is not instantiated"
        
        # Set to index 1 by default for gdal and rasterio
        if self._backend in ["gdal", "rasterio"]:
            self._handle.tidx = 1
            
        # Read data
        self._handle.read(thredds=thredds, use_pydap=use_pydap)
        self.setFileInputHandler()

    def write(self, fileName, jsonConfig):
        """Write raster object to a file.

        Parameters
        ----------
        fileName : str
            Path and name of the output file.
        jsonConfig : str/dict
            Configuration specified in the output file.

        Returns
        -------
        Nil
        """
        assert self._handle is not None, "RasterFile is not instantiated"
        if isinstance(jsonConfig, str):
            self._handle.write(fileName, jsonConfig.encode('utf-8'))
        elif isinstance(jsonConfig, dict):
            self._handle.write(fileName, json.dumps(jsonConfig).encode('utf-8'))

    def setFileInputHandler(self):
        """Set File input handler for the raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        Nil
        """
        assert self._handle is not None, "RasterFile is not instantiated"
        if  not self._has_input_handler:
            self._handle.setFileInputHandler()
            self._has_input_handler = True

    def setFileOutputHandler(self):
        """Set File output handler for the raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        Nil
        """
        assert self._handle is not None, "RasterFile is not instantiated"
        self._handle.setFileOutputHandler()

    @staticmethod
    def readFile(readerFunction, fileName, base_type=np.float32, data_type=np.float32,
        thredds=False, use_pydap=False):
        """Method to read a file into a data file handler.

        Parameters
        ----------
        readerFunction : DataHandler object
        fileName : str
        base_type : numpy.dtype
        data_type : numpy.dtype
        thredds : bool

        Returns
        -------
        out : RasterFile object
            An instance of RasterFile using Python libraries for reading file.

        Examples
        --------
        >>> import numpy as np
        >>> from geostack.readers import GDAL_Handler
        >>> out = DataFileHandler.readFile(GDAL_Handler, "test.tif",
        ...                                base_type=np.float32
        ...                                data_type=np.float32,
        ...                                thredds=False)
        """
        if readerFunction is None:
            raise ValueError("value of readerFunction cannot be None")
        if not issubclass(readerFunction, DataHandler):
            raise TypeError("readerFunction should be an instance of DataHandler")

        if fileName is None:
            raise ValueError("value of fileName cannot be None")

        if not HAS_NCDF and not HAS_PYDAP:
            if isinstance(readerFunction, NC_Handler):
                if not isinstance(fileName, str):
                    raise TypeError("fileName should be a string")
        if not HAS_GDAL:
            if isinstance(readerFunction, GDAL_Handler):
                if not isinstance(fileName, str):
                    raise TypeError("fileName should be a string")

        if base_type not in [np.float32, np.float64]:
            raise ValueError("dtype should be either np.float32 or np.float64")

        if base_type == np.float32:
            out.base_type = base_type
            out.data_type = data_type                
            out = RasterFile(base_type=None, file_handler=readerFunction,
                filePath=fileName)
            if data_type == np.uint32:
                out._handle = DataFileHandler_f_i.readFile(readerFunction, fileName,
                    thredds=thredds, use_pydap=use_pydap)
            else:
                out._handle = DataFileHandler_f.readFile(readerFunction, fileName,
                    thredds=thredds, use_pydap=use_pydap)
        elif base_type == np.float64:
            out.base_type = base_type
            out.data_type = data_type
            if data_type == np.uint32:
                out._handle = DataFileHandler_d_i.readFile(readerFunction, fileName,
                    thredds=thredds, use_pydap=use_pydap)
            else:
                out._handle = DataFileHandler_d.readFile(readerFunction, fileName,
                    thredds=thredds, use_pydap=use_pydap)
        return out

    @property
    def dimensions(self):
        return self.getRasterDimensions()

    def _custom_getattr(self, attr):
        if len(attr.split('.')) > 0:
            _attr_list = attr.split(".")
            for i, item in enumerate(_attr_list, 0):
                if i == 0:
                    ret = getattr(self, item)
                else:
                    ret = getattr(ret, item)
        else:
            ret = getattr(self, attr)
        return ret

    def hasProperty(self, prop):
        """Check if a property is defined for the raster object.

        Parameters
        ----------
        prop : str
            Name of the property.

        Returns
        -------
        out : bool
            True if property is defined else False.

        Examples
        --------
        >>> import numpy as np
        >>> from geostack.readers import GDAL_Handler
        >>> testRasterA = RasterFile(file_handler=GDAL_Handler,
        ...                               file_path="test.tif",
        ...                               data_type=np.float32,
        ...                               base_type=np.float32)
        >>> testRasterA.setProperty("name", "testRasterA", prop_type=str)
        >>> testRasterA.hasProperty("name")
        """
        if not isinstance(prop, str):
            raise TypeError("property name 'prop' should be of string type")
        if hasattr(self, '_handle'):
            cy_obj = "_handle"
            if hasattr(getattr(self, "_handle"), "cy_raster_obj"):
                cy_obj = "_handle.cy_raster_obj"
        else:
            raise AttributeError("Raster or Vector has not been created")
        if isinstance(prop, str):
            _prop = prop.encode('UTF-8')
        else:
            _prop = prop
        return self._custom_getattr(cy_obj).hasProperty(_prop)

    def getProperty(self, prop):
        """Get the value of a property from the Raster object.

        Parameters
        ----------
        prop : str
            Name of the property.

        Returns
        -------
        out : str/float/int
            Value of the property from the Raster object.

        Examples
        --------
        >>> import numpy as np
        >>> from geostack.readers import GDAL_Handler
        >>> testRasterA = RasterFile(file_handler=GDAL_Handler,
        ...                               file_path="test.tif",
        ...                               data_type=np.float32,
        ...                               base_type=np.float32)
        >>> testRasterA.setProperty("name", "testRasterA", prop_type=str)
        >>> testRasterA.hasProperty("name")
        """
        if not isinstance(prop, str):
            raise TypeError("property name 'prop' should be of string type")
        if hasattr(self, '_handle'):
            cy_obj = "_handle"
            if hasattr(getattr(self, "_handle"), "cy_raster_obj"):
                cy_obj = "_handle.cy_raster_obj"
        else:
            raise AttributeError("Raster or Vector has not been created")
        if self.hasProperty(prop):
            if isinstance(prop, str):
                _prop = prop.encode('UTF-8')
            else:
                _prop = prop
            out = self._custom_getattr(cy_obj).getProperty_s(_prop)
            if len(out) == 0:
                out = self._custom_getattr(cy_obj).getProperty_i(_prop)
                if out == 0:
                    out = self._custom_getattr(cy_obj).getProperty_d(_prop)
            return out
        else:
            raise KeyError("Property %s is not attached to the object" % prop)

    def setProperty(self, prop, value, prop_type=None):
        """Set a value of property 'prop' to a value 'propValue' and data type 'propType'.

        Parameters
        ----------
        prop : str type
            Name of the property to be defined for the raster object.
        value : str/int/float type
            Value of the property for the raster object.
        prop_type : str/int/float
            data type of the property defined for the raster object.

        Returns
        -------
        Nil

        Examples
        -------
        >>> import numpy as np
        >>> from geostack.readers import GDAL_Handler
        >>> testRasterA = RasterFile(file_handler=GDAL_Handler,
        ...                               file_path="test.tif",
        ...                               data_type=np.float32,
        ...                               base_type=np.float32)
        >>> testRasterA.setProperty("name", "testRasterA", prop_type=str)
        >>> testRasterA.hasProperty("name")
        """
        if not isinstance(prop, str):
            raise TypeError("property name 'prop' should be of string type")
        if hasattr(self, '_handle'):
            cy_obj = "_handle"
            if hasattr(getattr(self, "_handle"), "cy_raster_obj"):
                cy_obj = "_handle.cy_raster_obj"
        else:
            raise AttributeError("RasterFile and underlying Raster has not been created")
        if isinstance(prop, str):
            _prop = prop.encode('UTF-8')
        else:
            _prop = prop
        if prop_type is not None:
            if prop_type == int:
                self._custom_getattr(cy_obj).setProperty(_prop, int(value))
            elif prop_type == float:
                self._custom_getattr(cy_obj).setProperty(_prop, float(value))
            elif prop_type == str:
                self._custom_getattr(cy_obj).setProperty(_prop, f"{value}".encode('UTF-8'))
            else:
                raise TypeError("value of prop_type is not of acceptable type")
        else:
            if isinstance(value, (str, bytes)):
                if isinstance(value, str):
                    self._custom_getattr(cy_obj).setProperty(_prop, value.encode('UTF-8'))
                else:
                    self._custom_getattr(cy_obj).setProperty(_prop, value)
            elif isinstance(value, numbers.Real) and not isinstance(value, numbers.Integral):
                self._custom_getattr(cy_obj).setProperty(_prop, float(value))
            else:
                self._custom_getattr(cy_obj).setProperty(_prop, int(value))

    @property
    def bounds(self):
        return self.getBounds()

    def getData(self, ti=0, tj=0):
        """Get raster data for tile index (ti, tj) and time index 'tidx'.

        Parameters
        ----------
        ti : int
            Tile index in x-direction.
        tj : int
            Tile index in y-direction.

        Returns
        -------
        out : numpy.ndarray
            Tile data from the Raster object.
        """
        assert self._handle is not None, "RasterFile has not be initialized"
        if not isinstance(ti, numbers.Integral):
            raise TypeError("ti should on integer type")
        if not isinstance(tj, numbers.Integral):
            raise TypeError("tj should on integer type")

        if self._has_input_handler:
            return self._handle.getData(ti, tj, self._handle.tidx)
        else:
            raise RuntimeError("FileInputHandler is not yet set!!!")

    @property
    def data(self):
        return self.get_full_data()

    def getProperties(self, prop_type=None):
        """Get all the properties of an object of a given datatype.

        Parameters
        ----------
        prop_type: datatype
            A datatype corresponding the values of the properties of an object.

        Returns
        -------
        out: dict
            A dictionary containing properties and values of the properties.

        Examples
        --------
        >>> testRasterA = Raster(name="testRasterA")
        >>> testRasterA.getProperties(prop_type=int)
        """
        if hasattr(self, '_handle'):
            if hasattr(getattr(self, "_handle"), "cy_raster_obj"):
                cy_obj = "cy_raster_obj"
            else:
                raise AttributeError("Not a valid RasterFile")
        else:
            raise AttributeError("Raster or Vector has not been created")
        if prop_type is None:
            self._properties.update(getattr(self._handle, cy_obj).getProperties_s())
            self._properties.update(getattr(self._handle, cy_obj).getProperties_i())
            self._properties.update(getattr(self._handle, cy_obj).getProperties_d())
        else:
            if prop_type is int:
                self._properties.update(getattr(self._handle, cy_obj).getProperties_i())
            elif prop_type is float:
                self._properties.update(getattr(self._handle, cy_obj).getProperties_d())
            elif prop_type is str:
                self._properties.update(getattr(self._handle, cy_obj).getProperties_s())
            else:
                raise ValueError("value of prop_type is not of acceptable type")

        return self._properties

    def __iter__(self):
        self.tileNum = 0
        return self

    def __next__(self):
        return self.get_next_tile()

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__


def equalSpatialMetrics(this, other):
    '''Check alignment of input Dimensions

    Parameters
    ----------
    this: an instance of Dimensions class
    other: an instance of Dimensions class

    Returns
    -------
    out : Bool, True or False

    Examples
    --------
    >>> dimA = {'hx': 0.02999,
    ...         'hy': 0.02999,
    ...         'hz': 1.0,
    ...         'mx': 491,
    ...         'my': 723,
    ...         'nx': 491,
    ...         'ny': 723,
    ...         'nz': 1,
    ...         'ox': 142.16879,
    ...         'oy': -28.69602,
    ...         'oz': 0.0}
    >>> dimB = {'hx': 0.02999,
    ...         'hy': 0.02999,
    ...         'hz': 1.0,
    ...         'mx': 491,
    ...         'my': 723,
    ...         'nx': 491,
    ...         'ny': 723,
    ...         'nz': 1,
    ...         'ox': 142.16879,
    ...         'oy': -28.69602,
    ...         'oz': 0.0}
    >>> testDimensionsA = Dimensions.from_dict(dimA, dtype=np.float32)
    >>> testDimensionsB = Dimensions.from_dict(dimB, dtype=np.float32)
    >>> out = equalSpatialMetrics(testDimensionsA, testDimensionsB)
    '''
    if isinstance(this, Dimensions) and isinstance(other, Dimensions):
        assert this._dtype == other._dtype, "datatype mismatch"
        if this._dtype == np.float32:
            out = equalSpatialMetrics_f(this._handle, other._handle)
        elif this._dtype == np.float64:
            out = equalSpatialMetrics_d(this._handle, other._handle)
    elif isinstance(this, _Dimensions_f) and isinstance(other, _Dimensions_f):
        out = equalSpatialMetrics_f(this, other)
    elif isinstance(this, _Dimensions_d) and isinstance(other, _Dimensions_d):
        out = equalSpatialMetrics_d(this, other)
    else:
        raise TypeError("Input Parameters should be instance of Dimensions")
    return out
