# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import ctypes
import numpy as np
from ._cy_level_set import cyLevelSet_d, cyLevelSet_f
from ..raster import raster
from ..raster import _cy_raster
from ..vector import vector
import json

__all__ = ["LevelSet"]

class LevelSet:
    def __init__(self, dtype=np.float32):
        self._handle = None
        self.dtype = dtype
        if dtype is None or dtype == np.float32:
            self._handle = cyLevelSet_f()
            self.dtype = np.float32
        elif dtype == np.float64:
            self._handle = cyLevelSet_d()
            self.dtype = np.float64

    def init(self, jsonStartCondition, startCondition, vector_mask,
            var_data, inputLayers, outputLayers):
        if not isinstance(startCondition, vector.Vector):
            raise TypeError("startCondition should be an instance of Vector")

        if not isinstance(inputLayers, raster.RasterPtrList):
            raise TypeError("inputLayers should be an instance of RasterPtrList")

        if not isinstance(outputLayers, raster.RasterPtrList):
            raise TypeError("outputLayers should be an instance of RasterPtrList")

        if not isinstance(jsonStartCondition, (str, dict)):
            raise TypeError("jsonStartCondition should be str or dict")

        if not isinstance(vector_mask, vector.Vector):
            raise TypeError("vector mask should be an instance of Vector")

        if not isinstance(var_data, raster.Variables):
            raise TypeError("var data should be an instance of Variables")

        if isinstance(jsonStartCondition, str):
            _json_start_condition = jsonStartCondition.encode('utf-8')
        elif isinstance(jsonStartCondition, dict):
            _json_start_condition = json.dumps(jsonStartCondition).encode('utf-8')

        if self.dtype != startCondition._dtype:
            raise TypeError("Mismatch between datatype of startCondition and LevelSet instance")

        if self.dtype != inputLayers._dtype:
            raise TypeError("Mismatch between datatype of inputLayers and LevelSet instance")

        if self.dtype != outputLayers._dtype:
            raise TypeError("Mismatch between datatype of outputLayers and LevelSet instance")

        if self.dtype != vector_mask._dtype:
            raise TypeError("Mismatch between data type of vector_mask and LevelSet instance")

        if self.dtype != var_data._dtype:
            raise TypeError("Mismatch between data type of var_data and LevelSet instance")

        if self._handle is not None:
            try:
                rc = self._handle.init(_json_start_condition,
                    startCondition._handle, vector_mask._handle,
                    var_data._handle, inputLayers._handle,
                    outputLayers._handle)
            except Exception as e:
                raise RuntimeError(f"Unable to initialise solver {str(e)}")
        else:
            raise RuntimeError("LevelSet solver is not initialized")
        return rc

    def step(self):
        if self._handle is not None:
            return self._handle.step()
        else:
            raise RuntimeError("LevelSet solver is not initialized")

    def getParameters(self):
        if self._handle is not None:
            return self._handle.getParameters()
        else:
            raise RuntimeError("LevelSet solver is not initialized")

    def setParameters(self, param, value):
        if self._handle is not None:
            return self._handle.setParameters(param.encode('utf-8'), np.float64(value))
        else:
            raise RuntimeError("LevelSet solver is not initialized")

    def getDistance(self):
        if self._handle is not None:
            return raster.Raster.copy("", self._handle.getDistance())
        else:
            raise RuntimeError("LevelSet solver is not initialized")

    def getArrival(self):
        if self._handle is not None:
            return raster.Raster.copy("", self._handle.getArrival())
        else:
            raise RuntimeError("LevelSet solver is not initialized")

    def getClassification(self):
        if self._handle is not None:
            return raster.Raster.copy("", self._handle.getClassification())
        else:
            raise RuntimeError("LevelSet solver is not initialized")

    def getOutput(self):
        if self._handle is not None:
            return raster.Raster.copy("", self._handle.getOutput())
        else:
            raise RuntimeError("LevelSet solver is not initialized")

    def getAdvect_x(self):
        if self._handle is not None:
            return raster.Raster.copy("", self._handle.getAdvect_x())
        else:
            raise RuntimeError("LevelSet solver is not initialized")

    def getAdvect_y(self):
        if self._handle is not None:
            return raster.Raster.copy("", self._handle.getAdvect_y())
        else:
            raise RuntimeError("LevelSet solver is not initialized")

    def getEpochMilliseconds(self):
        out = self._handle.getEpochMilliseconds()
        return out

    def __repr__(self):
        return "<class 'geostack.solvers.%s'>" % self.__class__.__name__
