/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef GEOSTACK_PROPERTY_H
#define GEOSTACK_PROPERTY_H

#include <string>
#include <map>
#include <memory>
#include <stdexcept>

#include "gs_opencl.h"

namespace Geostack
{
    // Forward declarations
    class Property;

    // Aliases
    using PropertyPtr = std::shared_ptr<Property>;


    /**
    * %Property types
    */
    namespace PropertyType {
        enum Type {
            Undefined,  // Undefined
            String,     // String type 
            Integer,    // Signed int type
            Double,     // Double precision float type
            Index       // Unsigned long type
        };
    }

    /**
    * %Property class for vector data.
    * Contains either string, integer or double data.
    */
    class Property {
        public:

            // Default Constructor
            Property();

            // Property constructors
            explicit Property(std::string v);
            explicit Property(int v);
            explicit Property(double v);
            explicit Property(cl_uint v);

            // Property destructor
            ~Property();

            // Property copy constructor
            Property(const Property &r);

            // Property assignment operator
            Property &operator=(const Property &r);

            // Property data functions
            template <typename P>
            P get() const;

            template <typename P>
            void set(const P v);

            // Clear Property data
            void clear();
            
            /**
            * Get %Property type.
            * @return %Property type.
            */
            PropertyType::Type getType() const {
                return propertyType;
            }

        private:

            PropertyType::Type propertyType; ///< Property type

            /**
            * Union of %Property types.
            */
            union {
                std::string stringValue;
                int integerValue;
                double doubleValue;
                cl_uint indexValue;
            };

            // Copy Property data
            void copy(const Property &r);
    };

    /**
    * %PropertyHandler class for geometry properties.
    * Holds a map of properties
    */
    class PropertyHandler {

        public:

            // Set property
            void setProperty(std::string name, std::string v) const;
            void setProperty(std::string name, int v) const;
            void setProperty(std::string name, double v) const;
            void setProperty(std::string name, cl_uint v) const;

            // Get all properties of certain type.
            template <typename P>
            std::map<std::string, P> getProperties() const;

            // Get property
            template <typename P>
            P getProperty(std::string name) const;

            // Remove property
            void removeProperty(std::string name);

            /**
            * Check for %Property.
            * @return true if property name is found, false otherwise
            */
            bool hasProperty(std::string name) const {
                return (properties.find(name) != properties.end());
            }

        protected:
            mutable std::map<std::string, PropertyPtr> properties; ///< Map of named properties
    };
}

#endif
