# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import sys
import json
import numpy as np
import pytest
sys.path.insert(0, os.path.realpath('../../../'))

from geostack.vector import Coordinate
from geostack.io import geoJsonToVector

@pytest.fixture
def temp():
    testGeoJson = '''{"features": [
        {"geometry": {"coordinates": [0, 0.5], "type": "Point"}, 
            "properties": {"p0": "pstr", "p1": 1, "p2": 1.1000000000000001}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"}, 
            "properties": {"l0": "lstr", "l1": 2, "l2": 2.2000000000000002}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"}, 
            "properties": {"y0": "ystr", "y1": 3, "y2": 3.2999999999999998}, "type": "Feature"}], "type": "FeatureCollection"}'''
    out_vector = geoJsonToVector(testGeoJson, dtype=np.float32)
    return out_vector

def test_point_property(temp):
    pointIndexes = temp.getPointIndexes()
    assert temp.getProperty(pointIndexes[0], "p0", str) == "pstr"

def test_line_string_property(temp):
    lineStringIndexes = temp.getLineStringIndexes()
    assert temp.getProperty(lineStringIndexes[0], "l1", int) == 2

def test_polygon_property(temp):
    polygonIndexes = temp.getPolygonIndexes()
    assert temp.getProperty(polygonIndexes[0], "y2", float) == 3.3

def test_add_point(temp):
    point_idx = temp.addPoint([144.9631, -37.8136])
    temp.setProperty(point_idx, "newproperty", "newstr")
    out = temp.getPointCoordinate(point_idx)
    assert np.round(out[0], 4) == 144.9631 and np.round(out[1], 4) == -37.8136