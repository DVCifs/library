/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <algorithm>
#include <string>
#include <cmath>
#include <iostream>

#include "gs_string.h"
#include "gs_vector.h"
#include "gs_projection.h"

namespace Geostack
{
    /**
    * Get %Coordinate buffer from %VertexCollection.
    * @return %Coordinate buffer
    */
    template <typename CTYPE>
    VertexCollection<CTYPE>::VertexCollection():
        vertexBufferPtr(nullptr),
        vertexBufferOnDevice(false),
        vertexBufferSize(0) {
    }

    /**
    * Clone VertexCollection data
    * Makes a copy of the %VertexCollection
    */
    template <typename CTYPE>
    VertexCollection<CTYPE> *VertexCollection<CTYPE>::clone() {

        // Create new VertexCollection
        VertexCollection<CTYPE> *r = new VertexCollection<CTYPE>();

        // Copy coordinates
        r->coordinates = coordinates;

        return r;
    }

    /**
    * Get %Coordinate buffer from %VertexCollection.
    * @return %Coordinate buffer
    */
    template <typename CTYPE>
    const cl::Buffer &VertexCollection<CTYPE>::getVertexBuffer() {
    
        // Check if data has been created
        if (size() == 0)
            throw std::range_error("VertexCollection has no data");

        // Ensure data buffer is on device
        if (!vertexBufferOnDevice) {
        
            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();

            // Check data size
            if (size() != vertexBufferSize) {

                // Update buffer
                cl_int err = CL_SUCCESS;
                auto &context = solver.getContext();
                vertexBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, size()*sizeof(Coordinate<CTYPE>), coordinates.data(), &err);
                if (err != CL_SUCCESS) {
                    throw std::range_error("Cannot allocate memory for vertex buffer");
                }

                // Map to host
                vertexBufferOnDevice = true;
                mapVertexBuffer(queue);
            }

            // Unmap buffer
            unmapVertexBuffer(queue);
        }
        
        // Return reference to data buffer
        return vertexBuffer;
    }

    /**
    * Map OpenCL data buffer to host, this gives exclusive access to the host
    */    
    template <typename CTYPE>
    void VertexCollection<CTYPE>::mapVertexBuffer(cl::CommandQueue &queue) const {
        if (vertexBufferOnDevice) {
            vertexBufferPtr = queue.enqueueMapBuffer(vertexBuffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, size()*sizeof(Coordinate<CTYPE>));
        }
        vertexBufferOnDevice = false;
    }

    /**
    * Unmap OpenCL data buffer from host, this gives exclusive access to the device
    */    
    template <typename CTYPE>
    void VertexCollection<CTYPE>::unmapVertexBuffer(cl::CommandQueue &queue) const {
        if (!vertexBufferOnDevice) {
            queue.enqueueUnmapMemObject(vertexBuffer, vertexBufferPtr);
        }
        vertexBufferOnDevice = true;
    }

    /**
    * Ensure vertex data is on the host
    */
    template <typename CTYPE>
    void VertexCollection<CTYPE>::ensureVertexBufferOnHost() const {

        // Check and move data to host
        if (vertexBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapVertexBuffer(queue);
        }
    }

    /**
    * %Point construction.
    * Creates %Point from a %Vertex.
    * @param pointVertex_ %Vertex to use for point.
    */
    template <typename CTYPE>
    void Point<CTYPE>::addVertex(cl_uint pointVertex_) {
        pointVertex = pointVertex_;
    }

    /**
    * Clone Vertex data
    * Makes a copy of the %Point vertex
    */
    template <typename CTYPE>
    VectorGeometry<CTYPE> *Point<CTYPE>::clone() {

        // Create new point
        Point<CTYPE> *r = new Point<CTYPE>();

        // Set ID
        r->setID(VectorGeometry<CTYPE>::id);
        
        // Copy geometry
        r->pointVertex = pointVertex;

        return r;
    }
    
    /**
    * Get %Point bounding box.
    * @return pair of co-located coordinates.
    */
    template <typename CTYPE>
    BoundingBox<CTYPE> Point<CTYPE>::getBounds() const {
        return BoundingBox<CTYPE> { bc, bc };
    }
    
    /**
    * Update %Point bounding box.
    */
    template <typename CTYPE>
    void Point<CTYPE>::updateBounds(const Vector<CTYPE> &v) {

        // Store coordinate for bounds
        bc = v.getCoordinate(pointVertex);
    }
    
    /**
    * %LineString construction.
    * Adds a %Vertex to a line string
    * @param pointVertex_ %Vertex to add to line.
    */
    template <typename CTYPE>
    void LineString<CTYPE>::addVertex(cl_uint lineVertex_) {

        // Add to list
        lineVertices.push_back(lineVertex_);
    }

    /**
    * Clone Vertex data
    * Makes a copy of the %LineString vertices
    */
    template <typename CTYPE>
    VectorGeometry<CTYPE> *LineString<CTYPE>::clone() {

        // Create new linestring
        LineString<CTYPE> *r = new LineString<CTYPE>();

        // Set ID
        r->setID(VectorGeometry<CTYPE>::id);

        // Copy geometry
        r->lineVertices = lineVertices;

        // Copy bounds
        r->bounds = bounds;

        return r;
    }

    /**
    * Update %LineString bounding box.
    */
    template <typename CTYPE>
    void LineString<CTYPE>::updateBounds(const Vector<CTYPE> &v) {

        // Reset bounds
        bounds.reset();
    
        // Update bounds
        for (auto &index : lineVertices)
            bounds.extend(v.getCoordinate(index));
    }

    /**
    * %Polygon construction.
    * Adds a %Polygon to a line string
    * @param pointVertex_ %Polygon to add to line.
    * @param index sub-polygon index.
    */
    template <typename CTYPE>
    void Polygon<CTYPE>::addVertex(cl_uint polygonVertex_) {

        // Add to list
        polygonVertices.push_back(polygonVertex_);
    }

    /**
    * Clone Vertex data
    * Makes a copy of the %Polygon vertices
    */
    template <typename CTYPE>
    VectorGeometry<CTYPE> *Polygon<CTYPE>::clone() {

        // Create new polygon
        Polygon<CTYPE> *r = new Polygon<CTYPE>();

        // Set ID
        r->setID(VectorGeometry<CTYPE>::id);

        // Copy geometry
        r->polygonVertices = polygonVertices;
        r->polygonSubIndexes = polygonSubIndexes;
        r->polygonBounds = polygonBounds;

        return r;
    }

    /**
    * %Polygon construction.
    * Adds a sub-polygon to a %Polygon.
    * The first sub-polygon contains vertices for the external polygon.
    * Subsequent sub-polygon contain vertices for holes in the polygon.
    * @param length number of vertices in sub-polygon.
    */
    template <typename CTYPE>
    void Polygon<CTYPE>::addSubPolygon(cl_uint length) {
        polygonSubIndexes.push_back(length);
    }
        
    /**
    * Update %Polygon bounding box.
    */
    template <typename CTYPE>
    void Polygon<CTYPE>::updateBounds(const Vector<CTYPE> &v) {
        
        // Clear list
        polygonBounds.clear();

        // Update bounds
        cl_uint index = 0;
        for (auto &len : polygonSubIndexes) {
        
            // Find sub-polygon bounds
            BoundingBox<CTYPE> subPolygonBounds;
            for (cl_uint i = index; i < index+len; i++) {
                subPolygonBounds.extend(v.getCoordinate(polygonVertices[i]));
            }

            // Add bounds to list
            polygonBounds.push_back(subPolygonBounds);

            // Update index
            index+=len;
        }
    }

    /**
    * Default %Vector constructor
    * dimensions will be POD initialised to zero
    */
    template <typename CTYPE>
    Vector<CTYPE>::Vector():
        proj() {

        // Create data
        pVertices = std::make_shared<VertexCollection<CTYPE> >();
        pGeometry = std::make_shared<std::vector<VectorGeometryPtr<CTYPE> > >();
        pProperties = std::make_shared<std::map<std::string, std::vector<Property> > >();
    }
        
    /**
    * Destructor
    */
    template <typename CTYPE>
    Vector<CTYPE>::~Vector() {
    }

    /**
    * Copy constructor
    */
    template <typename CTYPE>
    Vector<CTYPE>::Vector(const Vector<CTYPE> &v):
        proj(v.proj),
        pVertices(v.pVertices),
        pGeometry(v.pGeometry),
        geometryIndexes(v.geometryIndexes),
        pointIndexes(v.pointIndexes),
        lineStringIndexes(v.lineStringIndexes),
        polygonIndexes(v.polygonIndexes),
        pProperties(v.pProperties) {

        // Copy data
        tree = v.tree;
    }

    /**
    * Clear %Vector data
    */
    template <typename CTYPE>
    void Vector<CTYPE>::clear() {

        // Clear projection
        proj = ProjectionParameters<CTYPE>();

        // Clear geometry
        tree.clear();
        pVertices->clear();
        pGeometry->clear();
        geometryIndexes.clear();
        pointIndexes.clear();
        lineStringIndexes.clear();
        polygonIndexes.clear();

        // Clear properties
        pProperties->clear();
    }

    /**
    * Assignment operator
    * @param v %Vector to assign.
    */
    template <typename CTYPE>
    Vector<CTYPE> &Vector<CTYPE>::operator=(const Vector<CTYPE> &v) { 

        if (this != &v) {

            // Copy projection 
            proj = v.proj;

            // Copy geometry
            tree = v.tree;
            pVertices = v.pVertices;
            pGeometry = v.pGeometry;
            geometryIndexes = v.geometryIndexes;
            pointIndexes = v.pointIndexes;
            lineStringIndexes = v.lineStringIndexes;
            polygonIndexes = v.polygonIndexes;

            // Copy properties
            pProperties = v.pProperties;
        }

        return *this; 
    }

    /**
    * Add @VectorGeometry to %Vector
    * @param g @VectorGeometry to add.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::add(VectorGeometryPtr<CTYPE> g) {

        // Update vector
        tree.insert(g);
        pGeometry->push_back(g);

        // Update geometry index
        cl_uint index = pGeometry->size()-1;
        geometryIndexes.push_back(index);
        g->setID(index);
        g->updateVector(*this, index);

        // Return index
        return index;
    }

    /**
    * Add %Point to %Vector.
    * @param c_ Coordinate pair.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::addPoint(Coordinate<CTYPE> c_) {   
    
        // Create point
        PointPtr<CTYPE> p = std::make_shared<Point<CTYPE> >();
        
        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getCoordinates();

        // Create vertex
        cl_uint index = vertices.size();
        vertices.push_back(c_);

        // Associate vertex with point
        p->addVertex(index);

        // Update bounds
        p->updateBounds(*this);

        // Add and return index of geometry
        return add(p);
    }

    /**
    * Add %LineString to %Vector
    * @param cs_ List of coordinate pairs.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::addLineString(CoordinateList<CTYPE> cs_) {

        // Check size
        if (cs_.size() == 0)
            throw std::length_error("No coordinates provided for line string creation");
         
        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getCoordinates();

        // Create new line string
        LineStringPtr<CTYPE> l = std::make_shared<LineString<CTYPE> >();        
        for (auto &c : cs_) {

            // Add vertices
            cl_uint index = vertices.size();
            vertices.push_back(c);

            // Add to line
            l->addVertex(index);
        }

        // Update bounds
        l->updateBounds(*this);

        // Add and return index of geometry
        return add(l);
    }

    /**
    * Add %Polygon to %Vector
    * @param pcs_ List of list of coordinate pairs.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::addPolygon(std::vector<CoordinateList<CTYPE> > pcs_) {

        // Check size
        if (pcs_.size() == 0 || pcs_[0].size() == 0)
            throw std::length_error("No coordinates provided for polygon creation");
            
        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getCoordinates();
        
        // Create new polygon
        PolygonPtr<CTYPE> p = std::make_shared<Polygon<CTYPE> >();
        for (auto &cs : pcs_) {
        
            // Polygon bounds
            BoundingBox<CTYPE> bounds = { cs[0], cs[0] };

            // Add vertices
            for (auto &c : cs) {

                // Update bounds
                bounds.extend(c);

                // Add vertices
                cl_uint index = vertices.size();
                vertices.push_back(c);

                // Add to polygon
                p->addVertex(index);
            }

            // Create new sub polygon
            p->addSubPolygon(cs.size());
        }

        // Update bounds
        p->updateBounds(*this);

        // Add and return index of geometry
        return add(p);
    }

    /**
    * Get %Point %Coordinate from %Vector.
    * @return %Point coordinate
    */
    template <typename CTYPE>
    Coordinate<CTYPE> Vector<CTYPE>::getPointCoordinate(const cl_uint index) const {
    
        // Get geometry references
        auto &c = pVertices->getCoordinates();
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

        // Get point reference
        auto &point = *std::static_pointer_cast<Point<CTYPE> >(geometry[index]);

        // Return coordinate
        return c[point.pointVertex];
    }

    /**
    * Get %LineString coordinates from %Vector.
    * @return %LineString coordinates
    */
    template <typename CTYPE>
    CoordinateList<CTYPE> Vector<CTYPE>::getLineStringCoordinates(const cl_uint index) const {
    
        // Get geometry references
        auto &c = pVertices->getCoordinates();
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        
        // Get line string reference
        auto &lineString = *std::static_pointer_cast<LineString<CTYPE> >(geometry[index]);
        
        // Create coordinate list
        CoordinateList<CTYPE> cl;
        for (auto li : lineString.lineVertices)
            cl.push_back(c[li]);
            
        // Return coordinate list
        return cl;
    }

    /**
    * Get %Polygon coordinates from %Vector.
    * @return %Polygon coordinates
    */
    template <typename CTYPE>
    CoordinateList<CTYPE> Vector<CTYPE>::getPolygonCoordinates(const cl_uint index) const {
    
        // Get geometry references
        auto &c = pVertices->getCoordinates();
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        
        // Get polygon reference
        auto &polygon = *std::static_pointer_cast<Polygon<CTYPE> >(geometry[index]);
        
        // Create coordinate list
        CoordinateList<CTYPE> cl;
        for (auto pi : polygon.polygonVertices)
            cl.push_back(c[pi]);
            
        // Return coordinate list
        return cl;
    }
    
    /**
    * Get %Polygon indexes from %Vector.
    * @return index list
    */
    template <typename CTYPE>
    const std::vector<cl_uint> &Vector<CTYPE>::getPolygonVertexIndexes(const cl_uint index) const {
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        return std::static_pointer_cast<Polygon<CTYPE> >(geometry[index])->getVertexIndexes();
    }
    
    /**
    * Get %Polygon sub-indexes from %Vector.
    * @return index list
    */
    template <typename CTYPE>
    const std::vector<cl_uint> &Vector<CTYPE>::getPolygonSubIndexes(const cl_uint index) const {
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        return std::static_pointer_cast<Polygon<CTYPE> >(geometry[index])->getSubIndexes();
    }
    
    /**
    * Get %Polygon sub-bounds from %Vector.
    * @return %BoundingBox list
    */
    template <typename CTYPE>
    const std::vector<BoundingBox<CTYPE> > &Vector<CTYPE>::getPolygonSubBounds(const cl_uint index) const {
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        return std::static_pointer_cast<Polygon<CTYPE> >(geometry[index])->getSubBounds();
    }

    /**
    * Set property for %VectorGeometry in %Vector.
    * @param index %VectorGeometry item.
    * @param name @Property name.
    * @param v @Property value.
    */
    template <typename CTYPE>
    template <typename PTYPE> 
    void Vector<CTYPE>::setProperty(cl_uint index, std::string name, PTYPE v) {
            
        // Get data handles
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        std::map<std::string, std::vector<Property> > &properties = *pProperties;

        // Get property vector
        auto &pv = properties[name];
        if (pv.size() != geometry.size()) {

            // Resize all property vectors
            for (auto &p : properties)
                p.second.resize(geometry.size());
        }

        // Set property
        pv[index].template set<PTYPE>(v);
    }

    /**
    * Get property for %VectorGeometry in %Vector.
    * @param index %VectorGeometry item.
    * @param name @Property name.
    * @return @Property value
    */
    template <typename CTYPE>
    template <typename PTYPE> 
    PTYPE Vector<CTYPE>::getProperty(cl_uint index, std::string name, PTYPE nullValue) {
        
        // Find property vector
        std::map<std::string, std::vector<Property> > &properties = *pProperties;
        auto ipv = properties.find(name);
        if (ipv == properties.end()) {

            // Property not found, return null value
            return nullValue;
        }

        // Return property value
        return ipv->second[index].template get<PTYPE>();
    }

    /**
    * Delete property for %VectorGeometry in %Vector.
    * @param index %VectorGeometry item.
    * @param name @Property name.
    */
    template <typename CTYPE>
    void Vector<CTYPE>::deleteProperty(cl_uint index, std::string name) {
            
        // Find property vector
        std::map<std::string, std::vector<Property> > &properties = *pProperties;
        auto ipv = properties.find(name);
        if (ipv != properties.end()) {

            // Clear property
            ipv->second[index].clear();
        }
    }

    /**
    * Convert %Vector projection
    * @param to projection to convert to
    * @return true if projection is successful, false otherwise
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::convert(ProjectionParameters<CTYPE> to) {

        Vector<CTYPE> v;

        // Check for data
        if (hasData()) {

            // Clone vertices
            v.pVertices = std::shared_ptr<VertexCollection<CTYPE> >(pVertices->clone());

            // Copy geometry data
            v.geometryIndexes = geometryIndexes;
            v.pointIndexes = pointIndexes;
            v.lineStringIndexes = lineStringIndexes;
            v.polygonIndexes = polygonIndexes;

            // Copy property data
            v.pProperties = pProperties;
        
            // Clone data
            auto &geometry = *pGeometry;
            auto &rGeometry = *v.pGeometry;
            for (auto &g : geometry) {
                rGeometry.push_back(std::shared_ptr<VectorGeometry<CTYPE> >(g->clone()));
            }

            // Return empty Vector if there are no coordinates or projection fails
            if (v.pVertices->size() == 0 || !Projection<CTYPE>::convert(*v.pVertices, to, proj))
                return v;
        
            // Update vertices
            for (auto &g : rGeometry) {

                // Update bounding box
                g->updateBounds(v);

                // Update tree
                v.tree.insert(g);
            }

            // Update projection
            v.proj = to;
        }

        return v;
    }

    /**
    * Get %Vector region from %Vector.
    * Returns all geometry intersecting region.
    * @param bounds Bounds of region
    * @param geometryTypes the types of geometry to include in resulting %Vector
    * @return %Vector of geometry within region.
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::region(BoundingBox<CTYPE> bounds, size_t geometryTypes) {

        // Get region
        std::vector<GeometryBasePtr<CTYPE> > geometryList;
        tree.search(bounds, geometryList, geometryTypes);
        
        // Create return vector
        Vector<CTYPE> v;

        // Set projection
        v.setProjectionParameters(proj);

        // Sort by ID
        std::sort(geometryList.begin(), geometryList.end(), 
            [](const GeometryBasePtr<CTYPE> &l, const GeometryBasePtr<CTYPE> &r) { return l->getID() < r->getID(); });
        
        // Copy geometry
        v.pVertices = pVertices;
        v.pGeometry = pGeometry;
        for (auto &g : geometryList) {

            // Add geometry
            v.tree.insert(g);

            // Update vector
            v.geometryIndexes.push_back(g->getID());
            std::static_pointer_cast<VectorGeometry<CTYPE> >(g)->updateVector(v, g->getID());
        }

        // Copy properties
        v.pProperties = pProperties;

        return v;
    }

    /**
    * Return nearest %VectorGeometry to bounds
    * @param bounds Bounds of region
    * @param geometryTypes the types of geometry to include in resulting %Vector
    * @return %Vector containing geometry nearest to region.
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::nearest(BoundingBox<CTYPE> bounds, size_t geometryTypes) {

        // Find nearest
        std::vector<GeometryBasePtr<CTYPE> > geometryList;
        tree.nearest(bounds, geometryList, geometryTypes);

        // Create return vector
        Vector<CTYPE> v;

        // Set projection
        v.setProjectionParameters(proj);

        // Sort by ID
        std::sort(geometryList.begin(), geometryList.end(), 
            [](const GeometryBasePtr<CTYPE> &l, const GeometryBasePtr<CTYPE> &r) { return l->getID() < r->getID(); });
        
        // Copy geometry
        v.pVertices = pVertices;
        v.pGeometry = pGeometry;
        for (auto &g : geometryList) {

            // Add geometry
            v.tree.insert(g);

            // Update vector
            v.geometryIndexes.push_back(g->getID());
            std::static_pointer_cast<VectorGeometry<CTYPE> >(g)->updateVector(v, g->getID());
        }

        // Copy properties
        v.pProperties = pProperties;

        return v;
    }

    /**
    * Get list of geometry attached to coordinate.
    * @param %Coordinate to use.
    * @param geometryTypes the types of geometry to include in list.
    * @return list of geometry within region.
    */
    template <typename CTYPE>
    std::vector<GeometryBasePtr<CTYPE> > Vector<CTYPE>::attached(Coordinate<CTYPE> c, size_t geometryTypes) {

        // Get attached geometry
        std::vector<GeometryBasePtr<CTYPE> > geometryList;
        tree.search({c, c}, geometryList, geometryTypes);
        return geometryList;
    }

    /**
    * Remove all coincident vertices from @Vector
    */
    template <typename CTYPE>
    void Vector<CTYPE>::deduplicateVertices() {
    
        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getCoordinates();

        // Check size
        if (vertices.size() == 0)
            return;

        // Create z-index list of vertices
        std::vector<std::pair<uint64_t, std::size_t> > zIndexes;
        for (std::size_t i = 0; i < vertices.size(); i++) {
            zIndexes.push_back ( { getBounds().createZIndex(vertices[i]), i } );
        }

        // Sort z-indexes
        std::sort(zIndexes.begin(), zIndexes.end(), 
            [](const std::pair<uint64_t, std::size_t> &l, const std::pair<uint64_t, std::size_t> &r) { return l.first < r.first; });

        // Find coincident vertices
        std::vector<std::size_t> duplicateMap;
        duplicateMap.resize(vertices.size());
        auto zIndex = zIndexes.begin();
        do {
            // Set first index
            auto currentIndex = zIndex->first;
            auto currentVertex = zIndex->second;
            duplicateMap[currentVertex] = currentVertex;

            // Map all coincident vertices to first index
            while (++zIndex != zIndexes.end() && zIndex->first == currentIndex && vertices[zIndex->second] == vertices[currentVertex]) {
                duplicateMap[zIndex->second] = currentVertex;
            }

        } while (zIndex != zIndexes.end());

        // Build duplicate map and update vertices
        std::size_t newIndex = 0;
        std::map<std::size_t, std::size_t> newIndexes;
        for (std::size_t i = 0; i < vertices.size(); i++) {

            // Check for duplicate
            if (i == duplicateMap[i]) {

                // Update map
                newIndexes.insert( { i, newIndex } );

                // Update vertex
                vertices[newIndex] = vertices[i];

                // Increment index
                newIndex++;
            }
        }

        // Exit if there are no duplicates
        if (vertices.size() == newIndexes.size())
            return;

        // Resize vertices
        vertices.resize(newIndexes.size());

        // Update duplicate indexes
        for (std::size_t i = 0; i < duplicateMap.size(); i++) {
            duplicateMap[i] = newIndexes[duplicateMap[i]];
        }

        // Get geometry handle
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

        // Update all geometry
        for (const auto &gi : geometryIndexes) {
            auto &g = geometry[gi];

            // Process type
            if (g->isType(GeometryType::Point)) {

                // Update point vertex index
                auto &p = *std::static_pointer_cast<Point<CTYPE> >(g);
                p.pointVertex = duplicateMap[p.pointVertex];

            } else if (g->isType(GeometryType::LineString)) {

                // Update line string vertex indexes
                auto &lineString = *std::static_pointer_cast<LineString<CTYPE> >(g);
                for (std::size_t i = 0; i < lineString.lineVertices.size(); i++)
                    lineString.lineVertices[i] = duplicateMap[lineString.lineVertices[i]];

            } else if (g->isType(GeometryType::Polygon)) {

                // Update polygon vertex indexes
                auto &polygon = *std::static_pointer_cast<Polygon<CTYPE> >(g);
                for (std::size_t i = 0; i < polygon.polygonVertices.size(); i++)
                    polygon.polygonVertices[i] = duplicateMap[polygon.polygonVertices[i]];
            }
        }

    }

    /**
    * Map distance from geometry, returning a resulting raster.
    * @param resolution resolution of resulting %Raster
    * @param geometryTypes the types of geometry to include in resulting %Raster
    * @param bounds %BoundingBox of resulting %Raster, defaults to %Vector bounding box
    * @return %Raster of %Vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    Raster<RTYPE, CTYPE> Vector<CTYPE>::mapDistance(CTYPE resolution, size_t geometryTypes, BoundingBox<CTYPE> bounds) {
    
        // Create raster
        Raster<RTYPE, CTYPE> r;

        // Check resolution
        if (resolution <= 0.0) {

            // Error for invalid raster resolutions
            std::cout << "ERROR: Raster resolution must be positive" << std::endl;
            return r;
        }

        // Check bounds
        uint32_t buffer = 0;
        if (bounds.extent().p == 0.0 && bounds.extent().q == 0.0) {

            // Set to vector bounds
            bounds = getBounds();

            // Calculate dimensions with 1 cell boundary
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution)+2;
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution)+2;

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p-resolution, bounds.min.q-resolution);

        } else {

            // Calculate dimensions
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution);
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution);

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p, bounds.min.q);
        }

        // Set name and projection
        r.setProperty("name", "output");
        r.setProjectionParameters(proj);

        // Map vector to raster
        r.mapVector(*this, geometryTypes);

        return r;
    }

    /**
    * Rasterize vector data, returning a resulting raster.
    * @param resolution resolution of resulting %Raster
    * @param geometryTypes the types of geometry to include in resulting %Raster
    * @param bounds %BoundingBox of resulting %Raster, defaults to %Vector bounding box
    * @return %Raster of %Vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    Raster<RTYPE, CTYPE> Vector<CTYPE>::rasterise(CTYPE resolution, std::string script, size_t geometryTypes, BoundingBox<CTYPE> bounds) {
    
        // Create raster
        Raster<RTYPE, CTYPE> r;

        // Check resolution
        if (resolution <= 0.0) {

            // Error for invalid raster resolutions
            std::cout << "ERROR: Raster resolution must be positive" << std::endl;
            return r;
        }

        // Check bounds
        uint32_t buffer = 0;
        if (bounds.extent().p == 0.0 && bounds.extent().q == 0.0) {

            // Set to vector bounds
            bounds = getBounds();

            // Calculate dimensions with 1 cell boundary
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution)+2;
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution)+2;

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p-resolution, bounds.min.q-resolution);

        } else {

            // Calculate dimensions
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution);
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution);

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p, bounds.min.q);
        }
        
        // Set name and projection
        r.setProperty("name", "output");
        r.setProjectionParameters(proj);

        // Map vector to raster
        r.rasterise(*this, script, geometryTypes);

        return r;
    }
    
    /**
    * Sample raster at %Point locations, writing values to %Point property
    * @param r %Raster to sample
    */
    template <typename CTYPE>
    template <typename RTYPE>
    bool Vector<CTYPE>::pointSample(Raster<RTYPE, CTYPE> &r) {

        // Check coordinate systems are the same
        if (r.getProjectionParameters() != proj) {
            std::cout << "ERROR: vector and raster projections must be the same for sampling" << std::endl;
            return false;
        }

        // Check name
        auto name = r.template getProperty<std::string>("name");
        if (name.length() != 0) {

            // Get geometry handle
            std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
            
            // Sample at points
            for (auto i : pointIndexes) {

                // Get handle to point
                auto &p = *std::static_pointer_cast<Point<CTYPE> >(geometry[i]);
                auto c = getPointCoordinate(i);

                // Get raster value
                auto v = r.getNearestValue(c.p, c.q);
                
                if (v == v) {

                    // Write non-null values to Point
                    setProperty<double>(geometry[i]->getID(), name, (double)v);

                } else {

                    // Remove any existing data if value is null
                    deleteProperty(geometry[i]->getID(), name);
                }
            }
        } else {

            // Warning if raster is not named
            std::cout << "ERROR: un-named raster used for sampling, no samples have been taken" << std::endl;
            return false;
        }

        return true;
    }
    
    // Float type definitions
    template class VertexCollection<float>;
    template class VectorGeometry<float>;
    template class Point<float>;
    template class LineString<float>;
    template class Polygon<float>;
    template class Vector<float>;
    
    template bool Vector<float>::pointSample<float>(Raster<float, float> &);
    template Raster<float, float> Vector<float>::mapDistance<float>(float, size_t, BoundingBox<float>);
    template Raster<float, float> Vector<float>::rasterise<float>(float, std::string, size_t, BoundingBox<float>);

    template void Vector<float>::setProperty<std::string>(cl_uint, std::string, std::string v);
    template void Vector<float>::setProperty<int>(cl_uint, std::string, int v);
    template void Vector<float>::setProperty<double>(cl_uint, std::string, double v);
    template void Vector<float>::setProperty<cl_uint>(cl_uint, std::string, cl_uint v);

    template std::string Vector<float>::getProperty<std::string>(cl_uint, std::string, std::string);
    template int Vector<float>::getProperty<int>(cl_uint, std::string, int);
    template double Vector<float>::getProperty<double>(cl_uint, std::string, double);
    template cl_uint Vector<float>::getProperty<cl_uint>(cl_uint, std::string, cl_uint);
        
    // Double type definitions
    template class VertexCollection<double>;
    template class VectorGeometry<double>;
    template class Point<double>;
    template class LineString<double>;
    template class Polygon<double>;
    template class Vector<double>;

    template bool Vector<double>::pointSample<double>(Raster<double, double> &);
    template Raster<double, double> Vector<double>::mapDistance<double>(double, size_t, BoundingBox<double>);
    template Raster<double, double> Vector<double>::rasterise<double>(double, std::string, size_t, BoundingBox<double>);
    
    template void Vector<double>::setProperty<std::string>(cl_uint, std::string, std::string v);
    template void Vector<double>::setProperty<int>(cl_uint, std::string, int v);
    template void Vector<double>::setProperty<double>(cl_uint, std::string, double v);
    template void Vector<double>::setProperty<cl_uint>(cl_uint, std::string, cl_uint v);
    
    template std::string Vector<double>::getProperty<std::string>(cl_uint, std::string, std::string);
    template int Vector<double>::getProperty<int>(cl_uint, std::string, int);
    template double Vector<double>::getProperty<double>(cl_uint, std::string, double);
    template cl_uint Vector<double>::getProperty<cl_uint>(cl_uint, std::string, cl_uint);
}
