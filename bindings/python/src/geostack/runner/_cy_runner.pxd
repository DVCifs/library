# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
# cython: language_level=3

from libcpp.string cimport string
from libcpp.vector cimport vector
from ..raster._cy_raster cimport (_RasterBaseList_d, _RasterBaseList_f,
    _cyRaster_d, _cyRaster_f, _cyRaster_d_i, _cyRaster_f_i)
from ..raster._cy_raster cimport Raster
from libc.stdint cimport uint32_t
from libcpp cimport bool

cdef extern from "utils.h":
    void cy_copy[T](T& a, T& b) nogil

cdef extern from "<functional>" namespace "std" nogil:
    cdef cppclass reference_wrapper[T]:
        # reference_wrapper() except +
        reference_wrapper(T& ref) except +
        reference_wrapper(T&&) except +
        reference_wrapper(reference_wrapper[T]&) except +
        reference_wrapper& get() except +
        T& get() except +
        reference_wrapper operator=(reference_wrapper&) except +
        reference_wrapper& operator[]() except +
    reference_wrapper[T] cpp_ref "std::ref" [T](T& t) except +
    reference_wrapper[T] cpp_ref "std::ref" [T](reference_wrapper[T] t) except +

cdef extern from "gs_raster.h" namespace "Geostack":
    cdef cppclass RasterBase[C]:
        pass

    bool runScriptNoOut[C](string script,
        vector[reference_wrapper[RasterBase[C]]] inputRasters,
        size_t parameters) nogil except +

    Raster[R, C] runScript[R, C](string script,
        vector[reference_wrapper[RasterBase[C]]] inputRasters,
        size_t parameters) nogil except +

    Raster[R, C] runAreaScript[R, C](string script,
        RasterBase[C] inputRaster, int width) nogil except +

cdef extern from "pyTestScript.h" namespace "Geostack::pyGeostack":
    cdef bool testSolver(bool verbosity) nogil except +

ctypedef reference_wrapper[RasterBase[double]] RasterBaseRef_d
ctypedef reference_wrapper[RasterBase[float]] RasterBaseRef_f

cpdef bool _run_script_noout_d(bytes script, _RasterBaseList_d input_rasters,
    size_t parameters) except +
cpdef bool _run_script_noout_f(bytes script, _RasterBaseList_f input_rasters,
    size_t parameters) except +
    
cpdef _cyRaster_d _run_script_d(bytes script, _RasterBaseList_d input_rasters,
    size_t parameters) except +
cpdef _cyRaster_f _run_script_f(bytes script, _RasterBaseList_f input_rasters,
    size_t parameters) except +
cpdef _cyRaster_d_i _run_script_d_i(bytes script, _RasterBaseList_d input_rasters,
    size_t parameters) except +
cpdef _cyRaster_f_i _run_script_f_i(bytes script, _RasterBaseList_f input_rasters,
    size_t parameters) except +

cpdef _cyRaster_d _run_areascript_d(bytes script, _cyRaster_d input_raster,
    int width) except +
cpdef _cyRaster_f _run_areascript_f(bytes script, _cyRaster_f input_raster,
    int width) except +
cpdef _cyRaster_d_i _run_areascript_d_i(bytes script, _cyRaster_d_i input_raster,
    int width) except +
cpdef _cyRaster_f_i _run_areascript_f_i(bytes script, _cyRaster_f_i input_raster,
    int width) except +

cpdef bool test_solver(bool verbosity) except+