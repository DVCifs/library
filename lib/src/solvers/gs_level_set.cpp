/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <iostream>
#include <regex>

#include "json11/json11.hpp"

#include "gs_solver.h"
#include "gs_vector.h"
#include "gs_geojson.h"
#include "gs_projection.h"
#include "gs_level_set.h"

// Include code for projection
#include "cl_date_head.c"

namespace Geostack
{               
        
    template <typename TYPE>
    std::string LevelSet<TYPE>::buildNormal = "REALVEC2 normal_vector = normalize((REALVEC2)(0.5*(_distance_E-_distance_W)/_dim.hx, 0.5*(_distance_N-_distance_S)/_dim.hy)); \n";

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectNormal = "REALVEC2 advect_normal_vector = (REALVEC2)(_dx, _dy)/_mag; \n";

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectVector = "REALVEC2 advect_vector = (REALVEC2)(_advect_x, _advect_y); \n";

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectDotNormal = "REAL advect_dot_normal = fmax(dot(advect_vector, advect_normal_vector), (REAL)0.0); \n";

    using namespace json11;

    // Set static members
    template <typename TYPE>
    const TYPE LevelSet<TYPE>::CFL = (TYPE)0.75;

    template <typename TYPE>
    const TYPE LevelSet<TYPE>::timeStepMin = (TYPE)1.0E-6;

    template <typename TYPE>
    const TYPE LevelSet<TYPE>::timeStepMax = (TYPE)60.0;

    template <typename TYPE>
    const TYPE LevelSet<TYPE>::narrowBandCells = (TYPE)5.0;
    
    template <typename TYPE>
    bool LevelSet<TYPE>::init(
        std::string jsonConfig,
        Vector<TYPE> &sources_,
        Vector<TYPE> &mask_,
        std::shared_ptr<Variables<TYPE> > &variables_,
        std::vector<RasterBasePtr<TYPE> > &inputLayers_,
        std::vector<RasterBasePtr<TYPE> > &outputLayers_) {

        uint32_t tileSize = TileMetrics::tileSize;
        
        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();

        // Parse configuration
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            std::cout << "ERROR: " << jsonErr << std::endl;
            return false;
        }

        // Get and create simulation projection
        std::string projString;
        if (config["projection"].is_string()) {
            projString = config["projection"].string_value();
        }
        auto proj = Projection<TYPE>::parsePROJ4(projString);

        // Get and check resolution
        resolution = 0.0;
        if (config["resolution"].is_number()) {
            resolution = (TYPE)config["resolution"].number_value();
        } else {
            std::cout << "ERROR: No 'resolution' value supplied." << std::endl;
            return false;
        }
        if (resolution <= 0.0) {
            std::cout << "ERROR: Requested resolution " << resolution << " must be positive." << std::endl;
            return false;
        }
        
        // Get and check scripts
        std::string initScript;
        if (config["initialisationScript"].is_string()) {
            initScript = config["initialisationScript"].string_value();
        }
        if (initScript.length() == 0) {
            std::cout << "WARNING: No 'initialisationScript' value supplied." << std::endl;
        }

        std::string advectScript;
        if (config["advectionScript"].is_string()) {
            advectScript = config["advectionScript"].string_value();
        }
        if (advectScript.length() == 0) {
            std::cout << "WARNING: No 'advectionScript' value supplied, default advection vector of (0, 0) applied." << std::endl;
            advectScript = "advect_x = 0.0; advect_y = 0.0;";
        }

        std::string buildScript;
        if (config["buildScript"].is_string()) {
            buildScript = config["buildScript"].string_value();
        }
        if (buildScript.length() == 0) {
            std::cout << "WARNING: No 'buildScript' value supplied." << std::endl;
        }

        std::string updateScript;
        if (config["updateScript"].is_string()) {
            updateScript = config["updateScript"].string_value();
        }
        if (updateScript.length() == 0) {
            std::cout << "WARNING: No 'updateScript' value supplied." << std::endl;
        }

        // Process start date and time
        hasStartDate = false;
        currentEpochMilliseconds = 0;
        startTime = date::sys_time<std::chrono::milliseconds>();
        startDay = date::sys_time<std::chrono::milliseconds>();
        if (config["startDateISO8601"].is_string()) {

            // Get date
            auto dateString = config["startDateISO8601"].string_value();
            if (dateString.length() > 0) {
            
                // Parse date
                std::istringstream in(dateString);
                if (dateString.back() == 'Z') {

                    // Try to parse Zulu format
                    in >> date::parse("%FT%TZ", startTime);

                } else {

                    // Try to parse time zone
                    in >> date::parse("%FT%T%Ez", startTime);
                }

                // Get start day
                if (!in.fail()) {
                    startDay = date::floor<date::days>(startTime);
                    hasStartDate = true;
                }
            }
        }

        // Initialise parameters
        p.time = 0.0;
        p.dt = timeStepMin;
        p.maxSpeed = 0.0;
        p.area = 0.0;
        p.bandWidth = 0.0;

        if (hasStartDate) {
            auto currentTime = startTime;
            currentEpochMilliseconds = currentTime.time_since_epoch().count();
            p.JulianFraction = (REAL)((double)(currentTime-startDay).count()/(double)86400000.0);
            p.JulianDate = (REAL)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);
        } else {
            p.JulianFraction = 0;
            p.JulianDate = (REAL)DateCalculation::JulianDateFromEpoch<double>(0.0);
        }

        // Copy start conditions and layers
        sources = sources_;
        mask = mask_;
        variables = variables_;
        inputLayers = inputLayers_;
        outputLayers = outputLayers_;

        // Re-project input layers
        if (sources.hasData() && sources.getProjectionParameters() != proj)
            sources = sources.convert(proj);
        if (mask.hasData() && mask.getProjectionParameters() != proj)
            mask = mask.convert(proj);

        // Create dummy if variables is null
        if (variables == nullptr) {
            variables = std::make_shared<Variables<TYPE> >();
            variables->set("dummy", 0.0);
        }
        
        // Get distance map bounds
        auto bounds = sources.getBounds();

        // Add buffer of narrow band
        p.bandWidth = (TYPE)narrowBandCells*resolution;
        bounds.extend((TYPE)2.0*p.bandWidth);

        // Calculate distance map cells
        auto boundsExtent = bounds.extent();
        uint32_t nx = (uint32_t)ceil(boundsExtent.p/resolution);
        uint32_t ny = (uint32_t)ceil(boundsExtent.q/resolution);

        // Calculate number of tiles
        uint32_t ntx = (nx+((tileSize-(nx%tileSize))%tileSize))/tileSize;
        uint32_t nty = (ny+((tileSize-(ny%tileSize))%tileSize))/tileSize;

        // Calculate distance map offset
        TYPE ox = bounds.min.p+(TYPE)0.5*(boundsExtent.p-(resolution*ntx*tileSize));
        TYPE oy = bounds.min.q+(TYPE)0.5*(boundsExtent.q-(resolution*nty*tileSize));
                    
        // Snap to multiple of global resolution
        ox = floor(ox/resolution)*resolution;
        oy = floor(oy/resolution)*resolution;

        // Create raster dimensions
        Dimensions<TYPE> dim;
        dim.nx = ntx*tileSize;
        dim.ny = nty*tileSize;
        dim.nz = 0;
        dim.hx = resolution;
        dim.hy = resolution;
        dim.hz = 1.0;
        dim.ox = ox;
        dim.oy = oy;
        dim.oz = 0.0;
        
        // Create layers
        layers.resize(LevelSetLayers::LevelSetLayers_END+inputLayers.size());

        // Populate layers
        if (!classification.init(dim)) 
            return false;
        classification.setProjectionParameters(proj);

        for (auto &l : layers) {
            if (!l.init(dim)) 
                return false;
            l.setProjectionParameters(proj);
        }

        for (auto &pl : outputLayers) {
            if (!pl->init(dim)) 
                return false;
            pl->setProjectionParameters(proj);
        }

        // Set base names
        classification.setProperty("name", "_class");
        layers[LevelSetLayers::Distance].setProperty("name", "_distance");
        layers[LevelSetLayers::DistanceUpdate].setProperty("name", "_distance_update");
        layers[LevelSetLayers::Rate].setProperty("name", "_rate");
        layers[LevelSetLayers::Speed].setProperty("name", "_speed");
        layers[LevelSetLayers::Arrival].setProperty("name", "_arrival");
        layers[LevelSetLayers::Mask].setProperty("name", "mask");
        layers[LevelSetLayers::Advect_x].setProperty("name", "_advect_x");
        layers[LevelSetLayers::Advect_y].setProperty("name", "_advect_y");
        //layers[LevelSetLayers::StartTime].setProperty("name", "_start_time");
        //layers[LevelSetLayers::StartTimeUpdate].setProperty("name", "_start_time_update");

        // Set input layer names
        std::string initScriptPrefix;
        for (std::size_t i = 0; i < inputLayers.size(); i++) {

            // Get input name and set layer name
            std::string name = inputLayers[i]->template getProperty<std::string>("name");
            layers[LevelSetLayers::LevelSetLayers_END+i].setProperty("name", name);

            // Prefix name of unaligned raster layer
            inputLayers[i]->setProperty("name", "INPUT_" + name);

            // Update prefix for initialisation script
            initScriptPrefix += name + " = INPUT_" + name + ";\n";
        }

        // Parse initialisation script
        initScript = std::regex_replace(initScript, std::regex("\\bstate\\b"), "_class_state");
        initScript = std::regex_replace(initScript, std::regex("\\bclass\\b"), "_class_lo");
        initScript = std::regex_replace(initScript, std::regex("\\bsubclass\\b"), "_class_sub_lo");
        initScript = std::regex_replace(initScript, std::regex("\\bJulian_date\\b"), "_p.Julian_date");
        initScript = initScriptPrefix+initScript;

        // Parse advect script
        advectScript = std::regex_replace(advectScript, std::regex("\\bstate\\b"), "_class_state");
        advectScript = std::regex_replace(advectScript, std::regex("\\bclass\\b"), "_class_lo");
        advectScript = std::regex_replace(advectScript, std::regex("\\bsubclass\\b"), "_class_sub_lo");
        advectScript = std::regex_replace(advectScript, std::regex("\\badvect_x\\b"), "_advect_x");
        advectScript = std::regex_replace(advectScript, std::regex("\\badvect_y\\b"), "_advect_y");
        advectScript = std::regex_replace(advectScript, std::regex("\\btime\\b"), "_p.time");
        advectScript = std::regex_replace(advectScript, std::regex("\\bJulian_date\\b"), "_p.Julian_date");
        
        // Parse build script
        bool needsNormal = false;
        bool needsAdvectNormal = false;
        bool needsAdvectVector = false;
        bool needsAdvectDotNormal = false;
        if (std::regex_search(buildScript, std::regex("\\bnormal_vector\\b"))) {
            needsNormal = true;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_normal_vector\\b"))) {
            needsAdvectNormal = true;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_vector\\b"))) {
            needsAdvectVector = true;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_dot_normal\\b"))) {
            needsAdvectNormal = true;
            needsAdvectVector = true;
            needsAdvectDotNormal = true;
        }
        
        if (needsAdvectDotNormal)
            buildScript = buildAdvectDotNormal + buildScript;
        if (needsAdvectVector)
            buildScript = buildAdvectVector + buildScript;
        if (needsAdvectNormal)
            buildScript = buildAdvectNormal + buildScript;
        if (needsNormal)
            buildScript = buildNormal + buildScript;

        buildScript = std::regex_replace(buildScript, std::regex("\\bstate\\b"), "_class_state");
        buildScript = std::regex_replace(buildScript, std::regex("\\bclass\\b"), "_class_lo");
        buildScript = std::regex_replace(buildScript, std::regex("\\bsubclass\\b"), "_class_sub_lo");
        buildScript = std::regex_replace(buildScript, std::regex("\\btime\\b"), "_p.time");
        buildScript = std::regex_replace(buildScript, std::regex("\\bhour\\b"), "(uint)(_p.Julian_fraction*24.0)");
        buildScript = std::regex_replace(buildScript, std::regex("\\bJulian_date\\b"), "_p.Julian_date");
        buildScript = std::regex_replace(buildScript, std::regex("\\barrival\\b"), "_arrival");
        //buildScript = std::regex_replace(buildScript, std::regex("\\bstart_time\\b"), "_start_time");
        
        // Parse update script
        updateScript = std::regex_replace(updateScript, std::regex("\\bstate\\b"), "_class_state");
        updateScript = std::regex_replace(updateScript, std::regex("\\bclass\\b"), "_class_lo");
        updateScript = std::regex_replace(updateScript, std::regex("\\bsubclass\\b"), "_class_sub_lo");
        updateScript = std::regex_replace(updateScript, std::regex("\\btime\\b"), "_p.time");
        updateScript = std::regex_replace(updateScript, std::regex("\\bhour\\b"), "(uint)(_p.Julian_fraction*24.0)");
        updateScript = std::regex_replace(updateScript, std::regex("\\bJulian_date\\b"), "_p.Julian_date");
        updateScript = std::regex_replace(updateScript, std::regex("\\barrival\\b"), "_arrival");
        //updateScript = std::regex_replace(updateScript, std::regex("\\bstart_time\\b"), "_start_time_update");
        
        // Patch kernel script with variable names
        std::string variableList;
        auto type = Solver::getTypeString<TYPE>();
        for (auto vi : variables->getIndexes()) {
            variableList += "const " + type + " " + vi.first + " = (*(_vars +" + std::to_string(vi.second) + "));\n";
        }
        if (variableList.size() > 0) {
            
            // Replace instances
            initScript = variableList+initScript;
            advectScript = variableList+advectScript;
            buildScript = variableList+buildScript;
            updateScript = variableList+updateScript;
        }

        // Create initialisation kernel
        // Requires:
        //   classification       - land classification layer (aligned)
        //   distance             - distance from perimeter (aligned)
        //   mask                 - user-defined mask (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)
        //   output               - output layer (aligned)
        initRefs = { 
            classification, 
            layers[LevelSetLayers::Distance], 
            layers[LevelSetLayers::Mask], 
            layers[LevelSetLayers::Advect_x], 
            layers[LevelSetLayers::Advect_y]
        };

        for (std::size_t i = 0; i < inputLayers.size(); i++) 
            initRefs.push_back(layers[i+LevelSetLayers::LevelSetLayers_END]);
            
        for (std::size_t i = 0; i < inputLayers.size(); i++) 
            initRefs.push_back(*inputLayers[i]);

        for (std::size_t i = 0; i < outputLayers.size(); i++) 
            initRefs.push_back(*outputLayers[i]);

        auto initKernelScript = classification.generateKernelScript(initScript, std::string(R_cl_level_set_init_c), initRefs);

        // Create advection kernel
        // Requires:
        //   classification       - land classification layer (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)
        //   output               - output layer (aligned)
        advectRefs = { 
            classification, 
            layers[LevelSetLayers::Advect_x], 
            layers[LevelSetLayers::Advect_y]
        };
        
        for (std::size_t i = 0; i < inputLayers.size(); i++) 
            advectRefs.push_back(layers[i+LevelSetLayers::LevelSetLayers_END]);

        for (std::size_t i = 0; i < outputLayers.size(); i++) 
            advectRefs.push_back(*outputLayers[i]);

        auto advectKernelScript = classification.generateKernelScript(advectScript, std::string(R_cl_level_set_advect_c), advectRefs);
        
        // Create build kernel
        // Requires:
        //   classification       - land classification layer (aligned, neighbours)
        //   distance             - distance from perimeter (aligned, neighbours)
        //   distance update      - updated distance from perimeter (aligned)
        //   rate                 - time derivative of update (aligned)
        //   speed                - speed of perimeter (aligned)
        //   mask                 - user-defined mask (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)
        //   output               - output layer (aligned)
        classification.setRequiredNeighbours(Neighbours::Rook);
        layers[LevelSetLayers::Distance].setRequiredNeighbours(Neighbours::Rook);
        layers[LevelSetLayers::Distance].setNeedsWrite(false);
        buildRefs = { 
            classification, 
            layers[LevelSetLayers::Distance], 
            layers[LevelSetLayers::DistanceUpdate],
            layers[LevelSetLayers::Rate], 
            layers[LevelSetLayers::Speed],
            layers[LevelSetLayers::Mask], 
            layers[LevelSetLayers::Advect_x], 
            layers[LevelSetLayers::Advect_y] 
            //layers[LevelSetLayers::StartTime], 
            //layers[LevelSetLayers::StartTimeUpdate]
        };
        
        for (std::size_t i = 0; i < inputLayers.size(); i++) 
            buildRefs.push_back(layers[i+LevelSetLayers::LevelSetLayers_END]);

        for (std::size_t i = 0; i < outputLayers.size(); i++) 
            buildRefs.push_back(*outputLayers[i]);

        auto buildKernelScript = classification.generateKernelScript(buildScript, std::string(R_cl_level_set_build_c), buildRefs);

        classification.setRequiredNeighbours(Neighbours::None);
        layers[LevelSetLayers::Distance].setRequiredNeighbours(Neighbours::None);
        layers[LevelSetLayers::Distance].setNeedsWrite(true);
        
        // Create update kernel
        // Requires:
        //   classification       - land classification layer (aligned)
        //   distance             - distance from perimeter (aligned)
        //   distance update      - updated distance from perimeter (aligned, neighbours)
        //   rate                 - time derivative of update (aligned)
        //   speed                - speed of perimeter (aligned)
        //   arrival              - arrival time (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)
        //   output               - output layer (aligned)
        layers[LevelSetLayers::DistanceUpdate].setRequiredNeighbours(Neighbours::Rook);
        layers[LevelSetLayers::DistanceUpdate].setNeedsWrite(false);
        layers[LevelSetLayers::Distance].setNeedsStatus(true);
        //layers[LevelSetLayers::StartTime].setRequiredNeighbours(Neighbours::Queen);
        layers[LevelSetLayers::Arrival].setNeedsStatus(true);
        layers[LevelSetLayers::Speed].setReductionType(Reduction::Maximum);
        layers[LevelSetLayers::Rate].setNeedsWrite(false);
        layers[LevelSetLayers::Mask].setNeedsWrite(false);
        updateRefs = {
            classification, 
            layers[LevelSetLayers::Distance], 
            layers[LevelSetLayers::DistanceUpdate], 
            layers[LevelSetLayers::Rate], 
            layers[LevelSetLayers::Speed], 
            layers[LevelSetLayers::Arrival], 
            layers[LevelSetLayers::Mask], 
            layers[LevelSetLayers::Advect_x], 
            layers[LevelSetLayers::Advect_y]
            //layers[LevelSetLayers::StartTime], 
            //layers[LevelSetLayers::StartTimeUpdate]
        };
        
        for (std::size_t i = 0; i < inputLayers.size(); i++) 
            updateRefs.push_back(layers[i+LevelSetLayers::LevelSetLayers_END]);

        for (std::size_t i = 0; i < outputLayers.size(); i++) 
            updateRefs.push_back(*outputLayers[i]);

        auto updateKernelScript = classification.generateKernelScript(updateScript, std::string(R_cl_level_set_update_c), updateRefs);

        layers[LevelSetLayers::DistanceUpdate].setRequiredNeighbours(Neighbours::None);
        layers[LevelSetLayers::DistanceUpdate].setNeedsWrite(true);
        //layers[LevelSetLayers::StartTime].setRequiredNeighbours(Neighbours::None);
        layers[LevelSetLayers::Distance].setNeedsStatus(false);
        layers[LevelSetLayers::Arrival].setNeedsStatus(false);
        layers[LevelSetLayers::Speed].setReductionType(Reduction::None);
        layers[LevelSetLayers::Rate].setNeedsWrite(true);
        layers[LevelSetLayers::Mask].setNeedsWrite(true);

        // Create reinitialisation kernel
        // Requires:
        //   distance             - distance from perimeter (aligned, neighbours)
        //   distance update      - updated distance from perimeter (aligned)
        //   speed                - speed of perimeter (aligned, neighbours)
        layers[LevelSetLayers::Distance].setRequiredNeighbours(Neighbours::Rook);
        layers[LevelSetLayers::Distance].setNeedsWrite(false);
        layers[LevelSetLayers::Speed].setRequiredNeighbours(Neighbours::Rook);
        layers[LevelSetLayers::Speed].setNeedsWrite(false);
        layers[LevelSetLayers::Mask].setNeedsWrite(false);
        reinitRefs = { 
            layers[LevelSetLayers::Distance], 
            layers[LevelSetLayers::DistanceUpdate], 
            layers[LevelSetLayers::Speed]
        };
        reinitFlipRefs = { 
            layers[LevelSetLayers::DistanceUpdate], 
            layers[LevelSetLayers::Distance], 
            layers[LevelSetLayers::Speed]
        };

        auto reinitKernelScript = classification.generateKernelScript("", std::string(R_cl_level_set_reinit_c), reinitRefs);

        layers[LevelSetLayers::Distance].setRequiredNeighbours(Neighbours::None);
        layers[LevelSetLayers::Distance].setNeedsWrite(true);
        layers[LevelSetLayers::Speed].setRequiredNeighbours(Neighbours::None);
        layers[LevelSetLayers::Speed].setNeedsWrite(true);
        layers[LevelSetLayers::Mask].setNeedsWrite(true);

        // Build script and get kernels
        KernelScript levelSetScript;
        levelSetScript.script = 
            std::string(R_cl_date_head_c) +
            std::string(R_cl_level_set_head_c) +
            advectKernelScript.script + 
            initKernelScript.script + 
            buildKernelScript.script + 
            updateKernelScript.script + 
            reinitKernelScript.script;
        levelSetScript.usingProjection = 
            advectKernelScript.usingProjection |
            initKernelScript.usingProjection |
            buildKernelScript.usingProjection |
            updateKernelScript.usingProjection |
            reinitKernelScript.usingProjection;

        auto levelSetHash = classification.buildScript(levelSetScript);
        if (levelSetHash == solver.getNullHash())
            return false;
            
        advectKernel = solver.getKernel(levelSetHash, "advect");
        initKernel = solver.getKernel(levelSetHash, "init");
        buildKernel = solver.getKernel(levelSetHash, "build");
        updateKernel = solver.getKernel(levelSetHash, "update");
        reinitKernel = solver.getKernel(levelSetHash, "reinitWeighted");
        
        // Add mask
        if (mask.hasData()) {
            layers[LevelSetLayers::Mask].mapVector(mask, GeometryType::Point | GeometryType::LineString | GeometryType::Polygon, "radius");
        }

        // Get variables        
        cl::Buffer varBuffer;
        if (!variables->getBuffer(varBuffer)) {
            std::cout << "ERROR: No variable data supplied." << std::endl;
            return false;
        }

        // Run kernels and set initial active tiles
        activeTileMap.clear();
        auto rDim = classification.getRasterDimensions();
        for (uint32_t tj = 0; tj < rDim.ty; tj++) {
            for (uint32_t ti = 0; ti < rDim.tx; ti++) {    

                // Run initialisation kernel
                initKernel.setArg(0, p);
                initKernel.setArg(1, varBuffer);
                classification.runTileKernel(ti, tj, initKernel, initRefs, 2);        

                // Run advection kernel
                advectKernel.setArg(0, p);
                advectKernel.setArg(1, varBuffer);
                classification.runTileKernel(ti, tj, advectKernel, advectRefs, 2);

                // Add to active tile list
                activeTileMap.insert( { ti, tj } );
            }
        }

        // Set initialisation flag
        initialised = true;

        return true;
    }

    template <typename TYPE>
    bool LevelSet<TYPE>::step() {

        // Check initialisation
        if (!initialised) {
            std::cout << "ERROR: Solver not initialised." << std::endl;
            return false;
        }

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();

        // Update times
        if (hasStartDate) {
            auto currentTime = startTime+std::chrono::seconds((uint64_t)((double)p.time));
            currentEpochMilliseconds = currentTime.time_since_epoch().count();
            p.JulianFraction = (REAL)((double)(currentTime-startDay).count()/(double)86400000.0);
            p.JulianDate = (REAL)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);
        } else {
            currentEpochMilliseconds = (uint64_t)((double)p.time*1000.0);
            p.JulianFraction = (REAL)((double)currentEpochMilliseconds/(double)86400000.0);
            p.JulianDate = (REAL)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);
        }

        // Find sources in timeslice
        auto bounds = sources.getBounds();
        auto currentSources = sources.region(BoundingBox<TYPE>( 
            { { bounds.min.p, bounds.min.q, 0.0, p.time-p.dt }, { bounds.max.p, bounds.max.q, 0.0, p.time } } ) );
        if (currentSources.hasData()) {

            // Add sources
            layers[LevelSetLayers::Distance].mapVector(currentSources, GeometryType::Point | GeometryType::LineString | GeometryType::Polygon, "radius");
        }

        // Run solver
        auto rDim = classification.getRasterDimensions();

        // Get variables
        cl::Buffer varBuffer;
        if (!variables->getBuffer(varBuffer)) {
            std::cout << "ERROR: No variable data supplied." << std::endl;
            return false;
        }
        
        // Build first numerical integration stage
        layers[LevelSetLayers::Distance].setRequiredNeighbours(Neighbours::Rook);
        for (auto &t : activeTileMap) {

            // Get indexes
            uint32_t ti = t.first;
            uint32_t tj = t.second;

            // Run advection kernel            
            classification.setRequiredNeighbours(Neighbours::None);
            advectKernel.setArg(0, p);
            advectKernel.setArg(1, varBuffer);
            classification.runTileKernel(ti, tj, advectKernel, advectRefs, 2);

            // Run build kernel
            classification.setRequiredNeighbours(Neighbours::Rook);
            buildKernel.setArg(0, p);
            buildKernel.setArg(1, varBuffer);
            classification.runTileKernel(ti, tj, buildKernel, buildRefs, 2);
        }
        classification.setRequiredNeighbours(Neighbours::None);
        layers[LevelSetLayers::Distance].setRequiredNeighbours(Neighbours::None);
            
        // Run second numerical integration stage and update
        layers[LevelSetLayers::DistanceUpdate].setRequiredNeighbours(Neighbours::Rook);
        //layers[LevelSetLayers::StartTime].setRequiredNeighbours(Neighbours::Queen);
        layers[LevelSetLayers::Distance].setNeedsStatus(true);
        layers[LevelSetLayers::Arrival].setNeedsStatus(true);
        layers[LevelSetLayers::Speed].setReductionType(Reduction::Maximum);
        cl_uint totalCount = 0;
        for (auto &t : activeTileMap) {

            // Get indexes
            uint32_t ti = t.first;
            uint32_t tj = t.second;

            // Reset boundary status bits
            layers[LevelSetLayers::Distance].setTileStatus(ti, tj, 0);

            // Run update kernel
            updateKernel.setArg(0, p);
            updateKernel.setArg(1, varBuffer);
            classification.runTileKernel(ti, tj, updateKernel, updateRefs, 2);

            // Update cell count
            cl_uint count = 0;
            layers[LevelSetLayers::Arrival].getResetTileStatus(ti, tj, count, 0);
            totalCount += count;
        }

        // Get maximum speed and minimum distance to boundary
        p.maxSpeed = (TYPE)layers[LevelSetLayers::Speed].reduce();
        p.area = (TYPE)(totalCount*resolution*resolution);

        // Reset layer parameters
        layers[LevelSetLayers::DistanceUpdate].setRequiredNeighbours(Neighbours::None);
        //layers[LevelSetLayers::StartTime].setRequiredNeighbours(Neighbours::None);
        layers[LevelSetLayers::Distance].setNeedsStatus(false);
        layers[LevelSetLayers::Arrival].setNeedsStatus(false);
        layers[LevelSetLayers::Speed].setReductionType(Reduction::None);

        // Build active tile map
        cl_uint resizeBits = buildActiveTiles(rDim);

        // Expand raster if perimeter is approaching boundary
        if (resizeBits != 0) {
            
            // Set new size
            uint32_t ox = 0;
            uint32_t oy = 0;
            if (resizeBits & 0x01) {
                rDim.ty+=1;
            }
            if (resizeBits & 0x02) {
                rDim.tx+=1;
            }
            if (resizeBits & 0x04) {
                rDim.ty+=1;
                oy = 1;
            }
            if (resizeBits & 0x08) {
                rDim.tx+=1;
                ox = 1;
            }

            // Get resizing indexes
            auto newIndexes = classification.resize2DIndexes(rDim.tx, rDim.ty, ox, oy);

            // Resize rasters
            classification.resize2D(rDim.tx, rDim.ty, ox, oy);
            for (auto &l : layers)
                l.resize2D(rDim.tx, rDim.ty, ox, oy);
            for (auto &pl : outputLayers)
                pl->resize2D(rDim.tx, rDim.ty, ox, oy);

            // Update and initialise tiles
            for (auto indexes : newIndexes) {

                // Add mask
                if (mask.hasData()) {
                    layers[LevelSetLayers::Mask].mapTileVector(indexes.first, indexes.second, mask, 
                        GeometryType::Point | GeometryType::LineString | GeometryType::Polygon, "radius");
                }

                // Run initialisation kernel
                initKernel.setArg(0, p);
                initKernel.setArg(1, varBuffer);
                classification.runTileKernel(indexes.first, indexes.second, initKernel, initRefs, 2);
            }
            for (auto indexes : newIndexes) {

                // Set new distance tile to twice the narrow band width
                layers[LevelSetLayers::Distance].setAllTileCellValues(indexes.first, indexes.second, (TYPE)2.0*p.bandWidth);
                layers[LevelSetLayers::DistanceUpdate].setAllTileCellValues(indexes.first, indexes.second, (TYPE)2.0*p.bandWidth);
            }

            // Reset dimensions
            rDim = classification.getRasterDimensions();

            // Rebuild active tile map
            buildActiveTiles(rDim);
        }
            
        // Run reinitialisation kernel
        layers[LevelSetLayers::Speed].setRequiredNeighbours(Neighbours::Rook);
        layers[LevelSetLayers::Distance].setRequiredNeighbours(Neighbours::Rook);
        for (auto &t : activeTileMap) {

            // Get indexes
            uint32_t ti = t.first;
            uint32_t tj = t.second;

            // Run reinitialisation kernel
            reinitKernel.setArg(0, p);
            classification.runTileKernel(ti, tj, reinitKernel, reinitRefs, 1);
        }

        layers[LevelSetLayers::Distance].setRequiredNeighbours(Neighbours::None);
        layers[LevelSetLayers::DistanceUpdate].setRequiredNeighbours(Neighbours::Rook);
        for (auto &t : activeTileMap) {

            // Get indexes
            uint32_t ti = t.first;
            uint32_t tj = t.second;
            
            // Run reinitialisation kernel
            reinitKernel.setArg(0, p);
            classification.runTileKernel(ti, tj, reinitKernel, reinitFlipRefs, 1);
        }
        layers[LevelSetLayers::DistanceUpdate].setRequiredNeighbours(Neighbours::None);
        layers[LevelSetLayers::Speed].setRequiredNeighbours(Neighbours::None);

        // Update time
        p.time += p.dt;

        // Check maximum speed
        if (p.maxSpeed == 0.0) {
            std::cout << "WARNING: Speed is zero everywhere, ending simulation." << std::endl;
            return false;
        }

        // Update time step
        TYPE idt = p.maxSpeed/resolution;                
        p.dt = (idt > 0.0) ? CFL/idt : timeStepMin; // Set time step using CFL condition
        if (p.dtLast > 0.0 && p.dt > p.dtLast*1.5)
            p.dt = (TYPE)1.5*p.dtLast;              // Allow, at most, a 50% increase in the time step
        p.dt = std::min(p.dt, timeStepMax);         // Upper limit of time step
        p.dtLast = p.dt;

        // Change max speed to take timestep into account
        p.maxSpeed = resolution/p.dt;

        return true;
    }

    template <typename TYPE>
    cl_uint LevelSet<TYPE>::buildActiveTiles(const RasterDimensions<TYPE> &rDim) {

        // Clear tiles
        activeTileMap.clear();

        // Process tiles
        cl_uint resizeBits = 0;
        for (uint32_t tj = 0; tj < rDim.ty; tj++) {
            for (uint32_t ti = 0; ti < rDim.tx; ti++) {

                // Get and reset tile status
                cl_uint status = 0;
                layers[LevelSetLayers::Distance].getTileStatus(ti, tj, status);
                
                if (status != 0) {

                    // Add tile
                    activeTileMap.insert( { ti, tj } );

                    // Check tile boundaries
                    if (status & 0x01) {
                    
                        // North boundary
                        if (tj < rDim.ty-1) {
                            activeTileMap.insert( { ti, tj+1 } );
                        } else {
                            resizeBits |= 0x01;
                        }
                    }

                    if (status & 0x02) {
                    
                        // East boundary
                        if (ti < rDim.tx-1) {
                            activeTileMap.insert( { ti+1, tj } );
                        } else {
                            resizeBits |= 0x02;
                        }
                    }

                    if (status & 0x04) {
                    
                        // South boundary
                        if (tj > 0) {
                            activeTileMap.insert( { ti, tj-1 } );
                        } else {
                            resizeBits |= 0x04;
                        }
                    }

                    if (status & 0x08) {
                    
                        // West boundary
                        if (ti > 0) {
                            activeTileMap.insert( { ti-1, tj } );
                        } else {
                            resizeBits |= 0x08;
                        }
                    }
                }
            }
        }

        // Return resize bits
        return resizeBits;
    }

    // Float type definitions
    template class LevelSet<float>;

    // Double type definitions
    template class LevelSet<double>;
}
