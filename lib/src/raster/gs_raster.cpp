/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <limits>
#include <cmath>
#include <cstring>
#include <numeric>
#include <regex>
#include <set>

#include "gs_solver.h"
#include "gs_raster.h"
#include "gs_vector.h"
#include "gs_string.h"
#include "gs_ascii.h"
#include "gs_flt.h"
#include "gs_geotiff.h"
#include "gs_gsr.h"

#include "miniz.h"

namespace Geostack
{
    /**
    * Dimensions equality check.
    * @param l input dimensions.
    * @param r input dimensions.
    * @return true if dimensions match
    */
    template <typename CTYPE>
    bool equalSpatialMetrics(const Dimensions<CTYPE> l, const Dimensions<CTYPE> r) {
    
        return 
            l.nx == r.nx && l.ny == r.ny && l.nz == r.nz &&
            l.hx == r.hx && l.hy == r.hy && l.hz == r.hz &&
            l.ox == r.ox && l.oy == r.oy && l.oz == r.oz;
    }

    /**
    * %Raster dimension structure output stream
    * @param os output stream.
    * @param r %RasterDimensions structure.
    * @return updated ostream.
    */
    template <typename CTYPE>
    std::ostream &operator<<(std::ostream &os, const RasterDimensions<CTYPE> &r) {

        // Write data
        return os
            << sizeof(r) << ", " << 
            r.d.nx << ", " <<
            r.d.ny << ", " << 
            r.d.nz << ", " << 
            r.d.hx << ", " << 
            r.d.hy << ", " << 
            r.d.hz << ", " << 
            r.d.ox << ", " << 
            r.d.oy << ", " << 
            r.d.oz << ", " << 
            r.ex << ", " << 
            r.ey << ", " << 
            r.ez << ", " << 
            r.tx << ", " << 
            r.ty;
    }
    
#ifdef USE_TILE_CACHING
    /**
    * Singleton instance of %TileCacheManager.
    */
    template <typename RTYPE, typename CTYPE>
    TileCacheManager<RTYPE, CTYPE> &TileCacheManager<RTYPE, CTYPE>::instance() {

        static TileCacheManager tileManager;
        return tileManager;
    }
    
    /**
    * TileCacheManager destructor
    */
    template <typename RTYPE, typename CTYPE>
    TileCacheManager<RTYPE, CTYPE>::~TileCacheManager() {
    
#ifdef USE_THREADED_TILE_CACHING

        // Finish all thread writes
        for (auto &thread_it : cacheTileThreadMap) {

            // Get cache thread
            auto &cacheThread = thread_it.second;
            if (cacheThread.joinable()) {

                // Finish any cache operations
                try {
                    cacheThread.join();
                } catch (std::system_error e) {
                    std::cout << std::endl << "ERROR: exception: " << e.what() << std::endl;
                }
            }
        }

#endif

    }
    
#ifdef USE_THREADED_TILE_CACHING
    /**
    * Flush any pending cache operations
    */
    template <typename RTYPE, typename CTYPE>
    void TileCacheManager<RTYPE, CTYPE>::flush(TilePtr<RTYPE, CTYPE> &tile) {    

        // Find cache thread
        auto thread_it = cacheTileThreadMap.find(tile);
        if (thread_it != cacheTileThreadMap.end()) {
        
            // Get cache thread
            auto &cacheThread = thread_it->second;
            if (cacheThread.joinable()) {

                // Finish any cache operations
                try {
                    cacheThread.join();
                } catch (std::system_error e) {
                    std::cout << std::endl << "ERROR: exception: " << e.what() << std::endl;
                }
            }

            // Delete thread entry
            cacheTileThreadMap.erase(tile);
        }
    }

#endif
    
    /**
    * Update the tile cache entries and write tile data to cache if required
    */
    template <typename RTYPE, typename CTYPE>
    void TileCacheManager<RTYPE, CTYPE>::update(TilePtr<RTYPE, CTYPE> &tile) {
            
        // Check for tile entry
        auto access_it = cacheTileAccessMap.find(tile);
        if (access_it != cacheTileAccessMap.end()) {

            // Remove existing map entries
            auto cacheAccess = access_it->second;
            cacheTileAccessMap.erase(tile);
            cacheAccessTileMap.erase(cacheAccess);
        }

        // Update maps
        cacheTileAccessMap[tile] = cacheAccessCount;
        cacheAccessTileMap[cacheAccessCount] = tile;

        // Increment cache count
        cacheAccessCount++;

        // Cache last accessed tile if memory limit is exceeded
        if (cacheCount > tileLimit) {

            // Get index of last tile accessed
            auto access_it = cacheAccessTileMap.begin();
            auto cacheAccess = access_it->first;
            TilePtr<RTYPE, CTYPE> &cacheTile = access_it->second;
            
#ifdef USE_THREADED_TILE_CACHING

            // Finish any cache operations
            flush(tile);

            // Create cache thread
            cacheTileThreadMap[cacheTile] = std::thread(&Tile<RTYPE, CTYPE>::writeToCache, cacheTile);

#else

            // Write to cache
            cacheTile->writeToCache();

#endif

            // Clear index entry
            cacheTileAccessMap.erase(cacheTile);
            cacheAccessTileMap.erase(cacheAccess);

            // Decrement cache counter
            cacheCount--;
        }
    }
    
    /**
    * Increment the tile cache counter
    */
    template <typename RTYPE, typename CTYPE>
    void TileCacheManager<RTYPE, CTYPE>::increment() {

        // Increment cache counter
        cacheCount++;
    }
#endif

    /**
    * Default %Variables constructor
    */
    template <typename RTYPE>
    Variables<RTYPE>::Variables():
        bufferMapPtr(nullptr),
        bufferOnDevice(false) {
    }

    /**
    * %Variables Destructor
    */
    template <typename RTYPE>
    Variables<RTYPE>::~Variables() {
    }
    
    /**
    * Set variable value in map
    * @param name the variable name.
    * @param value the variable value to set.
    */
    template <typename RTYPE>
    void Variables<RTYPE>::set(std::string name, RTYPE value) {

        // Ensure data is on host
        ensureDataOnHost();

        // Set value
        dataMap[name] = value;
    }

    /**
    * Get variable value from map
    * @param name the variable name.
    * @param value the variable value to set.
    */
    template <typename RTYPE>
    RTYPE Variables<RTYPE>::get(std::string name) {
    
        // Ensure data is on host
        ensureDataOnHost();

        // Get value
        auto it = dataMap.find(name);
        if (it == dataMap.end())
            return getNullValue<RTYPE>();
        return it->second;
    }

    /**
    * Get variable map
    * @param name the variable name.
    * @param value the variable value to set.
    */
    template <typename RTYPE>
    const std::map<std::string, std::size_t> &Variables<RTYPE>::getIndexes() {    

        // Copy data from map to vector
        refreshData();

        // Return map
        return dataIndexes;
    }

    /**
    * Map OpenCL variable buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE>
    void Variables<RTYPE>::mapBuffer(cl::CommandQueue &queue) {

        if (hasData()) {

            // Map buffer
            if (bufferOnDevice) {
                bufferMapPtr = queue.enqueueMapBuffer(buffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, dataVec.size()*sizeof(RTYPE));
            }
            bufferOnDevice = false;
        }
    }

    /**
    * Unmap OpenCL variable buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE>
    void Variables<RTYPE>::unmapBuffer(cl::CommandQueue &queue) {

        if (hasData()) {

            // Unmap buffer
            if (!bufferOnDevice) {
                queue.enqueueUnmapMemObject(buffer, bufferMapPtr);
            }
            bufferOnDevice = true;
        }
    }
    
    /**
    * Copy data from map to vector and create buffer
    */
    template <typename RTYPE>
    void Variables<RTYPE>::refreshData() {
    
        // Ensure data is on host
        ensureDataOnHost();

        // Check data sizes
        if (dataMap.size() != dataVec.size()) {        

            // Get solver handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();

            // Initialise data
            dataVec.resize(dataMap.size(), getNullValue<RTYPE>());

            // Create buffers
            cl_int err = CL_SUCCESS;
            auto &context = solver.getContext();
            buffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, dataVec.size()*sizeof(RTYPE), dataVec.data(), &err);
            if (err != CL_SUCCESS) {
                throw std::range_error("Cannot allocate memory for variable buffer");
            }

            // Initialise buffer
            bufferOnDevice = true;
            mapBuffer(queue);

            // Create data indexes
            std::size_t i = 0;
            dataIndexes.clear();
            for (auto it : dataMap) {
                dataIndexes[it.first] = i;
                i++;
            }
        }

        // Copy data into vector
        std::size_t i = 0;
        for (auto it : dataMap) {
            dataVec[i] = it.second;
            i++;
        }
    }

    /**
    * Ensure variable data is on the host
    */
    template <typename RTYPE>
    void Variables<RTYPE>::ensureDataOnHost() {
    
        // Check if data is on device
        if (bufferOnDevice) {

            // Map data from device to host
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapBuffer(queue);

            // Copy data into map
            for (auto it : dataIndexes)
                dataMap[it.first] = dataVec[it.second];
        }
    }

    /**
    * Get %Variables buffer.
    * @return reference to %Variables buffer.
    */
    template <typename RTYPE>
    bool Variables<RTYPE>::getBuffer(cl::Buffer &buffer_) {
    
        // Check data
        if (hasData()) {

            // Ensure reduction buffer is on device
            if (!bufferOnDevice) {

                // Get solver handles
                Geostack::Solver &solver = Geostack::Solver::getSolver();
                auto &queue = solver.getQueue();

                // Copy data from map to vector
                refreshData();

                // Map to device
                unmapBuffer(queue);
            }
        
            // Return reference to buffer
            buffer_ = buffer;
            return true;
        }
        return false;
    }

    /**
    * Default %Tile constructor
    * dimensions will be POD initialised to zero
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::Tile():
        dim(), 
        dataBufferMapPtr(nullptr),
        reduceBufferMapPtr(nullptr),
        statusBufferMapPtr(nullptr),
        dataBufferOnDevice(false),
        reduceBufferOnDevice(false),
        statusBufferOnDevice(false),
        dataInitialised(false),
        status(0) { }
     
    /**
    * %Tile Destructor
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::~Tile() {
    }

    /**
    * %Tile Comparison operator
    * @param l left-hand Raster to compare.
    * @param r right-hand Raster to compare.
    */
    template <typename RTYPE, typename CTYPE>
    bool operator==(const Tile<RTYPE, CTYPE> &l, const Tile<RTYPE, CTYPE> &r) { 

        // Compare objects
        if (&l == &r)
            return true;

        // Ensure buffers are on host
        l.ensureDataBufferOnHost();
        r.ensureDataBufferOnHost();
         
        // Compare values
        if (l.v != r.v)
            return false;

        return true; 
    }

    /**
    * %Tile Comparison operator.
    * @param l left-hand Raster to compare.
    * @param r right-hand Raster to compare.
    */
    template <typename RTYPE, typename CTYPE>
    bool operator!=(const Tile<RTYPE, CTYPE> &l, const Tile<RTYPE, CTYPE> &r) { 
        return !operator==(l, r);
    }
        
#ifdef USE_TILE_CACHING
    /**
    * %TileCache constructor
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::TileCache::TileCache():
        isCached(false),
        hasCacheFile(false) {

        // Generate cache name
        cacheName = std::string(std::tmpnam(nullptr))+".tile";
    }

    /**
    * %TileCache destructor
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::TileCache::~TileCache() {
        
        // Delete cache file
        if (hasCacheFile)
            std::remove(cacheName.c_str());
    }

    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::TileCache::read(Tile<RTYPE, CTYPE> &tile) {

        // Open stream
        std::ifstream cacheStream(cacheName, std::ios::binary);

        // Check file
        if (!cacheStream.is_open()) {
            std::cout << "ERROR: cannot open '" << cacheName << "' for writing" << std::endl;
            return false;
        }

        // Create compressed vector        
        std::vector<unsigned char> compressedData(tile.dataVec.size()*sizeof(RTYPE));

        // Read data length
        uint32_t dataLength = 0;
        cacheStream.read(reinterpret_cast<char *>(&dataLength), sizeof(uint32_t));

        // Read tile data
        mz_ulong compressedDataLen = (mz_ulong)dataLength;
        cacheStream.read(reinterpret_cast<char *>(compressedData.data()), compressedDataLen);
        if (cacheStream.eof()) {
            std::cout << "ERROR: unexpected end of file while reading data" << std::endl;
            return false;
        }

        // Uncompress
        mz_ulong len = (mz_ulong)compressedData.size();
        int status = uncompress(reinterpret_cast<unsigned char *>(tile.dataVec.data()), &len, 
            reinterpret_cast<const unsigned char *>(compressedData.data()), compressedDataLen);

        if (status != Z_OK) {
            std::cout << "ERROR: uncompression failed '" << mz_error(status) << "'" << std::endl;
            return false;
        } 

        // Check size
        if (len != (mz_ulong)compressedData.size()) {
            std::cout << "ERROR: unexpected data size during uncompression" << std::endl;
            return false;
        }
        
        // Set cache flag
        hasCacheFile = true;
        isCached = false;

        return true;
    }

    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::TileCache::write(Tile<RTYPE, CTYPE> &tile) {
    
        // Open stream
        std::ofstream cacheStream(cacheName, std::ios::binary | std::fstream::trunc);

        // Check file
        if (!cacheStream.is_open()) {
            std::cout << "ERROR: cannot open '" << cacheName << "' for writing" << std::endl;
            return false;
        }

        // Set data sizes
        uint32_t dataLength = (uint32_t)tile.dataVec.size()*sizeof(RTYPE);
        mz_ulong compressedDataLen = compressBound(dataLength);

        // Compress
        std::vector<unsigned char> compressedData(compressedDataLen);
        mz_ulong len = compressedDataLen;
        int status = compress2(compressedData.data(), &len, 
            reinterpret_cast<const unsigned char *>(tile.dataVec.data()), (mz_ulong)dataLength, MZ_BEST_SPEED);

        if (status != Z_OK) {
            std::cout << "ERROR: compression failed '" << mz_error(status) << "'" << std::endl;
            return false;
        }
        
        // Write data length
        uint32_t writeDataLength = (uint32_t)len;
        cacheStream.write(reinterpret_cast<const char *>(&writeDataLength), sizeof(uint32_t));

        // Write tile data
        cacheStream.write(reinterpret_cast<const char *>(compressedData.data()), len);

        // Close file
        cacheStream.close();
        
        // Set cache flag
        isCached = true;

        return true;
    }
#endif

    /**
    * Create data %Tile data.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::createData() {
    
        // Allocate data buffer
        size_t dataSize = getDataSize();
        dataVec.resize(dataSize, getNullValue<RTYPE>());
        
        // Allocate reduction buffer
        reduceVec.resize(dim.d.my*dim.d.nz, getNullValue<RTYPE>());

        // Reset status
        status = 0;

        // Create buffers
        cl_int err = CL_SUCCESS;
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        dataBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, dataSize*sizeof(RTYPE), dataVec.data(), &err);
        reduceBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, dim.d.my*dim.d.nz*sizeof(RTYPE), reduceVec.data(), &err);
        statusBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), &status, &err);
        if (err != CL_SUCCESS) {
            throw std::range_error("Cannot allocate memory for tile buffer");
        }

        // Map to host
        auto &queue = solver.getQueue();
        dataBufferOnDevice = true;
        mapDataBuffer(queue);
        reduceBufferOnDevice = true;
        mapReduceBuffer(queue);
        statusBufferOnDevice = true;
        mapStatusBuffer(queue);

        // Set initialisation flag
        dataInitialised = true;
    }

    /**
    * Read %Tile data from handler.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::readData(Raster<RTYPE, CTYPE> &r, dataHandlerReadFunction<RTYPE, CTYPE> readDataHandler) {

        // Check data handler
        if (readDataHandler) {        

            // Read from file
            readDataHandler(dim, dataVec, r);

            // Check data on return
            if (dataVec.size() != getDataSize()) {
                throw std::runtime_error("Tile data from handler is invalid");
            }
        }
    }

    /**
    * Delete %Tile data.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::deleteData(bool clearCacheFlag) {

        // Map to host
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        mapDataBuffer(queue);
        mapReduceBuffer(queue);
        mapStatusBuffer(queue);

        // Clear host data
        std::vector<RTYPE>().swap(dataVec);
        std::vector<RTYPE>().swap(reduceVec);
        status = 0;

        // Clear buffers
        dataBuffer = cl::Buffer();
        reduceBuffer = cl::Buffer();
        statusBuffer = cl::Buffer();

        // Set initialisation flag
        dataInitialised = false;
        
#ifdef USE_TILE_CACHING

        // Clear cache flag
        if (clearCacheFlag)
            tileCache.setAsUncached();

#endif
    }

    /**
    * %Tile initialisation.
    * @param nz_ the number of cells in the z dimension.
    */
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::init(
        uint32_t ti_, 
        uint32_t tj_, 
        RasterDimensions<CTYPE> rdim_) {

        // Set tile index
        dim.ti = ti_;
        dim.tj = tj_;
        
        // Set raster offset
        dim.d.ox = rdim_.d.ox+(CTYPE)(dim.ti*TileMetrics::tileSize)*rdim_.d.hx;
        dim.d.oy = rdim_.d.oy+(CTYPE)(dim.tj*TileMetrics::tileSize)*rdim_.d.hy; 
        dim.d.oz = rdim_.d.oz;
        
        // Set size
        dim.d.nx = std::min(rdim_.d.nx-TileMetrics::tileSize*dim.ti, TileMetrics::tileSize);
        dim.d.ny = std::min(rdim_.d.ny-TileMetrics::tileSize*dim.tj, TileMetrics::tileSize);
        dim.d.nz = rdim_.d.nz;

        // Set memory size
        dim.d.mx = TileMetrics::tileSize;
        dim.d.my = TileMetrics::tileSize;

        // Set spacing
        dim.d.hx = rdim_.d.hx;
        dim.d.hy = rdim_.d.hy;
        dim.d.hz = rdim_.d.hz;

        // Calculate end points
        dim.ex = dim.d.ox+(CTYPE)dim.d.nx*dim.d.hx;
        dim.ey = dim.d.oy+(CTYPE)dim.d.ny*dim.d.hy;
        dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;

        // Set buffer flags
        dataBufferOnDevice = false;
        reduceBufferOnDevice = false;

        return true;
    }

    /**
    * Get %Tile OpenCL data buffer.
    * @return Reference %Tile data buffer.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Tile<RTYPE, CTYPE>::getDataBuffer() {

        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Ensure data buffer is on device
        if (!dataBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            unmapDataBuffer(queue);
        }
        
        // Return reference to data buffer
        return dataBuffer;
    }

    /**
    * Get %Tile OpenCL reduction buffer.
    * @return Reference %Tile reduction buffer.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Tile<RTYPE, CTYPE>::getReduceBuffer() {
    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Ensure reduction buffer is on device
        if (!reduceBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            unmapReduceBuffer(queue);
        }
        
        // Return reference to reduce buffer
        return reduceBuffer;
    }

    /**
    * Get %Tile OpenCL status buffer.
    * @return Reference %Tile status buffer.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Tile<RTYPE, CTYPE>::getStatusBuffer() {
    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Ensure reduction buffer is on device
        if (!statusBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            unmapStatusBuffer(queue);
        }
        
        // Return reference to status buffer
        return statusBuffer;
    }

    /**
    * Ensure tile data is on the host
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::ensureDataBufferOnHost() {
    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Check and move data to host
        if (dataBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapDataBuffer(queue);
        }
    }

    /**
    * Ensure reduction data is on the host
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::ensureReduceBufferOnHost() {
    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Check and move data to host
        if (reduceBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapReduceBuffer(queue);
        }
    }

    /**
    * Ensure status data is on the host
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::ensureStatusBufferOnHost() {
    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Check and move data to host
        if (statusBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapStatusBuffer(queue);
        }
    }

    /**
    * Handle to %Tile cell.
    * @param i x cell index.
    * @param j y cell index.
    * @param k z cell index.
    * @return Reference to cell data.
    */
    template <typename RTYPE, typename CTYPE>
    inline RTYPE &Tile<RTYPE, CTYPE>::operator()(uint32_t i, uint32_t j, uint32_t k) { 

        // Ensure data is available
        ensureDataBufferOnHost();

        // Return reference to value
        return dataVec[i+((j+(k<<TileMetrics::tileSizePower))<<TileMetrics::tileSizePower)];
    }

    /**
    * Check tile for data.
    * @return true is %Tile contains data.
    */
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::hasData() {

        // Lock mutex
        std::lock_guard<std::mutex> lock(tileMutex);

        // Check vector
        return !dataVec.empty();
    }

    /**
    * Copy data from %Tile.
    * @param data vector to copy into.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::getData(tIterator iTo, size_t start, size_t end) {

        // Lock mutex
        std::lock_guard<std::mutex> lock(tileMutex);

        // Ensure data is available
        ensureDataBufferOnHost();

        // Copy data
        std::copy(dataVec.begin()+start, dataVec.begin()+end, iTo);
    }
    
    /**
    * Copy data to %Tile.
    * @param data vector to copy into.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setData(tIterator iFromStart, tIterator iFromEnd, size_t start) {
    
        // Lock mutex
        std::lock_guard<std::mutex> lock(tileMutex);

        // Ensure data is available
        ensureDataBufferOnHost();

        // Copy data
        std::copy(iFromStart, iFromEnd, dataVec.begin()+start);
    }

    /**
    * Set %Tile index.
    * @param ti %Tile x index.
    * @param tj %Tile y index.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setIndex(uint32_t ti_, uint32_t tj_) {

        // Update indexes
        dim.ti = ti_;
        dim.tj = tj_;
    }
    
    /**
    * Set value in all %Tile cells.
    * Sets entire raster to specified value
    * @param v Value to set.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setAllCellValues(RTYPE val) {
            
        // Lock mutex
        std::lock_guard<std::mutex> lock(tileMutex);
            
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Check and move data to host
        if (dataBufferOnDevice) {

            // Set device cell values
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            queue.enqueueFillBuffer<RTYPE>(dataBuffer, val, 0, getDataSize()*sizeof(RTYPE));

        } else {
        
            // Set host cell values
            tIterator iStart = dataVec.begin();
            tIterator iEnd = dataVec.begin()+dim.d.nx;
            for (uint32_t k = 0; k < dim.d.mx*dim.d.my*dim.d.nz; k+=dim.d.mx*dim.d.my)
                for (uint32_t j = 0; j < dim.d.mx*dim.d.ny; j+=dim.d.mx)
                    std::fill(iStart+j+k, iEnd+j+k, val); 
        }
    }

    /**
    * Maximum operator.
    * @return maximum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Tile<RTYPE, CTYPE>::max() {
    
        // Return null for non-initialised tiles
        if (!dataInitialised)
            return getNullValue<RTYPE>();

        // Ensure data is available
        ensureDataBufferOnHost();

        // Run through tile
        RTYPE max = std::numeric_limits<RTYPE>::lowest();
        for (auto val : dataVec)
            max = Geostack::max<RTYPE>(val, max);
            
        // Return null if limits are not found    
        return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
    }

    /**
    * Minimum operator.
    * @return minimum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Tile<RTYPE, CTYPE>::min() {

        // Return null for non-initialised tiles
        if (!dataInitialised)
            return getNullValue<RTYPE>();

        // Ensure data is available
        ensureDataBufferOnHost();

        // Run through tile
        RTYPE min = std::numeric_limits<RTYPE>::max();
        for (auto val : dataVec)
            min = Geostack::min<RTYPE>(val, min);
            
        // Return null if limits are not found    
        return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;    
    }

    /**
    * Reduction operator.
    * @return reduction value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Tile<RTYPE, CTYPE>::reduce(Reduction::Type type) {

        // Reduce values
        switch (type) {

            case(Reduction::Maximum): {

                // Ensure data is available
                ensureReduceBufferOnHost();
                
                // Get data
                RTYPE max = std::numeric_limits<RTYPE>::lowest();
                for (auto v : reduceVec)
                    max = Geostack::max<RTYPE>(v, max);
            
                // Return null if limits are not found    
                return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
            }
            
            case(Reduction::Minimum): {

                // Ensure data is available
                ensureReduceBufferOnHost();
                
                // Get data
                RTYPE min = std::numeric_limits<RTYPE>::max();
                for (auto v : reduceVec)
                    min = Geostack::min<RTYPE>(v, min);
            
                // Return null if limits are not found    
                return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;    
            }
            
            case(Reduction::Sum): {

                // Ensure data is available
                ensureReduceBufferOnHost();

                // Get data
                RTYPE sum = getNullValue<RTYPE>();
                for (auto v : reduceVec) {

                    // Check for no-data
                    if (v == v) {
                        sum = (sum == sum) ? sum + v : v;
                    }
                }
                return sum;    
            }
            
            case(Reduction::Count): {

                // Ensure data is available
                ensureStatusBufferOnHost();
                
                // Get data
                return status;    
            }
        }

        // Return null value
        return getNullValue<RTYPE>();
    }
    
    /**
    * Clear reduction buffer
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::resetReduction() {

        // Ensure data is available
        ensureReduceBufferOnHost();

        // Clear reduction buffer
        reduceVec.assign(dim.d.my*dim.d.nz, getNullValue<RTYPE>());
    }
    

    /**
    * Get status.
    * @return status bits.
    */  
    template <typename RTYPE, typename CTYPE>
    cl_uint Tile<RTYPE, CTYPE>::getStatus() {

        // Ensure data is available
        ensureStatusBufferOnHost();

        // Return status
        return status;
    }

    /**
    * Set status.
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setStatus(cl_uint newStatus) {

        // Ensure data is available
        ensureStatusBufferOnHost();

        // Set status
        status = newStatus;
    }

    /**
    * Get status and reset.
    * @param newStatus new value for status.
    * @return status bits.
    */  
    template <typename RTYPE, typename CTYPE>
    cl_uint Tile<RTYPE, CTYPE>::getResetStatus(cl_uint newStatus) {

        // Ensure data is available
        ensureStatusBufferOnHost();

        // Return status
        cl_uint lastStatus = status;
        status = newStatus;
        return lastStatus;
    }

#ifdef USE_TILE_CACHING

    /**
    * Check whether %Tile data is cached
    * @return true if %Tile is cached, false otherwise
    */  
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::isCached()  {

        // Lock mutex
        std::lock_guard<std::mutex> lock(tileMutex);

        // Return cache status
        return tileCache.getIsCached();
    }

    /**
    * Write %Tile data to cache
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::writeToCache() {

        // Lock mutex
        std::lock_guard<std::mutex> lock(tileMutex);

        // Check for cached data
        if (tileCache.getIsCached()) {
            std::cout << "WARNING: Tile " << dim.ti << ", " << dim.tj << " is already cached" << std::endl;
            return;
        }

        // Write data to cache
        tileCache.write(*this);

        // Clear data
        deleteData();
    }

    /**
    * Read %Tile data from cache
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::readFromCache() {
    
        // Lock mutex
        std::lock_guard<std::mutex> lock(tileMutex);

        // Check for cached data
        if (!tileCache.getIsCached()) {
            std::cout << "WARNING: Tile " << dim.ti << ", " << dim.tj << " is not cached" << std::endl;
            return;
        }

        // Write data to cache
        tileCache.read(*this);
    }

#endif

    /**
    * Get bounds of tile.
    * @return %Tile %BoundingBox
    */
    template <typename RTYPE, typename CTYPE>
    BoundingBox<CTYPE> Tile<RTYPE, CTYPE>::getBounds() const { 
        return { { dim.d.ox, dim.d.oy }, { dim.ex, dim.ey} }; 
    }
    
    /**
    * Map OpenCL data buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapDataBuffer(cl::CommandQueue &queue) const {
        if (dataBufferOnDevice)
            dataBufferMapPtr = queue.enqueueMapBuffer(dataBuffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, getDataSize()*sizeof(RTYPE));
        dataBufferOnDevice = false;
    }

    /**
    * Unmap OpenCL data buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::unmapDataBuffer(cl::CommandQueue &queue) {
        if (!dataBufferOnDevice)
            queue.enqueueUnmapMemObject(dataBuffer, dataBufferMapPtr);
        dataBufferOnDevice = true;
    }
    
    /**
    * Map OpenCL reduction buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapReduceBuffer(cl::CommandQueue &queue) const {
        if (reduceBufferOnDevice)
            reduceBufferMapPtr = queue.enqueueMapBuffer(reduceBuffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, dim.d.my*dim.d.nz*sizeof(RTYPE));
        reduceBufferOnDevice = false;
    }

    /**
    * Unmap OpenCL reduction buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::unmapReduceBuffer(cl::CommandQueue &queue) {
        if (!reduceBufferOnDevice)
            queue.enqueueUnmapMemObject(reduceBuffer, reduceBufferMapPtr);
        reduceBufferOnDevice = true;
    }
    
    /**
    * Map OpenCL status buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapStatusBuffer(cl::CommandQueue &queue) const {
        if (statusBufferOnDevice)
            statusBufferMapPtr = queue.enqueueMapBuffer(statusBuffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(cl_uint));
        statusBufferOnDevice = false;
    }

    /**
    * Unmap OpenCL status buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::unmapStatusBuffer(cl::CommandQueue &queue) {
        if (!statusBufferOnDevice)
            queue.enqueueUnmapMemObject(statusBuffer, statusBufferMapPtr);
        statusBufferOnDevice = true;
    }

    /**
    * Map %Vector distances into %Tile
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param widthPropertyName name of feature width property
    */
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::mapVector(Vector<CTYPE> &v, std::size_t clHash, ProjectionParameters<CTYPE> rproj, 
        size_t geometryTypes, std::string widthPropertyName) {

        // Check geometry types
        if (!(geometryTypes & (GeometryType::Point | GeometryType::LineString | GeometryType::Polygon))) {
            std::cout << "ERROR: No requested geometry types to map" << std::endl;
            return false;
        }

        try { 
            
            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();

            // Get projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (rproj != vproj);

            // Get bounds and expand to cover all time
            BoundingBox<CTYPE> b({ 
                { dim.d.ox, dim.d.oy, 0.0, std::numeric_limits<CTYPE>::lowest() }, 
                { dim.ex, dim.ey, 0.0, std::numeric_limits<CTYPE>::max() } } );

            // Reproject bounds
            if (usingProjection) {
                b = b.convert(vproj, rproj);
            }

            // Get geometry nearest to tile
            auto vTile = v.nearest(b, geometryTypes);

            // Map points
            if (geometryTypes & GeometryType::Point) {
            
                // Point data
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<CTYPE> width;     // Geometry width vector
                
                // Get points
                const auto &pointIndexes = vTile.getPointIndexes();
                    
                // Check size
                if (pointIndexes.size() > 0) {
                
                    // Build coordinate list
                    for (auto &p : pointIndexes) {
                        coords.push_back(vTile.getPointCoordinate(p));
                    }
                    
                    // Get geometry width
                    if (widthPropertyName.length() == 0) {

                        // Set width to zero
                        width.assign(coords.size(), 0.0);

                    } else {

                        // Get width
                        for (auto &p : pointIndexes) {
                            width.push_back((CTYPE)vTile.template getProperty<double>(p, widthPropertyName));
                        }
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<CTYPE> widthBuffer(context, queue, width);

                    // Populate points kernel arguments
                    auto map2DPointDistanceKernel = solver.getKernel(clHash, "map2DPointDistance");
                    map2DPointDistanceKernel.setArg(0, getDataBuffer());
                    map2DPointDistanceKernel.setArg(1, getDimensions());
                    map2DPointDistanceKernel.setArg(2, coordsBuffer.getBuffer());
                    map2DPointDistanceKernel.setArg(3, widthBuffer.getBuffer());
                    map2DPointDistanceKernel.setArg(4, (cl_uint)coords.size());    
                    if (usingProjection) {
                        map2DPointDistanceKernel.setArg(5, rproj);
                        map2DPointDistanceKernel.setArg(6, vproj);
                    }

                    // Execute points kernel
                    queue.enqueueNDRangeKernel(map2DPointDistanceKernel, cl::NullRange, 
                        TileMetrics::rangeTile2DGlobal, TileMetrics::rangeTile2DLocal);
                }
            }
                
            // Map line strings
            if (geometryTypes & GeometryType::LineString) {
            
                // Line string data
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<uint32_t> offset; // Offset vector
                std::vector<CTYPE> width;     // Geometry width vector
                
                // Get line strings
                const auto &lineStringIndexes = vTile.getLineStringIndexes();
                    
                // Check size
                if (lineStringIndexes.size() > 0) {
                    
                    // Build coordinate list
                    for (auto &l : lineStringIndexes) {

                        // Get line string coordinates
                        auto lc = vTile.getLineStringCoordinates(l);

                        // Append coordinates
                        coords.insert(coords.end(), lc.begin(), lc.end());

                        // Update offset list
                        offset.push_back((uint32_t)coords.size()-1);
                    }
                    
                    // Get geometry width
                    if (widthPropertyName.length() == 0) {

                        // Set width to zero
                        width.assign(offset.size(), 0.0);

                    } else {

                        // Get width
                        for (auto &l : lineStringIndexes) {
                            width.push_back((CTYPE)vTile.template getProperty<double>(l, widthPropertyName));
                        }
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<uint32_t> offsetBuffer(context, queue, offset);
                    AutoBuffer<CTYPE> widthBuffer(context, queue, width);
                
                    // Populate line string kernel arguments
                    auto map2DLineStringDistanceKernel = solver.getKernel(clHash, "map2DLineStringDistance");
                    map2DLineStringDistanceKernel.setArg(0, getDataBuffer());
                    map2DLineStringDistanceKernel.setArg(1, getDimensions());
                    map2DLineStringDistanceKernel.setArg(2, coordsBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(3, widthBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(4, offsetBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(5, (cl_uint)offset.size());  
                    if (usingProjection) {
                        map2DLineStringDistanceKernel.setArg(6, rproj);
                        map2DLineStringDistanceKernel.setArg(7, vproj);
                    }

                    // Execute line string kernel
                    queue.enqueueNDRangeKernel(map2DLineStringDistanceKernel, cl::NullRange, 
                        TileMetrics::rangeTile2DGlobal, TileMetrics::rangeTile2DLocal);
                }
            }

            // Map polygons
            if (geometryTypes & GeometryType::Polygon) {
            
                // Polygon data
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<uint32_t> offset; // Offset vector
                CoordinateList<CTYPE> bounds; // Bounding box vector

                // Get polygon indexes
                const auto &polygonIndexes = vTile.getPolygonIndexes();
                    
                // Check size
                if (polygonIndexes.size() > 0) {

                    // Build coordinate list
                    for (auto &p : polygonIndexes) {

                        // Get polygon coordinates
                        auto pc = vTile.getPolygonCoordinates(p);

                        // Get polygon sub-indexes
                        uint32_t currentOffset = (uint32_t)coords.size();
                        for (const auto &subIndex : vTile.getPolygonSubIndexes(p)) {
                            currentOffset += (uint32_t)subIndex;
                            offset.push_back(currentOffset-1);
                        }

                        // Append coordinates
                        coords.insert(coords.end(), pc.begin(), pc.end());

                        // Append bounds
                        for (const auto &subBounds : vTile.getPolygonSubBounds(p)) {
                            bounds.push_back(subBounds.min);
                            bounds.push_back(subBounds.max);
                        }
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<uint32_t> offsetBuffer(context, queue, offset);
                    AutoBuffer<Coordinate<CTYPE> > boundsBuffer(context, queue, bounds);

                    // Populate line string kernel arguments
                    auto map2DPolygonDistanceKernel = solver.getKernel(clHash, "map2DPolygonDistance");
                    map2DPolygonDistanceKernel.setArg(0, getDataBuffer());
                    map2DPolygonDistanceKernel.setArg(1, getDimensions());
                    map2DPolygonDistanceKernel.setArg(2, coordsBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(3, offsetBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(4, boundsBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(5, (cl_uint)offset.size());
                    if (usingProjection) {
                        map2DPolygonDistanceKernel.setArg(6, rproj);
                        map2DPolygonDistanceKernel.setArg(7, vproj);
                    }

                    // Execute line string kernel
                    queue.enqueueNDRangeKernel(map2DPolygonDistanceKernel, cl::NullRange, 
                        TileMetrics::rangeTile2DGlobal, TileMetrics::rangeTile2DLocal);
                }
            }

            // Flush queue
            queue.flush();

            return true;

        } catch (cl::Error e) {
            std::cout << std::endl << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
        }
        return false;
    }

    /**
    * Rasterise %Vector onto %Tile
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param script script to run for rasterisation
    */
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::rasterise(Vector<CTYPE> &v, std::size_t clHash, 
        std::vector<std::string> fields, ProjectionParameters<CTYPE> rproj, size_t geometryTypes) {

        try { 
            
            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            size_t clWorkgroupSize, clPaddedWorkgroupSize;

            // Get projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (rproj != vproj);

            // Get bounds and expand to cover all time
            BoundingBox<CTYPE> b({ 
                { dim.d.ox, dim.d.oy, 0.0, std::numeric_limits<CTYPE>::lowest() }, 
                { dim.ex, dim.ey, 0.0, std::numeric_limits<CTYPE>::max() } } );

            // Reproject bounds
            if (usingProjection) {
                b = b.convert(vproj, rproj);
            }
            
            // Get geometry within tile
            Vector<CTYPE> vTile = v.region(b, geometryTypes);

            // Create fields
            std::vector<RTYPE> field;  // Geometry field vector

            // Rasterise points
            if (geometryTypes & GeometryType::Point) {
            
                // Point data
                CoordinateList<CTYPE> coords; // Coordinate vector
                
                // Get points
                const auto &pointIndexes = vTile.getPointIndexes();
                    
                // Check size
                if (pointIndexes.size() > 0) {

                    // Build coordinate list
                    for (auto &p : pointIndexes) {
                        coords.push_back(vTile.getPointCoordinate(p));
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);

                    // Get kernel
                    auto rasterise2DPointKernel = solver.getKernel(clHash, "rasterise2DPoint"); 

                    // Populate points kernel arguments
                    cl_uint arg = 0;
                    rasterise2DPointKernel.setArg(arg++, getDataBuffer());
                    rasterise2DPointKernel.setArg(arg++, getDimensions());
                    rasterise2DPointKernel.setArg(arg++, coordsBuffer.getBuffer());
                    rasterise2DPointKernel.setArg(arg++, (cl_uint)coords.size());     
                    if (usingProjection) {
                        rasterise2DPointKernel.setArg(arg++, rproj);
                        rasterise2DPointKernel.setArg(arg++, vproj);
                    }
                
                    // Get fields
                    std::vector<AutoBuffer<RTYPE> *> autoBuffers;
                    for (auto &fieldName : fields) {
                        field.clear();

                        // Populate field array
                        for (auto &p : pointIndexes) {
                            field.push_back((RTYPE)vTile.template getProperty<double>(p, fieldName, NAN));
                        }

                        // Create buffers
                        autoBuffers.push_back(new AutoBuffer<RTYPE>(context, queue, field));

                        // Add to arguments
                        rasterise2DPointKernel.setArg(arg++, autoBuffers.back()->getBuffer());         
                    }
                    
                    // Execute point kernel
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(rasterise2DPointKernel);
                    clPaddedWorkgroupSize = (size_t)(coords.size()+((clWorkgroupSize-(coords.size()%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(rasterise2DPointKernel, cl::NullRange, 
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                    // Clear buffers
                    for (auto autoBuffer : autoBuffers)
                        delete autoBuffer;
                }
            }
                
            // Rasterise line strings
            if (geometryTypes & GeometryType::LineString) {
            
                // Line string data
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<uint32_t> offset; // Offset vector
                
                // Get line strings
                const auto &lineStringIndexes = vTile.getLineStringIndexes();
                    
                // Check size
                if (lineStringIndexes.size() > 0) {
                    
                    // Build coordinate list
                    for (auto &l : lineStringIndexes) {

                        // Get line string coordinates
                        auto lc = vTile.getLineStringCoordinates(l);

                        // Append coordinates
                        coords.insert(coords.end(), lc.begin(), lc.end());

                        // Update offset list
                        offset.push_back((uint32_t)coords.size()-1);
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<uint32_t> offsetBuffer(context, queue, offset);

                    // Get kernel
                    auto rasterise2DLineStringKernel = solver.getKernel(clHash, "rasterise2DLineString"); 
                
                    // Populate line string kernel arguments
                    cl_uint arg = 0;
                    rasterise2DLineStringKernel.setArg(arg++, getDataBuffer());
                    rasterise2DLineStringKernel.setArg(arg++, getDimensions());
                    rasterise2DLineStringKernel.setArg(arg++, coordsBuffer.getBuffer());
                    rasterise2DLineStringKernel.setArg(arg++, offsetBuffer.getBuffer());
                    rasterise2DLineStringKernel.setArg(arg++, (cl_uint)offset.size());
                    if (usingProjection) {
                        rasterise2DLineStringKernel.setArg(arg++, rproj);
                        rasterise2DLineStringKernel.setArg(arg++, vproj);
                    }
                    
                    // Get fields
                    std::vector<AutoBuffer<RTYPE> *> autoBuffers;
                    for (auto &fieldName : fields) {
                        field.clear();

                        // Populate field array
                        for (auto &l : lineStringIndexes) {
                            field.push_back((RTYPE)vTile.template getProperty<double>(l, fieldName, NAN));
                        }

                        // Create buffers
                        autoBuffers.push_back(new AutoBuffer<RTYPE>(context, queue, field));

                        // Add to arguments
                        rasterise2DLineStringKernel.setArg(arg++, autoBuffers.back()->getBuffer());         
                    }

                    // Execute line string kernel
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(rasterise2DLineStringKernel);
                    clPaddedWorkgroupSize = (size_t)(offset.size()+((clWorkgroupSize-(offset.size()%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(rasterise2DLineStringKernel, cl::NullRange, 
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                    // Clear buffers
                    for (auto autoBuffer : autoBuffers)
                        delete autoBuffer;
                }
            }
            
            // Rasterise polygons
            if (geometryTypes & GeometryType::Polygon) {
            
                // Polygon data
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<int32_t> offset;  // Offset vector
                CoordinateList<CTYPE> bounds; // Bounding box vector

                // Get polygon indexes
                const auto &polygonIndexes = vTile.getPolygonIndexes();
                    
                // Check size
                if (polygonIndexes.size() > 0) {

                    // Build geometry list
                    for (auto &p : polygonIndexes) {
                    
                        // Get polygon coordinates
                        auto pc = vTile.getPolygonCoordinates(p);

                        // Append coordinates
                        coords.insert(coords.end(), pc.begin(), pc.end());

                        // Get polygon sub-indexes
                        auto &si = vTile.getPolygonSubIndexes(p);

                        // Add sub-indexes, sub-polygons are marked as negative
                        offset.push_back((int32_t)si[0]);
                        for (cl_uint s = 1; s < si.size(); s++) {
                            offset.push_back(-(int32_t)si[s]);
                        }

                        // Append bounds
                        for (const auto &subBounds : vTile.getPolygonSubBounds(p)) {
                            bounds.push_back(subBounds.min);
                            bounds.push_back(subBounds.max);
                        }
                    }

                    // Add terminator to offset
                    offset.push_back(0);

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<int32_t> offsetBuffer(context, queue, offset);
                    AutoBuffer<Coordinate<CTYPE> > boundsBuffer(context, queue, bounds);

                    // Get kernel
                    auto rasterise2DPolygonKernel = solver.getKernel(clHash, "rasterise2DPolygon"); 

                    // Populate line string kernel arguments
                    cl_uint arg = 0;
                    rasterise2DPolygonKernel.setArg(arg++, getDataBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, getDimensions());
                    rasterise2DPolygonKernel.setArg(arg++, coordsBuffer.getBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, offsetBuffer.getBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, boundsBuffer.getBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, (cl_uint)(offset.size()-1));
                    if (usingProjection) {
                        rasterise2DPolygonKernel.setArg(arg++, rproj);
                        rasterise2DPolygonKernel.setArg(arg++, vproj);
                    }

                    // Get fields
                    std::vector<AutoBuffer<RTYPE> *> autoBuffers;
                    for (auto &fieldName : fields) {
                        field.clear();

                        // Populate field array
                        for (auto &p : polygonIndexes) {
                            field.push_back((RTYPE)vTile.template getProperty<double>(p, fieldName, NAN));
                        }

                        // Create buffers
                        autoBuffers.push_back(new AutoBuffer<RTYPE>(context, queue, field));

                        // Add to arguments
                        rasterise2DPolygonKernel.setArg(arg++, autoBuffers.back()->getBuffer());         
                    }

                    // Execute polygon kernel
                    queue.enqueueNDRangeKernel(rasterise2DPolygonKernel, cl::NullRange, 
                        TileMetrics::rangeTile2DGlobal, TileMetrics::rangeTile2DLocal);

                    // Clear buffers
                    for (auto autoBuffer : autoBuffers)
                        delete autoBuffer;
                }
            }

            // Flush queue
            queue.flush();

            return true;

        } catch (cl::Error e) {
            std::cout << std::endl << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
        }
        return false;
    }

    /**
    * %RasterFileHandler constructor.
    */
    template <typename RTYPE, typename CTYPE>
    RasterFileHandler<RTYPE, CTYPE>::RasterFileHandler() {

        // Create file stream
        fileStream = std::make_shared<std::fstream>();
    }

    /**
    * RasterFileHandler copy constructor.
    * @param rf %RasterFileHandler to copy.
    */
    template <typename RTYPE, typename CTYPE>
    RasterFileHandler<RTYPE, CTYPE>::RasterFileHandler(const RasterFileHandler<RTYPE, CTYPE> &rf): 
        readDataHandler(rf.readDataHandler), writeDataHandler(rf.writeDataHandler) {

        // Set file stream
        if (fileStream == nullptr)
            fileStream = std::make_shared<std::fstream>();
        else
            fileStream = rf.fileStream;
    }

    /**
    * %RasterFileHandler destructor.
    */
    template <typename RTYPE, typename CTYPE>
    RasterFileHandler<RTYPE, CTYPE>::~RasterFileHandler() {
    }

    /**
    * Register function to read tile data to %Raster.
    * @param dataHandler_ function for data handler.
    */
    template <typename RTYPE, typename CTYPE>
    void RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(dataHandlerReadFunction<RTYPE, CTYPE> dataHandler_) {

        // Move function handle
        readDataHandler = std::move(dataHandler_);
    }

    /**
    * Register function to write tile data from %Raster.
    * @param dataHandler_ function for data handler.
    */
    template <typename RTYPE, typename CTYPE>
    void RasterFileHandler<RTYPE, CTYPE>::registerWriteDataHandler(dataHandlerWriteFunction<RTYPE, CTYPE> dataHandler_) {

        // Move function handle
        writeDataHandler = std::move(dataHandler_);
    }

    /**
    * Default %RasterBase constructor
    * dimensions will be POD initialised to zero
    */
    template <typename CTYPE>
    RasterBase<CTYPE>::RasterBase():
        PropertyHandler(),
        dim(),
        proj() {

        // Create variable data
        vars = std::make_shared<Variables<CTYPE> >();
    }
        
    /**
    * Destructor
    */
    template <typename CTYPE>
    RasterBase<CTYPE>::~RasterBase() {
    }

    /**
    * Copy constructor
    * Creates shallow copy
    */
    template <typename CTYPE>
    RasterBase<CTYPE>::RasterBase(const RasterBase<CTYPE> &rb):
        PropertyHandler(rb),
        dim(rb.dim),
        proj(rb.proj) {

        // Copy data
        tree = rb.tree;
    }

    /**
    * Generates OpenCL kernel.
    * @param script OpenCL script to run.
    * @param kernelBlock OpenCL code block.
    * @param rasterBaseRefs list of input raster references.
    * @param rasterNullValue handling type for null values.
    * @return processed OpenCL code block, null script on error.
    */
    template <typename CTYPE>
    KernelScript RasterBase<CTYPE>::generateKernelScript(std::string script, std::string kernelBlock, 
        std::vector<RasterBaseRef<CTYPE> > rasterBaseRefs, RasterNullValue::Type rasterNullValue) {

        // Create variable strings
        KernelScript kernelScript;
        std::string argsList;
        std::string variableListHi;
        std::string variableListLow;
        std::string reduceHead;
        std::string reduceCode;
        std::string reducePost;
        std::string post;

        // Parse rasters
        std::set<std::string> rasterNames;
        std::set<std::string> rasterVariableNames;
        std::map<std::string, std::string> rasterVariableTable;
        
        // Check for neighbours
        for (int i = 0; i < rasterBaseRefs.size(); i++) {

            // Get raster handle
            RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();

            if (rasterBase.getRequiredNeighbours()) {
                variableListHi += "uint _boundary = (uint)(_i == 0) | ((uint)(_j == 0) << 1) | ((uint)(_i == _dim.nx-1) << 2) | ((uint)(_j == _dim.ny-1) << 3);\n";
                break;
            }
        }

        // Add variable for valid raster count
        if (rasterNullValue != RasterNullValue::Null) {
            variableListHi += "uint __layer_count = 0;\n";
        }

        bool needsReduction = false;
        for (int i = 0; i < rasterBaseRefs.size(); i++) {
                    
            // Get raster handle
            RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();

            if (!rasterBase.hasData()) {
                std::cout << "ERROR: kernel cannot use uninitialised raster" << std::endl;
                return KernelScript();
            }

            // Check input raster is named
            if (!rasterBase.hasProperty("name")) {
                std::cout << "ERROR: kernel cannot use unnamed raster" << std::endl;
                return KernelScript();
            }
            
            // Get name
            std::string name = rasterBase.template getProperty<std::string>("name");

            // Check name
            if (name.empty()) {
                std::cout << "ERROR: raster in kernel has empty name" << std::endl;
                return KernelScript();
            }

            // Check for duplicate names
            if (rasterNames.find(name) != rasterNames.end()) {
                std::cout << "ERROR: raster name '" << name << "' is not unique" << std::endl;
                return KernelScript();
            }
            rasterNames.insert(name);

            // Get raster information
            auto type = rasterBase.getOpenCLTypeString();
            auto rasterDim = rasterBase.getRasterDimensions();
            bool is2D = rasterDim.d.nz == 1;
            
            // Check for aligned layers these are be optimised for access
            if (equalSpatialMetrics(rasterDim.d, dim.d)) {

                // Update parameter list
                argsList += "__global " + type + " *_" + name + ",\n";
                
                // Update variable list
                if (rasterBase.vars->hasData()) {
                    argsList += "__global " + type + " *_vars_" + name + ",\n";

                    // Add variables to lookup table
                    for (auto vi : rasterBase.vars->getIndexes()) {
                        rasterVariableTable[name + "::" + vi.first] = "(*(_vars_" + name + "+" + std::to_string(vi.second) + "))";
                    }
                }

                // Update script for raster value
                if (is2D) {
                    variableListHi += type + " " + name + " = _val2D_a(_" + name + ", _i, _j);\n";
                } else {
                    variableListHi += type + " " + name + " = _val3D_a(_" + name + ", _i, _j, _k);\n";
                }
                                
                // Check whether neighbours are needed for processing
                if (rasterBase.getRequiredNeighbours() & Neighbours::Rook) {

                    // Update neighbour parameter list
                    argsList += "__global " + type + " *_" + name + "_N,\n";
                    argsList += "__global " + type + " *_" + name + "_E,\n";
                    argsList += "__global " + type + " *_" + name + "_S,\n";
                    argsList += "__global " + type + " *_" + name + "_W,\n";

                    // Update script for raster value
                    if (is2D) {
                        variableListHi += type + " " + name + "_N = _boundary & 0x08 ? _val2D_a(_" + name + "_N, _i, 0        ) : _val2D_a(_" + name + ", _i, _j+1);\n";
                        variableListHi += type + " " + name + "_E = _boundary & 0x04 ? _val2D_a(_" + name + "_E,  0, _j       ) : _val2D_a(_" + name + ", _i+1, _j);\n";
                        variableListHi += type + " " + name + "_S = _boundary & 0x02 ? _val2D_a(_" + name + "_S, _i, _dim.my-1) : _val2D_a(_" + name + ", _i, _j-1);\n";
                        variableListHi += type + " " + name + "_W = _boundary & 0x01 ? _val2D_a(_" + name + "_W, _dim.mx-1, _j) : _val2D_a(_" + name + ", _i-1, _j);\n";
                    } else {
                        variableListHi += type + " " + name + "_N = _boundary & 0x08 ? _val3D_a(_" + name + "_N, _i, 0        , _k) : _val3D_a(_" + name + ", _i, _j+1, _k);\n";
                        variableListHi += type + " " + name + "_E = _boundary & 0x04 ? _val3D_a(_" + name + "_E,  0, _j       , _k) : _val3D_a(_" + name + ", _i+1, _j, _k);\n";
                        variableListHi += type + " " + name + "_S = _boundary & 0x02 ? _val3D_a(_" + name + "_S, _i, _dim.my-1, _k) : _val3D_a(_" + name + ", _i, _j-1, _k);\n";
                        variableListHi += type + " " + name + "_W = _boundary & 0x01 ? _val3D_a(_" + name + "_W, _dim.mx-1, _j, _k) : _val3D_a(_" + name + ", _i-1, _j, _k);\n";
                    }
                }

                if (rasterBase.getRequiredNeighbours() & Neighbours::Bishop) {

                    argsList += "__global " + type + " *_" + name + "_NE,\n";
                    argsList += "__global " + type + " *_" + name + "_SE,\n";
                    argsList += "__global " + type + " *_" + name + "_SW,\n";
                    argsList += "__global " + type + " *_" + name + "_NW,\n";

                    // Update script for raster value
                    if (is2D) {

                        variableListHi += type + " " + name + "_NE;" +
                            "if (_boundary == 0x0C) { " + name + "_NE = _val2D_a(_" + name + "_NE, 0, 0); }" +
                            "else if (_boundary & 0x04) { " + name + "_NE = _val2D_a(_" + name + "_E,  0, _j+1); }" +
                            "else if (_boundary & 0x08) { " + name + "_NE = _val2D_a(_" + name + "_N , _i+1, 0); }" +
                            "else { " + name + "_NE = _val2D_a(_" + name + ", _i+1, _j+1); } \n";

                        variableListHi += type + " " + name + "_SE;" +
                            "if (_boundary == 0x06) { " + name + "_SE = _val2D_a(_" + name + "_SE, 0, _dim.my-1); }" +
                            "else if (_boundary & 0x04) { " + name + "_SE = _val2D_a(_" + name + "_E,  0, _j-1); }" +
                            "else if (_boundary & 0x02) { " + name + "_SE = _val2D_a(_" + name + "_S, _i+1, _dim.my-1); }" +
                            "else { " + name + "_SE = _val2D_a(_" + name + ", _i+1, _j-1); } \n";
                            
                        variableListHi += type + " " + name + "_SW;" +
                            "if (_boundary == 0x03) { " + name + "_SW = _val2D_a(_" + name + "_SW, _dim.mx-1, _dim.my-1); }" +
                            "else if (_boundary & 0x01) { " + name + "_SW = _val2D_a(_" + name + "_W, _dim.mx-1, _j-1); }" +
                            "else if (_boundary & 0x02) { " + name + "_SW = _val2D_a(_" + name + "_S, _i-1, _dim.my-1); }" +
                            "else { " + name + "_SW = _val2D_a(_" + name + ", _i-1, _j-1); } \n";

                        variableListHi += type + " " + name + "_NW;" +
                            "if (_boundary == 0x09) { " + name + "_NW = _val2D_a(_" + name + "_NW, _dim.mx-1, 0); }" +
                            "else if (_boundary & 0x01) { " + name + "_NW = _val2D_a(_" + name + "_W, _dim.mx-1, _j+1); }" +
                            "else if (_boundary & 0x08) { " + name + "_NW = _val2D_a(_" + name + "_N , _i-1, 0); }" +
                            "else { " + name + "_NW = _val2D_a(_" + name + ", _i-1, _j+1); } \n";

                    } else {

                        variableListHi += type + " " + name + "_NE;" +
                            "if (_boundary == 0x0C) { " + name + "_NE = _val3D_a(_" + name + "_NE, 0, 0, _k); }" +
                            "else if (_boundary & 0x04) { " + name + "_NE = _val3D_a(_" + name + "_E,  0, _j+1, _k); }" +
                            "else if (_boundary & 0x08) { " + name + "_NE = _val3D_a(_" + name + "_N , _i+1, 0, _k); }" +
                            "else { " + name + "_NE = _val3D_a(_" + name + ", _i+1, _j+1, _k); } \n";

                        variableListHi += type + " " + name + "_SE;" +
                            "if (_boundary == 0x06) { " + name + "_SE = _val3D_a(_" + name + "_SE, 0, _dim.my-1, _k); }" +
                            "else if (_boundary & 0x04) { " + name + "_SE = _val3D_a(_" + name + "_E,  0, _j-1, _k); }" +
                            "else if (_boundary & 0x02) { " + name + "_SE = _val3D_a(_" + name + "_S, _i+1, _dim.my-1, _k); }" +
                            "else { " + name + "_SE = _val3D_a(_" + name + ", _i+1, _j-1, _k); } \n";
                            
                        variableListHi += type + " " + name + "_SW;" +
                            "if (_boundary == 0x03) { " + name + "_SW = _val3D_a(_" + name + "_SW, _dim.mx-1, _dim.my-1, _k); }" +
                            "else if (_boundary & 0x01) { " + name + "_SW = _val3D_a(_" + name + "_W, _dim.mx-1, _j-1, _k); }" +
                            "else if (_boundary & 0x02) { " + name + "_SW = _val3D_a(_" + name + "_S, _i-1, _dim.my-1, _k); }" +
                            "else { " + name + "_SW = _val3D_a(_" + name + ", _i-1, _j-1, _k); } \n";

                        variableListHi += type + " " + name + "_NW;" +
                            "if (_boundary == 0x09) { " + name + "_NW = _val3D_a(_" + name + "_NW, _dim.mx-1, 0, _k); }" +
                            "else if (_boundary & 0x01) { " + name + "_NW = _val3D_a(_" + name + "_W, _dim.mx-1, _j+1, _k); }" +
                            "else if (_boundary & 0x08) { " + name + "_NW = _val3D_a(_" + name + "_N , _i-1, 0, _k); }" +
                            "else { " + name + "_NW = _val3D_a(_" + name + ", _i-1, _j+1, _k); } \n";
                    }
                }

                if (rasterBase.getRequiredNeighbours() == Neighbours::Queen) {

                    // Check for general offset
                    if (is2D) {
                        script = std::regex_replace(script, std::regex("\\b" + name + "\\b\\("), 
                            "_getValue2D(_" + name + ","
                            "_" + name + "_N, " +
                            "_" + name + "_E, " +
                            "_" + name + "_S, " +
                            "_" + name + "_W, " +
                            "_" + name + "_NE, " +
                            "_" + name + "_SE, " +
                            "_" + name + "_SW, " +
                            "_" + name + "_NW, " +
                            "_dim, _i, _j, ");
                    }
                }

                // Check whether reductions are required on raster
                auto reduction = rasterBase.getReductionType();
                if (reduction != Reduction::None) {

                    // Update reduction parameter list
                    argsList += "__global " + type + " *_" + name + "_reduce,\n";
                    std::string reduceVar = "_" + name + "_cache+lid";

                    // Set reduction function
                    if (reduction == Reduction::Maximum) {
                        post += "*(" + reduceVar + ") = " + name + ";\n";
                        reduceHead += "__local " + type + " _" + name + "_cache[" + std::to_string(TileMetrics::tileReduceSize) + "];\n";
                        reduceHead += "*(" + reduceVar + ") = noData_" + type + ";\n";
                        reduceCode += "if (isValid_" + type + "(*(" + reduceVar + "+_step))) { *(" + reduceVar + ") = fmax(*(" + reduceVar + "), *(" + reduceVar + "+_step)); }\n";
                        reducePost += "*(_" + name + "_reduce+gid) = *_" + name + "_cache;\n";
                    }
                    
                    if (reduction == Reduction::Minimum) {
                        post += "*(" + reduceVar + ") = " + name + ";\n";
                        reduceHead += "__local " + type + " _" + name + "_cache[" + std::to_string(TileMetrics::tileReduceSize) + "];\n";
                        reduceHead += "*(" + reduceVar + ") = noData_" + type + ";\n";
                        reduceCode += "if (isValid_" + type + "(*(" + reduceVar + "+_step))) { *(" + reduceVar + ") = fmin(*(" + reduceVar + "), *(" + reduceVar + "+_step)); }\n";
                        reducePost += "*(_" + name + "_reduce+gid) = *_" + name + "_cache;\n";
                    }
                    
                    if (reduction == Reduction::Sum || reduction == Reduction::Mean) {
                        post += "*(" + reduceVar + ") = isValid_" + type + "(" + name + ") ? " + name + " : 0.0;\n";
                        reduceHead += "__local " + type + " _" + name + "_cache[" + std::to_string(TileMetrics::tileReduceSize) + "];\n";
                        reduceHead += "*(" + reduceVar + ") = noData_" + type + ";\n";
                        reduceCode += "if (isValid_" + type + "(*(" + reduceVar + "+_step))) { *(" + reduceVar + ") += *(" + reduceVar + "+_step); }\n";
                        reducePost += "*(_" + name + "_reduce+gid) = *_" + name + "_cache;\n";
                    }
                    
                    if (reduction == Reduction::Count || reduction == Reduction::Mean) {    
                        post += "if (isValid_" + type + "(" + name + ")) { atomic_inc(&_" + name + "_count); } \n";                
                        reduceHead += "__local uint _" + name + "_count; if (lid == 0) { _" + name + "_count = 0; } \n";
                        reducePost += "atomic_add(_" + name + "_status, _" + name + "_count);\n";                            
                    }

                    // Set flag to enable reduction code
                    needsReduction = true;
                }

                // Check whether status bits are required on raster
                if (rasterBase.getNeedsStatus() || 
                    reduction == Reduction::Count ||
                    reduction == Reduction::Mean) {

                    // Update reduction parameter list
                    argsList += "__global uint *_" + name + "_status,\n";
                }

                if (rasterBase.getNeedsWrite()) {

                    // Update script for writing aligned layers
                    if (is2D) {
                        post += "_val2D_a(_" + name + ", _i, _j) = " + name + ";\n";
                    } else {
                        post += "_val3D_a(_" + name + ", _i, _j, _k) = " + name + ";\n";
                    }
                }

            } else {

                // Update parameter list
                argsList += "__global " + type + " *_" + name + ",\n";
                argsList += "const Dimensions _dim_" + name + ",\n";

                // Update script for raster value
                if (proj == rasterBase.getProjectionParameters()) {

                    // Use same projection
                    if (is2D) {
                        variableListHi += type + " " + name + " = getNearestValue2D" + type + 
                            "(_" + name + ", x, y, _dim_" + name + ");\n";
                    } else {
                        variableListHi += type + " " + name + " = getNearestValue3D" + type + 
                            "(_" + name + ", x, y, z, _dim_" + name + ");\n";
                    }

                } else {

                    // Add projection information
                    kernelScript.usingProjection = true;
                    argsList += "const ProjectionParameters _proj_" + name + ",\n";
                    if (is2D) {
                        variableListHi += type + " " + name + " = getProjectedValue2D" + type + 
                            "(_" + name + ", x, y, _dim_" + name + ", _proj_" + name + ", _proj);\n";
                    } else {
                        variableListHi += type + " " + name + " = getProjectedValue3D" + type + 
                            "(_" + name + ", x, y, z, _dim_" + name + ", _proj_" + name + ", _proj);\n";
                    }
                }
                
                // Update variable list
                if (rasterBase.vars->hasData()) {
                    argsList += "__global " + type + " *_vars_" + name + ",\n";

                    // Add variables to lookup table
                    for (auto vi : rasterBase.vars->getIndexes()) {
                        rasterVariableTable[name + "::" + vi.first] = "(*(_vars_" + name + "+" + std::to_string(vi.second) + "))";
                    }
                }
            }

            if (rasterNullValue != RasterNullValue::Null) {

                // Update script count for non-null values
                variableListHi += "if (isValid_" + type + "(" + name + ")) { __layer_count++; }\n";

                // Update script processing for fill values
                if (rasterNullValue == RasterNullValue::Zero)
                    variableListLow += "if (__layer_count > 0 && !isValid_" + type + "(" + name + ")) " + name + " = ("+ type +")0;\n";
                else if (rasterNullValue == RasterNullValue::One)
                    variableListLow += "if (__layer_count > 0 && !isValid_" + type + "(" + name + ")) " + name + " = ("+ type +")1;\n";
            }

        }

        // Add variable for valid raster count
        if (rasterNullValue != RasterNullValue::Null) {
            variableListLow += "const uint _layer_count = __layer_count;";
        }
        
        // Add additional parameters
        if (kernelScript.usingProjection) {
            argsList += "const ProjectionParameters _proj,\n";
        }
        argsList += "const Dimensions _dim,\n";

        // Remove two trailing characters of arguments (',\n')
        if (argsList.size() > 0) {
            argsList.pop_back();
            argsList.pop_back();
        }
            
        // Load kernel codes
        kernelScript.script = kernelBlock;

        // Check if reduction is required
        if (needsReduction) {
            
            // Enable reduction code
            kernelScript.script = std::regex_replace(kernelScript.script, std::regex("\\/\\*__REDUCE__|__REDUCE__\\*\\/"), "");
            kernelScript.script = std::regex_replace(kernelScript.script, std::regex("__REDUCE_HEAD__"), reduceHead);
            kernelScript.script = std::regex_replace(kernelScript.script, std::regex("__REDUCE_CODE__"), reduceCode);
            kernelScript.script = std::regex_replace(kernelScript.script, std::regex("__REDUCE_POST__"), reducePost);
        }

        // Patch kernel template with code
        kernelScript.script = std::regex_replace(kernelScript.script, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
        kernelScript.script = std::regex_replace(kernelScript.script, std::regex("\\/\\*__VARS__\\*\\/"), variableListHi + variableListLow);
        kernelScript.script = std::regex_replace(kernelScript.script, std::regex("\\/\\*__POST__\\*\\/"), post);

        // Patch kernel template with script
        if (!script.empty()) {
            script = Solver::processScript(script);
            kernelScript.script = std::regex_replace(kernelScript.script, std::regex("\\/\\*__CODE__\\*\\/"), script);
        }

        // Find all variables in string
        std::smatch variableMatch;
        std::set<std::string> replacementVariables;
        std::string::const_iterator searchStart(kernelScript.script.cbegin());
        while (std::regex_search(searchStart, kernelScript.script.cend(), variableMatch, std::regex("\\w+:{2}\\w+"))) {

            // Check if variable is valid
            if (rasterVariableTable.find(variableMatch[0]) == rasterVariableTable.end()) {
                std::cout << "ERROR: invalid variable name '" << variableMatch[0] << "'" << std::endl;
                return KernelScript();
            }

            // Add to replacement set
            replacementVariables.insert(variableMatch[0]);

            // Update search position
            searchStart = variableMatch.suffix().first;
        }

        // Patch kernel script with variable names
        for (auto &replacement : replacementVariables) {
            kernelScript.script = std::regex_replace(kernelScript.script, 
                std::regex("\\b" + replacement + "\\b"), rasterVariableTable[replacement]);
        }

        // Replace spatial indexes
        kernelScript.script = std::regex_replace(kernelScript.script, std::regex("\\bipos\\b"), "_i");
        kernelScript.script = std::regex_replace(kernelScript.script, std::regex("\\bjpos\\b"), "_j");
        kernelScript.script = std::regex_replace(kernelScript.script, std::regex("\\bkpos\\b"), "_k");

        //// Output script
        //std::cout << kernelScript.script << std::endl; // TEST

        // Return processed script
        return kernelScript;
    }

    /**
    * Runs a kernel on the %Raster %Tile at index ti and tj
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param kernel OpenCL kernel to run.
    * @param rasterBaseRefs list of input raster references.
    * @return true if kernel executes, false otherwise.
    */
    template <typename CTYPE>
    bool RasterBase<CTYPE>::runTileKernel(uint32_t ti, uint32_t tj, cl::Kernel kernel, 
        std::vector<RasterBaseRef<CTYPE> > rasterBaseRefs, int startArg) {
    
        try {            
            // Get tile dimensions
            TileDimensions<CTYPE> tileDim = getTileDimensions(ti, tj);

            // Get OpenCL solver handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();

            if (solver.openCLInitialised()) {

                // Create holding lists for buffers
                std::vector<cl::Buffer> kernelBuffers; 
                std::vector<Dimensions<CTYPE> > kernelDims; 
                std::vector<ProjectionParameters<CTYPE> > kernelProjs; 
                
                // Populate kernel arguments
                int arg = startArg;
                bool needsProjection = false;
                for (auto rasterBaseRef : rasterBaseRefs) {

                    // Check if raster has equal dimensions to this raster
                    cl::Buffer kernelBuffer, variableBuffer;
                    auto &rasterBase = rasterBaseRef.get();
                        
                    // Reset variables for reduction operations
                    auto reduction = rasterBase.getReductionType();
                    if (reduction != Reduction::None) {
                        rasterBase.resetTileReduction(ti, tj);
                        if (reduction == Reduction::Count || reduction == Reduction::Mean)
                            rasterBase.setTileStatus(ti, tj, 0);
                    }

                    if (equalSpatialMetrics(rasterBase.getRasterDimensions().d, dim.d)) {
                                
                        // Get tile buffer and dimensions
                        rasterBase.getTileDataBuffer(tileDim.ti, tileDim.tj, kernelBuffer);

                        // Add buffer and dimensions to kernel
                        kernelBuffers.push_back(kernelBuffer);
                        kernel.setArg(arg++, kernelBuffers.back());

                        // Update variable list
                        if (rasterBase.vars->hasData()) {
                            rasterBase.vars->getBuffer(variableBuffer);
                            kernelBuffers.push_back(variableBuffer);
                            kernel.setArg(arg++, kernelBuffers.back());
                        }

                        // Check whether neighbours are needed for processing
                        if (rasterBase.getRequiredNeighbours() & Neighbours::Rook) {

                            // Add north buffer
                            if (tileDim.tj < dim.ty-1) {
                                cl::Buffer kernelBufferN;
                                rasterBase.getTileDataBuffer(tileDim.ti, tileDim.tj+1, kernelBufferN);
                                kernelBuffers.push_back(kernelBufferN);
                            } else {
                                kernelBuffers.push_back(rasterBase.getTileNullBuffer());
                            }
                            kernel.setArg(arg++, kernelBuffers.back());

                            // Add east buffer
                            if (tileDim.ti < dim.tx-1) {
                                cl::Buffer kernelBufferE;
                                rasterBase.getTileDataBuffer(tileDim.ti+1, tileDim.tj, kernelBufferE);
                                kernelBuffers.push_back(kernelBufferE);
                            } else {
                                kernelBuffers.push_back(rasterBase.getTileNullBuffer());
                            }
                            kernel.setArg(arg++, kernelBuffers.back());

                            // Add south buffer
                            if (tileDim.tj > 0) {
                                cl::Buffer kernelBufferS;
                                rasterBase.getTileDataBuffer(tileDim.ti, tileDim.tj-1, kernelBufferS);
                                kernelBuffers.push_back(kernelBufferS);
                            } else {
                                kernelBuffers.push_back(rasterBase.getTileNullBuffer());
                            }
                            kernel.setArg(arg++, kernelBuffers.back());

                            // Add west buffer
                            if (tileDim.ti > 0) {
                                cl::Buffer kernelBufferW;
                                rasterBase.getTileDataBuffer(tileDim.ti-1, tileDim.tj, kernelBufferW);
                                kernelBuffers.push_back(kernelBufferW);
                            } else {
                                kernelBuffers.push_back(rasterBase.getTileNullBuffer());
                            }
                            kernel.setArg(arg++, kernelBuffers.back());

                        }

                        if (rasterBase.getRequiredNeighbours() & Neighbours::Bishop) {
                        
                            // Add north east buffer
                            if (tileDim.ti < dim.tx-1 && tileDim.tj < dim.ty-1) {
                                cl::Buffer kernelBufferNE;
                                rasterBase.getTileDataBuffer(tileDim.ti+1, tileDim.tj+1, kernelBufferNE);
                                kernelBuffers.push_back(kernelBufferNE);
                            } else {
                                kernelBuffers.push_back(rasterBase.getTileNullBuffer());
                            }
                            kernel.setArg(arg++, kernelBuffers.back());

                            // Add south east buffer
                            if (tileDim.ti < dim.tx-1 && tileDim.tj > 0) {
                                cl::Buffer kernelBufferSE;
                                rasterBase.getTileDataBuffer(tileDim.ti+1, tileDim.tj-1, kernelBufferSE);
                                kernelBuffers.push_back(kernelBufferSE);
                            } else {
                                kernelBuffers.push_back(rasterBase.getTileNullBuffer());
                            }
                            kernel.setArg(arg++, kernelBuffers.back());

                            // Add south west buffer
                            if (tileDim.ti > 0 && tileDim.tj > 0) {
                                cl::Buffer kernelBufferSW;
                                rasterBase.getTileDataBuffer(tileDim.ti-1, tileDim.tj-1, kernelBufferSW);
                                kernelBuffers.push_back(kernelBufferSW);
                            } else {
                                kernelBuffers.push_back(rasterBase.getTileNullBuffer());
                            }
                            kernel.setArg(arg++, kernelBuffers.back());
                            
                            // Add north west buffer
                            if (tileDim.ti > 0 && tileDim.tj < dim.ty-1) {
                                cl::Buffer kernelBufferNW;
                                rasterBase.getTileDataBuffer(tileDim.ti-1, tileDim.tj+1, kernelBufferNW);
                                kernelBuffers.push_back(kernelBufferNW);
                            } else {
                                kernelBuffers.push_back(rasterBase.getTileNullBuffer());
                            }
                            kernel.setArg(arg++, kernelBuffers.back());
                        }

                        // Check whether reductions are required on raster
                        if (reduction != Reduction::None) {
                        
                            // Add reduction buffer
                            cl::Buffer kernelReduceBuffer;
                            rasterBase.getTileReduceBuffer(tileDim.ti, tileDim.tj, kernelReduceBuffer);
                            kernelBuffers.push_back(kernelReduceBuffer);
                            kernel.setArg(arg++, kernelBuffers.back());
                        }

                        // Check whether status bits are required on raster
                        if (rasterBase.getNeedsStatus() || 
                            reduction == Reduction::Count ||
                            reduction == Reduction::Mean) {

                            // Add status buffer
                            cl::Buffer kernelStatusBuffer;
                            rasterBase.getTileStatusBuffer(tileDim.ti, tileDim.tj, kernelStatusBuffer);
                            kernelBuffers.push_back(kernelStatusBuffer);
                            kernel.setArg(arg++, kernelBuffers.back());
                        }

                    } else {

                        // Get raster projection
                        auto rasterProj = rasterBase.getProjectionParameters();
                        bool differentProjections = (proj != rasterProj);

                        // Get bounds and reproject if necessary
                        auto tileBounds = getTileBounds(ti, tj);
                        if (differentProjections) {

                            // Reproject bounds
                            tileBounds = tileBounds.convert(rasterProj, proj);
                            needsProjection = true;
                        }
                        
                        // Get raster region intersecting current tile
                        RasterDimensions<CTYPE> rasterDim;
                        if (rasterBase.createRegionBuffer(tileBounds, kernelBuffer, rasterDim)) {

                            // Add buffer and dimensions to kernel
                            kernelBuffers.push_back(kernelBuffer);
                            kernelDims.push_back(rasterDim.d);
                            if (differentProjections)
                                kernelProjs.push_back(rasterProj);

                        } else {

                            // Add null buffer and dimensions to kernel, this will be detected in the kernel
                            kernelBuffers.push_back(rasterBase.getTileNullBuffer());
                            kernelDims.push_back(Dimensions<CTYPE>());
                            if (differentProjections)
                                kernelProjs.push_back(ProjectionParameters<CTYPE>());
                        }
                            
                        // Add buffer and dimensions to kernel
                        kernel.setArg(arg++, kernelBuffers.back());
                        kernel.setArg(arg++, kernelDims.back());
                        if (differentProjections)
                            kernel.setArg(arg++, kernelProjs.back());

                        // Update variable list
                        if (rasterBase.vars->hasData()) {
                            rasterBase.vars->getBuffer(variableBuffer);
                            kernelBuffers.push_back(variableBuffer);
                            kernel.setArg(arg++, kernelBuffers.back());
                        }
                    }
                }
                if (needsProjection)
                    kernel.setArg(arg++, proj);
                kernel.setArg(arg++, tileDim.d);

                // Execute kernel
                queue.enqueueNDRangeKernel(kernel, 
                    cl::NullRange, 
                    cl::NDRange(TileMetrics::tileSize, tileDim.d.ny, tileDim.d.nz), 
                    cl::NDRange(TileMetrics::tileSize, 1, 1));

                // Flush queue
                queue.flush();

                return true;
            }
        } catch (cl::Error e) {
            std::cout << std::endl << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
        } catch (std::exception e) {
            std::cout << std::endl << "ERROR: " << e.what() << std::endl;
        }
        return false;
    }
    
    /**
    * Return variable data for %RasterBase.
    * @return copy of variable data vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    RTYPE RasterBase<CTYPE>::getVariableData(std::string name) {

        // Check data
        if (vars != nullptr) {
            return vars->get(name);
        } else {
            return getNullValue<RTYPE>();
        }
    }

    /**
    * Set variable data for %RasterBase.
    * @param dataVec_ vector data to set.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    void RasterBase<CTYPE>::setVariableData(std::string name, RTYPE value) {

        // Check data
        if (vars != nullptr) {
            vars->set(name, value);
        }
    }

    /**
    * Generates OpenCL kernel.
    * @param kernelScript processed OpenCL code block.
    * @return hash of program string
    */
    template <typename CTYPE>
    std::size_t RasterBase<CTYPE>::buildScript(KernelScript kernelScript) {
        
        // Get solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();

        // Create hash
        std::size_t clHash = solver.getNullHash();

        try {

            // Load kernel codes
            std::string kernelStr = std::string(R_cl_raster_head_c);
            if (kernelScript.usingProjection) {
               kernelStr += std::string(R_cl_projection_head_c);
            }
            kernelStr += kernelScript.script;
                        
            // Build program
            clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                std::cout << "ERROR: Cannot build program" << std::endl;

                // Output kernel code
                //std::cout << kernelStr << std::endl;
            }

        } catch (cl::Error e) {
            std::cout << std::endl << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
        } catch(std::regex_error e) {
            std::cout << std::endl << "ERROR: regex exception: " << e.what() << std::endl;
        }
        return clHash;
    }

    /**
    * Default %Raster constructor
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::Raster():
        requiredNeighbours(Neighbours::None),
        reductionType(Reduction::None),
        needsStatus(false),
        needsWrite(true) {
    }

    /**
    * Named %Raster constructor
    * dimensions will be POD initialised to zero
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::Raster(std::string name_):
        requiredNeighbours(Neighbours::None),
        reductionType(Reduction::None),
        needsStatus(false),
        needsWrite(true),
        nullTile(nullptr) {

        // Set name
        RasterBase<CTYPE>::setProperty("name", name_);

    }
        
    /**
    * Destructor
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::~Raster() {
    }

    /**
    * Copy constructor
    * Creates shallow copy
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::Raster(const Raster<RTYPE, CTYPE> &r):
        RasterBase<CTYPE>::RasterBase(r),
        requiredNeighbours(r.requiredNeighbours),
        reductionType(r.reductionType),
        needsStatus(r.needsStatus),
        needsWrite(r.needsWrite),
        nullTile(nullptr) {
        
        // Lock both mutexes during copy
        std::unique_lock<std::mutex> tlock(mtx, std::defer_lock), rlock(r.mtx, std::defer_lock);
        std::lock(tlock, rlock);

        // Copy data
        tiles = r.tiles;
        vars = r.vars;
        fileHandlerIn = r.fileHandlerIn;
        fileHandlerOut = r.fileHandlerOut;
    }

    /**
    * Assignment operator
    * Creates shallow copy
    * @param r %Raster to assign.
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> &Raster<RTYPE, CTYPE>::operator=(const Raster<RTYPE, CTYPE> &r) { 

        if (this != &r) {

            // Lock both mutexes during copy
            std::unique_lock<std::mutex> tlock(mtx, std::defer_lock), rlock(r.mtx, std::defer_lock);
            std::lock(tlock, rlock);

            // Save name
            auto name = PropertyHandler::template getProperty<std::string>("name");

            // Copy data
            properties = r.properties;
            dim = r.dim;
            tree = r.tree;
            tiles = r.tiles;
            vars = r.vars;
            requiredNeighbours = r.requiredNeighbours;
            reductionType = r.reductionType;
            needsStatus = r.needsStatus;
            needsWrite = r.needsWrite;
            fileHandlerIn = r.fileHandlerIn;
            fileHandlerOut = r.fileHandlerOut;

            // Copy projection
            proj = r.proj;

            // Set null tile
            nullTile = nullptr;

            // Preserve original name
            RasterBase<CTYPE>::setProperty("name", name);
        }

        return *this; 
    }
    
    /**
    * Get type string for RTYPE data.
    * @return RTYPE as OpenCL compatible type string.
    */
    template <typename RTYPE, typename CTYPE>
    std::string Raster<RTYPE, CTYPE>::getOpenCLTypeString() {
        return Solver::getTypeString<RTYPE>();
    }

    /**
    * %Raster comparison operator
    * @param l left-hand Raster to compare.
    * @param r right-hand Raster to compare.
    */
    template <typename RTYPE, typename CTYPE>
    bool operator==(const Raster<RTYPE, CTYPE> &l, const Raster<RTYPE, CTYPE> &r) { 

        // Compare objects
        if (&l == &r)
            return true;

        // Compare metrics
        if (!equalSpatialMetrics(l.dim.d, r.dim.d))
            return false;
        
        // Compare number of tiles
        auto &ltiles = l.tiles;
        auto &rtiles = r.tiles;
        if (ltiles.size() != rtiles.size())
            return false;
            return false;
            
        // Lock both mutexes during comparison
        std::unique_lock<std::mutex> llock(l.mtx, std::defer_lock), rlock(r.mtx, std::defer_lock);
        std::lock(llock, rlock);
        
        // Compare tiles
        for (std::size_t c = 0; c < ltiles.size(); c++) {
            
            // Compare tile values
            if (ltiles[c] != rtiles[c])
                return false;
        }

        return true; 
    }
        
    /**
    * %Raster %Tile creation.
    * Creates %Tile at index ti and tj
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    */
    template <typename RTYPE, typename CTYPE>
    TilePtr<RTYPE, CTYPE> Raster<RTYPE, CTYPE>::getTile(uint32_t ti, uint32_t tj) {
    
        // Get pointer
        TilePtr<RTYPE, CTYPE> tp = tiles[ti+tj*dim.tx];
        if (tp != nullptr) {
            
            // Get tile
            Tile<RTYPE, CTYPE> &tile = *tp;
            
#ifdef USE_TILE_CACHING


#ifdef USE_THREADED_TILE_CACHING

            // Flush any cache operations
            TileCacheManager<RTYPE, CTYPE>::instance().flush(tp);

#endif

            // Populate tile
            if (!tile.hasData()) {

                // Create data
                tile.createData();

                if (tile.isCached()) {

                    // Read from cache
                    tile.readFromCache();

                } else {
                    
                    // Populate data
                    if (fileHandlerIn != nullptr) {
                        tile.readData(*this, fileHandlerIn->getReadDataHandler());
                    }
                }

                // Increment cache counter
                TileCacheManager<RTYPE, CTYPE>::instance().increment();

            }           

            // Update cache - TODO: this can invalidate neighbours if the number of cache tiles is too small
            TileCacheManager<RTYPE, CTYPE>::instance().update(tp);

#else

            // Populate tile
            if (!tile.hasData()) {

                // Create data
                tile.createData();
                    
                // Populate data
                if (fileHandlerIn != nullptr) {
                    tile.readData(*this, fileHandlerIn->getReadDataHandler());
                }
            }           

#endif

        } else {
            throw std::range_error("Requested tile out of range in raster");
        }

        // Return tile pointer
        return tp;
    }  

    /**
    * %Raster null %Tile buffer.
    * @return Buffer from Tile containing null data
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer Raster<RTYPE, CTYPE>::getTileNullBuffer() {
    
        // Check for tile
        if (nullTile == nullptr) {

            // Initialise null tile
            nullTile = std::make_shared<Tile<RTYPE, CTYPE> >();
            nullTile->init(0, 0, dim);
            nullTile->createData();
        }
        
        // Return buffer
        return nullTile->getDataBuffer();
    }
            
    /**
    * %Raster initialisation.
    * Initialises %Raster, creating tiles and internal raster variables.
    * @param nx_ the number of cells in the x dimension.
    * @param ny_ the number of cells in the y dimension.
    * @param nz_ the number of cells in the z dimension.
    * @param hx_ the spacing in the x direction.
    * @param hy_ the spacing in the y direction.
    * @param hz_ the spacing in the z direction.
    * @param ox_ the origin in the x direction.
    * @param oy_ the origin in the y direction.
    * @param oz_ the origin in the z direction.
    * @return \b true if successfully initialised, \b false otherwise.
    */     
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init(
        uint32_t nx_, 
        uint32_t ny_, 
        uint32_t nz_, 
        CTYPE hx_, 
        CTYPE hy_, 
        CTYPE hz_, 
        CTYPE ox_, 
        CTYPE oy_,  
        CTYPE oz_) {
        
        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);

        // Clear any existing data
        tiles.clear();
        tree.clear();
              
        // Check bounds
        if (hx_ < 0.0) return false;
        if (hy_ < 0.0) return false;
        if (hz_ < 0.0) return false;
            
        if (nx_ <= 0) nx_ = 1;
        if (ny_ <= 0) ny_ = 1;
        if (nz_ <= 0) nz_ = 1;
                            
        // Set variables
        dim.d.nx = nx_; dim.d.ny = ny_; dim.d.nz = nz_;
        dim.d.hx = hx_; dim.d.hy = hy_; dim.d.hz = hz_;
        dim.d.ox = ox_; dim.d.oy = oy_; dim.d.oz = oz_;
        dim.d.mx = nx_; dim.d.my = ny_; 

        // Calculate end points
        dim.ex = dim.d.ox+(CTYPE)dim.d.nx*dim.d.hx;
        dim.ey = dim.d.oy+(CTYPE)dim.d.ny*dim.d.hy;
        dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;

        // Calculate number of tiles needed
        dim.tx = (uint32_t)ceil((CTYPE)nx_/(CTYPE)TileMetrics::tileSize);
        dim.ty = (uint32_t)ceil((CTYPE)ny_/(CTYPE)TileMetrics::tileSize);

        // Initialise tiles
        for (uint32_t tj = 0; tj < dim.ty; tj++)
            for (uint32_t ti = 0; ti < dim.tx; ti++) {
            
                // Create empty tile
                TilePtr<RTYPE, CTYPE> tp = std::make_shared<Tile<RTYPE, CTYPE> >();
                if (!tp->init(ti, tj, dim))
                    return false;

                // Add to tiles
                tiles.push_back(tp);

                // Add to tree
                tree.insert(tp);
            }

        return true;
    }

    /**
    * Two-dimensional raster initialisation.
    * Initialises raster, creating tiles and internal raster variables.
    * @param nx_ the number of cells in the x dimension.
    * @param ny_ the number of cells in the y dimension.
    * @param hx_ the spacing in the x direction.
    * @param hy_ the spacing in the y direction.
    * @param ox_ the origin in the x direction.
    * @param oy_ the origin in the y direction.
    * @return \b true if successfully initialised, \b false otherwise.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init2D(
        uint32_t nx_, 
        uint32_t ny_, 
        CTYPE hx_, 
        CTYPE hy_, 
        CTYPE ox_, 
        CTYPE oy_) {
            return init(nx_, ny_, 0, hx_, hy_, 1.0, ox_, oy_, 0.0);
    }

    /**
    * Raster initialisation from dimensions.
    * Initialises %Raster, creating tiles and internal raster variables.
    * @param dim %Raster dimensions.
    * @return \b true if successfully initialised, \b false otherwise.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init(Dimensions<CTYPE> &dim) {
        return init(dim.nx, dim.ny, dim.nz, dim.hx, dim.hy, dim.hz, dim.ox, dim.oy, dim.oz);
    }

    /**
    * Raster initialisation from bounding box.
    * Initialises raster, creating tiles and internal raster variables.
    * @param bounds %Raster bounds.
    * @param resolution %Raster resolution.
    * @return \b true if successfully initialised, \b false otherwise.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init(const BoundingBox<CTYPE> bounds, CTYPE resolution) {

        // Check resolution
        if (resolution > 0.0) {

            // Calculate dimensions with 1 cell boundary
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution)+2;
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution)+2;

            // Initialise raster
            return init2D(nx, ny, resolution, resolution, bounds.min.p-resolution, bounds.min.q-resolution);

        } else {

            // Error for invalid raster resolutions
            std::cout << "ERROR: Raster resolution must be positive" << std::endl;
        }

        return false;
    }
    
    /**
    * Two-dimensional raster resize.
    * Resizes raster to new number of tile dimensions.
    * @param tnx_ the new number of tiles in the x dimension.
    * @param tny_ the new number of tiles in the y dimension.
    * @param tox_ the tile x origin.
    * @param toy_ the tile y origin.
    * @return set of new tiles as pairs of tile indexes.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::resize2D(uint32_t tnx_, uint32_t tny_, uint32_t tox_, uint32_t toy_) {

        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);
              
        // Check bounds
        if (tnx_ == 0 || tny_ == 0) return false;
        if (tox_ > tnx_-1 || toy_ > tny_-1) return false;

        // Store current tiles
        auto oldTiles = tiles;
        uint32_t otx = dim.tx;
        uint32_t oty = dim.ty;

        // Clear any existing data
        tiles.clear();
        tree.clear();
                            
        // Set variables
        dim.d.nx = tnx_*TileMetrics::tileSize; 
        dim.d.ny = tny_*TileMetrics::tileSize;

        dim.d.mx = dim.d.nx; 
        dim.d.my = dim.d.ny; 

        dim.d.ox -= (CTYPE)(tox_*TileMetrics::tileSize)*dim.d.hx;
        dim.d.oy -= (CTYPE)(toy_*TileMetrics::tileSize)*dim.d.hy;

        // Calculate end points
        dim.ex = dim.d.ox+(CTYPE)dim.d.nx*dim.d.hx;
        dim.ey = dim.d.oy+(CTYPE)dim.d.ny*dim.d.hy;
        dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;

        // Set new number of tiles
        dim.tx = tnx_;
        dim.ty = tny_;

        // Initialise tiles
        TilePtr<RTYPE, CTYPE> tp;
        for (uint32_t tj = 0; tj < tny_; tj++) {
            for (uint32_t ti = 0; ti < tnx_; ti++) {
            
                // Check for old tiles
                if (ti >= tox_ && tj >= toy_ && ti < tox_+otx  && tj < toy_+oty) {

                    // Use existing tile
                    tp = oldTiles[(ti-tox_)+(tj-toy_)*otx];
                    tp->setIndex(ti, tj);

                } else {

                    // Create new empty tile
                    tp = std::make_shared<Tile<RTYPE, CTYPE> >();
                    if (!tp->init(ti, tj, dim))
                        return false;
                }

                // Add to tiles
                tiles.push_back(tp);

                // Add to tree
                tree.insert(tp);
            }
        }

        return true;
    }

    /**
    * Indexes for two-dimensional raster resize.
    * @param tnx_ the new number of tiles in the x dimension.
    * @param tny_ the new number of tiles in the y dimension.
    * @param tox_ the tile x origin.
    * @param toy_ the tile y origin.
    * @return set of pairs of new tile indexes.
    */  
    template <typename RTYPE, typename CTYPE>
    tileIndexSet Raster<RTYPE, CTYPE>::resize2DIndexes(uint32_t tnx_, uint32_t tny_, uint32_t tox_, uint32_t toy_) {
    
        // Create return set
        tileIndexSet newTiles;
              
        // Check bounds
        if (tnx_ == 0 || tny_ == 0) return newTiles;
        if (tox_ > tnx_-1 || toy_ > tny_-1) return newTiles;

        // Store current tiles
        uint32_t otx = dim.tx;
        uint32_t oty = dim.ty;

        // Initialise tiles
        TilePtr<RTYPE, CTYPE> tp;
        for (uint32_t tj = 0; tj < tny_; tj++) {
            for (uint32_t ti = 0; ti < tnx_; ti++) {
            
                // Check for old tiles
                if (ti < tox_ || tj < toy_ || ti >= tox_+otx  || tj >= toy_+oty) {
                    newTiles.insert( { ti, tj } );
                }
            }
        }

        return newTiles;
    }

    /**
    * Maximum operator.
    * @return maximum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::max() const {
    
        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);

        // Run through raster
        RTYPE max = std::numeric_limits<RTYPE>::lowest();
        for (auto &tp : tiles) {
            if (tp != nullptr)
                max = Geostack::max<RTYPE>(tp->max(), max);
        }    

        // Return null if limits are not found    
        return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
    }

    /**
    * Minimum operator.
    * @return minimum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::min() const {
    
        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);

        // Run through raster
        RTYPE min = std::numeric_limits<RTYPE>::max();
        for (auto &tp : tiles) {
            if (tp != nullptr)
                min = Geostack::min<RTYPE>(tp->min(), min);
        }

        // Return null if limits are not found
        return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;
    }

    /**
    * Raster reduction operator.
    * @return reduction value for %Raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::reduce() const {
    
        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);

        // Reduce values
        switch (reductionType) {

            case(Reduction::Maximum): {
                RTYPE max = std::numeric_limits<RTYPE>::lowest();
                for (auto &tp : tiles) {
                    if (tp != nullptr)
                        max = Geostack::max<RTYPE>(tp->reduce(Reduction::Maximum), max);
                }

                // Return null if limits are not found    
                return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
            }
            
            case(Reduction::Minimum): {
                RTYPE min = std::numeric_limits<RTYPE>::max();
                for (auto &tp : tiles) {
                    if (tp != nullptr)
                        min = Geostack::min<RTYPE>(tp->reduce(Reduction::Minimum), min);
                }

                // Return null if limits are not found
                return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;
            }
            
            case(Reduction::Sum): {
                RTYPE sum = getNullValue<RTYPE>();
                for (auto &tp : tiles) {
                    if (tp != nullptr) {
                        RTYPE v = tp->reduce(Reduction::Sum);
                        sum = (sum == sum) ? sum + v : v;
                    }
                }
                return sum;
            }
            
            case(Reduction::Count): {
                RTYPE count = 0.0;
                for (auto &tp : tiles) {
                    if (tp != nullptr) {
                        count += tp->reduce(Reduction::Count);
                    }
                }
                return count;
            }
            
            case(Reduction::Mean): {
                RTYPE sum = 0.0;
                RTYPE count = 0.0;
                for (auto &tp : tiles) {
                    if (tp != nullptr) {
                        sum += tp->reduce(Reduction::Sum);
                        count += tp->reduce(Reduction::Count);
                    }
                }
                return count == 0.0 ? getNullValue<RTYPE>() : sum/count;
            }
        }

        // Return null value
        return getNullValue<RTYPE>();
    }
    
    /**
    * Gets %Tile data buffer from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param dataBuffer a reference to the data buffer to return.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::getTileDataBuffer(uint32_t ti, uint32_t tj, cl::Buffer &dataBuffer) {

        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Set buffer and dimensions
        dataBuffer = t.getDataBuffer();
    }
    
    /**
    * Gets %Tile reduction buffer from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param reductionBuffer a reference to the data buffer to return.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::getTileReduceBuffer(uint32_t ti, uint32_t tj, cl::Buffer &reductionBuffer) {

        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Set buffer and dimensions
        reductionBuffer = t.getReduceBuffer();
    }

    /**
    * Resets reduction buffer of %Tile in %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::resetTileReduction(uint32_t ti, uint32_t tj) {

        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Reset reduction buffer
        t.resetReduction();
    }
    

    /**
    * Gets %Tile status buffer from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param statusBuffer a reference to the data buffer to return.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::getTileStatusBuffer(uint32_t ti, uint32_t tj, cl::Buffer &statusBuffer) {

        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Set buffer and dimensions
        statusBuffer = t.getStatusBuffer();
    }
    
    /**
    * Gets %Dimensions for a %Tile from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @return %TileDimensions of %Tile.
    */
    template <typename RTYPE, typename CTYPE>
    TileDimensions<CTYPE> Raster<RTYPE, CTYPE>::getTileDimensions(uint32_t ti, uint32_t tj) {
    
        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Set buffer and dimensions
        return t.getTileDimensions();
    }
    
    /**
    * Gets %BoundingBox for a %Tile from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @return %BoundingBox of %Tile.
    */
    template <typename RTYPE, typename CTYPE>
    BoundingBox<CTYPE> Raster<RTYPE, CTYPE>::getTileBounds(uint32_t ti, uint32_t tj) {
    
        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Set buffer and dimensions
        return t.getBounds();
    }

    /**
    * Get copy of tile data from %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param data handle to vector of type RTYPE to copy raster data into.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::getTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE> &tileData) {

        // Check bounds
        if (ti > dim.tx-1 || tj > dim.ty-1)
            return;

        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Set tile to nodata
        size_t dataSize = t.getDataSize();
        tileData.resize(dataSize);
        tileData.assign(dataSize, getNullValue<RTYPE>());

        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);

        // Copy tile data
        t.getData(tileData.begin(), 0, dataSize);
    }

    /**
    * Set tile data in %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param data handle to vector of type CTYPE to copy raster data from.
    * @return true if data is successfully set, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::setTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE>& data) {

        // Check bounds
        if (ti > dim.tx-1 || tj > dim.ty-1)
            return false;

        // Get tile
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Check size
        size_t dataSize = t.getDataSize();
        if (data.size() != dataSize)
            return false;

        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);

        // Copy tile data
        t.setData(data.begin(), data.begin()+dataSize, 0);

        return true;
    }

    /**
    * Get tile status in %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param reference to status.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::getTileStatus(uint32_t ti, uint32_t tj, cl_uint &rStatus) {
    
        // Check bounds
        if (ti > dim.tx-1 || tj > dim.ty-1)
            return false;

        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Return status
        rStatus = t.getStatus();
        return true;
    }

    /**
    * Set tile status in %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param newStatus new status value.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::setTileStatus(uint32_t ti, uint32_t tj, cl_uint newStatus) {
    
        // Check bounds
        if (ti > dim.tx-1 || tj > dim.ty-1)
            return false;

        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Return status
        t.setStatus(newStatus);
        return true;
    }

    /**
    * Get tile status in %Raster and reset to new status.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param reference to status.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::getResetTileStatus(uint32_t ti, uint32_t tj, cl_uint &rStatus, cl_uint newStatus) {
    
        // Check bounds
        if (ti > dim.tx-1 || tj > dim.ty-1)
            return false;

        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Return status
        rStatus = t.getResetStatus(newStatus);
        return true;
    }

    /**
    * Set tile values in raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param val value to set.
    * @return true if data is successfully set, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::setAllTileCellValues(uint32_t ti, uint32_t tj, RTYPE val) {

        // Check bounds
        if (ti > dim.tx-1 || tj > dim.ty-1)
            return false;

        // Get tile
        Tile<RTYPE, CTYPE> &tp = *getTile(ti, tj);

        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);
        
        // Set all values in tile
        tp.setAllCellValues(val);

        return true;
    }

    /**
    * Get value from raster cell.
    * @param i x cell index.
    * @param j y cell index.
    * @param k z cell index.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getCellValue(uint32_t i, uint32_t j, uint32_t k) {
        
        // Check bounds
        if (i > dim.d.nx-1 || j > dim.d.ny-1 || k > dim.d.nz-1)
            return getNullValue<RTYPE>();

        // Get tile, tile index is quotient (using bitshift)
        uint32_t ti = i>>TileMetrics::tileSizePower;
        uint32_t tj = j>>TileMetrics::tileSizePower;
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Return value in tile, tile cell index is remainder (using mask)
        return t(i & TileMetrics::tileSizeMask, j & TileMetrics::tileSizeMask, k);
    }

    /**
    * Get nearest-neighbour value at position in raster coordinates. f
    * @param[in] x x-position in world coordinates.
    * @param[in] y y-position in world coordinates.
    * @param[in] z z-position in world coordinates.
    * @return Nearest-neighbour cell-centred value.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getNearestValue(CTYPE x, CTYPE y, CTYPE z) {
    
        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);

        // Convert to raster coordinates
        CTYPE rx = (x-dim.d.ox)/dim.d.hx;
        CTYPE ry = (y-dim.d.oy)/dim.d.hy;
        CTYPE rz = (z-dim.d.oz)/dim.d.hz;
        
        // Check bounds
        if (rx < 0.0 || ry < 0.0 || rz < 0.0 || 
            rx >= (CTYPE)dim.d.nx || ry >= (CTYPE)dim.d.ny || rz >= (CTYPE)dim.d.nz) {
            return getNullValue<RTYPE>();
        } else {
        
            // Convert to cell coordinates
            uint32_t i = (uint32_t)rx;
            uint32_t j = (uint32_t)ry;
            uint32_t k = (uint32_t)rz;

            return getCellValue(i, j, k);
        }
    }

    /**
    * Get bilinearly interpolated value at position in raster coordinates. 
    * @param[in] x x-position in world coordinates.
    * @param[in] y y-position in world coordinates.
    * @param[in] z z-position in world coordinates.
    * @return Bilinearly interpolated cell-centred value.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getBilinearValue(CTYPE x, CTYPE y, CTYPE z) {

        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);
    
        // Convert to raster coordinates
        double rx = (double)(x-dim.d.ox)/dim.d.hx;
        double ry = (double)(y-dim.d.oy)/dim.d.hy;
        double rz = (double)(z-dim.d.oz)/dim.d.hz;
        
        // Check bounds
        if (rx < 0.0 || ry < 0.0 || rz < 0.0 || 
            rx > (double)dim.d.nx || ry > (double)dim.d.ny || rz > (double)dim.d.nz) {
            return getNullValue<RTYPE>();
        } else {        
        
            // Offset to cell centre for interpolation
            rx-=0.5;
            ry-=0.5;
            rz-=0.5;
            
            // Offset within grid cell
            double a = 0.0;
            double b = 0.0;
            double c = 0.0;
            
            uint32_t i, ip;
            if (rx < 0.0) {
                i = 0;
                ip = 0;
                a = 0.0;
            } else if (rx < (double)(dim.d.nx-1)) {
                i = (uint32_t)rx;
                ip = i+1;
                a = rx-(double)i;
            } else {
                i = dim.d.nx-1;
                ip = dim.d.nx-1;
                a = 0.0;
            }
            
            uint32_t j, jp;
            if (ry < 0.0) {
                j = 0;
                jp = 0;
                b = 0.0;
            } else if (ry < (double)(dim.d.ny-1)) {
                j = (uint32_t)ry;
                jp = j+1;
                b = ry-(double)j;
            } else {
                j = dim.d.ny-1;
                jp = dim.d.ny-1;
                b = 0.0;
            }
            
            uint32_t k, kp;
            if (rz < 0.0) {
                k = 0;
                kp = 0;
                c = 0.0;
            } else if (rz < (double)(dim.d.nz-1)) {
                k = (uint32_t)rz;
                kp = k+1;
                c = rz-(double)k;
            } else {
                k = dim.d.nz-1;
                kp = dim.d.nz-1;
                c = 0.0;
            }

            double u000 = (double)getCellValue(i , j , k );
            double u100 = (double)getCellValue(ip, j , k );
            double u010 = (double)getCellValue(i , jp, k );
            double u110 = (double)getCellValue(ip, jp, k );
            double u001 = (double)getCellValue(i , j , kp);
            double u101 = (double)getCellValue(ip, j , kp);
            double u011 = (double)getCellValue(i , jp, kp);
            double u111 = (double)getCellValue(ip, jp, kp);
            
            // Calculate interpolated value
            double v = 0.0;
            v = (double)(u111*     a *     b *     c );
            v+= (double)(u011*(1.0-a)*     b *     c );
            v+= (double)(u101*     a *(1.0-b)*     c );
            v+= (double)(u110*     a *     b *(1.0-c));
            v+= (double)(u001*(1.0-a)*(1.0-b)*     c );
            v+= (double)(u010*(1.0-a)*     b *(1.0-c));
            v+= (double)(u100*     a *(1.0-b)*(1.0-c));
            v+= (double)(u000*(1.0-a)*(1.0-b)*(1.0-c));

            return (RTYPE)v;
        }
    }

    /**
    * Set value in raster cell.
    * This is an expensive operation.
    * @param val value to set.
    * @param i x cell index.
    * @param j y cell index.
    * @param k z cell index.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setCellValue(RTYPE val, uint32_t i, uint32_t j, uint32_t k) {
    
        // Check bounds
        if (i > dim.d.nx-1 || j > dim.d.ny-1 || k > dim.d.nz-1)
            return;
        
        // Get tile, tile index is quotient (using bitshift)
        uint32_t ti = i>>TileMetrics::tileSizePower;
        uint32_t tj = j>>TileMetrics::tileSizePower;
        
        // Get tile handle
        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

        // Return value in tile, tile cell index is remainder (using mask)
        t(i & TileMetrics::tileSizeMask, j & TileMetrics::tileSizeMask, k) = val;
    }

    /**
    * Set value in all raster cells.
    * Sets entire raster to specified value
    * @param val value to set.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setAllCellValues(RTYPE val) { 

        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);

        // Set value
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {
        
                // Create new tile if no tile currently exists
                TilePtr<RTYPE, CTYPE> tp = getTile(ti, tj);

                // Set all values in tile
                tp->setAllCellValues(val);
            }
        }
    }
    
    /**
    * Deletes all %Raster data in tiles
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::deleteRasterData() {

        // Lock mutex
        std::lock_guard<std::mutex> lock(mtx);

        // Set value
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {

                // Get tile pointer
                TilePtr<RTYPE, CTYPE> tp = tiles[ti+tj*dim.tx];
                if (tp != nullptr) {
      
#ifdef USE_TILE_CACHING

#ifdef USE_THREADED_TILE_CACHING

                    // Flush any cache operations
                    TileCacheManager<RTYPE, CTYPE>::instance().flush(tp);

#endif

#endif

                    // Delete tile data
                    tp->deleteData(true);
                }
            }
        }
    }
    
    /**
    * Get %Raster region from %Raster.
    * Returns all tiles intersecting region.
    * @param bounds Bounds of region
    * @return %Raster of geometry within region.
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> Raster<RTYPE, CTYPE>::region(BoundingBox<CTYPE> bounds) {

        // Get region
        std::vector<GeometryBasePtr<CTYPE> > tileList;
        tree.search(bounds, tileList, GeometryType::Tile);
        
        // Create return raster
        Raster<RTYPE, CTYPE> r;
        r.setProjectionParameters(proj);

        // Check for null result
        if (tileList.size() == 0)
            return r;

        // Set data
        r.fileHandlerIn = fileHandlerIn;
        r.fileHandlerOut = fileHandlerOut;

        // Set cell spacing
        r.dim.d.hx = dim.d.hx; 
        r.dim.d.hy = dim.d.hy; 
        r.dim.d.hz = dim.d.hz;

        // Create bounds
        r.dim.d.ox = std::numeric_limits<CTYPE>::max();
        r.dim.d.oy = std::numeric_limits<CTYPE>::max();
        r.dim.d.oz = dim.d.oz;

        // Index ranges
        uint32_t tio = std::numeric_limits<uint32_t>::max();
        uint32_t tjo = std::numeric_limits<uint32_t>::max();
        uint32_t tie = std::numeric_limits<uint32_t>::lowest();
        uint32_t tje = std::numeric_limits<uint32_t>::lowest();
                
        // Loop through tiles
        for (auto &g : tileList) {
        
            // Get tile pointer
            auto tp = std::static_pointer_cast<Tile<RTYPE, CTYPE> >(g);
            
            // Update bounds
            auto tDim = tp->getTileDimensions();
            r.dim.d.ox = std::min(r.dim.d.ox, tDim.d.ox);
            r.dim.d.oy = std::min(r.dim.d.oy, tDim.d.oy);
            tio = std::min(tio, tDim.ti);
            tjo = std::min(tjo, tDim.tj);
            tie = std::max(tie, tDim.ti);
            tje = std::max(tje, tDim.tj);
        }

        // Set tile count
        r.dim.tx = tie-tio+1;
        r.dim.ty = tje-tjo+1;

        // Set cell counts
        r.dim.d.nx = r.dim.tx*TileMetrics::tileSize; 
        r.dim.d.ny = r.dim.ty*TileMetrics::tileSize; 
        r.dim.d.nz = dim.d.nz;

        // Set memory size
        r.dim.d.mx = r.dim.d.nx; 
        r.dim.d.my = r.dim.d.ny; 

        // Set end limit
        r.dim.ex = r.dim.d.ox+r.dim.d.hx*(CTYPE)r.dim.d.nx;
        r.dim.ey = r.dim.d.oy+r.dim.d.hy*(CTYPE)r.dim.d.ny;
        r.dim.ez = dim.ez;

        // Tile addition table
        std::map<std::pair<uint32_t, uint32_t>, TilePtr<RTYPE, CTYPE> > tileTable;

        // Add tiles
        for (auto &g : tileList) {

            // Get tile pointer
            auto tp = std::static_pointer_cast<Tile<RTYPE, CTYPE> >(g);
            
            // Tile relative position
            auto tDim = tp->getTileDimensions();
            uint32_t tpx = tDim.ti-tio;
            uint32_t tpy = tDim.tj-tjo;

            tileTable[std::make_pair(tpx, tpy)] = tp;
        }
        
        // Create or add tiles
        for (uint32_t j = 0; j < r.dim.ty; j++) {
            for (uint32_t i = 0; i < r.dim.tx; i++) {

                // Search for tile
                TilePtr<RTYPE, CTYPE> tp = nullptr;
                auto it = tileTable.find(std::make_pair(i, j));
                if (it != tileTable.end()) {

                    // Get tile pointer
                    tp = std::static_pointer_cast<Tile<RTYPE, CTYPE> >(it->second);

                    // Add to tree
                    r.tree.insert(tp);
                }

                // Add to vector
                r.tiles.push_back(tp);
            }
        }

        return r;
    }
    
    /**
    * Map %Vector distances into %Raster
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param widthPropertyName name of feature width property
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::mapVector(Vector<CTYPE> &v, size_t geometryTypes, std::string widthPropertyName) {
    
        try {                        

            // Check projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (proj != vproj);
            if (usingProjection && (proj.type == 0 || vproj.type == 0)) {
                std::cout << "ERROR: one or more projections is undefined for distance mapping" << std::endl;
                return false;
            }

            // Build kernel code
            std::string kernelStr;
            if (usingProjection) {

                // Create program string with projection
                kernelStr = std::string(R_cl_projection_head_c)+std::string(R_cl_map_distance_c);
            
                // Create argument list
                std::string argsList;
                argsList += ", const ProjectionParameters _rproj";
                argsList += ", const ProjectionParameters _vproj";
            
                // Parse script
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), "");

            } else {

                // Create program string without projection
                kernelStr = std::string(R_cl_map_distance_c);
            }

            // Build program
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            std::size_t clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                std::cout << "ERROR: Cannot build program" << std::endl;
                return false;
            }

            // Loop through tiles
            for (uint32_t tj = 0; tj < dim.ty; tj++) {
                for (uint32_t ti = 0; ti < dim.tx; ti++) {

                    // Get handle to tile
                    Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

                    // Map vector to tile
                    if (!t.mapVector(v, clHash, proj, geometryTypes, widthPropertyName))
                        return false;
                }
            }
            return true;

        } catch (cl::Error e) {
            std::cout << std::endl << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
        } catch (std::exception e) {
            std::cout << std::endl << "ERROR: " << e.what() << std::endl;
        }
        return false;
    }
    
    /**
    * Map %Vector distances into %Raster %Tile at index ti and tj
    * Raster and Vector projections must be the same
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param widthPropertyName name of feature width property
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::mapTileVector(uint32_t ti, uint32_t tj,
        Vector<CTYPE> &v, size_t geometryTypes, std::string widthPropertyName) {

            // Check projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (proj != vproj);
            if (usingProjection && (proj.type == 0 || vproj.type == 0)) {
                std::cout << "ERROR: one or more projections is undefined for distance mapping" << std::endl;
                return false;
            }

            // Build kernel code
            std::string kernelStr;
            if (usingProjection) {

                // Create program string with projection
                kernelStr = std::string(R_cl_projection_head_c)+std::string(R_cl_map_distance_c);
                
                // Create argument list
                std::string argsList;
                argsList += ", const ProjectionParameters _rproj";
                argsList += ", const ProjectionParameters _vproj";
            
                // Parse script
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), "");

            } else {

                // Create program string without projection
                kernelStr = std::string(R_cl_map_distance_c);
            }

            // Build program
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            std::size_t clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                std::cout << "ERROR: Cannot build program" << std::endl;
                return false;
            }

        try { 
        
            // Get handle to tile
            Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

            // Map vector to tile
            return t.mapVector(v, clHash, proj, geometryTypes, widthPropertyName);

        } catch (cl::Error e) {
            std::cout << std::endl << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
        } catch (std::exception e) {
            std::cout << std::endl << "ERROR: " << e.what() << std::endl;
        } 
        return false;
    }
    
    /**
    * Rasterise %Vector onto %Tile
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param script script to run for rasterisation
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::rasterise(Vector<CTYPE> &v, std::string script, size_t geometryTypes) {
    
        try {

            // Check geometry types
            if (!(geometryTypes & (GeometryType::Point | GeometryType::LineString | GeometryType::Polygon))) {
                std::cout << "ERROR: No requested geometry types to map" << std::endl;
                return false;
            }

            // Check for underscores in script, these are reserved for internal variables
            if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                std::cout << "ERROR: kernel code cannot contain leading underscores '_' in script" << std::endl;
                return false;
            }

            // Check for empty script
            if (script.length() == 0) {

                // Default script
                script = "output = 1.0;";
            }
            
            // Check script
            if (!std::regex_search(script, std::regex("\\boutput\\b|\\batomic_\\w*\\("))) {
                std::cout << "ERROR: rasterise kernel code must contain 'output' variable or atomic function" << std::endl;
                return false;
            }

            // Check projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (proj != vproj);
            if (usingProjection && (proj.type == 0 || vproj.type == 0)) {
                std::cout << "ERROR: one or more projections is undefined for rasterisation" << std::endl;
                return false;
            }

            // Build kernel code
            std::string kernelStr;
            if (usingProjection) {
                kernelStr = std::string(R_cl_projection_head_c)+std::string(R_cl_rasterise_c);
            } else {
                kernelStr = std::string(R_cl_rasterise_c);
            }

            // Replace type
            kernelStr = std::regex_replace(kernelStr, std::regex("TYPE"), Solver::getTypeString<RTYPE>());
            
            // Create argument list
            std::string argsList;
            if (usingProjection) {
                argsList += ", const ProjectionParameters _rproj";
                argsList += ", const ProjectionParameters _vproj";
            }

            // Search for fields
            std::vector<std::string> fields;
            for (auto &p : v.getProperties()) {
                if (std::regex_search(script, std::regex("\\b" + p.first + "\\b"))) {

                    // Replace field
                    std::string fieldName = std::string("_field_") + std::to_string(fields.size());
                    script = std::regex_replace(script, std::regex("\\b" + p.first + "\\b"), "*(" + fieldName + "+index)");

                    // Add to field list
                    fields.push_back(p.first);

                    // Add to argument list
                    argsList += ",\n__global REAL *" + fieldName;
                }
            }

            // Search for output pointer
            if (std::regex_search(script, std::regex("\\batomic_\\w*\\("))) {

                // Replace atomic functions
                script = std::regex_replace(script, std::regex("\\batomic_(inc|dec)\\(\\)"), "atomic_$1(_pval2D_a(_u, _i, _j))");
                script = std::regex_replace(script, std::regex("\\batomic_(add|sub|xchg|cmpxchg|min|max|and|or)\\((.*)\\)"), "atomic_$1(_pval2D_a(_u, _i, _j), $2)");

            } else {

                // Enable write
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__WRITE__|__WRITE__\\*\\/"), "");
            }

            // Parse script
            script = Solver::processScript(script);
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__CODE__\\*\\/"), script);
            if (usingProjection) {
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), "");
            }
                        
            // Build program
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            std::size_t clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                std::cout << "ERROR: Cannot build program" << std::endl;
                return false;
            }

            // Loop through tiles
            for (uint32_t tj = 0; tj < dim.ty; tj++) {
                for (uint32_t ti = 0; ti < dim.tx; ti++) {

                    // Get handle to tile
                    Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);

                    // Map vector to tile
                    if (!t.rasterise(v, clHash, fields, proj, geometryTypes))
                        return false;
                }
            }
            return true;

        } catch (cl::Error e) {
            std::cout << std::endl << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
        } catch (std::exception e) {
            std::cout << std::endl << "ERROR: " << e.what() << std::endl;
        }
        return false;
    }    

    /**
    * Vectorise %Raster data, returning a closed contour.
    * @return contour %Vector of %Raster.
    */
    template <typename RTYPE, typename CTYPE>
    Vector<CTYPE> Raster<RTYPE, CTYPE>::vectorise(std::vector<RTYPE> countourValues, RTYPE noDataValue) {
        
        // Create vector
        Vector<CTYPE> v;

        // Check contour values
        if (countourValues.size() == 0) {
            std::cout << "WARNING: No contour values to vectorise" << std::endl;
            return v;
        }

        try {
            
            // Get OpenCL solver handle               
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            if (solver.openCLInitialised()) {

                // Get OpenCL handles
                cl_int err = CL_SUCCESS;
                auto &context = solver.getContext();
                auto &queue = solver.getQueue();

                // Build kernel code
                std::string kernelStr = std::string(R_cl_vectorise_c);

                // Build program
                std::size_t clHash = solver.buildProgram(kernelStr);
                if (clHash == solver.getNullHash()) {
                    std::cout << "ERROR: Cannot build program" << std::endl;
                    return v;
                }

                // Get kernel
                auto vectoriseKernel = solver.getKernel(clHash, "vectorise");

                // Create index and vertex buffers
                cl::Buffer bi = cl::Buffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_uint), NULL, &err);
                cl::Buffer bc = cl::Buffer(context, CL_MEM_WRITE_ONLY, 2*TileMetrics::tileSizeSquared*sizeof(Coordinate<RTYPE>), NULL, &err);
                if (err != CL_SUCCESS) {
                    std::cout << std::endl << "ERROR: Cannot create buffers" << std::endl;
                    return v;
                }

                // Create buffer of contour values
                cl::Buffer bcv = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                    countourValues.size()*sizeof(RTYPE), countourValues.data(), &err);
                
                // Loop through tiles
                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {
                    
                        // Get tile handle
                        Tile<RTYPE, CTYPE> &t = *getTile(ti, tj);
                        
                        // Clear index buffer
                        queue.enqueueFillBuffer<cl_uint>(bi, 0, 0 , sizeof(cl_uint));

                        // Vectorise tile
                        vectoriseKernel.setArg( 0, t.getDataBuffer());

                        // Add north buffer
                        if (tj < dim.ty-1) {
                            Tile<RTYPE, CTYPE> &tN = *getTile(ti, tj+1);
                            vectoriseKernel.setArg( 1, tN.getDataBuffer());
                        } else {
                            vectoriseKernel.setArg( 1, getTileNullBuffer());
                        }

                        // Add east buffer
                        if (ti < dim.tx-1) {
                            Tile<RTYPE, CTYPE> &tE = *getTile(ti+1, tj);
                            vectoriseKernel.setArg( 2, tE.getDataBuffer());
                        } else {
                            vectoriseKernel.setArg( 2, getTileNullBuffer());
                        }

                        // Add north east buffer
                        if (ti < dim.tx-1 && tj < dim.ty-1) {
                            Tile<RTYPE, CTYPE> &tNE = *getTile(ti+1, tj+1);
                            vectoriseKernel.setArg( 3, tNE.getDataBuffer());
                        } else {
                            vectoriseKernel.setArg( 3, getTileNullBuffer());
                        }

                        vectoriseKernel.setArg( 4, t.getDimensions());
                        vectoriseKernel.setArg( 5, noDataValue);
                        vectoriseKernel.setArg( 6, bcv);
                        vectoriseKernel.setArg( 7, (cl_uint)countourValues.size());
                        vectoriseKernel.setArg( 8, bc);
                        vectoriseKernel.setArg( 9, bi);
                        queue.enqueueNDRangeKernel(vectoriseKernel, cl::NullRange, 
                            TileMetrics::rangeTile2DGlobal, TileMetrics::rangeTile2DLocal);

                        // Flush queue
                        queue.flush();

                        // Read number of coordinates
                        cl_uint nc = 0;
                        queue.enqueueReadBuffer(bi, CL_TRUE, 0, sizeof(cl_uint), &nc);
            
                        if (nc > 0) {

                            // Read coordinates
                            CoordinateList<CTYPE> c;
                            c.resize(nc);
                            queue.enqueueReadBuffer(bc, CL_TRUE, 0, nc*sizeof(Coordinate<CTYPE>), c.data());
            
                            // Add line segments to Vector
                            for (cl_uint i = 0; i < nc; i+=2) {
                                v.addLineString( { c[i], c[i+1] } );
                            }
                        }
                    }
                }

            } else {
                std::cout << "ERROR: OpenCL not initialised" << std::endl;
            }
        } catch (cl::Error e) {
            std::cout << std::endl << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
        }

        // De-duplicate
        v.deduplicateVertices();

        // Set projection
        v.setProjectionParameters(proj);
        
        return v;
    }

    /**
    * Run script on %Raster
    * @param script OpenCL script to run.
    * @param rasterBaseRefs list of input raster references.
    * @param parameters parameter flags for combination and aliasing.
    * @return true if script is successful, false otherwise
    */
    template <typename CTYPE>
    bool runScriptNoOut(std::string script, std::vector<RasterBaseRef<CTYPE> > rasterBaseRefs, size_t parameters) {

        // Check for underscores in script, these are reserved for internal variables
        if (std::regex_search(script, std::regex("\\b_\\w*"))) {
            std::cout << "ERROR: kernel code cannot contain leading underscores '_' in script" << std::endl;
            return false;
        }

        // Check for rasters
        if (rasterBaseRefs.size() == 0) {
            std::cout << "ERROR: no rasters for script" << std::endl;
            return false;
        }

        // Check if script has an output variable
        if (std::regex_search(script, std::regex("\\boutput\\b"))) {            
            std::cout << "ERROR: script cannot contain output definition" << std::endl;
            return false;
        }
            
        // Get OpenCL handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {
                    
            // Parse rasters
            std::string scriptAppend;
            bool hasProjection = false;
            std::map<std::size_t, Neighbours::Type> rasterNeighbours;
            for (int i = 0; i < rasterBaseRefs.size(); i++) {     
                    
                // Get raster handle
                RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();
                auto proj = rasterBase.getProjectionParameters();

                // Check raster
                if (!rasterBase.hasData()) {
                    std::cout << "ERROR: raster index " << i << " in script has no data" << std::endl;
                    return false;
                }

                // Check projection
                if (i == 0 && proj.type != 0)
                    hasProjection = true;
                if (proj.type == 0 && hasProjection) {
                    std::cout << "ERROR: one or more raster projections is undefined" << std::endl;
                    return false;
                }

                // Check projection type
                if (hasProjection && !Projection<CTYPE>::checkProjection(proj)) {
                    std::cout << "ERROR: invalid projection type" << std::endl;
                    return false;
                }

                // Check reduction
                if (rasterBase.getReductionType() != Reduction::None) {
                    
                    // Add dummy code to the script to alter the kernel hash
                    scriptAppend += 
                        "\n// Raster: " + std::to_string(i) + 
                        " reduction: " + std::to_string(rasterBase.getReductionType());
                }

                // Get name
                std::string name;
                if (!rasterBase.hasProperty("name")) {
                    std::cout << "ERROR: kernel cannot use unnamed raster" << std::endl;
                    return false;
                }
                name = rasterBase.template getProperty<std::string>("name");
                if (name.empty()) {
                    std::cout << "ERROR: raster in kernel has empty name" << std::endl;
                    return false;
                }

                // Find references to neighbours
                if (std::regex_search(script, std::regex("\\b" + name + "(_NE|_SE|_SW|_NW)\\b"))) {
                    rasterNeighbours[i] = rasterBase.getRequiredNeighbours();
                    rasterBase.setRequiredNeighbours(Neighbours::Queen);
                } else if (std::regex_search(script, std::regex("\\b" + name + "(_N|_E|_S|_W)\\b"))) {
                    rasterNeighbours[i] = rasterBase.getRequiredNeighbours();
                    rasterBase.setRequiredNeighbours(Neighbours::Rook);
                }
            }
            
            // Get first raster
            RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
            auto dimFirst = rasterBaseFirst.getRasterDimensions();

            // Search cache for pre-compiled program
            std::size_t clRasterHash = solver.getNullHash();
            auto hit = rasterBaseFirst.getHashCache().find(script+scriptAppend);
            if (hit == rasterBaseFirst.getHashCache().end()) {

                // Generate kernel
                RasterNullValue::Type rasterNullValue = static_cast<RasterNullValue::Type>(parameters & 0xC0);
                auto kernelScript = rasterBaseFirst.generateKernelScript(
                    script, std::string(R_cl_raster_block_c), rasterBaseRefs, rasterNullValue);
                    
                // Check script
                if (kernelScript.script.size() == 0) {
                    std::cout << "ERROR: cannot generate script" << std::endl;
                    return false;
                }

                // Build script
                clRasterHash = rasterBaseFirst.buildScript(kernelScript);

                // Add to cache
                rasterBaseFirst.getHashCache()[script+scriptAppend] = clRasterHash;

            } else {

                // Get hash
                clRasterHash = hit->second;
            }

            // Check hash
            if (clRasterHash == solver.getNullHash()) {
                std::cout << "ERROR: Cannot get kernel for script" << std::endl;
                return false;
            }

            // Get kernel
            cl::Kernel clRasterKernel = solver.getKernel(clRasterHash, "raster"); 

            // Loop through tiles
            for (uint32_t tj = 0; tj < dimFirst.ty; tj++) {
                for (uint32_t ti = 0; ti < dimFirst.tx; ti++) {
                    
                    // Run kernel
                    rasterBaseFirst.runTileKernel(ti, tj, clRasterKernel, rasterBaseRefs);
                }
            }

            // Preserve neighbours
            for (auto r : rasterNeighbours) {
                RasterBase<CTYPE> &rasterBase = rasterBaseRefs[r.first].get();
                rasterBase.setRequiredNeighbours(r.second);
            }

        } else {
            std::cout << "ERROR: OpenCL not initialised" << std::endl;
            return false;
        }

        return true;
    }

    /**
    * Run script on %Raster
    * @param script OpenCL script to run.
    * @param rasterBaseRefs list of input raster references.
    * @param parameters parameter flags for combination and aliasing.
    * @return output %Raster
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runScript(std::string script, std::vector<RasterBaseRef<CTYPE> > rasterBaseRefs, size_t parameters) {

        // Check for underscores in script, these are reserved for internal variables
        if (std::regex_search(script, std::regex("\\b_\\w*"))) {
            std::cout << "ERROR: kernel code cannot contain leading underscores '_' in script" << std::endl;
            return Raster<RTYPE, CTYPE>();
        }

        // Get OpenCL handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            // Run simplified script if there is no output specified
            if (!std::regex_search(script, std::regex("\\boutput\\b"))) {
                runScriptNoOut<CTYPE>(script, rasterBaseRefs, parameters);
                return Raster<RTYPE, CTYPE>();
            }

            // Get parameters
            RasterCombination::Type rasterCombination = static_cast<RasterCombination::Type>(parameters & 0x03);
            RasterResolution::Type rasterResolution = static_cast<RasterResolution::Type>(parameters & 0x0C);
            RasterInterpolation::Type rasterInterpolation = static_cast<RasterInterpolation::Type>(parameters & 0x30);
            RasterNullValue::Type rasterNullValue = static_cast<RasterNullValue::Type>(parameters & 0xC0);
            Reduction::Type rasterOutputReduction = static_cast<Reduction::Type>(parameters & 0xF00);

            // Initialise bounds of output
            RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
            auto dimFirst = rasterBaseFirst.getRasterDimensions();
            auto projFirst = rasterBaseFirst.getProjectionParameters();
            uint32_t nx = dimFirst.d.nx;
            uint32_t ny = dimFirst.d.ny;
            uint32_t nz = dimFirst.d.nz;
            CTYPE hx = dimFirst.d.hx;
            CTYPE hy = dimFirst.d.hy;
            CTYPE hz = dimFirst.d.hz;
            CTYPE xMin = dimFirst.d.ox;
            CTYPE yMin = dimFirst.d.oy;
            CTYPE zMin = dimFirst.d.oz;
            CTYPE xMax = dimFirst.ex;
            CTYPE yMax = dimFirst.ey;
            CTYPE zMax = dimFirst.ez;

            // Get name
            std::string name;
            if (!rasterBaseFirst.hasProperty("name")) {
                std::cout << "ERROR: kernel cannot use unnamed raster" << std::endl;
                return Raster<RTYPE, CTYPE>();
            }
            name = rasterBaseFirst.template getProperty<std::string>("name");
            if (name.empty()) {
                std::cout << "ERROR: raster in kernel has empty name" << std::endl;
                return Raster<RTYPE, CTYPE>();
            }

            // Find references to neighbours
            std::map<std::size_t, Neighbours::Type> rasterNeighbours;
            if (std::regex_search(script, std::regex("\\b" + name + "(_NE|_SE|_SW|_NW)\\b"))) {
                rasterNeighbours[0] = rasterBaseFirst.getRequiredNeighbours();
                rasterBaseFirst.setRequiredNeighbours(Neighbours::Queen);
            } else if (std::regex_search(script, std::regex("\\b" + name + "(_N|_E|_S|_W)\\b"))) {
                rasterNeighbours[0] = rasterBaseFirst.getRequiredNeighbours();
                rasterBaseFirst.setRequiredNeighbours(Neighbours::Rook);
            }

            // Check rasters all have a projection, or none have a projection
            bool hasProjection = projFirst.type != 0;

            // Check projection type
            if (hasProjection && !Projection<CTYPE>::checkProjection(projFirst)) {
                std::cout << "ERROR: invalid projection type" << std::endl;
                return Raster<RTYPE, CTYPE>();
            }
                    
            // Parse rasters
            for (int i = 1; i < rasterBaseRefs.size(); i++) {     
                    
                // Get raster handle
                RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();
                auto dim = rasterBase.getRasterDimensions();
                auto proj = rasterBase.getProjectionParameters();

                // Check raster
                if (!rasterBase.hasData()) {
                    std::cout << "ERROR: raster index " << i << " in script has no data" << std::endl;
                    return Raster<RTYPE, CTYPE>();
                }

                // Check projection
                if (proj.type == 0 && hasProjection) {
                    std::cout << "ERROR: one or more raster projections is undefined" << std::endl;
                    return Raster<RTYPE, CTYPE>();
                }

                // Check projection type
                if (hasProjection && !Projection<CTYPE>::checkProjection(proj)) {
                    std::cout << "ERROR: invalid projection type" << std::endl;
                    return Raster<RTYPE, CTYPE>();
                }

                // Get name
                std::string name;
                if (!rasterBase.hasProperty("name")) {
                    std::cout << "ERROR: kernel cannot use unnamed raster" << std::endl;
                    return Raster<RTYPE, CTYPE>();
                }
                name = rasterBase.template getProperty<std::string>("name");
                if (name.empty()) {
                    std::cout << "ERROR: raster in kernel has empty name" << std::endl;
                    return Raster<RTYPE, CTYPE>();
                }

                // Find references to neighbours
                if (std::regex_search(script, std::regex("\\b" + name + "(_NE|_SE|_SW|_NW)\\b"))) {
                    rasterNeighbours[i] = rasterBase.getRequiredNeighbours();
                    rasterBase.setRequiredNeighbours(Neighbours::Queen);
                } else if (std::regex_search(script, std::regex("\\b" + name + "(_N|_E|_S|_W)\\b"))) {
                    rasterNeighbours[i] = rasterBase.getRequiredNeighbours();
                    rasterBase.setRequiredNeighbours(Neighbours::Rook);
                }

                // Get bounds
                auto bounds = rasterBase.getBounds();
                auto rhx = dim.d.hx;
                auto rhy = dim.d.hy;
                auto rhz = dim.d.hz;
                if (projFirst != proj) {
                
                    // Convert distance
                    auto centroid = bounds.centroid();
                    auto offsetCentroid = centroid;
                    offsetCentroid.p += rhx;
                    offsetCentroid.q += rhy;
                    Projection<CTYPE>::convert(centroid, projFirst, proj);
                    Projection<CTYPE>::convert(offsetCentroid, projFirst, proj);
                    auto offset = centroid-offsetCentroid;
                    CTYPE rh = 0.5*(fabs(offset.p)+fabs(offset.q));
                    rhx = rh;
                    rhy = rh;

                    // Re-project and process bounds
                    bounds = bounds.convert(projFirst, proj);
                }

                // Process resolution
                hx = (rasterResolution == RasterResolution::Maximum) ? std::max(hx, rhx) : std::min(hx, rhx);
                hy = (rasterResolution == RasterResolution::Maximum) ? std::max(hy, rhy) : std::min(hy, rhy);
                hz = (rasterResolution == RasterResolution::Maximum) ? std::max(hz, rhz) : std::min(hz, rhz);

                // Process bounds
                xMin = (rasterCombination == RasterCombination::Union) ? std::min(xMin, bounds.min.p) : std::max(xMin, bounds.min.p);
                yMin = (rasterCombination == RasterCombination::Union) ? std::min(yMin, bounds.min.q) : std::max(yMin, bounds.min.q);
                zMin = (rasterCombination == RasterCombination::Union) ? std::min(zMin, bounds.min.r) : std::max(zMin, bounds.min.r);
                xMax = (rasterCombination == RasterCombination::Union) ? std::max(xMax, bounds.max.p) : std::min(xMax, bounds.max.p);
                yMax = (rasterCombination == RasterCombination::Union) ? std::max(yMax, bounds.max.q) : std::min(yMax, bounds.max.q);
                zMax = (rasterCombination == RasterCombination::Union) ? std::max(zMax, bounds.max.r) : std::min(zMax, bounds.max.r);
            }

            // Check difference from base dimensions
            if (xMin != dimFirst.d.ox || yMin != dimFirst.d.oy || zMin != dimFirst.d.oz || 
                xMax != dimFirst.ex || yMax != dimFirst.ey || zMax != dimFirst.ez || 
                hx != dimFirst.d.hx || hy != dimFirst.d.hy || hz != dimFirst.d.hz) {

                // Calculate extent
                CTYPE lx = xMax-xMin;
                CTYPE ly = yMax-yMin;
                CTYPE lz = zMax-zMin;
                if (lx <= 0.0 || ly <= 0.0 || lz <= 0.0) {
                    std::cout << "ERROR: output raster size from script is zero" << std::endl;
                    return Raster<RTYPE, CTYPE>();
                }

                // Calculate number of cells
                nx = (uint32_t)std::ceil(lx/hx);
                ny = (uint32_t)std::ceil(ly/hy);
                nz = (uint32_t)std::ceil(lz/hz);
            }

            // Create output
            Raster<RTYPE, CTYPE> rasterOut;
            rasterOut.init(nx, ny, nz, hx, hy, hz, xMin, yMin, zMin);
            rasterOut.setProperty("name", "output");
            rasterOut.setProjectionParameters(projFirst);
            rasterOut.setReductionType(rasterOutputReduction);
            rasterBaseRefs.push_back(rasterOut);

            // Generate kernel
            auto kernelScript = rasterOut.generateKernelScript(
                script, std::string(R_cl_raster_block_c), rasterBaseRefs, rasterNullValue);

            // Check script
            if (kernelScript.script.size() == 0) {
                std::cout << "ERROR: cannot generate script" << std::endl;
                return Raster<RTYPE, CTYPE>();
            }

            std::size_t clRasterHash = rasterOut.buildScript(kernelScript);
            if (clRasterHash == solver.getNullHash()) {
                std::cout << "ERROR: Cannot get kernel for script" << std::endl;
                return Raster<RTYPE, CTYPE>();
            }

            // Get kernel
            cl::Kernel clRasterKernel = solver.getKernel(clRasterHash, "raster");        

            // Loop through tiles
            auto dimOut = rasterOut.getRasterDimensions();
            for (uint32_t tj = 0; tj < dimOut.ty; tj++) {
                for (uint32_t ti = 0; ti < dimOut.tx; ti++) {

                    // Run kernel
                    rasterOut.runTileKernel(ti, tj, clRasterKernel, rasterBaseRefs);
                }
            }

            // Preserve neighbours
            for (auto r : rasterNeighbours) {
                RasterBase<CTYPE> &rasterBase = rasterBaseRefs[r.first].get();
                rasterBase.setRequiredNeighbours(r.second);
            }

            // Return raster
            return rasterOut;

        } else {
            std::cout << "ERROR: OpenCL not initialised" << std::endl;
        }

        // Return empty raster
        return Raster<RTYPE, CTYPE>();
    }
    
    /**
    * Runs a script on the raster, returning a resulting raster.
    * @param script OpenCL script to run.
    * @param rasterBaseRef input raster.
    * @param parameters parameters for sampling and output raster.
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runAreaScript(std::string script, RasterBase<CTYPE> &rasterBase, cl_int width) {

        // Check for underscores in script, these are reserved for internal variables
        if (std::regex_search(script, std::regex("\\b_\\w*"))) {
            std::cout << "ERROR: kernel code cannot contain leading underscores '_' in script" << std::endl;
            return Raster<RTYPE, CTYPE>();
        }

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            // Get raster properties
            // RasterBase<CTYPE> &rasterBase = rasterBaseRef.get();
            std::string name = rasterBase.template getProperty<std::string>("name");
            auto dim = rasterBase.getRasterDimensions();
            auto proj = rasterBase.getProjectionParameters();

            // Create output
            Raster<RTYPE, CTYPE> rasterOut;
            rasterOut.init(dim.d);
            rasterOut.setProperty("name", "output");
            rasterOut.setProjectionParameters(proj);            
            
            // Replace variable name in script
            std::string kernelBlock = std::string(R_cl_raster_area_op2D_c);
            kernelBlock = std::regex_replace(kernelBlock, std::regex("__VARIABLE__"), name);
            
            // Store and set raster properties
            bool rasterBaseNeedsWrite = rasterBase.getNeedsWrite();
            auto rasterBaseNeighbours = rasterBase.getRequiredNeighbours();
            rasterOut.setNeedsWrite(false);
            rasterBase.setNeedsWrite(false);
            rasterBase.setRequiredNeighbours(Neighbours::Queen);
            
            // Generate kernel
            std::vector<RasterBaseRef<CTYPE> > rasterBaseRefs;            
            rasterBaseRefs.push_back(rasterBase);
            rasterBaseRefs.push_back(rasterOut);
            auto kernelScript = rasterOut.generateKernelScript(
                script, kernelBlock, rasterBaseRefs, RasterNullValue::Null);
                
            // Check script
            if (kernelScript.script.size() == 0) {
                std::cout << "ERROR: cannot generate script" << std::endl;
                return Raster<RTYPE, CTYPE>();
            }

            std::size_t clRasterHash = rasterOut.buildScript(kernelScript);
            if (clRasterHash == solver.getNullHash()) {
                std::cout << "ERROR: Cannot get kernel for script" << std::endl;
                return Raster<RTYPE, CTYPE>();
            }

            // Get kernel
            cl::Kernel clRasterKernel = solver.getKernel(clRasterHash, "rasterArea2D");        

            // Loop through tiles
            auto dimOut = rasterOut.getRasterDimensions();
            for (uint32_t tj = 0; tj < dimOut.ty; tj++) {
                for (uint32_t ti = 0; ti < dimOut.tx; ti++) {

                    // Run kernel
                    clRasterKernel.setArg(0, width);
                    rasterOut.runTileKernel(ti, tj, clRasterKernel, rasterBaseRefs, 1);
                }
            }

            // Preserve original properties
            rasterOut.setNeedsWrite(true);
            rasterBase.setNeedsWrite(rasterBaseNeedsWrite);
            rasterBase.setRequiredNeighbours(rasterBaseNeighbours);

            return rasterOut;

        } else {
            std::cout << "ERROR: OpenCL not initialised" << std::endl;
        }

        // Return empty raster
        return Raster<RTYPE, CTYPE>();
    }

    /**
    * Creates an OpenCL buffer from a %Raster region.
    * @param bounds the %BoundingBox of the region.
    * @param regionBuffer a reference to a region buffer to return.
    * @param rasterRegionDim a reference to a %RasterDimensions object to return.
    * @return true if the raster overlaps the region, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::createRegionBuffer(BoundingBox<CTYPE> bounds, cl::Buffer &rasterRegionBuffer, RasterDimensions<CTYPE> &rasterRegionDim) {

        // Check raster
        if (hasData()) {

            // Get region
            Raster<RTYPE, CTYPE> subRaster = region(bounds);

            // Get dimensions
            rasterRegionDim = subRaster.getRasterDimensions();

            // Check dimensions, if any spacing value is zero the raster is null
            if (rasterRegionDim.d.hx > 0.0 || rasterRegionDim.d.hy > 0.0 || rasterRegionDim.d.hz > 0.0) {
            
                // Get OpenCL handles
                cl_int err = CL_SUCCESS;
                Geostack::Solver &solver = Geostack::Solver::getSolver();
                auto &context = solver.getContext();
                auto &queue = solver.getQueue();

                // Set block sizes
                cl::detail::size_t_array srcOrigin;
                srcOrigin[0] = 0;
                srcOrigin[1] = 0;
                srcOrigin[2] = 0;

                cl::detail::size_t_array region;
                region[0] = TileMetrics::tileSize*sizeof(RTYPE);
                region[1] = TileMetrics::tileSize;
                region[2] = rasterRegionDim.d.nz;

                // Try to find cache buffer of same size
                std::size_t dataSize = rasterRegionDim.d.nx*rasterRegionDim.d.ny*rasterRegionDim.d.nz*sizeof(RTYPE);
                std::map<std::size_t, cl::Buffer>::iterator cit = regionBufferCache.find(dataSize);
                if (cit == regionBufferCache.end()) {

                    // Create buffer
                    cit = regionBufferCache.insert( { dataSize, cl::Buffer(context, CL_MEM_READ_ONLY, dataSize, NULL, &err) } ).first;
                }

                // Get buffer
                rasterRegionBuffer = (*cit).second;
                queue.enqueueFillBuffer(rasterRegionBuffer, getNullValue<RTYPE>(), 0, dataSize);

                // Loop over tiles
                for (uint32_t rtj = 0; rtj < rasterRegionDim.ty; rtj++) {
                    for (uint32_t rti = 0; rti < rasterRegionDim.tx; rti++) {

                        // Get tile handle
                        auto &subRasterTile = *subRaster.getTile(rti, rtj);
                        auto subRasterTileDim = subRasterTile.getDimensions();
                                        
                        // Set destination block
                        cl::detail::size_t_array dstOrigin;
                        dstOrigin[0] = rti*TileMetrics::tileSize*sizeof(RTYPE);
                        dstOrigin[1] = rtj*TileMetrics::tileSize;
                        dstOrigin[2] = 0;

                        // Copy tiles into buffer
                        queue.enqueueCopyBufferRect(subRasterTile.getDataBuffer(), rasterRegionBuffer, srcOrigin, dstOrigin, region, 
                            subRasterTileDim.mx*sizeof(RTYPE), subRasterTileDim.mx*subRasterTileDim.my*sizeof(RTYPE), 
                            rasterRegionDim.d.nx*sizeof(RTYPE), rasterRegionDim.d.nx*rasterRegionDim.d.ny*sizeof(RTYPE));
                    }
                }
                return true;
            }
        }
        return false;
    }

    /**
    * Read file to %Raster.
    * @param fileName name of file to read.
    * @return true on successful read.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::read(std::string fileName) {
        
        // Get filename extension
        auto split = Strings::splitPath(fileName);

        if (split[2] == "asc") {

            // Create handler
            fileHandlerIn = std::make_shared<AsciiHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "flt") {

            // Create handler
            fileHandlerIn = std::make_shared<FltHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "tif" || split[2] == "tiff") {

            // Create handler
            fileHandlerIn = std::make_shared<GeoTIFFHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "gsr") {

            // Create handler
            fileHandlerIn = std::make_shared<GsrHandler<RTYPE, CTYPE> >();

        } else {
            std::cout << "ERROR: file name has unrecognised extension '" << split[2] << "'" << std::endl;
            return false;
        }

        // Process file
        return fileHandlerIn->read(fileName, *this);
    }

    /**
    * Write file to %Raster.
    * @param fileName name of file to read.
    * @return true on successful write.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::write(std::string fileName, std::string jsonConfig) {
    
        // Check for data
        if (!hasData()) {
            std::cout << "ERROR: raster has no data to write" << std::endl;
            return false;
        }
    
        // Get filename extension
        auto split = Strings::splitPath(fileName);

        if (split[2] == "asc") {

            // Create handler
            fileHandlerOut = std::make_shared<AsciiHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "flt") {

            // Create handler
            fileHandlerOut = std::make_shared<FltHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "tif" || split[2] == "tiff") {

            // Create handler
            fileHandlerOut = std::make_shared<GeoTIFFHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "gsr") {

            // Create handler
            fileHandlerOut = std::make_shared<GsrHandler<RTYPE, CTYPE> >();

        } else {

            std::cout << "ERROR: file name has unrecognised extension '" << split[2] << "'" << std::endl;
            return false;
        }

        // Process file
        return fileHandlerOut->write(fileName, *this, jsonConfig);
    }

    // CTYPE float definitions
    template bool equalSpatialMetrics(const Dimensions<float> l, const Dimensions<float> r);
    template class Variables<float>;
    template class RasterBase<float>;
    template std::ostream &operator<<(std::ostream &, const RasterDimensions<float> &);
    template bool runScriptNoOut<float>(std::string, std::vector<std::reference_wrapper<RasterBase<float> > >, size_t);
    
    // CTYPE double definitions
    template bool equalSpatialMetrics(const Dimensions<double> l, const Dimensions<double> r);
    template class Variables<double>;
    template class RasterBase<double>;
    template std::ostream &operator<<(std::ostream &, const RasterDimensions<double> &);
    template bool runScriptNoOut<double>(std::string, std::vector<std::reference_wrapper<RasterBase<double> > >, size_t);

    // RTYPE float, CTYPE float definitions
    template class Tile<float, float>;
    template class RasterFileHandler<float, float>;
    template class Raster<float, float>;
    
    template float RasterBase<float>::getVariableData<float>(std::string);
    template void RasterBase<float>::setVariableData<float>(std::string, float);
    template bool operator==(const Raster<float, float> &, const Raster<float, float> &);
    template Raster<float, float> runScript<float, float>(std::string, std::vector<std::reference_wrapper<RasterBase<float> > >, size_t);
    template Raster<float, float> runAreaScript<float, float>(std::string, RasterBase<float> &r, cl_int);
    
    // RTYPE uint, CTYPE float definitions
    template class Tile<uint32_t, float>;
    template class RasterFileHandler<uint32_t, float>;
    template class Raster<uint32_t, float>;
    
    template uint32_t RasterBase<float>::getVariableData<uint32_t>(std::string);
    template void RasterBase<float>::setVariableData<uint32_t>(std::string, uint32_t);
    template bool operator==(const Raster<uint32_t, float> &, const Raster<uint32_t, float> &);
    template Raster<uint32_t, float> runScript<uint32_t, float>(std::string, std::vector<std::reference_wrapper<RasterBase<float> > >, size_t);
    template Raster<uint32_t, float> runAreaScript<uint32_t, float>(std::string, RasterBase<float> &r, cl_int);
    
    // RTYPE double, CTYPE double definitions
    template class Tile<double, double>;
    template class RasterFileHandler<double, double>;
    template class Raster<double, double>;
        
    template double RasterBase<double>::getVariableData<double>(std::string);
    template void RasterBase<double>::setVariableData<double>(std::string, double);
    template bool operator==(const Raster<double, double> &, const Raster<double, double> &);
    template Raster<double, double> runScript<double, double>(std::string, std::vector<std::reference_wrapper<RasterBase<double> > >, size_t);
    template Raster<double, double> runAreaScript<double, double>(std::string, RasterBase<double> &r, cl_int);

    // RTYPE uint, CTYPE double definitions
    template class Tile<uint32_t, double>;
    template class RasterFileHandler<uint32_t, double>;
    template class Raster<uint32_t, double>;
    
    template uint32_t RasterBase<double>::getVariableData<uint32_t>(std::string);
    template void RasterBase<double>::setVariableData<uint32_t>(std::string, uint32_t);
    template bool operator==(const Raster<uint32_t, double> &, const Raster<uint32_t, double> &);
    template Raster<uint32_t, double> runScript<uint32_t, double>(std::string, std::vector<std::reference_wrapper<RasterBase<double> > >, size_t);
    template Raster<uint32_t, double> runAreaScript<uint32_t, double>(std::string, RasterBase<double> &r, cl_int);
}
