# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3

from libcpp.string cimport string
from libcpp.memory cimport shared_ptr, unique_ptr
from libcpp cimport bool
from ..vector._cy_vector cimport Vector, _Vector_d, _Vector_f
from ..raster._cy_raster cimport GeometryType

cdef extern from "gs_shapefile.h" namespace "Geostack":
    cdef cppclass ShapeFile[T]:
        @staticmethod
        Vector[T] shapefileToVector(string shapefileName) except+
        @staticmethod
        bool vectorToShapefile(Vector[T] &v, string shapefileName, GeometryType)

cdef class shapefile_f:
    @staticmethod
    cdef _Vector_f _shapefileToVector(string shapefileName) except+
    @staticmethod
    cdef bool _vectorToShapefile(_Vector_f v, string shapefileName, GeometryType) except+

cdef class shapefile_d:
    @staticmethod
    cdef _Vector_d _shapefileToVector(string shapefileName) except+
    @staticmethod
    cdef bool _vectorToShapefile(_Vector_d v, string shapefileName, GeometryType) except+