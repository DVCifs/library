# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import ctypes
import numpy as np
from ._cy_property import (_PropertyHandler, _Property_d,
    _Property_i, _Property_s)
from typing import Union, List, Tuple, Any, Iterable, Hashable
import numbers

__all__ = ["PropertyHandler"]


class PropertyHandler:
    def __init__(self):
        self._properties = {}

    def getProperty(self, prop: str) -> Union[int,float,str]:
        """Get a property of an object.

        Parameters
        ----------
        prop: str
            A property of a Raster or a Vector object.

        Returns
        -------
        out : int/double/str
            Value of object property.

        Examples
        --------
        >>> testA = Raster(name="testRasterA")
        >>> testA.getProperty("name")
        testRasterA
        """
        if not isinstance(prop, str):
            raise TypeError("property name 'prop' should be of string type")
        if hasattr(self, '_handle'):
            cy_obj = "_handle"
        else:
            raise AttributeError("Raster or Vector has not been created")
        if self.hasProperty(prop):
            if isinstance(prop, str):
                _prop = prop.encode('UTF-8')
            else:
                _prop = prop
            out = getattr(self, cy_obj).getProperty_s(_prop)
            if len(out) == 0:
                out = getattr(self, cy_obj).getProperty_i(_prop)
                if out == 0:
                    out = getattr(self, cy_obj).getProperty_d(_prop)
            return out
        else:
            raise KeyError("Property %s is not attached to the object" % prop)

    def setProperty(self, prop: str, value: Union[int,float,str],
        prop_type=None):
        """Set a property of an object.

        Parameters
        ----------
        prop: str
            A property to be set for an object.
        value: int/float/str
            A value of the property to be set for an object.
        prop_type: datatype
            A data type of the property being set for an object.

        Returns
        -------
        Nil

        Examples
        --------
        >>> testRasterA = Raster(name="testRasterA")
        >>> testRasterA.setProperty("name", "windSpeed", prop_type=str)
        """
        if not isinstance(prop, str):
            raise TypeError("property name 'prop' should be of string type")
        if hasattr(self, '_handle'):
            cy_obj = "_handle"
        else:
            raise AttributeError("Raster or Vector has not been created")
        if isinstance(prop, str):
            _prop = prop.encode('UTF-8')
        else:
            _prop = prop

        if prop_type is not None:
            if prop_type == int:
                getattr(self, cy_obj).setProperty(_prop, int(value))
            elif prop_type == float:
                getattr(self, cy_obj).setProperty(_prop, float(value))
            elif prop_type == str:
                getattr(self, cy_obj).setProperty(_prop, f"{value}".encode('UTF-8'))
            else:
                raise TypeError("value of prop_type is not of acceptable type")
        else:
            if isinstance(value, (str, bytes)):
                if isinstance(value, str):
                    getattr(self, cy_obj).setProperty(_prop, value.encode('UTF-8'))
                else:
                    getattr(self, cy_obj).setProperty(_prop, value)
            elif isinstance(value, numbers.Real) and not isinstance(value, numbers.Integral):
                getattr(self, cy_obj).setProperty(_prop, float(value))
            else:
                getattr(self, cy_obj).setProperty(_prop, int(value))

    def hasProperty(self, prop: str) -> bool:
        """Check if a property is set for the object.

        Parameters
        ----------
        prop: str
            A property to be checked for an object.
        
        Returns
        -------
        out: bool
            True if property is set, False otherwise.

        Examples
        --------
        >>> testRasterA = Raster(name="testRasterA")
        >>> testRasterA.hasProperty("orientation")
        False
        """
        if not isinstance(prop, str):
            raise TypeError("property name 'prop' should be of string type")
        if hasattr(self, '_handle'):
            cy_obj = "_handle"
        else:
            raise AttributeError("Raster or Vector has not been created")
        if isinstance(prop, str):
            _prop = prop.encode('UTF-8')
        else:
            _prop = prop
        return getattr(self, cy_obj).hasProperty(_prop)

    def removeProperty(self, prop: str):
        """Remove a property from the object.

        Parameters
        ----------
        prop: str
            A property to be removed from an object.
        
        Returns
        -------
        Nil

        Examples
        --------
        >>> testRasterA = Raster(name="testRasterA")
        >>> testRasterA.setProperty("orientation", 1)
        >>> testRasterA.hasProperty("orientation")
        True
        >>> testRasterA.removeProperty("orientation")
        >>> testRasterA.hasProperty("orientation")
        False
        """
        if not isinstance(prop, str):
            raise TypeError("property name 'prop' should be of string type")
        if hasattr(self, '_handle'):
            cy_obj = "_handle"
        else:
            raise AttributeError("Raster or Vector has not been created")
        if isinstance(prop, str):
            _prop = prop.encode('UTF-8')
        else:
            _prop = prop
        
        getattr(self, cy_obj).removeProperty(_prop)

    def getProperties(self, prop_type=None) -> Hashable:
        """Get all the properties of an object of a given datatype.

        Parameters
        ----------
        prop_type: datatype
            A datatype corresponding the values of the properties of an object.

        Returns
        -------
        out: dict
            A dictionary containing properties and values of the properties.

        Examples
        --------
        >>> testRasterA = Raster(name="testRasterA")
        >>> testRasterA.getProperties(prop_type=int)
        {"name": "testRasterA"}
        """
        if hasattr(self, '_handle'):
            cy_obj = "_handle"
        else:
            raise AttributeError("Raster or Vector has not been created")
        if prop_type is None:
            self._properties.update(getattr(self, cy_obj).getProperties_s())
            self._properties.update(getattr(self, cy_obj).getProperties_i())
            self._properties.update(getattr(self, cy_obj).getProperties_d())
        else:
            if prop_type == int:
                self._properties.update(getattr(self, cy_obj).getProperties_i())
            elif prop_type == float:
                self._properties.update(getattr(self, cy_obj).getProperties_d())
            elif prop_type == str:
                self._properties.update(getattr(self, cy_obj).getProperties_s())
            else:
                raise ValueError("value of prop_type is not of acceptable type")

        return self._properties


    def __repr__(self):
        return "<class 'geostack.core.%s'>" % self.__class__.__name__