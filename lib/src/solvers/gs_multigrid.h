/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_LEVEL_SET_SOLVER_H
#define GEOSTACK_LEVEL_SET_SOLVER_H

#include <string>
#include <vector>
#include <set>

#include "gs_raster.h"

// Link to external resources
extern const char R_cl_multigrid_c[];
extern const uint32_t R_cl_multigrid_c_size;

namespace Geostack
{
	
    /**
    * %Multigrid class for perimeter growth. 
    */
    template <typename TYPE>
    class Multigrid {

        public:
        
            /**
            * Multigrid constructor
            */
            Multigrid():initialised(false) { }

            // Initialisation
            bool init();

            // Execution
            bool step();

        private:            

            // Internal variables
            volatile bool initialised;               ///< Solver initialised
    };
}

#endif