# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3

from cython.operator import dereference as deref
from libcpp.memory cimport shared_ptr, unique_ptr
from libc.stdint cimport uint32_t, int32_t
from libcpp import nullptr_t, nullptr
from libcpp.string cimport string
from libcpp cimport bool
import numpy as np
cimport numpy as np


np.import_array()


cdef extern from "gs_solver.h" namespace "Geostack":
    cdef cppclass Solver:
        Solver() except +
    
    Solver& getSolver()
    void setVerbose(bool verbose_)
    string getError()
    bool openCLInitialized()
    size_t buildProgram(string uid, string clProgram)
    int getMaxWorkGroupSize(string)
    int getAlignMemorySize(string)
    string processScript(string script)
    size_t getNullHash()
    void resetTimers()

    void switchTimers(string, string)
    void incrementTimers(string, string, double)
    void displayTimers()
    string currentTimer(string)


cdef class cySolver:
    cdef unique_ptr[Solver] *thisptr