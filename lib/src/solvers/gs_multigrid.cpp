/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <iostream>
#include <regex>

#include "gs_solver.h"
#include "gs_multigrid.h"

namespace Geostack
{ 

    template <typename TYPE>
    bool Multigrid<TYPE>::init() {
        return true;
    }

    template <typename TYPE>
    bool Multigrid<TYPE>::step() {
        return true;
    }

    // Float type definitions
    template class Multigrid<float>;

    // Double type definitions
    template class Multigrid<double>;
}
