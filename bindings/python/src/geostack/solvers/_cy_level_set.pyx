# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref
from libcpp.memory cimport unique_ptr, shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport nullptr_t, nullptr
from libc.stdint cimport uint32_t, uint64_t
from libcpp cimport bool
import numpy as np
cimport numpy as np
from ..vector._cy_vector cimport Vector
from ..raster._cy_raster cimport (_cyRaster_d, _cyRaster_f,
        _cyRaster_d_i, _cyRaster_f_i)
from ..vector._cy_vector cimport _Vector_d, _Vector_f
from ..raster._cy_raster cimport _RasterPtrList_d, _RasterPtrList_f
from ..raster._cy_raster cimport _Variables_f, _Variables_d


np.import_array()


cdef extern from "gs_property.h" namespace "Geostack":
    cdef cppclass PropertyHandler:
        pass


cdef extern from "gs_raster.h" namespace "Geostack":
    cdef cppclass RasterBase[T](PropertyHandler):
        pass

    cdef cppclass Raster[RTYPE, CTYPE](RasterBase[CTYPE]):
        pass

    cdef cppclass Variables[CTYPE]:
        pass

cdef extern from "gs_level_set.h" namespace "Geostack::LevelSetLayers":
    cdef enum LevelSetLayersType "Type":
        Distance
        DistanceUpdate
        Rate
        Speed
        Arrival
        Advect_x
        Advect_y
        LevelSetLayers_END

cdef extern from "gs_level_set.h" namespace "Geostack":
    cdef cppclass LevelSetParameters[T]:
        T time
        T dt
        T dtLast
        T maxSpeed
        T area
        T bandWidth
        T JulianDate
        T JulianFraction

    cdef cppclass LevelSet[T]:
        LevelSet() except +
        bool init(string jsonStartConditions,
            Vector[T] &sources,
            Vector[T] &mask,
            shared_ptr[Variables[T]] &variables,
            vector[shared_ptr[RasterBase[T]]] &input_layers,
            vector[shared_ptr[RasterBase[T]]] &output_layers) nogil except +
        bool step() nogil except +
        Raster[T, T]& getDistance() except +
        Raster[T, T]& getArrival() except +
        Raster[uint32_t, T]& getClassification() except +
        Raster[T, T]& getAdvect_x() except +
        Raster[T, T]& getAdvect_y() except +
        LevelSetParameters[T]& getParameters() except +
        uint64_t getEpochMilliseconds() except +

ctypedef LevelSetParameters[double] levelSetParams_d
ctypedef LevelSetParameters[float] levelSetParams_f

cdef class cyLevelSet_f:
    cdef unique_ptr[LevelSet[float]] thisptr

    def __cinit__(self):
        self.thisptr.reset(new LevelSet[float]())

    cpdef bool init(self, string jsonStartConditions,
        _Vector_f sources, _Vector_f mask, _Variables_f variables,
        _RasterPtrList_f inp_layers,
        _RasterPtrList_f out_layers) except +:
        cdef bool out = False
        if self.thisptr != nullptr:
            with nogil:
                out = deref(self.thisptr).init(jsonStartConditions,
                    deref(sources.thisptr),
                    deref(mask.thisptr), variables.thisptr,
                    deref(inp_layers.thisptr), deref(out_layers.thisptr))
        return out

    cpdef uint64_t getEpochMilliseconds(self):
        cdef uint64_t out
        out = deref(self.thisptr).getEpochMilliseconds()
        return out

    cpdef bool step(self) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.thisptr).step()
        return out

    cpdef _cyRaster_f getDistance(self):
        out = _cyRaster_f()
        out.c_rastercopy(deref(self.thisptr).getDistance())
        return out

    cpdef _cyRaster_f getArrival(self):
        out = _cyRaster_f()
        out.c_rastercopy(deref(self.thisptr).getArrival())
        return out

    cpdef _cyRaster_f_i getClassification(self):
        out = _cyRaster_f_i()
        out.c_rastercopy(deref(self.thisptr).getClassification())
        return out

    cpdef _cyRaster_f getAdvect_x(self):
        out = _cyRaster_f()
        out.c_rastercopy(deref(self.thisptr).getAdvect_x())
        return out

    cpdef _cyRaster_f getAdvect_y(self):
        out = _cyRaster_f()
        out.c_rastercopy(deref(self.thisptr).getAdvect_y())
        return out

    def getParameters(self):
        cdef levelSetParams_f _out
        _out = deref(self.thisptr).getParameters()
        out = {
            "dt": _out.dt,
            "dtLast": _out.dtLast,
            "time": _out.time,
            "maxSpeed": _out.maxSpeed,
            "bandWidth": _out.bandWidth,
            "area": _out.area,
            "JulianDate": _out.JulianDate,
            "JulianFraction": _out.JulianFraction,
        }
        return out

    def setParameters(self, string param, double value):
        if param == b"dt":
            deref(self.thisptr).getParameters().dt = value
        elif param == b"dtLast":
            deref(self.thisptr).getParameters().dtLast = value
        elif param == b"time":
            deref(self.thisptr).getParameters().time = value
        elif param == b"maxSpeed":
            deref(self.thisptr).getParameters().maxSpeed = value
        elif param == b"bandWidth":
            deref(self.thisptr).getParameters().bandWidth = value
        elif param == b"area":
            deref(self.thisptr).getParameters().area = value
        elif param == b"JulianDate":
            deref(self.thisptr).getParameters().JulianDate = value
        elif param == b"JulianFraction":
            deref(self.thisptr).getParameters().JulianFraction = value
        else:
            raise KeyError("parameter is unknown")

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if self.thisptr != nullptr:
            if type(self) is cyLevelSet_f:
                self.thisptr.reset()

    def __exit__(self):
        self.__dealloc__()


cdef class cyLevelSet_d:
    cdef unique_ptr[LevelSet[double]] thisptr

    def __cinit__(self):
        self.thisptr.reset(new LevelSet[double]())

    cpdef bool init(self, string jsonStartConditions,
        _Vector_d sources, _Vector_d mask, _Variables_d variables,
        _RasterPtrList_d inp_layers, _RasterPtrList_d out_layers) except +:
        cdef bool out = False
        if self.thisptr != nullptr:
            with nogil:
                out = deref(self.thisptr).init(jsonStartConditions,
                    deref(sources.thisptr),
                    deref(mask.thisptr), variables.thisptr,
                    deref(inp_layers.thisptr), deref(out_layers.thisptr))
        return out

    cpdef uint64_t getEpochMilliseconds(self):
        cdef uint64_t out
        out = deref(self.thisptr).getEpochMilliseconds()
        return out

    cpdef bool step(self) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.thisptr).step()
        return out

    cpdef _cyRaster_d getDistance(self):
        out = _cyRaster_d()
        out.c_rastercopy(deref(self.thisptr).getDistance())
        return out

    cpdef _cyRaster_d getArrival(self):
        out = _cyRaster_d()
        out.c_rastercopy(deref(self.thisptr).getArrival())
        return out

    cpdef _cyRaster_d_i getClassification(self):
        out = _cyRaster_d_i()
        out.c_rastercopy(deref(self.thisptr).getClassification())
        return out

    cpdef _cyRaster_d getAdvect_x(self):
        out = _cyRaster_d()
        out.c_rastercopy(deref(self.thisptr).getAdvect_x())
        return out

    cpdef _cyRaster_d getAdvect_y(self):
        out = _cyRaster_d()
        out.c_rastercopy(deref(self.thisptr).getAdvect_y())
        return out

    def getParameters(self):
        cdef levelSetParams_d _out
        _out = deref(self.thisptr).getParameters()
        out = {
            "dt": _out.dt,
            "dtLast": _out.dtLast,
            "Time": _out.time,
            "maxSpeed": _out.maxSpeed,
            "bandWidth": _out.bandWidth,
            "area": _out.area,
            "JulianDate" : _out.JulianDate,
            "JulianFraction": _out.JulianFraction,
        }
        return out

    def setParameters(self, string param, double value):
        if param == b"dt":
            deref(self.thisptr).getParameters().dt = value
        elif param == b"dtLast":
            deref(self.thisptr).getParameters().dtLast = value
        elif param == b"time":
            deref(self.thisptr).getParameters().time = value
        elif param == b"maxSpeed":
            deref(self.thisptr).getParameters().maxSpeed = value
        elif param == b"bandWidth":
            deref(self.thisptr).getParameters().bandWidth = value
        elif param == b"area":
            deref(self.thisptr).getParameters().area = value
        elif param == b"JulianDate":
            deref(self.thisptr).getParameters().JulianDate = value
        elif param == b"JulianFraction":
            deref(self.thisptr).getParameters().JulianFraction = value
        else:
            raise KeyError("parameter is unknown")

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if self.thisptr != nullptr:
            if type(self) is cyLevelSet_d:
                self.thisptr.reset()

    def __exit__(self):
        self.__dealloc__()
