/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_RASTER_H
#define GEOSTACK_RASTER_H

#include <mutex>
#include <atomic>
#include <memory>
#include <vector>
#include <list>
#include <set>
#include <iterator>
#include <functional>
#include <fstream>

#ifdef USE_TILE_CACHING
#include <thread>
#endif

#include "gs_opencl.h"
#include "gs_geometry.h"
#include "gs_property.h"
#include "gs_projection.h"

#define STRINGIFY(s) (#s)

/**
* Helper macro to create a named raster with the same name as the internal variable
*/
#define makeRaster(NAME, RTYPE, CTYPE) Geostack::Raster<RTYPE, CTYPE> NAME(STRINGIFY(NAME));

// Link to external resources
extern const char R_cl_raster_head_c[];
extern const uint32_t R_cl_raster_head_c_size;

extern const char R_cl_raster_area_op2D_c[];
extern const uint32_t R_cl_raster_area_op2D_c_size;

extern const char R_cl_raster_block_c[];
extern const uint32_t R_cl_raster_block_c_size;

extern const char R_cl_map_distance_c[];
extern const uint32_t R_cl_map_distance_c_size;

extern const char R_cl_rasterise_c[];
extern const uint32_t R_cl_rasterise_c_size;

extern const char R_cl_vectorise_c[];
extern const uint32_t R_cl_vectorise_c_size;

extern const char R_cl_projection_head_c[];
extern const uint32_t R_cl_projection_head_c_size;

namespace Geostack
{
    // Forward declarations
    class Solver;

    template <typename CTYPE>
    class BoundingBox;

    template <typename CTYPE>
    class Vector;

    template <typename RTYPE, typename CTYPE>
    class Tile;
    
    template <typename CTYPE>
    class RasterBase;

    template <typename RTYPE, typename CTYPE>
    class Raster;
    
    // Definitions
    template <typename RTYPE, typename CTYPE>
    using TilePtr = std::shared_ptr<Tile<RTYPE, CTYPE> >;

    template <typename CTYPE>
    using RasterBasePtr = std::shared_ptr<RasterBase<CTYPE> >;
    
    template <typename CTYPE>
    using RasterBaseRef = std::reference_wrapper<RasterBase<CTYPE> >;

    using tileIndexSet = std::set<std::pair<uint32_t, uint32_t> >; 

    /**
    * Combination types
    */
    namespace RasterCombination {
        enum Type {
            Union        = 0,      ///< Combination is union of rasters
            Intersection = 1 << 0, ///< Combination is intersection of rasters
        };
    }

    /**
    * Resolution type
    */
    namespace RasterResolution {
        enum Type {
            Minimum = 0,      ///< Resolution is maximum resolution of rasters
            Maximum = 1 << 2, ///< Resolution is minimum resolution of rasters
        };
    }

    /**
    * Interpolation types.
    */
    namespace RasterInterpolation {
        enum Type {
            Nearest  = 0,      ///< Nearest neighbour interpolation
            Bilinear = 1 << 4, ///< Bilinear interpolation
            Bicubic  = 2 << 4, ///< Bicubic interpolation
        };
    }

    /**
    * Value for nodata in calculation.
    */
    namespace RasterNullValue {
        enum Type {
            Null = 0,      ///< Use null
            Zero = 1 << 6, ///< Use zero
            One  = 2 << 6, ///< Use one
        };
    }

    /**
    * Raster reduction type
    */
    namespace Reduction {
        enum Type {
            None    = 0,       ///< No reduction
            Maximum = 1 << 8,  ///< Maximum over raster
            Minimum = 2 << 8,  ///< Minimum over raster
            Sum     = 3 << 8,  ///< Sum over raster
            Count   = 4 << 8,  ///< Count of raster data values
            Mean    = 5 << 8,  ///< Mean over raster
        };
    }

    /**
    * Raster neighbour type
    */
    namespace Neighbours {
        enum Type {
            None = 0x00,   ///< No neighbours
            Rook = 0x01,   ///< N, E, S and W neighbours
            Bishop = 0x02, ///< NE, SW, SW and NW neighbours
            Queen = 0x03,  ///< All neighbours
        };
    }

    namespace TileMetrics {
    
        // Tile size power
        const uint32_t tileSizePower = 8;

        // Tile size
        const uint32_t tileSize = 1 << tileSizePower;

        // Tile size squared
        const uint32_t tileSizeSquared = tileSize*tileSize;

        // Tile mask
        const uint32_t tileSizeMask = tileSize-1;
        
        // Tile ranges
        const cl::NDRange rangeTile2DGlobal = cl::NDRange(tileSize, tileSize);
        const cl::NDRange rangeTile2DLocal = cl::NDRange(tileSize, 1);

        // Tile reduction sizes
        const uint32_t tileReduceSize = (tileSize*tileSize)/(rangeTile2DLocal[0]*rangeTile2DLocal[1]);
    }
    
    /**
    * %General dimension structure
    */
    template <typename CTYPE>
    struct alignas(8) Dimensions {

        uint32_t nx; ///< Number of data cells in x dimension
        uint32_t ny; ///< Number of data cells in y dimension
        uint32_t nz; ///< Number of data cells in z dimension
        CTYPE hx;    ///< Spacing in x dimension
        CTYPE hy;    ///< Spacing in y dimension
        CTYPE hz;    ///< Spacing in z dimension
        CTYPE ox;    ///< Start coordinate in x dimension
        CTYPE oy;    ///< Start coordinate in y dimension
        CTYPE oz;    ///< Start coordinate in z dimension
        uint32_t mx; ///< Number of cells stored in memory for x dimension
        uint32_t my; ///< Number of cells stored in memory for y dimension
    };

    /**
    * %Tile dimension structure
    */
    template <typename CTYPE>
    struct alignas(8) TileDimensions {
    
        Dimensions<CTYPE> d; ///< General dimensions
        CTYPE ex;            ///< End coordinate in x dimension
        CTYPE ey;            ///< End coordinate in y dimension
        CTYPE ez;            ///< End coordinate in z dimension
        uint32_t ti;         ///< Tile index in x dimension
        uint32_t tj;         ///< Tile index in y dimension
    };

    /**
    * %Raster dimension structure
    */
    template <typename CTYPE>
    struct alignas(8) RasterDimensions {
    
        Dimensions<CTYPE> d; ///< General dimensions
        CTYPE ex;            ///< End coordinate in x dimension
        CTYPE ey;            ///< End coordinate in y dimension
        CTYPE ez;            ///< End coordinate in z dimension
        uint32_t tx;         ///< Number of tiles in x dimension
        uint32_t ty;         ///< Number of tiles in y dimension
    };
    
    /**
    * %Raster script class
    * Holds parsed script and flags for script building
    */
    class KernelScript {
    public:

        /**
        * %KernelScript constructor
        */
        KernelScript(): script(), usingProjection(false) { }

        std::string script;   ///< Parsed script
        bool usingProjection; ///< Flag for using projections in script
    };

    // Dimensions equality check.
    template <typename CTYPE>
    bool equalSpatialMetrics(const Dimensions<CTYPE> l, const Dimensions<CTYPE> r);

    // Data handlers
    template <typename RTYPE, typename CTYPE>
    using dataHandlerReadFunction = std::function<void(TileDimensions<CTYPE>, std::vector<RTYPE> &, Raster<RTYPE, CTYPE> &)>;

    template <typename RTYPE, typename CTYPE>
    using dataHandlerWriteFunction = std::function<void(TileDimensions<CTYPE>, std::vector<RTYPE> &, Raster<RTYPE, CTYPE> &)>;

    // Raster dimensions output stream
    template <typename CTYPE>
    std::ostream &operator<<(std::ostream &os, const RasterDimensions<CTYPE> &r);

    // Raster script operations
    template <typename CTYPE>
    bool runScriptNoOut(std::string script, std::vector<RasterBaseRef<CTYPE> > inputRasters, size_t parameters = 0);

    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runScript(std::string script, std::vector<RasterBaseRef<CTYPE> > inputRasters, size_t parameters = 0);

    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runAreaScript(std::string script, RasterBase<CTYPE> &r, cl_int width);    
    
    /**
    * Run script on %Raster
    * Aliased runScript function
    * @param script OpenCL script to run.
    * @param rasterBaseRefs list of input raster references.
    * @param parameters parameter flags for combination and aliasing.
    * @return true if script is successful, false otherwise
    */
    template <typename CTYPE>
    bool runScript(std::string script, std::vector<RasterBaseRef<CTYPE> > inputRasters, size_t parameters = 0) {
        return runScriptNoOut(script, inputRasters, parameters);
    }

    // Comparison operators
    template <typename RTYPE, typename CTYPE>
    bool operator==(const Tile<RTYPE, CTYPE> &l, const Tile<RTYPE, CTYPE> &r);

    template <typename RTYPE, typename CTYPE>
    bool operator!=(const Tile<RTYPE, CTYPE> &l, const Tile<RTYPE, CTYPE> &r);

    template <typename RTYPE, typename CTYPE>
    bool operator==(const Raster<RTYPE, CTYPE> &l, const Raster<RTYPE, CTYPE> &r);
    
#ifdef USE_TILE_CACHING

    /**
    * %TileCacheManager class for %Tile data. 
    */
    template <typename RTYPE, typename CTYPE>
    class TileCacheManager {
    
    public:
        
        const uint64_t tileMemoryLimit = 2*(uint64_t)1073741824; // Tile memory limit in bytes
        const uint64_t tileLimit = (uint64_t)tileMemoryLimit/(TileMetrics::tileSizeSquared*sizeof(REAL)); // Tile limit

        // Disable functions
        TileCacheManager(const TileCacheManager &) = delete;
        TileCacheManager(TileCacheManager &&) = delete;
        TileCacheManager& operator=(const TileCacheManager &) = delete;
        TileCacheManager& operator=(TileCacheManager &&) = delete;

        // Get singleton instance of %TileCacheManager.
        static TileCacheManager &instance();
        void update(TilePtr<RTYPE, CTYPE> &tile);
        void increment();

#ifdef USE_THREADED_TILE_CACHING

        void flush(TilePtr<RTYPE, CTYPE> &tile);

#endif

    private:
        
        // Disable functions outside class
        TileCacheManager():cacheCount(0), cacheAccessCount(0) { }
        ~TileCacheManager();

        // Member variables
        std::atomic<std::uint64_t> cacheCount;
        std::atomic<std::uint64_t> cacheAccessCount;
        std::map<TilePtr<RTYPE, CTYPE>, uint64_t> cacheTileAccessMap;
        std::map<uint64_t, TilePtr<RTYPE, CTYPE> > cacheAccessTileMap;

#ifdef USE_THREADED_TILE_CACHING

        std::map<TilePtr<RTYPE, CTYPE>, std::thread> cacheTileThreadMap;

#endif

    };

#endif
    
    /**
    * %Variables class for one dimensional named variables.
    */
    template <typename RTYPE>
    class Variables {
    
    public:

        // Constructor
        Variables();

        // Destructor
        ~Variables();
        
        // Disable copy constructors
        Variables(Variables const &) = delete;
        void operator=(Variables const &) = delete;

        // Set and get operations
        void set(std::string name, RTYPE value);
        RTYPE get(std::string name);
        const std::map<std::string, std::size_t> &getIndexes();
        
        // Data handlers
        bool getBuffer(cl::Buffer &buffer);

        /**
        * Check %Variables data.
        * @return true if map contains data, false otherwise
        */
        bool hasData() { return !dataMap.empty(); }

    private:    

        void refreshData();                             ///< Copy data from map to vector
        std::map<std::string, RTYPE> dataMap;           ///< Data map
        std::map<std::string, std::size_t> dataIndexes; ///< Indexes of data in vector
        std::vector<RTYPE> dataVec;                     ///< Data vector
        
        cl::Buffer buffer;                              ///< %Variables buffer
        void *bufferMapPtr;                             ///< Holder for pointer returned from variable buffer mapping
        volatile bool bufferOnDevice;                   ///< Flag indicating if variable buffer is currently on device
        volatile bool dataInitialised;                  ///< Flag indicating whether variable data has been initialised

        void mapBuffer(cl::CommandQueue &);             ///< Map buffer to host
        void unmapBuffer(cl::CommandQueue &);           ///< Unmap buffer from host
        void ensureDataOnHost();                        ///< Ensure the %Variables data is on the host
    };

    /**
    * %Tile class for one, two or three dimensional geospatial data.
    * This holds data for a square tile of a @Raster. 
    */
    template <typename RTYPE, typename CTYPE>
    class Tile : public GeometryBase<CTYPE> {

    public:

        // Constructor
        Tile();

        // Destructor
        ~Tile();
        
        // Disable Tile copy constructors.
        Tile(Tile const &) = delete;
        void operator=(Tile const &) = delete;

        friend bool operator==<RTYPE, CTYPE>(const Tile<RTYPE, CTYPE> &, const Tile<RTYPE, CTYPE> &);
        friend bool operator!=<RTYPE, CTYPE>(const Tile<RTYPE, CTYPE> &, const Tile<RTYPE, CTYPE> &);

        // Tile initialisation
        void createData();
        void readData(Raster<RTYPE, CTYPE> &r, dataHandlerReadFunction<RTYPE, CTYPE> readDataHandler = nullptr);
        bool init(uint32_t ti_, uint32_t tj_, RasterDimensions<CTYPE> rdim_);

        // Tile deletion
        void deleteData(bool clearCacheFlag = false);

        // Get bounds and dimensions
        BoundingBox<CTYPE> getBounds() const override;

        /**
        * Get base dimensions.
        * @return base dimensions of %Tile.
        */
        Dimensions<CTYPE> getDimensions() { 
            return dim.d;
        }

        /**
        * Get data size dimensions.
        * @return data size in bytes of %Tile.
        */
        size_t getDataSize() const { 
            return dim.d.mx*dim.d.my*dim.d.nz;
        }

        /**
        * Get %Tile dimensions.
        * @return dimensions of %Tile.
        */
        TileDimensions<CTYPE> getTileDimensions() { 
            return dim;
        }
        
        /**
        * Get type of Geometry
        * @return Tile identifier.
        */
        bool isType(size_t typeMask) const override {
            return (bool)(typeMask & GeometryType::Tile);
        }

        // Buffer functions
        cl::Buffer &getDataBuffer();      ///< Get %Tile data buffer
        cl::Buffer &getReduceBuffer();    ///< Get %Tile reduction buffer
        cl::Buffer &getStatusBuffer();    ///< Get %Tile status buffer

        // Data handlers
        using tIterator = typename std::vector<RTYPE>::iterator;
        bool hasData();                                                       ///< Check %Tile data
        void setAllCellValues(RTYPE val);                                     ///< Set all tile values
        void getData(tIterator iTo, size_t start, size_t end);                ///< Copy %Tile data to vector
        void setData(tIterator iFrom, tIterator iFromEnd, size_t start);      ///< Copy vector to %Tile data vector
        void setIndex(uint32_t ti_, uint32_t tj_);                            ///< Set %Tile index
        inline RTYPE &operator()(uint32_t i, uint32_t j = 0, uint32_t k = 0); ///< Get %Tile value

        RTYPE max(); // Tile maximum value TODO remove
        RTYPE min(); // Tile minimum value TODO remove        

        // Reduction handlers
        RTYPE reduce(Reduction::Type type);        ///< %Tile reduction
        void resetReduction();                     ///< Clear reduction buffer

        // Status handlers
        cl_uint getStatus();                       ///< Get %Tile status
        void setStatus(cl_uint newStatus);         ///< Set %Tile status
        cl_uint getResetStatus(cl_uint newStatus); ///< %Tile status with reset
        
#ifdef USE_TILE_CACHING

        // Cache handler
        bool isCached();
        void writeToCache();
        void readFromCache();

#endif

        // Tile operations
        bool mapVector(Vector<CTYPE> &v, std::size_t clHash, ProjectionParameters<CTYPE> rproj,
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon,
            std::string widthPropertyName = std::string());

        bool rasterise(Vector<CTYPE> &v, std::size_t clHash, std::vector<std::string> fields, ProjectionParameters<CTYPE> rproj,
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon);

    private:

        // Data mutex
        mutable std::mutex tileMutex;
        
        // Tile data
        volatile bool dataInitialised; ///< Tile data initialised
        TileDimensions<CTYPE> dim;     ///< Tile dimensions
        std::vector<RTYPE> dataVec;    ///< Data vector
        std::vector<RTYPE> reduceVec;  ///< Reduction vector
        cl_uint status;                ///< Status bits

#ifdef USE_TILE_CACHING

        /**
        * %TileCache class for writing %Tile data to a temporary file on disk. 
        */
        class TileCache {

        public:
            
            TileCache();
            ~TileCache(); 

            bool read(Tile<RTYPE, CTYPE> &tile);
            bool write(Tile<RTYPE, CTYPE> &tile);
            bool getIsCached() { return isCached; }
            void setAsUncached() { isCached = false; }

        private:

            std::string cacheName;
            volatile bool isCached;
            bool hasCacheFile;
        };
        TileCache tileCache;
#endif

        // OpenCL data
        cl::Buffer dataBuffer;                          ///< Tile OpenCL data buffer
        mutable void *dataBufferMapPtr;                 ///< Holder for pointer returned from data buffer mapping
        mutable volatile bool dataBufferOnDevice;       ///< Flag indicating if data buffer is currently on device
        void mapDataBuffer(cl::CommandQueue &) const;   ///< Map OpenCL data buffer to host
        void unmapDataBuffer(cl::CommandQueue &);       ///< Unmap OpenCL data buffer from host
        void ensureDataBufferOnHost();                  ///< Ensure the tile data is on the host

        cl::Buffer reduceBuffer;                        ///< Tile OpenCL reduction buffer
        mutable void *reduceBufferMapPtr;               ///< Holder for pointer returned from reduction buffer mapping
        mutable volatile bool reduceBufferOnDevice;     ///< Flag indicating if reduction buffer is currently on device
        void mapReduceBuffer(cl::CommandQueue &) const; ///< Map OpenCL reduction buffer to host
        void unmapReduceBuffer(cl::CommandQueue &);     ///< Unmap OpenCL reduction buffer from host
        void ensureReduceBufferOnHost();                ///< Ensure the tile reduction data is on the host

        cl::Buffer statusBuffer;                        ///< Tile OpenCL status buffer
        mutable void *statusBufferMapPtr;               ///< Holder for pointer returned from status buffer mapping
        mutable volatile bool statusBufferOnDevice;     ///< Flag indicating if status buffer is currently on device
        void mapStatusBuffer(cl::CommandQueue &) const; ///< Map OpenCL status buffer to host
        void unmapStatusBuffer(cl::CommandQueue &);     ///< Unmap OpenCL status buffer from host
        void ensureStatusBufferOnHost();                ///< Ensure the tile status data is on the host
    };

    /**
    * %RasterFileHandler class for %Raster file IO. 
    */
    template <typename RTYPE, typename CTYPE>
    class RasterFileHandler {

    public:
        
        // Constructor.
        RasterFileHandler();
        
        // Copy constructor.
        RasterFileHandler(const RasterFileHandler &); 
        
        // Disable assignment operator as reference cannot be initialised.
        void operator=(RasterFileHandler const &) = delete;
        
        /**
        * %RasterFileHandler destructor.
        */
        ~RasterFileHandler();

        virtual bool read(std::string fileName, Raster<RTYPE, CTYPE> &r) = 0; ///< Open file for reading to %Raster
        virtual bool write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig = "") = 0; ///< Write %Raster to file

        // Data handler callback function
        void registerReadDataHandler(dataHandlerReadFunction<RTYPE, CTYPE> dataHandler_);
        void registerWriteDataHandler(dataHandlerWriteFunction<RTYPE, CTYPE> dataHandler_);
        
        /**
        * Get input data handler function.
        */
        const dataHandlerReadFunction<RTYPE, CTYPE> &getReadDataHandler() const {
            return readDataHandler;
        }
        
        /**
        * Get output data handler function.
        */
        const dataHandlerWriteFunction<RTYPE, CTYPE> &getWriteDataHandler() const {
            return writeDataHandler;
        }

        std::shared_ptr<std::fstream> fileStream; ///< %Raster file handle

    private:

        // Callback function to read or write data
        dataHandlerReadFunction<RTYPE, CTYPE> readDataHandler;
        dataHandlerWriteFunction<RTYPE, CTYPE> writeDataHandler;

    };
    
    /**
    * %RasterBase class.
    * Base class for %Raster.
    **/
    template <typename CTYPE>
    class RasterBase : public PropertyHandler {

    public:

        // Parent aliases
        using PropertyHandler::properties;
    
        // Constructor
        RasterBase();

        // Copy constructor
        RasterBase(const RasterBase &rb);

        // Destructor
        ~RasterBase();
        
        /**
        * Get %RasterBase dimensions.
        * @return dimensions of %RasterBase.
        */
        RasterDimensions<CTYPE> getRasterDimensions() { 
            return dim;
        }

        /**
        * Set %RasterBase %ProjectionParameters.
        */
        void setProjectionParameters(ProjectionParameters<CTYPE> proj_) {
            proj = proj_;
        }

        /**
        * Get %RasterBase %ProjectionParameters.
        */
        ProjectionParameters<CTYPE> getProjectionParameters() {
            return proj;
        }

        /**
        * Get bounds of %RasterBase.
        * @return bounds of %RasterBase.
        */
        BoundingBox<CTYPE> getBounds() {
            return BoundingBox<CTYPE>( { { dim.d.ox, dim.d.oy, dim.d.oz }, { dim.ex, dim.ey, dim.ez } } );
        }

        // Raster initialisation from dimensions
        virtual bool init(Dimensions<CTYPE> &dim) = 0;

        // Generate OpenCL kernel
        KernelScript generateKernelScript(std::string script, std::string kernelBlock,
            std::vector<RasterBaseRef<CTYPE> > rastersIn, RasterNullValue::Type rasterNullValue = RasterNullValue::Null);
        std::size_t buildScript(KernelScript kernelScript);

        // Run kernel on raster
        bool runTileKernel(uint32_t ti, uint32_t tj, cl::Kernel kernel, 
            std::vector<RasterBaseRef<CTYPE> > rasterBaseRefs, int startArg = 0);

        // Get string for OpenCL scripts
        virtual std::string getOpenCLTypeString() = 0;

        // Raster data check
        virtual bool hasData() = 0;

        // Raster data delete
        virtual void deleteRasterData() = 0;
        
        // Get tile data
        virtual cl::Buffer getTileNullBuffer() = 0;
        virtual void getTileDataBuffer(uint32_t ti, uint32_t tj, cl::Buffer &dataBuffer) = 0;
        virtual void getTileReduceBuffer(uint32_t ti, uint32_t tj, cl::Buffer &reductionBuffer) = 0;
        virtual void resetTileReduction(uint32_t ti, uint32_t tj) = 0;
        virtual void getTileStatusBuffer(uint32_t ti, uint32_t tj, cl::Buffer &statusBuffer) = 0;
        virtual bool getTileStatus(uint32_t ti, uint32_t tj, cl_uint &status) = 0;
        virtual bool setTileStatus(uint32_t ti, uint32_t tj, cl_uint newStatus) = 0;
        virtual TileDimensions<CTYPE> getTileDimensions(uint32_t ti, uint32_t tj) = 0;
        virtual BoundingBox<CTYPE> getTileBounds(uint32_t ti, uint32_t tj) = 0;

        // 2D raster resize
        virtual bool resize2D(uint32_t nx_, uint32_t ny_, uint32_t tox_, uint32_t toy_) = 0;
        
        // Buffer from region operation
        virtual bool createRegionBuffer(BoundingBox<CTYPE> bounds, cl::Buffer &rasterRegionBuffer, RasterDimensions<CTYPE> &rasterRegionDim) = 0;
        
        // Neighbour processing options
        virtual Neighbours::Type getRequiredNeighbours() = 0;
        virtual void setRequiredNeighbours(Neighbours::Type requiredNeighbours_) = 0;

        // Reduction processing options
        virtual Reduction::Type getReductionType() = 0;
        virtual void setReductionType(Reduction::Type reductionType_) = 0;

        // Status processing options
        virtual bool getNeedsStatus() = 0;
        virtual void setNeedsStatus(bool needsStatus_) = 0;

        // Write options
        virtual bool getNeedsWrite() = 0;
        virtual void setNeedsWrite(bool needsWrite_) = 0;

        virtual bool read(std::string fileName) = 0; //< Read file to %Raster
        virtual bool write(std::string fileName, std::string jsonConfig = "") = 0; //< Write %Raster to file

        // Variable handling
        template <typename RTYPE>
        RTYPE getVariableData(std::string name);             ///< Get variable data

        template <typename RTYPE>
        void setVariableData(std::string name, RTYPE value); ///< Set variable data

        // Script cache
        std::map<std::string, std::size_t> &getHashCache() { return kernelHashCache; }

    protected:

        // Hash cache
        std::map<std::string, std::size_t> kernelHashCache;

        // Variables data
        std::shared_ptr<Variables<CTYPE> > vars; ///% Variables data
    
        ProjectionParameters<CTYPE> proj;   ///< %Raster projection
        RTree<CTYPE> tree;                  ///< %RTree for %Tile geometry
        RasterDimensions<CTYPE> dim;        ///< %Raster dimensions
    };

    /**
    * %Raster class for one, two or three dimensional geospatial data.
    * This is the main class for gridded raster data. The raster holds a handle 
    * to a memory array as well as dimensions, cell sizes, an offset value from the 
    * origin (0, 0, 0) raster cell, projection information and a null value. 
    */
    template <typename RTYPE, typename CTYPE>
    class Raster : public RasterBase<CTYPE> {

    public:

        // Parent aliases
        using PropertyHandler::properties;
        using RasterBase<CTYPE>::tree;
        using RasterBase<CTYPE>::dim;
        using RasterBase<CTYPE>::proj;
        using RasterBase<CTYPE>::vars;

        // Constructors
        Raster();
        Raster(std::string name);

        // Destructor
        ~Raster();

        // Copy constructor
        Raster(const Raster &r);

        // Assignment operator
        Raster &operator=(const Raster &r);
        
        // Get OpenCL type string
        std::string getOpenCLTypeString();
        
        // Raster initialisation
        bool init(
            uint32_t nx_, 
            uint32_t ny_, 
            uint32_t nz_, 
            CTYPE hx_, 
            CTYPE hy_, 
            CTYPE hz_, 
            CTYPE ox_ = 0.0, 
            CTYPE oy_ = 0.0,  
            CTYPE oz_ = 0.0);

        // Raster initialisation for 2D layer
        bool init2D(
            uint32_t nx_, 
            uint32_t ny_, 
            CTYPE hx_, 
            CTYPE hy_, 
            CTYPE ox_ = 0.0, 
            CTYPE oy_ = 0.0);

        // Raster initialisation from dimensions
        bool init(Dimensions<CTYPE> &dim);

        // Raster initialisation from bounds
        bool init(const BoundingBox<CTYPE> bounds, CTYPE resolution);            
            
        // 2D raster resize
        bool resize2D(uint32_t nx_, uint32_t ny_, uint32_t tox_, uint32_t toy_);
        tileIndexSet resize2DIndexes(uint32_t nx_, uint32_t ny_, uint32_t tox_, uint32_t toy_);

        // Raster maximum reductions
        RTYPE max() const; // TODO remove
        RTYPE min() const; // TODO remove

        RTYPE reduce() const; // Raster reduction
        
        // Read and write operations
        RTYPE getCellValue(uint32_t i, uint32_t j = 0, uint32_t k = 0);
        RTYPE getNearestValue(CTYPE x, CTYPE y = 0.0, CTYPE z = 0.0);
        RTYPE getBilinearValue(CTYPE x, CTYPE y = 0.0, CTYPE z = 0.0);

        void setAllCellValues(RTYPE val);
        void setCellValue(RTYPE val, uint32_t i, uint32_t j = 0, uint32_t k = 0);
        
        TileDimensions<CTYPE> getTileDimensions(uint32_t ti, uint32_t tj);
        BoundingBox<CTYPE> getTileBounds(uint32_t ti, uint32_t tj);
        void getTileDataBuffer(uint32_t ti, uint32_t tj, cl::Buffer &dataBuffer);
        void getTileReduceBuffer(uint32_t ti, uint32_t tj, cl::Buffer &reductionBuffer);
        void resetTileReduction(uint32_t ti, uint32_t tj);
        void getTileStatusBuffer(uint32_t ti, uint32_t tj, cl::Buffer &statusBuffer);
        void getTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE>& data);
        bool setTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE>& data);
        bool getTileStatus(uint32_t ti, uint32_t tj, cl_uint &status);
        bool setTileStatus(uint32_t ti, uint32_t tj, cl_uint newStatus);
        bool getResetTileStatus(uint32_t ti, uint32_t tj, cl_uint &status, cl_uint newStatus);
        bool setAllTileCellValues(uint32_t ti, uint32_t tj, RTYPE val);
        cl::Buffer getTileNullBuffer();

        /**
        * Check if %Raster has data.
        * @return true if Raster contains data.
        */
        bool hasData() { return !tiles.empty(); }

        // Delete tile data
        void deleteRasterData();

        // Raster region
        Raster<RTYPE, CTYPE> region(BoundingBox<CTYPE> bounds);

        // Map vector onto raster
        bool mapVector(Vector<CTYPE> &v, 
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon,
            std::string widthPropertyName = std::string());
            
        // Map vector onto raster tile
        bool mapTileVector(uint32_t ti, uint32_t tj, Vector<CTYPE> &v, 
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon,
            std::string widthPropertyName = std::string());

        bool rasterise(Vector<CTYPE> &v, 
            std::string script = std::string(),
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon);

        // Vectorise raster
        Vector<CTYPE> vectorise(std::vector<RTYPE> countourValues, RTYPE noDataValue = getNullValue<RTYPE>());

        // Friend functions
        friend bool operator==<RTYPE, CTYPE>(const Raster<RTYPE, CTYPE> &, const Raster<RTYPE, CTYPE> &);

        bool read(std::string fileName); //< Read file to %Raster
        bool write(std::string fileName, std::string jsonConfig = ""); //< Write %Raster to file

        /**
        * Directly set input file handler
        * @param fileHandler to set.
        */
        void setFileInputHandler(std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerIn_) {
            fileHandlerIn = fileHandlerIn_;
        }

        /**
        * Directly set output file handler
        * @param fileHandler to set.
        */
        void setFileOutputHandler(std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerOut_) {
            fileHandlerOut = fileHandlerOut_;
        }

        /**
        * Get whether %Raster needs continuous data at boundaries.
        * @return true if continuous data is needed, false otherwise.
        */
        Neighbours::Type getRequiredNeighbours() { return requiredNeighbours; }

        /**
        * Set whether %Raster needs continuous data at boundaries.
        */
        void setRequiredNeighbours(Neighbours::Type requiredNeighbours_) { requiredNeighbours = requiredNeighbours_; }

        /**
        * Get whether %Raster requires reductions.
        * @return true if reductions are needed, false otherwise.
        */
        Reduction::Type getReductionType() { return reductionType; }

        /**
        * Set whether %Raster requires reductions.
        */
        void setReductionType(Reduction::Type reductionType_) { reductionType = reductionType_; }

        /**
        * Get whether %Raster requires status.
        * @return true if status are needed, false otherwise.
        */
        bool getNeedsStatus() { return needsStatus; }

        /**
        * Set whether %Raster requires status.
        */
        void setNeedsStatus(bool needsStatus_) { needsStatus = needsStatus_; }

        /**
        * Get whether %Raster needs data written back in kernel.
        * @return true if write data is needed, false otherwise.
        */
        bool getNeedsWrite() { return needsWrite; }

        /**
        * Set whether %Raster requires write in kernel.
        */
        void setNeedsWrite(bool needsWrite_) { needsWrite = needsWrite_; }

    protected:
    
        // Data mutex
        mutable std::mutex mtx;

        // Tile data
        std::vector<TilePtr<RTYPE, CTYPE> > tiles; ///< %Tile array 
        Neighbours::Type requiredNeighbours;       ///< Raster neighbours need to be taken into account during tile handling
        bool needsStatus;                          ///< Whether status bits need to be taken into account during tile handling
        bool needsWrite;                           ///< Whether data needs to be written back to buffer in kernel
        TilePtr<RTYPE, CTYPE> nullTile;            ///< Special tile containing null values for boundaries
        Reduction::Type reductionType;             ///< Type of reduction required for %Raster
        
        // Raw pointer access
        TilePtr<RTYPE, CTYPE> getTile(uint32_t ti, uint32_t tj);

        // Raster file handlers
        std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerIn;
        std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerOut;

        // Region cache buffers
        std::map<std::size_t, cl::Buffer> regionBufferCache;

        // Buffer from region operation
        bool createRegionBuffer(BoundingBox<CTYPE> bounds, cl::Buffer &rasterRegionBuffer, RasterDimensions<CTYPE> &rasterRegionDim);
    };
}

#endif
