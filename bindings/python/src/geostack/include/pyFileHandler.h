/* This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <functional>
#include <string>
#include <vector>
#include <memory>
#include "gs_raster.h"

namespace Geostack
{

namespace pyGeostack
{

typedef struct pyException
{
    int rc;
    std::string errMessage;
} pyException;

template <typename RTYPE, typename CTYPE>
class pyFileHandler : public RasterFileHandler<RTYPE, CTYPE>
{
public:
    using data_reader = pyException (*)(void *, TileDimensions<CTYPE>,
        std::vector<RTYPE> &);
    pyFileHandler(data_reader py_f, void *py_class,
        std::string filename);
    bool read(std::string fileName, Raster<RTYPE, CTYPE> &r_);
    bool write(std::string fileName, Raster<RTYPE, CTYPE> &r_,
        std::string jsonConfig);
    void dataFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v);
    data_reader py_function;
    void *py_class_obj;
    std::string fileName;
    std::string getFileName();
};

template <typename CTYPE>
class pyFileHandler_i : public RasterFileHandler<uint32_t, CTYPE>
{
public:
    using data_reader_i = pyException (*)(void *, TileDimensions<CTYPE>,
        std::vector<uint32_t> &);
    pyFileHandler_i(data_reader_i py_f, void *py_class,
        std::string filename);
    bool read(std::string fileName, Raster<uint32_t, CTYPE> &r_);
    bool write(std::string fileName, Raster<uint32_t, CTYPE> &r_,
        std::string jsonConfig);
    void dataFunction(TileDimensions<CTYPE> tdim, std::vector<uint32_t> &v);
    data_reader_i py_function;
    void *py_class_obj;
    std::string fileName;
    std::string getFileName();
};

template <typename CTYPE>
void add_ref_to_vec(std::vector<RasterBaseRef<CTYPE>> &v,
                    RasterBase<CTYPE> &rf);

template <typename RTYPE, typename CTYPE>
void add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<CTYPE>>> &v,
                           std::shared_ptr<Raster<RTYPE, CTYPE>> &rf);

} // namespace pyGeostack
} // namespace Geostack
