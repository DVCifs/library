# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import ctypes
import numpy as np
from ._cy_network_flow import cyNetworkFlowSolver_d, cyNetworkFlowSolver_f
from ..vector import vector

__all__ = ["NetworkFlowSolver"]


class NetworkFlowSolver:
    def __init__(self, dtype=np.float32):
        if dtype is None or dtype == np.float32:
            self._handle = cyNetworkFlowSolver_f()
            self.dtype = np.float32
        elif dtype == np.float64:
            self._handle = cyNetworkFlowSolver_d()
            self.dtype = np.float64

    def init(self, input_vector, jsonConfig):
        if not isinstance(input_vector, vector.Vector):
            raise TypeError("input_vector should be an instance of vector class")
        if not isinstance(jsonConfig, (str, bytes)):
            raise TypeError("jsonConfig should be string or bytes")

        if input_vector._dtype != self.dtype:
            raise TypeError("Mismatch between type of network flow solver and input vector")

        if isinstance(jsonConfig, str):
            self._handle.init_solver(input_vector._handle, jsonConfig.encode('UTF-8'))
        elif isinstance(jsonConfig, bytes):
            self._handle.init_solver(input_vector._handle, jsonConfig)

    def run(self):
        out = self._handle.run()
        return out

    def getNetwork(self):
        out = self._handle.getNetwork()
        return vector.Vector._from_vector(out)

    def __repr__(self):
        return "<class 'geostack.solvers.%s'>" % self.__class__.__name__