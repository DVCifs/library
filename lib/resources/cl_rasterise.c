/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
// Aligned access
#define _pval2D_a(R, _i, _j) (((R)+(_i)+(_j)*_dim.mx))
#define _val2D_a(R, _i, _j) (*((R)+(_i)+(_j)*_dim.mx))

typedef struct DimensionsStruct {

    uint nx; ///< Number of cells in x dimension
    uint ny; ///< Number of cells in y dimension
    uint nz; ///< Number of cells in z dimension
    REAL hx; ///< Spacing in x dimension
    REAL hy; ///< Spacing in y dimension
    REAL hz; ///< Spacing in z dimension
    REAL ox; ///< Start coordinate in x dimension
    REAL oy; ///< Start coordinate in y dimension
    REAL oz; ///< Start coordinate in z dimension
    uint mx; ///< Number of cells stored in memory for x dimension
    uint my; ///< Number of cells stored in memory for y dimension

} __attribute__ ((aligned (8))) Dimensions;

#ifndef Coordinate_DEF
#define Coordinate_DEF
typedef struct CoordinateStruct {

    // Coordinates
    REAL p, q, r, s;
    
} __attribute__ ((aligned (8))) Coordinate;
#endif

/**
* Rasterise points.
* @param _u %Tile data.
* @param _dim %Dimensions structure.
* @param _c Array of coordinates.
* @param _n Number of coordinates.
*/
__kernel void rasterise2DPoint(
    __global TYPE *_u,
    const Dimensions _dim,
    __global Coordinate *_c,
    const uint _n/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = get_global_id(0);
    uint count = 0;

    // Check limits
    if (index < _n) {
    
/*__WRITE__
        // Set default output
        TYPE output = noData_TYPE;
__WRITE__*/
    
        // Get coordinate
        Coordinate _pc = *(_c+index);
        
/*__PROJECT__
        // Convert to raster projection
        convert(&_pc, _rproj, _vproj);
__PROJECT__*/

        // Convert to raster coordinates
        _pc.p = (_pc.p-_dim.ox)/_dim.hx;
        _pc.q = (_pc.q-_dim.oy)/_dim.hy;

        // Check position
        if (_pc.p < 0.0 || _pc.q < 0.0 || _pc.p >= (REAL)_dim.nx || _pc.q >= (REAL)_dim.ny) {
            return;
        }
        
        // Get coordinates
        size_t _i = (size_t)_pc.p;
        size_t _j = (size_t)_pc.q;
        
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox; 
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------
        
/*__WRITE__
        // Write to raster
        _val2D_a(_u, _i, _j) = output;
__WRITE__*/
    }
}

/**
* Rasterise line strings.
* @param _u %Tile data.
* @param _dim %Dimensions structure.
* @param _c Array of coordinates.
* @param _o Array of offsets denoting the start of each line string.
* @param _n Number of line strings.
*/
__kernel void rasterise2DLineString(
    __global TYPE *_u,
    const Dimensions _dim,
    __global Coordinate *_c, 
    __global uint *_o,
    const uint _ln/*__ARGS__*/
) {

    // Get line index
    const size_t index = get_global_id(0);
    uint count = 0;

    // Check limits
    if (index < _ln) {
    
/*__WRITE__
        // Set default output
        TYPE output = noData_TYPE;
__WRITE__*/

        // Get offsets
        size_t soff = index > 0 ? _o[index-1]+1 : 0;
        size_t eoff = _o[index]+1; 

        // Get coordinate
        Coordinate _lc = *(_c+soff);

/*__PROJECT__
        // Convert to raster projection
        convert(&_lc, _rproj, _vproj);
__PROJECT__*/

        // Convert to raster coordinates
        int sx, ex = (int)((_lc.p-_dim.ox)/_dim.hx);
        int sy, ey = (int)((_lc.q-_dim.oy)/_dim.hy);
    
        // Loop over lines
        for (size_t l = soff+1; l < eoff; l++) {
        
            // Get coordinates
            sx = ex;
            sy = ey;
            _lc = *(_c+l);            

/*__PROJECT__
            // Convert to raster projection
            convert(&_lc, _rproj, _vproj);
__PROJECT__*/

            // Convert to indexes
            ex = (int)((_lc.p-_dim.ox)/_dim.hx);
            ey = (int)((_lc.q-_dim.oy)/_dim.hy);

            // Calculate line points
            int dx = ex-sx;
            int dy = ey-sy;
            if (dx == 0 && dy == 0) {

                // Special case for single point
                if (sx >= 0 && sx < _dim.nx && sy >= 0 && sy < _dim.ny) {
                
                    // Get coordinates
                    int _i = sx;
                    int _j = sy;

                    const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox; 
                    const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;

                    // ---------------------------------
                    // User defined code
/*__CODE__*/
                    // ---------------------------------
                     
/*__WRITE__
                    // Write to raster
                    _val2D_a(_u, _i, _j) = output;
__WRITE__*/
                }

            } else {

                if (abs(dy) <= abs(dx) && ((sx > 0 && sx < _dim.nx) || (ex > 0 && ex < _dim.nx))) {

                    // Step in x
                    REAL m = (REAL)dy/(REAL)dx;
                    int inc = (dx > 0) ? 1 : -1;
                    for (int _i = sx ; _i != ex ; _i += inc) {
                        int _j = sy+(int)(m*(_i-sx));
                        if (_i >= 0 && _i < _dim.nx && _j >=0 && _j < _dim.ny) {

                            // Get coordinates
                            const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
                            const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;

                            // ---------------------------------
                            // User defined code
/*__CODE__*/
                            // ---------------------------------
                            
/*__WRITE__
                            // Write to raster
                            _val2D_a(_u, _i, _j) = output;
__WRITE__*/
                        }
                    }
                } else if (abs(dy) > abs(dx) && ((sy > 0 && sy < _dim.ny) || (ey > 0 && ey < _dim.ny))) {

                    // Step in y
                    REAL m = (REAL)dx/(REAL)dy;
                    int inc = (dy > 0) ? 1 : -1;
                    for (int _j = sy ; _j != ey ; _j += inc) {
                        int _i = sx+(int)(m*(_j-sy));
                        if (_i >= 0 && _i < _dim.nx && _j >=0 && _j < _dim.ny) {

                            // Get coordinates
                            const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
                            const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;

                            // ---------------------------------
                            // User defined code
/*__CODE__*/
                            // ---------------------------------

/*__WRITE__
                            // Write to raster
                            _val2D_a(_u, _i, _j) = output;
__WRITE__*/
                        }
                    }
                }
            }
        }
    }
}

/**
* Rasterise polygons.
* @param _u %Tile data.
* @param _dim %Dimensions structure.
* @param _c Array of coordinates.
* @param _o Array of offsets denoting the start of each polygon, must include terminator of 0.
* @param _b Array of polygon bounding boxes as coordinates.
* @param _n Total number of polygons
*/
__kernel void rasterise2DPolygon(
    __global TYPE *_u,
    const Dimensions _dim,
    __global REALVEC4 *_c,
    __global int *_o,
    __global REALVEC4 *_b,
    const uint _n/*__ARGS__*/
) {
      
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);

/*__WRITE__
    // Set default output
    TYPE output = noData_TYPE;
__WRITE__*/

    // Check limits
    bool isValid = _n != 0 && _i < _dim.nx && _j < _dim.ny;
    if (isValid) {
        
        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REALVEC2 _p = (REALVEC2)(x, y);        

/*__PROJECT__
        // Convert to vector projection
        convert(&_p, _vproj, _rproj);
__PROJECT__*/

        // Internal variables
        uint count = 0;     // Count of polygons in cell
        int _sign = 1;      // Sign of winding, 0 if outside polygon
        uint _k = 0;        // Coordinate offset
        uint _poly = 0;     // Total polygon count
        uint _top_poly = 0; // Top-level polygon count

        // Vector coordinates
        REALVEC2 c0, c1;

        // Loop over polygons
        do {

            // Loop over sub-polygons
            do {

                // Process if point is within polygon bounding box
                if (_p.x >= _b[_poly*2].x && _p.y >= _b[_poly*2].y && _p.x <= _b[_poly*2+1].x && _p.y <= _b[_poly*2+1].y) {

                    // Check if point is within polygon
                    int _winding = 0;
                    uint _k_next = _k+abs(_o[_poly])-1;

                    while (_k < _k_next) {
            
                        // Get coordinates
                        c0 = _c[_k].xy;
                        c1 = _c[_k+1].xy;

                        // Get segment
                        REALVEC2 l = c1-c0;
                        REALVEC2 v = _p-c0;

                        // Check _winding (http://geomalgorithms.com/a03-_inclusion.html)
                        if (c0.y <= _p.y) {
                            if (c1.y > _p.y && l.x*v.y > v.x*l.y)
                                _winding++;
                        } else {
                            if (c1.y <= _p.y && l.x*v.y < v.x*l.y)
                                _winding--;
                        }

                        // Increment coordinate
                        _k++;
                    }

                    // Increment coordinate
                    _k++;
            
                    // If the point is inside any polygon, flip the sign
                    if (_winding != 0) {
                        _sign = -_sign;
                    }

                } else {

                    // Skip polygon
                    _k+=abs(_o[_poly]);
                }

                // Increment polygon counter
                _poly++;

            } while(_o[_poly] < 0);

            // Increment top-level polygon counter
            _top_poly++;

            // Set index if any polygons are found
            if (_sign < 0) {
                uint index = _top_poly-1;
                count++;

                // ---------------------------------
                // User defined code
/*__CODE__*/
                // ---------------------------------
            }

            // Reset sign
            _sign = 1;

        } while( _poly < _n);
    }

    // Barrier before write
    barrier(CLK_GLOBAL_MEM_FENCE);
    
/*__WRITE__
    // Write if output is non-nodata value
    if (output == output && isValid) {
        
        // Write to raster
        _val2D_a(_u, _i, _j) = output;
    }
__WRITE__*/
}