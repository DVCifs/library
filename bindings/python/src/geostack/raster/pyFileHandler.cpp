/* This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <exception>
#include <memory>
#include "pyFileHandler.h"
#include "gs_raster.h"

namespace Geostack
{

namespace pyGeostack
{

template <typename RTYPE, typename CTYPE>
pyFileHandler<RTYPE, CTYPE>::pyFileHandler(data_reader py_f,
                    void *py_class, std::string filename) :
                    RasterFileHandler<RTYPE, CTYPE>(), py_function(py_f),
                    py_class_obj(py_class), fileName(filename)
{
    RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(
        std::bind(&pyFileHandler<RTYPE, CTYPE>::dataFunction,
                  this, std::placeholders::_1,
                  std::placeholders::_2));
};

template <typename RTYPE, typename CTYPE>
bool pyFileHandler<RTYPE, CTYPE>::read(std::string fileName,
    Raster<RTYPE, CTYPE> &r_)
{
    bool out = false;
    return out;
}

template <typename RTYPE, typename CTYPE>
bool pyFileHandler<RTYPE, CTYPE>::write(std::string fileName,
                                        Raster<RTYPE, CTYPE> &r_,
                                        std::string jsonConfig)
{
    bool out = false;
    return out;
}

template <typename RTYPE, typename CTYPE>
std::string pyFileHandler<RTYPE, CTYPE>::getFileName()
{
    return fileName;
}

template <typename RTYPE, typename CTYPE>
void pyFileHandler<RTYPE, CTYPE>::dataFunction(TileDimensions<CTYPE> tdim,
                                               std::vector<RTYPE> &vec)
{
    pyException outException;
    outException = py_function(py_class_obj, tdim, vec);
    if (outException.rc == -1)
    {
        throw std::runtime_error(outException.errMessage);
    }
    else if (outException.rc == -2)
    {
        throw std::out_of_range(outException.errMessage);
    }
    else if (outException.rc == -3)
    {
        throw std::invalid_argument(outException.errMessage);
    }
    else if (outException.rc == -4)
    {
        std::cout << outException.errMessage << std::endl;
        throw std::bad_cast();
    }
    else if (outException.rc == -5)
    {
        throw std::invalid_argument(outException.errMessage);
    }
    else if (outException.rc == -6)
    {
        throw std::invalid_argument(outException.errMessage);
    };
}

template <typename CTYPE>
pyFileHandler_i<CTYPE>::pyFileHandler_i(data_reader_i py_f,
                void *py_class, std::string filename) :
                RasterFileHandler<uint32_t, CTYPE>(), py_function(py_f),
                py_class_obj(py_class), fileName(filename)
{
    RasterFileHandler<uint32_t, CTYPE>::registerReadDataHandler(
        std::bind(&pyFileHandler_i<CTYPE>::dataFunction,
                  this, std::placeholders::_1,
                  std::placeholders::_2));
};

template <typename CTYPE>
bool pyFileHandler_i<CTYPE>::read(std::string fileName,
                                  Raster<uint32_t, CTYPE> &r_)
{
    bool out = false;
    return out;
}

template <typename CTYPE>
bool pyFileHandler_i<CTYPE>::write(std::string fileName,
                                   Raster<uint32_t, CTYPE> &r_,
                                   std::string jsonConfig)
{
    bool out = false;
    return out;
}

template <typename CTYPE>
std::string pyFileHandler_i<CTYPE>::getFileName()
{
    return fileName;
}

template <typename CTYPE>
void pyFileHandler_i<CTYPE>::dataFunction(TileDimensions<CTYPE> tdim,
                                          std::vector<uint32_t> &vec)
{
    pyException outException;
    outException = py_function(py_class_obj, tdim, vec);
    if (outException.rc == -1)
    {
        throw std::runtime_error(outException.errMessage);
    }
    else if (outException.rc == -2)
    {
        throw std::out_of_range(outException.errMessage);
    }
    else if (outException.rc == -3)
    {
        throw std::invalid_argument(outException.errMessage);
    }
    else if (outException.rc == -4)
    {
        std::cout << outException.errMessage << std::endl;
        throw std::bad_cast();
    }
    else if (outException.rc == -5)
    {
        throw std::invalid_argument(outException.errMessage);
    }
    else if (outException.rc == -6)
    {
        throw std::invalid_argument(outException.errMessage);
    };
}

template <typename CTYPE>
void add_ref_to_vec(std::vector<RasterBaseRef<CTYPE>> &v,
                    RasterBase<CTYPE> &rf)
{
    v.emplace_back(std::ref(rf));
}

template <typename RTYPE, typename CTYPE>
void add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<CTYPE>>> &v,
                           std::shared_ptr<Raster<RTYPE, CTYPE>> &rf)
{
    v.emplace_back(rf);
}

//RTYPE float, CTYPE float
template class pyFileHandler<float, float>;

//RTYPE uint32_t, CTYPE float
template class pyFileHandler_i<float>;

//RTYPE double, CTYPE double
template class pyFileHandler<double, double>;

//RTYPE uint32_t, CTYPE double
template class pyFileHandler_i<double>;

} // namespace pyGeostack

template void pyGeostack::add_ref_to_vec(std::vector<RasterBaseRef<double>> &v,
                                         RasterBase<double> &rf);
template void pyGeostack::add_ref_to_vec(std::vector<RasterBaseRef<float>> &v,
                                         RasterBase<float> &rf);
template void pyGeostack::add_ref_to_vec(std::vector<RasterBaseRef<uint32_t>> &v,
                                         RasterBase<uint32_t> &rf);

template void pyGeostack::add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<double>>> &v,
                                    std::shared_ptr<Raster<double, double>> &rf);
template void pyGeostack::add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<float>>> &v,
                                    std::shared_ptr<Raster<float, float>> &rf);
template void pyGeostack::add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<float>>> &v,
                                    std::shared_ptr<Raster<uint32_t, float>> &rf);
template void pyGeostack::add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<double>>> &v,
                                    std::shared_ptr<Raster<uint32_t, double>> &rf);

} // namespace Geostack
