# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""
geostack
==========

A python interface to Geostack, a high performance geospatial analytics 
library, with a focus on providing a generic framework to perform 
analytics on a variety of geospatial datasets.

The main classes in geostack are: class:`geostack.vector.Vector`, 
`geostack.raster.Raster`

To use these classes, you will need compile and install the c++ geostack 
library and cython interface to the c++ library.

"""
from . import core
from . import dataset
from . import io
from . import raster
from . import readers
from . import runner
from . import series
from . import solvers
from . import utils
from . import vector
from . import writers