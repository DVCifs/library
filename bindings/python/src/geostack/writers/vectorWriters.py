# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import sys
import os.path as pth
import numpy as np
import json
from copy import deepcopy

from ..dataset import HAS_GPD, RequireLib
import warnings

if HAS_GPD:
    import geopandas as gpd
    import pandas as pd

from ..vector import vector
from ..io import vectorToGeoJson
from ..core import projection
import time


__all__ = ["to_geopandas"]


@RequireLib("geopandas")
def to_geopandas(vector_object, file_path):
    """Create geopandas object from Vector object.
    """
    _check_input_args(vector_object, file_path)
    vec_geo_interface = json.loads(vectorToGeoJson(vector_object))
    vec_geo_interface = _add_props(vec_geo_interface)
    out = gpd.GeoDataFrame.from_features(vec_geo_interface)
    return out


def _check_input_args(vector_object, file_path):
    if not isinstance(vector_object, vector.Vector):
        raise TypeError("Input vector object should be an instance of Vector")
    if file_path is not None:
        if not isinstance(file_path, str):
            raise TypeError("Input file path should be a string")
        if not pth.isdir(pth.dirname(file_path)):
            raise ValueError(f"path {pth.dirname(file_path)} doesn't exist")


def _add_props(inp_geo_interface):
    inp_geo_interface = deepcopy(inp_geo_interface)
    for i, item in enumerate(inp_geo_interface['features'], 0):
        if 'properties' not in item:
            inp_geo_interface['features'][i]['properties'] = {}
    return inp_geo_interface