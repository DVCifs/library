# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import sys
import importlib
from . import vector
from .. import io
from ..dataset import import_or_skip
import warnings

__all__ = ["from_gdal", "from_geopandas", "from_shapely", "from_fiona", 
    "from_shapefile"]

global HAS_SHAPELY, HAS_FIONA, HAS_GEOPANDAS, HAS_OGR, HAS_SHAPEFILE

shapely, HAS_SHAPELY = import_or_skip("shapely")
shapefile, HAS_SHAPEFILE = import_or_skip("shapefile")
fiona, HAS_FIONA = import_or_skip("fiona")
geopandas, HAS_GEOPANDAS = import_or_skip("geopandas")
ogr, HAS_OGR = import_or_skip("ogr", package="osgeo")

class RequireLib:
    def __init__(self, libname):
        self.libname = libname
    
    def _dummy_function(self):
        warnings.warn(f"library {self.libname} is not installed", ImportWarning)

    def __call__(self, input_function, *args, **kwargs):

        def inner_func(*args, **kwargs):
            if globals()[f"HAS_{self.libname.upper()}"]:
                return input_function(*args, **kwargs)
            else:
                return self._dummy_function()
        return inner_func

@RequireLib("ogr")
def from_gdal(file_handle):
    '''Use gdal library to reader the vector file and parse into a Geostack
       vector object.
    '''
    if isinstance(file_handle, str):
        if pth.exists(file_handle) and pth.isfile(file_handle):

            if pth.basename(file_handle).split('.')[1] != "shp":
                raise TypeError("Currently, only shapefile are supported")

            driver = ogr.GetDriverByName("ESRI Shapefile")
            data_handle = driver.Open(file_handle)
    elif isinstance(file_handle, ogr.DataSource):
        data_handle = file_handle
    
    out = {}

    if isinstance(file_handle, str):
        del data_handle
    return out


@RequireLib("geopandas")
def from_geopandas(file_handle):
    '''Use geopandas library to reader the vector file and parse into a Geostack
       vector object.
    '''
    if isinstance(file_handle, str):
        if pth.exists(file_handle) and pth.isfile(file_handle):

            if pth.basename(file_handle).split('.')[1] != "shp":
                raise TypeError("Currently, only shapefile are supported")

            data_handle = geopandas.read_file(file_handle)
    elif isinstance(file_handle, geopandas.GeoDataFrame):
        data_handle = file_handle
    
    out = {}

    if isinstance(file_handle, str):
        del data_handle

    return out


@RequireLib("shapely")
def from_shapely(file_handle):
    '''Use shapely library to translate shapely vector object into a Geostack
       vector object.
    '''
    if not isinstance(file_handle, str):
        raise TypeError("shapely reader only supports shapely objects.")
    
    if isinstance(file_handle, (shapely.geometry.Point, 
        shapely.geometry.LineString, shapely.geometry.Polygon, 
        shapely.geometry.MultiPoint, shapely.geometry.MultiLineString,
        shapely.geometry.MultiPolygon, shapely.geometry.GeometryCollection, 
        shapely.geometry.collection)):
        data_handle = file_handle
    else:
        raise TypeError("only shapely.geometry, and collection of \
                shapely geometry objects are supported")
    out = {}
    return out


@RequireLib("shapefile")
def from_shapefile(file_handle):
    '''Use shapefile library to reader the vector file and parse into a Geostack
       vector object.
    '''
    if isinstance(file_handle, str):
        if pth.exists(file_handle) and pth.isfile(file_handle):

            if pth.basename(file_handle).split('.')[1] != "shp":
                raise TypeError("Currently, only shapefile are supported")

            data_handle = shapefile.Reader(file_handle)
    elif isinstance(file_handle, shapefile.Reader):
        data_handle = file_handle
    
    out = {}

    if isinstance(file_handle, str):
        del data_handle
    return out


@RequireLib("fiona")
def from_fiona(file_handle):
    '''Use fiona library to reader the vector file and parse into a Geostack
       vector object.
    '''
    if isinstance(file_handle, str):
        if pth.exists(file_handle) and pth.isfile(file_handle):

            if pth.basename(file_handle).split('.')[1] != "shp":
                raise TypeError("Currently, only shapefile are supported")

            data_handle = fiona.open(file_handle)
    elif isinstance(file_handle, fiona.collection.Collection):
        data_handle = file_handle
    
    out = {}

    if isinstance(file_handle, str):
        if not data_handle.closed:
            data_handle.close()
    return out