# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import numpy as np
from ._cy_flt import cyFlt_d_d, cyFlt_f_f, cyFlt_d_i, cyFlt_f_i
from .. import raster


class FltHandler:
    def __init__(self, dtype=np.float32):
        self._handle = None
        self.dtype = dtype
        if self.dtype == np.float64:
            self._handle = cyFlt_d_d()
        elif self.dtype == np.float32:
            self._handle = cyFlt_f_f()

##read flt to a raster
# @file_name: name of the file (string type) to be read
# @dtype: optional argument, data type of raster class instance
#
    def read(self, fileName, input_raster=None):
        '''write flt to raster file

        Parameters
        ----------
        file_name : string type
            name of the file to be read
        dtype : np.float32/np.float64 (optional)
            type of raster instance

        Returns
        -------
        out: Raster object
            An instance of raster class

        Examples
        --------
        >>> out = FltHandler()
        >>> out.read("testRasterA.flt")
        '''
        if input_raster is None:
            self._raster_handler = raster.Raster(base_type=self.dtype)
        else:
            if isinstance(input_raster, raster.Raster):
                if input_raster.base_type != self.dtype:
                    raise TypeError("Type mismatch between input raster and AsciiHandler")
                self._raster_handler = input_raster
            else:
                raise TypeError("input_raster should be an instance of raster.Raster")

        out = self._handle.read(fileName, self._raster_handler._handle)
        if not out:
            raise IOError("Unable to read file")
        return out

##write raster to flt file
# @this: an instance of raster class
# @file_name: name of the file (string type) to be written
#
    def write(self, fileName, jsonConfig, input_raster=None):
        '''write raster to flt file

        Parameters
        ----------
        this: an instance of raster class to be written out
        file_name: name of the file for writing raster, string type

        Returns
        -------
        out: integer value

        Examples
        --------
        >>> this = Raster("testRasterA", np.float32)
        >>> this.init(nx=5, ny=5, hx=1.0, hy=1.0)
        >>> file_name = "testRasterA.flt"
        >>> out = FltHandler(input_raster=this)
        >>> out.write(file_name, "")
        '''
        if input_raster is not None:
            if not isinstance(input_raster, raster.Raster):
                raise TypeError("input_raster should be an instance of raster.Raster")
            out = self._handle.write(fileName, input_raster._handle,
                    jsonConfig)
        else:
            if hasattr(self, "_raster_handler"):
                out = self._handle.write(fileName, self.raster._handle,
                        jsonConfig)
            else:
                raise RuntimeError("Need input_raster or a raster object in FltHandler")
        if not out:
            raise IOError("Unable to write file")
        return out

    @property
    def raster(self):
        if hasattr(self, "_raster_handler"):
            return self._raster_handler

    @raster.setter
    def raster(self, input_raster):
        if not isinstance(input_raster, raster.Raster):
            raise TypeError("input_raster should be an instance of raster.Raster")
        if self.dtype != input_raster.base_type:
            raise TypeError("Mismatch between data type of FltHandler and input_raster")
        self._raster_handler = input_raster

    @staticmethod
    def to_raster(fileName, dtype=np.float32):
        """
        Examples
        --------
        >>> test = Raster()
        >>> FltHandler.to_raster(test, "testRasterA.flt")
        """
        flt_handler = FltHandler(dtype=np.float32)
        out = flt_handler.read(fileName)
        return flt_handler

    @staticmethod
    def from_raster(output_raster, fileName, jsonConfig):
        """
        Examples
        --------
        >>> test = Raster(name="testRasterA")
        >>> test.init(nx=5, ny=5, hx=1.0, hy=1.0)
        >>> file_name = "testRasterA.flt"
        >>> FltHandler.from_raster(test, file_name, "")
        """
        if isinstance(output_raster, raster.Raster):
            flt_handler = FltHandler(dtype=output_raster.base_type)
            out = flt_handler.write(fileName, jsonConfig,
                    input_raster=output_raster)
            return out
        else:
            raise TypeError("output_raster should be an instance of raster.Raster class")

    @staticmethod
    def readFltFile(fileName, dtype=np.float32):
        """
        Examples
        --------
        >>> file_name = "testRasterA.flt"
        >>> out = FltHandler.readFltFile(file_name)
        """
        flt_handler = FltHandler(dtype=dtype)
        out = flt_handler.read(fileName)
        return flt_handler

    def __repr__(self):
        return "<class 'geostack.io.%s'>" %self.__class__.__name__
