/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_LEVEL_SET_SOLVER_H
#define GEOSTACK_LEVEL_SET_SOLVER_H

#include <string>
#include <vector>
#include <set>

#include "date.h"
#include "gs_raster.h"

// Link to external resources
extern const char R_cl_date_head_c[];
extern const uint32_t R_cl_date_head_c_size;
extern const char R_cl_level_set_advect_c[];
extern const uint32_t R_cl_level_set_advect_c_size;
extern const char R_cl_level_set_head_c[];
extern const uint32_t R_cl_level_set_head_c_size;
extern const char R_cl_level_set_init_c[];
extern const uint32_t R_cl_level_set_init_c_size;
extern const char R_cl_level_set_build_c[];
extern const uint32_t R_cl_level_set_build_c_size;
extern const char R_cl_level_set_update_c[];
extern const uint32_t R_cl_level_set_update_c_size;
extern const char R_cl_level_set_reinit_c[];
extern const uint32_t R_cl_level_set_reinit_c_size;

namespace Geostack
{    
    /**
    * %LevelSetParameters structure
    */
    template <typename TYPE>
    struct alignas(8) LevelSetParameters {
    
        TYPE time;           // Current time
        TYPE dt;             // Current time step
        TYPE dtLast;         // Current time step
        TYPE maxSpeed;       // Maximum speed in domain
        TYPE area;           // Area within perimeter
        TYPE bandWidth;      // Width of narrow band in world units
        TYPE JulianDate;     // Current Julian date
        TYPE JulianFraction; // Julian fraction for current day
    };

    /**
    * %LevelSet layer types.
    */
    namespace LevelSetLayers {
        enum Type {
            Distance,           ///< Distance raster
            DistanceUpdate,     ///< Distance update raster
            Rate,               ///< Time integration rate raster
            Speed,              ///< Speed raster
            Arrival,            ///< Arrival time raster
            Mask,               ///< Mask raster
            Advect_x,           ///< Advection vector x component
            Advect_y,           ///< Advection vector y component
            //StartTime,          ///< Start time raster
            //StartTimeUpdate,    ///< Start time update raster
            LevelSetLayers_END  ///< Placeholder for count of layers
        };
    }	
	
    /**
    * %LevelSet class for perimeter growth. 
    */
    template <typename TYPE>
    class LevelSet {

        public:
        
            /**
            * LevelSet constructor
            */
            LevelSet():initialised(false), p(), resolution((TYPE)0.0) { }

            // Initialisation
            bool init(
                std::string jsonConfig, 
                Vector<TYPE> &sources_, 
                Vector<TYPE> &mask_,
                std::shared_ptr<Variables<TYPE> > &variables_,
                std::vector<RasterBasePtr<TYPE> > &inputLayers_,
                std::vector<RasterBasePtr<TYPE> > &outputLayers_);

            // Execution
            bool step();            

            /**
            * Return level set classification %Raster
            */
            Raster<uint32_t, TYPE> &getClassification() { 
                return classification;
            }

            /**
            * Return level set distance %Raster
            */
            Raster<TYPE, TYPE> &getDistance() { 
                return layers[LevelSetLayers::Distance];
            }

            /**
            * Return arrival time %Raster
            */
            Raster<TYPE, TYPE> &getArrival() { 
                return layers[LevelSetLayers::Arrival];
            }

            /**
            * Return advection x-component %Raster
            */
            Raster<TYPE, TYPE> &getAdvect_x() { 
                return layers[LevelSetLayers::Advect_x];
            }

            /**
            * Return advection y-component %Raster
            */
            Raster<TYPE, TYPE> &getAdvect_y() { 
                return layers[LevelSetLayers::Advect_y];
            }

            /**
            * Return solver %LevelSetParameters
            */
            LevelSetParameters<TYPE> &getParameters() { 
                return p;
            }

            /**
            * Return epoch time
            */
            uint64_t getEpochMilliseconds() { 
                return currentEpochMilliseconds;
            }

        private:
            
            // Static variables
            static const TYPE CFL;                   ///< CFL ratio
            static const TYPE timeStepMin;           ///< Minimum time step
            static const TYPE timeStepMax;           ///< Maximum time step
            static const TYPE narrowBandCells;       ///< Level set narrow band width
            
            static std::string buildNormal;          ///< Definition of normal vector
            static std::string buildAdvectNormal;    ///< Definition of advective normal vector
            static std::string buildAdvectVector;    ///< Definition of advective vector
            static std::string buildAdvectDotNormal; ///< Definition of dot product of advective normal and advective vector

            // Internal variables
            LevelSetParameters<TYPE> p;              ///< Parameter structure
            TYPE resolution;                         ///< Cell size in world units
            volatile bool initialised;               ///< Solver initialised

            // Time and date variables
            bool hasStartDate;
            uint64_t currentEpochMilliseconds;
            date::sys_time<std::chrono::milliseconds> startTime;
            date::sys_time<std::chrono::milliseconds> startDay;

            // Internal raster layers
            Raster<uint32_t, TYPE> classification;   ///< Classification raster
            std::vector<Raster<TYPE, TYPE> > layers; ///< User-defined input layers

            // Internal kernels
            cl::Kernel advectKernel;                 ///< Advection kernel
            cl::Kernel initKernel;                   ///< Initialisation kernel
            cl::Kernel buildKernel;                  ///< Build kernel
            cl::Kernel updateKernel;                 ///< Update kernel
            cl::Kernel reinitKernel;                 ///< Reinitialisation kernel
            
            // Input data
            Vector<TYPE> sources;                             ///< Level set sources
            Vector<TYPE> mask;                                ///< Level set mask
            std::shared_ptr<Variables<TYPE> > variables;      ///< Pointer to user-defined variables
            std::vector<RasterBasePtr<TYPE> > inputLayers;    ///< User-defined input layers
            std::vector<RasterBasePtr<TYPE> > outputLayers;   ///< User-defined input layers

            // Internal kernel raster layers
            std::vector<RasterBaseRef<TYPE> > advectRefs;     ///< Advection kernel rasters
            std::vector<RasterBaseRef<TYPE> > initRefs;       ///< Initialisation kernel rasters
            std::vector<RasterBaseRef<TYPE> > buildRefs;      ///< Build kernel rasters
            std::vector<RasterBaseRef<TYPE> > updateRefs;     ///< Update kernel rasters
            std::vector<RasterBaseRef<TYPE> > reinitRefs;     ///< Reinitialisation kernel rasters
            std::vector<RasterBaseRef<TYPE> > reinitFlipRefs; ///< Reinitialisation kernel flipped distance rasters

            // Active tile processing
            std::set<std::pair<uint32_t, uint32_t> > activeTileMap;
            cl_uint buildActiveTiles(const RasterDimensions<TYPE> &rDim);
    };
}

#endif