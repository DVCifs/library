# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

cmake_minimum_required(VERSION 3.8)

# Get Python
if(NOT PYTHON)
    find_program(PYTHON "python")
endif()

# Building python bindings
option(CYTHON_BUILD_FLAG "Cythonize pyx files" ON)
if(NOT CYTHON_BUILD_FLAG)
    set(CYTHON_COMPILE_FLAG "0")
else()
    set(CYTHON_COMPILE_FLAG "1")
endif()

# Copy source tree
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/src DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

# Write configuration files
set(SETUP_PY_IN "${CMAKE_CURRENT_BINARY_DIR}/src/setup.py.in")
set(SETUP_PY    "${CMAKE_CURRENT_BINARY_DIR}/src/setup.py")
configure_file(${SETUP_PY_IN} ${SETUP_PY})
