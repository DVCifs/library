# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import sys
from pprint import pprint
import numpy as np
import json
from .. import dataset
from .. import utils
import warnings
from .ncutils import Pydap2NC
import numbers
from abc import ABCMeta, abstractmethod
import math

warnings.filterwarnings("ignore", category=UserWarning)

if dataset.HAS_GDAL:
    from osgeo import gdal, gdalconst, osr
    os.environ['GDAL_CACHEMAX'] = "100"

if dataset.HAS_NCDF:
    import netCDF4 as nc
    if dataset.HAS_PYDAP:
        from pydap.client import open_url

if dataset.HAS_XARRAY:
    import xarray as xr

if dataset.HAS_RASTERIO:
    import rasterio as rio
    from rasterio.windows import Window

from ..raster import raster, _cy_raster
from ..runner import runner


__all__ = ["get_gdal_geotransform", "DataHandler", "GDAL_Handler",
    "NC_Handler", "XR_Handler", "RIO_Handler"]


def get_gdal_geotransform(raster_dimensions):
    """Compute geotransform using raster dimensions.

    Parameters:
    -----------
    raster_dimensions: raster.RasterDimensions/ dict
        RasterDimensions of a raster.Raster object
    
    Returns:
    --------
    out: tuple
        gdal geotransform tuple representation of raster.RasterDimensions
    
    Raises:
    -------
    TypeError: Input raster dimensions should contain key 'dim'
    """
    if isinstance(raster_dimensions, dict):
        if 'dim' in raster_dimensions:
            ox = raster_dimensions['dim']['ox']
            hx = raster_dimensions['dim']['hx']
            hy = raster_dimensions['dim']['hy']
            oy = raster_dimensions['oy']
            if hy > 0:
                hy = hy * -1
                oy = raster_dimensions['ey']
        else:
            raise TypeError("Input raster dimensions should contain key 'dim'")
    elif isinstance(raster_dimensions, raster.RasterDimensions):
            ox = raster_dimensions.ox
            hx = raster_dimensions.hx
            if raster_dimensions.hy > 0:
                hy = -1 * raster_dimensions.hy
                oy = raster_dimensions.ey
            elif raster_dimensions.hy < 0:
                hy = raster_dimensions.hy
                oy = raster_dimensions.oy
    out = (ox, hx, 0.0, oy, 0.0, hy)
    return out

@dataset.RequireLib("gdal")
def return_proj4(gdal_file):
    """Get GDAL Dataset projection as Proj4 string.

    Note:
    ----
    Unable to use with gdal, possibly due to the thread safety 
    https://gdal.org/development/rfc/rfc16_ogr_reentrancy.html

    Parameters
    ----------
    gdal_file: gdal.Dataset/str
        A GDAL Dataset projection as a proj4 string

    Examples
    --------
    >>> test = gdal.Dataset("test.tif")
    >>> out_str = return_proj4(test)
    """
    out = ""
    if isinstance(gdal_file, gdal.Dataset):
        gdal_dataset = gdal_file
    elif isinstance(gdal_file, str):
        gdal_dataset = gdal.OpenEx(gdal_file)
    
    if hasattr(gdal_dataset, "GetSpatialRef"):
        out = gdal_dataset.GetSpatialRef().ExportToProj4()
    else:
        proj = gdal_dataset.GetProjection()
        if len(proj) > 1:
            proj_ref = osr.SpatialReference()
            proj_ref.ImportFromWkt(proj)
            out = proj_ref.ExportToProj4()
    return out

class DataHandler(object, metaclass=ABCMeta):
    __slots__ = ()

    @abstractmethod
    def reader(self, *args, **kwargs):
        raise NotImplementedError("reader method is not implemented")

    @abstractmethod
    def setter(self, *args, **kwargs):
        raise NotImplementedError("setter method is not implemented")

    @abstractmethod
    def writer(self, *args, **kwargs):
        raise NotImplementedError("writer method is not implemented")

    @abstractmethod
    def time(self, *args, **kwargs):
        raise NotImplementedError("time method is not implemented")

    @abstractmethod
    def __exit__(self, *args):
        raise NotImplementedError("exit method is not implemented")

class NC_Handler(DataHandler):

    def __init__(self, fileName=None, base_type=np.float32,
        data_type=np.float32):
        # if the filename is a path to a file, ensure it's an absolute path
        if fileName is not None and 'dodsC' not in fileName:
            fileName = pth.abspath(fileName) if pth.exists(fileName) else None
        self._file_name = fileName
        self._file_handler = None
        self.is_thredds = False
        self.use_pydap = False
        self.invert_y = False
        self.data_type = data_type
        self.base_type = base_type
        self.nullValue = raster.getNullValue(data_type)

    @dataset.RequireLib("netcdf")
    def reader(self, thredds=False, use_pydap=False):
        # set thredds and use_pydap arguments
        self.is_thredds = thredds
        self.use_pydap = use_pydap

        if self._file_name is None:
            raise ValueError("file_name cannot be None")

        # check file_name and open file
        if not self.is_thredds and not self.use_pydap:
            # when file is on disc
            if isinstance(self._file_name, bytes):
                self._file_handler = nc.Dataset(self._file_name.decode(), mode='r')
            elif isinstance(self._file_name, str):
                self._file_handler = nc.Dataset(self._file_name, mode='r')
        elif self.is_thredds:
            # when using thredds
            if self.use_pydap and dataset.HAS_PYDAP:
                # open using pydap
                # pydap can be used when thredds server requires
                # authentication

                # Pydap2NC provides a netCDF4.Dataset like interface to
                # pydap.Dataset
                if isinstance(self._file_name, Pydap2NC):
                    self._file_handler = self._file_name
                elif isinstance(self._file_name, str):
                    self._file_handler = Pydap2NC(open_url(self._file_name))
                else:
                    self._file_handler = Pydap2NC(self._file_name)
            else:
                # open using netcdf4 library
                if isinstance(self._file_name, str):
                    self._file_handler = nc.Dataset(self._file_name)
                elif isinstance(self._file_name, bytes):
                    self._file_handler = nc.Dataset(self._file_name.decode())

        if isinstance(self._file_name, nc.Dataset):
            # assign nc.Dataset to file_handler when file_name is nc.Dataset
            self._file_handler = self._file_name
            # change file_name to path of file
            self._file_name = self._file_name.filepath()
        if self._file_handler is None:
            raise ValueError("file_handler cannot be identified")

        # use projection information when available in netcdf4 file
        projStr = ""
        if 'crs' in self._file_handler.variables:
            if hasattr(self._file_handler.variables['crs'], "crs_wkt"):
                projStr = utils.proj4_from_wkt(self._file_handler.variables['crs'].getncattr("crs_wkt"))
            elif hasattr(self._file_handler.variables['crs'], "spatial_ref"):
                projStr = utils.proj4_from_wkt(self._file_handler.variables['crs'].getncattr("spatial_ref"))

        # get dimensions from netcdf file
        if 'lon' in self._file_handler.dimensions:
            lon = self._file_handler.variables['lon']
            nx = self._file_handler.dimensions['lon'].size
        elif 'longitude' in self._file_handler.dimensions:
            lon = self._file_handler.variables['longitude']
            nx = self._file_handler.dimensions['longitude'].size
        elif 'x' in self._file_handler.dimensions:
            lon = self._file_handler.variables['x']
            nx = self._file_handler.dimensions['x'].size
        else:
            raise KeyError("lon/longitude/x not found in file dimensions")

        if 'lat' in self._file_handler.dimensions:
            lat = self._file_handler.variables['lat']
            ny = self._file_handler.dimensions['lat'].size
        elif 'latitude' in self._file_handler.dimensions:
            lat = self._file_handler.variables['latitude']
            ny = self._file_handler.dimensions['latitude'].size
        elif 'y' in self._file_handler.dimensions:
            lat = self._file_handler.variables['y']
            ny = self._file_handler.dimensions['y'].size
        else:
            raise KeyError("lat/latitude/y not found in file dimensions")

        time = self.time(0)

        # compute dimension input for instantiating raster.Raster
        hx = lon[1] - lon[0]
        hy = lat[1] - lat[0]

        if isinstance(lon[0], np.ma.MaskedArray):
            ox = self.data_type(lon[0].data)
        elif isinstance(lon[0], np.ndarray):
            ox = self.data_type(lon[0])
        elif isinstance(lon[0], numbers.Real):
            ox = self.data_type(lon[0])

        if hy < 0:
            self.invert_y = True
            if isinstance(lat[-1], np.ma.MaskedArray):
                oy = self.data_type(lat[-1].data)
            elif isinstance(lat[-1], np.ndarray):
                oy = self.data_type(lat[-1])
            elif isinstance(lat[0], numbers.Real):
                oy = self.data_type(lat[-1])
            hy = abs(hy)
        else:
            if isinstance(lat[0], np.ma.MaskedArray):
                oy = self.data_type(lat[0].data)
            elif isinstance(lat[0], np.ndarray):
                oy = self.data_type(lat[0])
            elif isinstance(lat[0], numbers.Real):
                oy = self.data_type(lat[0])

        return nx, ny, hx, hy, ox, oy, projStr.encode("utf-8"), time

    def writer(self, fileName, jsonConfig):
        writer_config = json.loads(jsonConfig)
        raise NotImplementedError("writer not yet implemented")

    @dataset.RequireLib("netcdf")
    def check_handler(self):
        if not isinstance(self, NC_Handler):
            raise TypeError("Unable to understand the input class instance")

        if self._file_handler is None:
            raise ValueError("file_handler cannot be identified")

        if not isinstance(self._file_handler, (nc.Dataset, Pydap2NC)):
            raise TypeError("file_handler is of incorrect type")

        if not isinstance(self._file_handler, Pydap2NC):
            if self._file_handler.filepath() != self._file_name:
                raise ValueError("Mismatch between filepath '{}' and filename '{}'".format(
                    self._file_handler.filepath(), self._file_name))

    @dataset.RequireLib("netcdf")
    def setter(self, ti, tj, tx, ty, varname, tidx):
        # Check handle
        self.check_handler()

        # Create buffer
        tile_size = raster.TileSpecifications().tileSize
        buf_arr = np.full((tile_size, tile_size), self.nullValue,
            dtype=self.data_type)

        # Get dimensions
        if 'lon' in self._file_handler.dimensions:
            lon = self._file_handler.dimensions['lon']
        elif 'longitude' in self._file_handler.dimensions:
            lon = self._file_handler.dimensions['longitude']
        elif 'x' in self._file_handler.dimensions:
            lon = self._file_handler.dimensions['x']
        else:
            raise KeyError("lon/longitude/x doesn't exist in the file")

        if 'lat' in self._file_handler.dimensions:
            lat = self._file_handler.dimensions['lat']
        elif 'latitude' in self._file_handler.dimensions:
            lat = self._file_handler.dimensions['latitude']
        elif 'y' in self._file_handler.dimensions:
            lat = self._file_handler.dimensions['y']
        else:
            raise KeyError("lat/latitude/y doesn't exist in the file")

        x_start = ti * tile_size
        x_end = min(min((ti + 1), tx) * tile_size, lon.size)

        y_start = tj * tile_size
        y_end = min(min((tj + 1), ty) * tile_size, lat.size)

        # Get variable name
        if isinstance(varname, str):
            var_name = varname
        elif isinstance(varname, bytes):
            var_name = varname.decode()

        # Get data
        if var_name in self._file_handler.variables:
            # Check data dimensions
            if len(self._file_handler.variables[var_name].shape) == 3:
                temp = self._file_handler.variables[var_name][tidx,
                    y_start:y_end, x_start:x_end]
            elif len(self._file_handler.variables[var_name].shape) == 2:
                temp = self._file_handler.variables[var_name][y_start:y_end,
                    x_start:x_end]
            else:
                raise RuntimeError("Only 2D or 2D + time is currently supported")
            if len(temp.shape) > 2:
                temp = np.squeeze(temp)

            # Get missing value
            missing_value = None
            if hasattr(self._file_handler.variables[var_name], "missing_value"):
                missing_value = getattr(self._file_handler.variables[var_name], "missing_value")
            elif hasattr(self._file_handler.variables[var_name], "_FillValue"):
                missing_value = getattr(self._file_handler.variables[var_name], "_FillValue")

            # Fill missing values
            if isinstance(temp, np.ma.MaskedArray):
                temp = np.ma.filled(temp, fill_value=self.nullValue)
            else:
                if missing_value is not None:
                    if np.can_cast(missing_value, temp.dtype):
                        try:
                            missing_value = temp.dtype.type(missing_value)
                        except Exception as e:
                            print(f"Type Casting Error: {str(e)}")
                    else:
                        missing_value = None
                if missing_value is not None:
                    temp = np.where(temp == missing_value, self.nullValue, temp)
        else:
            raise KeyError(f"Item {var_name} not in NetCDF variables: {self._file_handler.variables.keys()}")

        # Copy data to buffer
        ysize, xsize = temp.shape
        if self.invert_y:
            buf_arr[:ysize, :xsize] = temp[::-1, :].astype(self.data_type)
            return buf_arr, ti, (ty - tj)
        else:
            buf_arr[:ysize, :xsize] = temp[:, :].astype(self.data_type)
            return buf_arr, ti, tj

    @dataset.RequireLib("netcdf")
    def time(self, tidx):
        time = 0.0
        if 'time' in self._file_handler.dimensions:
            time_var = self._file_handler.variables['time']
            if tidx < time_var.size:
                time = time_var[tidx]

        return time

    @dataset.RequireLib("netcdf")
    def __exit__(self, exc_type, exc_val, exc_tb):

        if self._file_handler is None:
            raise ValueError("file_handler could not be located")
        if dataset.HAS_PYDAP:
            if not isinstance(self._file_handler, (nc.Dataset, Pydap2NC)):
                raise TypeError("file_handler should be an instance of nc.Dataset or Pydap2NC")
        else:
            if not isinstance(self._file_handler, nc.Dataset):
                raise TypeError("file_handler should be an instance of nc.Dataset")

        if not self.is_thredds and not self.use_pydap:
            if self._file_handler.isopen():
                self._file_handler.close()

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "<geostack.readers.%s>" % self.__class__.__name__

class GDAL_Handler(DataHandler):

    def __init__(self, fileName=None, base_type=np.float32,
        data_type=np.float32):
        # if the filename is a path to a file, ensure it's an absolute path
        # doesn't check for a valid extension as gdal supports an extensive
        # list of file types
        if fileName is not None and not fileName.startswith('/vsi'):
            fileName = pth.abspath(fileName) if pth.exists(fileName) else None
        self._file_name = fileName
        self._file_handler = None
        self.is_thredds = False
        self.use_pydap = False
        self.invert_y = False
        self.data_type = data_type
        self.base_type = base_type
        self.nullValue = raster.getNullValue(data_type)

    @dataset.RequireLib("gdal")
    def reader(self, *args, **kwargs):
        if args:
            if isinstance(args[0], bool):
                self.is_thredds = args[0]
        elif kwargs:
            if 'thredds' in kwargs:
                self.is_thredds = kwargs['thredds']

        if self._file_name is None:
            raise ValueError("file_name cannot be None")

        if isinstance(self._file_name, str):
            # patch file path (when needed) for gdal virtual file system
            if self.is_thredds:
                if 'vsicurl' not in self._file_name:
                    self._file_name = f"/vsicurl/{self._file_name}"
                    if self._file_name.endswith('tar') or self._file_name.endswith('tgz'):
                        raise ValueError("Compressed archives require full path in the archive")
            else:
                if 'vsicurl' in self._file_name:
                    self.is_thredds = True
            self._file_handler = gdal.Open(self._file_name)
        elif isinstance(self._file_name, gdal.Dataset):
            # assign to file_handler, when file_name is gdal.Dataset
            self._file_handler = self._file_name
            # change file_name to path of file
            self._file_name = self._file_name.GetDescription()

        # checks if gdal managed to open file
        # handles cases where the file extension is not supported by gdal
        if self._file_handler is None and isinstance(self._file_handler, str):
            raise ValueError(f"Unable to open file {self._file_name}")

        # get geotransform
        geotransform = self._file_handler.GetGeoTransform()
        # get projection wkt string as proj4 string
        projStr = return_proj4(self._file_handler.GetDescription())

        if geotransform[5] < 0:
            self.invert_y = True
        else:
            self.invert_y = False

        # Dummy time variable
        time = self.time(None)

        nx = self._file_handler.RasterXSize
        ny = self._file_handler.RasterYSize
        hx = geotransform[1]
        hy = abs(geotransform[5])
        ox = geotransform[0]
        oy = geotransform[3] + self._file_handler.RasterYSize * geotransform[5]
        return nx, ny, hx, hy, ox, oy, projStr.encode("utf-8"), time

    def writer(self, fileName, jsonConfig):
        writer_config = json.loads(jsonConfig)
        raise NotImplementedError("writer not yet implemented")

    @staticmethod
    def compute_bounds(ti, tj, nx, ny, tileSize):
        xoff = min(ti * tileSize, nx)
        y_end = ny - min(tj * tileSize, ny)
        y_start = ny - min((tj + 1) * tileSize, ny)
        yoff = min(y_start, y_end)
        xsize = min((ti + 1) * tileSize, nx) - xoff
        ysize = abs(y_start - y_end)

        return xoff, yoff, xsize, ysize

    @dataset.RequireLib("gdal")
    def check_handler(self):
        if not isinstance(self, GDAL_Handler):
            raise TypeError("Unable to understand the input class instance")

        if self._file_handler is None:
            raise ValueError("file_handler could not be located")

        if not isinstance(self._file_handler, gdal.Dataset):
            raise TypeError("file_handler is not an instance of gdal.Dataset")

        if self._file_handler.GetDescription() != self._file_name:
            raise ValueError("Mismatch between file_name and description of file in gdal.Dataset")

    @dataset.RequireLib("gdal")
    def setter(self, ti, tj, tx, ty, varname, raster_band):
        # Check handler
        self.check_handler()

        # Check band index
        if raster_band < 1:
            raise ValueError("Raster band number should be greater than 0")
        elif raster_band > self._file_handler.RasterCount + 1:
            raise ValueError("Raster band number cannot be greater than number of bands in file")

        # Get band
        raster_band = self._file_handler.GetRasterBand(raster_band)
        gdal_to_npy = dataset.gdal_dtype_to_numpy(raster_band.DataType)

        # Get missing value
        missing_value = raster_band.GetNoDataValue()
        if missing_value is not None:
            try:
                if not raster_band.DataType < 6:
                    missing_value = gdal_to_npy(missing_value)
                else:
                    if (missing_value - gdal_to_npy(missing_value)) == 0:
                        missing_value = gdal_to_npy(missing_value)
                    else:
                        missing_value = None
            except Exception as _:
                print(f"WARNING: GDAL missing value '{missing_value}'"+
                    " cannot be converted to {gdal_to_npy.__name__}")
                missing_value = None

        #below code snippet maps or upgrades gdal data to Raster type
        if gdal_to_npy not in [np.float32, np.float64, np.uint32]:
            _temp_type = GDAL_Handler.data_type_compliance(gdal_to_npy)
        else:
            _temp_type = gdal_to_npy

        if _temp_type != self.data_type:
            _temp_type = self.data_type
            # raise TypeError("Mismatch between data type of raster band and class instance")

        # Create empty buffer
        tile_size = raster.TileSpecifications().tileSize
        buf_arr = np.full((tile_size, tile_size), self.nullValue,
            dtype=self.data_type)

        # Get data bounds
        xoff, yoff, xsize, ysize = GDAL_Handler.compute_bounds(ti, tj,
            self._file_handler.RasterXSize,
            self._file_handler.RasterYSize, tile_size)

        # Read tile from data set
        temp = raster_band.ReadAsArray(xoff=xoff, yoff=yoff, win_xsize=xsize,
            win_ysize=ysize, buf_type=raster_band.DataType)

        # Check and patch data
        if missing_value is not None:
            temp = np.where(temp == missing_value, self.nullValue, temp)

        # Copy data to buffer
        ysize, xsize = temp.shape
        if self.invert_y:
            buf_arr[:ysize, :xsize] = temp[::-1, :].astype(_temp_type)
        else:
            buf_arr[:ysize, :xsize] = temp[:, :].astype(_temp_type)

        del(temp, raster_band)
        return buf_arr, ti, tj

    def time(self, *args):
        return 0.0

    @staticmethod
    def data_type_compliance(input_type):
        if input_type in [np.uint8, np.uint16]:
            return np.uint32
        elif input_type in [np.int16, np.int32]:
            return np.float32
        else:
            raise TypeError("No mapping for input data type")

    @dataset.RequireLib("gdal")
    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._file_handler is not None:
            if isinstance(self._file_handler, gdal.Dataset):
                del self._file_handler
            else:
                raise TypeError("file_handler is not an instance of gdal.Dataset")
        else:
            raise ValueError("file_handler could not be located")

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "<geostack.readers.%s>" % self.__class__.__name__

class XR_Handler(DataHandler):

    def __init__(self, fileName=None, base_type=np.float32,
        data_type=np.float32):
        # if the filename is a file, ensure it's an absolute path
        if fileName is not None and 'dodsC' not in fileName:
            fileName = pth.abspath(fileName) if pth.exists(fileName) else None
        self._file_name = fileName
        self._file_handler = None
        self.is_thredds = False
        self.use_pydap = False
        self.invert_y = False
        self.data_type = data_type
        self.base_type = base_type
        self.nullValue = raster.getNullValue(data_type)

    @dataset.RequireLib("xarray")
    def reader(self, thredds=False, use_pydap=False):
        # set thredds flag when provided
        self.is_thredds = thredds

        if self._file_name is None:
            raise ValueError("file_name cannot be None")
        # open file without decoding time
        if not self.is_thredds:
            if isinstance(self._file_name, bytes):
                self._file_handler = xr.open_dataset(self._file_name.decode(),
                    decode_cf=False, decode_times=False)
            elif isinstance(self._file_name, str):
                self._file_handler = xr.open_dataset(self._file_name,
                    decode_cf=False, decode_times=False)
        else:
            if isinstance(self._file_name, str):
                self._file_handler = xr.open_dataset(self._file_name,
                    decode_cf=False, decode_times=False)
            elif isinstance(self._file_name, bytes):
                self._file_handler = xr.open_dataset(self._file_name.decode(),
                    decode_cf=False, decode_times=False)

        if isinstance(self._file_name, xr.Dataset):
            # assign file_name to file_handler when input is xr.Dataset
            self._file_handler = self._file_name
            # change file name to path of file
            self._file_name = self._file_name._file_obj._filename
        if self._file_handler is None:
            raise ValueError("file_handler cannot be identified")

        # get projection information from the file if available
        projStr = ""
        if 'crs' in self._file_handler.variables:
            if hasattr(self._file_handler.variables['crs'], "crs_wkt"):
                projStr = utils.proj4_from_wkt(self._file_handler.variables['crs'].crs_wkt)
            elif hasattr(self._file_handler.variables['crs'], "spatial_ref"):
                projStr = utils.proj4_from_wkt(self._file_handler.variables['crs'].getncattr("spatial_ref"))

        # get dimensions of the file
        if 'lon' in self._file_handler.dims:
            lon = self._file_handler.variables['lon']
            nx = self._file_handler.dims['lon']
        elif 'longitude' in self._file_handler.dims:
            lon = self._file_handler.variables['longitude']
            nx = self._file_handler.dims['longitude']
        elif 'x' in self._file_handler.dims:
            lon = self._file_handler.variables['x']
            nx = self._file_handler.dims['x']
        else:
            raise KeyError("lon/longitude/x not found in file dimensions")

        if 'lat' in self._file_handler.dims:
            lat = self._file_handler.variables['lat']
            ny = self._file_handler.dims['lat']
        elif 'latitude' in self._file_handler.dims:
            lat = self._file_handler.variables['latitude']
            ny = self._file_handler.dims['latitude']
        elif 'y' in self._file_handler.dims:
            lat = self._file_handler.variables['y']
            ny = self._file_handler.dims['y']
        else:
            raise KeyError("lat/latitude/y not found in file dimensions")

        time = self.time(0)
        # compute dimension input for instantiating raster.Raster
        hx = float(lon[1] - lon[0])
        hy = float(lat[1] - lat[0])

        if isinstance(lon[0].values, np.ma.MaskedArray):
            ox = self.data_type(lon[0].data)
        elif isinstance(lon[0].values, np.ndarray):
            ox = self.data_type(lon[0])
        elif isinstance(lon[0].values, numbers.Real):
            ox = self.data_type(lon[0])

        if hy < 0:
            self.invert_y = True
            if isinstance(lat[-1].values, np.ndarray):
                oy = self.data_type(lat[-1])
            elif isinstance(lat[-1], np.ma.MaskedArray):
                oy = self.data_type(lat[-1].data)
            elif isinstance(lat[-1].values, numbers.Real):
                oy = self.data_type(lat[-1])
            hy = abs(hy)
        else:
            if isinstance(lat[0].values, np.ndarray):
                oy = self.data_type(lat[0])
            elif isinstance(lat[0], np.ma.MaskedArray):
                oy = self.data_type(lat[0].data)
            elif isinstance(lat[0].values, numbers.Real):
                oy = self.data_type(lat[0])
            hy = abs(hy)
        return nx, ny, hx, hy, ox, oy, projStr.encode("utf-8"), time

    @dataset.RequireLib("xarray")
    def writer(self, jsonConfig):
        writer_config = json.loads(jsonConfig)
        raise NotImplementedError("writer not yet implemented")

    @dataset.RequireLib("xarray")
    def check_handler(self):
        if not isinstance(self, XR_Handler):
            raise TypeError("Unable to understand the input class instance")

        if self._file_handler is None:
            raise ValueError("file_handler cannot be identified")

        if not isinstance(self._file_handler, xr.Dataset):
            raise TypeError("file_handler is of incorrect type")

        if self._file_handler._file_obj._filename != self._file_name:
            raise ValueError("Mismatch between filepath and filename")

    @dataset.RequireLib("xarray")
    def setter(self, ti, tj, tx, ty, varname, tidx):
        # Check handler
        self.check_handler()

        # Create empty buffer
        tile_size = raster.TileSpecifications().tileSize
        buf_arr = np.full((tile_size, tile_size), self.nullValue,
            dtype=self.data_type)

        # Get dimensions
        if 'lon' in self._file_handler.dims:
            lon = self._file_handler.dims['lon']
        elif 'longitude' in self._file_handler.dims:
            lon = self._file_handler.dims['longitude']
        elif 'x' in self._file_handler.dims:
            lon = self._file_handler.dims['x']
        else:
            raise KeyError("lon/longitude/x doesn't exist in the file")

        if 'lat' in self._file_handler.dims:
            lat = self._file_handler.dims['lat']
        elif 'latitude' in self._file_handler.dims:
            lat = self._file_handler.dims['latitude']
        elif 'y' in self._file_handler.dims:
            lat = self._file_handler.dims['y']
        else:
            raise KeyError("lat/latitude/y doesn't exist in the file")

        x_start = ti * tile_size
        x_end = min(min((ti + 1), tx) * tile_size, lon)

        y_start = tj * tile_size
        y_end = min(min((tj + 1), ty) * tile_size, lat)

        # Get variable name
        if isinstance(varname, str):
            var_name = varname
        elif isinstance(varname, bytes):
            var_name = varname.decode()

        # Get data
        if var_name in self._file_handler.data_vars:
            # Check dimensions
            if len(self._file_handler.data_vars[var_name].shape) == 3:
                temp = self._file_handler.data_vars[var_name][tidx, 
                    y_start:y_end, x_start:x_end].to_masked_array()
            elif len(self._file_handler.data_vars[var_name].shape) == 2:
                temp = self._file_handler.data_vars[var_name][y_start:y_end,
                    x_start:x_end].to_masked_array()
            else:
                raise RuntimeError("Only 2D or 2D + time is currently supported")

            if len(temp.shape) > 2:
                temp = np.squeeze(temp)
            # Patch data
            temp = np.ma.filled(temp, fill_value=self.nullValue)
        else:
            raise KeyError(f"Item {var_name} not in NetCDF variables: {self._file_handler.data_vars.keys()}")
        # Copy data to buffer
        ysize, xsize = temp.shape
        if self.invert_y:
            buf_arr[:ysize, :xsize] = temp[::-1, :].astype(self.data_type)
            return buf_arr, ti, (ty - tj)
        else:
            buf_arr[:ysize, :xsize] = temp[:, :].astype(self.data_type)
            return buf_arr, ti, tj

    @dataset.RequireLib("xarray")
    def time(self, tidx):
        self.check_handler()
        time = 0.0
        if 'time' in self._file_handler.dims:
            time_var = self._file_handler.coords['time']
            if tidx < time_var.size:
                time = time_var[tidx]
        return time

    @dataset.RequireLib("xarray")
    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._file_handler is None:
            raise ValueError("file_handler could not be located")

        if not isinstance(self._file_handler, xr.Dataset):
            raise TypeError("file_handler should be an instance of xr.Dataset")
        if not self.is_thredds and not self.use_pydap:
            self._file_handler.close()

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "<geostack.readers.%s>" % self.__class__.__name__

class RIO_Handler(DataHandler):

    def __init__(self, fileName=None, base_type=np.float32,
        data_type=np.float32):
        # if the filename is a file, ensure it's an absolute path
        if fileName is not None and not fileName.startswith('/vsi'):
            fileName = pth.abspath(fileName) if pth.exists(fileName) else None
        self._file_name = fileName
        self._file_handler = None
        self.is_thredds = False
        self.use_pydap = False
        self.invert_y = False
        self.data_type = data_type
        self.base_type = base_type
        self.nullValue = raster.getNullValue(data_type)

    @dataset.RequireLib("rasterio")
    def reader(self, *args, **kwargs):
        # check if using remote file
        if args:
            if isinstance(args[0], bool):
                self.is_thredds = args[0]
        elif kwargs:
            if 'thredds' in kwargs:
                self.is_thredds = kwargs['thredds']

        if self._file_name is None:
            raise ValueError("file_name cannot be None")
        # patch path (when needed) for gdal virtual file system
        if isinstance(self._file_name, str):
            if self.is_thredds:
                if 'vsicurl' not in self._file_name:
                    self._file_name = f"/vsicurl/{self._file_name}"
                    if self._file_name.endswith('tar') or self._file_name.endswith('tgz'):
                        raise ValueError("Compressed archives require full path in the archive")
            else:
                if 'vsicurl' in self._file_name:
                    self.is_thredds = True
            self._file_handler = rio.open(self._file_name, mode='r')
        elif isinstance(self._file_name, rio.DatasetReader):
            # assign file_name to file_handler when input is DatasetReader object
            self._file_handler = self._file_name
            # change file_name to path of file
            self._file_name = self._file_handler.files[0]

        # get projection information from the file
        projStr = ""
        if hasattr(self._file_handler, 'crs'):
            projStr = self._file_handler.crs.to_proj4()

        if self._file_handler is None:
            raise ValueError("Unable to open file %s" % self._file_name)

        # get geostranform
        geotransform = self._file_handler.get_transform()
        if geotransform[5] < 0:
            self.invert_y = True
        else:
            self.invert_y = False

        time = self.time(None)
        # compute dimension input for instantiating raster.Raster
        nx = self._file_handler.width
        ny = self._file_handler.height
        hx = geotransform[1]
        hy = abs(geotransform[5])
        ox = geotransform[0]
        oy = geotransform[3] + ny * geotransform[5]

        return nx, ny, hx, hy, ox, oy, projStr.encode("utf-8"), time

    @staticmethod
    def compute_bounds(ti, tj, nx, ny, tileSize):
        xoff = min(ti * tileSize, nx)
        y_end = ny - min(tj * tileSize, ny)
        y_start = ny - min((tj + 1) * tileSize, ny)
        yoff = min(y_start, y_end)
        xsize = min((ti + 1) * tileSize, nx) - xoff
        ysize = abs(y_start - y_end)

        return xoff, yoff, xsize, ysize

    def writer(self):
        raise NotImplementedError()

    @dataset.RequireLib("rasterio")
    def check_handler(self):
        if not isinstance(self, RIO_Handler):
            raise TypeError("Unable to understand the input class instance")

        if self._file_handler is None:
            raise ValueError("file_handler could not be located")

        if not isinstance(self._file_handler, rio.DatasetReader):
            raise TypeError("file_handler is not an instance of rio.DatasetReader")

        if self._file_handler.files[0] != self._file_name:
            raise ValueError("Mismatch between file_name and description of file in rio.DatasetReader")

    @dataset.RequireLib("rasterio")
    def setter(self, ti, tj, tx, ty, varname, raster_band):
        # Check handler
        self.check_handler()

        # Check band index
        if raster_band < 1:
            raise ValueError("Raster band number should be greater than 0")
        elif raster_band > self._file_handler.count + 1:
            raise ValueError("Raster band number cannot be greater than number of bands in file")

        # Create empty buffer
        tile_size = raster.TileSpecifications().tileSize
        buf_arr = np.full((tile_size, tile_size), self.nullValue,
            dtype=self.data_type)

        # Get data bounds
        xoff, yoff, xsize, ysize = RIO_Handler.compute_bounds(ti, tj,
            self._file_handler.width, self._file_handler.height,
            tile_size)

        # Read tile from data set
        temp = self._file_handler.read(raster_band,
            window=Window(xoff, yoff, xsize, ysize))

        # Get missing value
        missing_value = self._file_handler.nodatavals[raster_band-1]
        # get raster data type
        raster_dtype = np.dtype(self._file_handler.dtypes[raster_band-1]).type
        if missing_value is not None:
            try:
                if raster_dtype in [np.float32, np.float64, np.complex_,
                    np.complex64, np.complex128]:
                    missing_value = raster_dtype(missing_value)
                else:
                    if (missing_value - raster_dtype(missing_value)) == 0:
                        missing_value = raster_dtype(missing_value)
                    else:
                        missing_value = None
            except Exception as _:
                print(f"WARNING: Rasterio missing value '{missing_value}' "+
                    f"cannot be converted to {raster_dtype.__name__}",)
                missing_value = None

        # Check and patch data
        if missing_value is not None:
            temp = np.where(temp == missing_value, self.nullValue, temp)

        # Copy data to buffer
        ysize, xsize = temp.shape
        if self.invert_y:
            buf_arr[:ysize, :xsize] = temp[::-1, :].astype(self.data_type)
            return buf_arr, ti, tj
        else:
            buf_arr[:ysize, :xsize] = temp[:, :].astype(self.data_type)
            return buf_arr, ti, tj

    @dataset.RequireLib("rasterio")
    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._file_handler is not None:
            if isinstance(self._file_handler, rio.DatasetReader):
                self._file_handler.close()
            else:
                raise TypeError("file_handler is not an instance of rio.DatasetReader")
        else:
            raise ValueError("file_handler could not be located")

    def time(self, *args):
        return 0.0

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "<geostack.readers.%s>" % self.__class__.__name__
