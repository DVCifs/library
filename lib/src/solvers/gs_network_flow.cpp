/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <set>
#include <iostream>
#include <eigen/Sparse>

#include "json11.hpp"

#include "gs_network_flow.h"

namespace Geostack
{

    using namespace json11;

    typedef Eigen::Triplet<double> Et;
    
    template <typename T>
    bool NetworkFlowSolver<T>::init(Vector<T> &network_, std::string jsonConfig) {
    
        // Make internal copy of network
        network = network_;

        // Add indexes to all points
        auto pointIndexes = network.getPointIndexes();
        std::size_t N = pointIndexes.size();
        for (std::size_t k = 0; k < N; k++) {
            network.template setProperty<cl_uint>(pointIndexes[k], "id", k);
        }

        // Parse config
        std::string err;
        auto config = Json::parse(jsonConfig, err);
        if (!err.empty()) {
            std::cout << "ERROR: " << err << std::endl;
            return false;
        }

        double constant = 1.0;
        if (config["constant"].is_number()) {
            constant = config["constant"].number_value();
        } else {
            std::cout << "WARNING: No 'constant' value supplied, using default value of " << constant << std::endl;
        }

        int defaultSegmentType = static_cast<int>(NetworkSegmentType::HazenWilliams);
        if (config["defaultLinkType"].is_number()) {
            defaultSegmentType = config["defaultLinkType"].int_value();
        } else {
            std::cout << "WARNING: No 'default link type' value supplied, using default value of " << defaultSegmentType << std::endl;
        }

        // Parse line strings
        std::vector<GeometryBasePtr<T> > attached;
        for (const auto &l : network.getLineStringIndexes()) {
            const auto &c = network.getLineStringCoordinates(l);

            // Check number of points
            if (c.size() > 2) {
                std::cout << "ERROR: Network must be segments composed of two vertices" << std::endl;
                return false;
            }

            // Update table of links to points
            std::pair<Geostack::PointPtr<T>, Geostack::PointPtr<T> > lp { nullptr, nullptr };
            for (int i = 0; i < 2; i++) {
                
                // Find attached points
                attached = network.attached(c[i]);

                // Loop through geometry
                int nAttachedLines = 0;
                for (auto &g : attached) {
                    if (g->isType(GeometryType::Point)) {

                        // Set link pointers, ignoring any colocated points
                        if ((i == 0 ? lp.first : lp.second) == nullptr)
                            (i == 0 ? lp.first : lp.second) = std::static_pointer_cast<Point<T> >(g);

                    } else if (g->isType(GeometryType::LineString)) {

                        // Count attached line strings
                        nAttachedLines++;
                    }
                }

                // Check for points attached to only one line
                if (nAttachedLines == 1) {

                    // Set all points with only one line attached to terminators
                    for (auto &g : attached) {
                        if (g->isType(GeometryType::Point)) {

                            // Get handle and set point
                            network.template setProperty<int>(g->getID(), "type", (int)NetworkNodeType::Terminator);
                        }
                    }
                }
            }

            // Check link
            cl_uint id0, id1;
            if (lp.first == nullptr && lp.second == nullptr) {
                std::cout << "ERROR: Link has no attached points" << std::endl;
                return false;
            }
            for (int i = 0; i < 2; i++) {

                // Get or create ids
                cl_uint id;
                if ((i == 0 ? lp.first : lp.second) == nullptr) {

                    // Add terminating point and update data
                    id = network.addPoint(c[i]);
                    network.setProperty(id, "type", (int)NetworkNodeType::Terminator);
                    network.setProperty(id, "id", i);

                } else {

                    // Get id
                    id = network.template getProperty<cl_uint>( (i == 0 ? lp.first : lp.second)->getID(), "id");
                }

                // Set id
                (i == 0 ? id0 : id1) = id;
            }

            // Add to table     
            linkPoints.push_back( { id0, id1} );

            // Get diameter
            double diameter = network.template getProperty<double>(l, "diameter");
            if (diameter <= 0.0) {
                std::cout << std::flush << "ERROR: Network segment has zero or negative diameter" << std::endl;
                return false;
            }
        
            // Calculate length
            double length = std::hypot(c[0].p-c[1].p, c[0].q-c[1].q);
            network.template setProperty<double>(l, "length", length);

            // Get type
            NetworkSegmentType::Type type = static_cast<NetworkSegmentType::Type>(network.template getProperty<int>(l, "type"));
            if (type == NetworkSegmentType::Undefined) {
                type = static_cast<NetworkSegmentType::Type>(defaultSegmentType);
                network.template setProperty<int>(l, "type", (int)type);
            }

            // Calculate constants
            double K = 0.0;
            double k = 0.0;
            switch(type) {

                case NetworkSegmentType::HazenWilliams: {

                    // Set pipe constants from Hazen-Williams formula.
                    // Q = (SK)^(1/1.852)
                    // K = (10.67 L) / (C^1.85 D^4.87)
                    K = (pow(constant, 1.85)*pow(diameter, 4.87))/(10.67*length);
                    k = (1.0/1.852);
                    }
                    break;
                
                case NetworkSegmentType::ManningOpenChannel: {

                    // Set pipe constants from Manning open-channel formula.
                    // Q = (SK)^(1/2)
                    // K = D^2.5 / (n^2 L)
                    K = pow(diameter, 2.5)/(constant*constant*length);
                    k = 0.5;
                    }
                    break;
                
                case NetworkSegmentType::Logarithmic: {
            
                    // Set pipe constants for logarithmic flow relation
                    // Q = K ln(kS + 1)
                    K = diameter*1.0E4;
                    k = 0.01;
                    }
                    break;
                
                case NetworkSegmentType::SqrtExp: {
            
                    // Set pipe constants for nonlinear flow relation
                    // Q = K sqrt(S) exp(-kS)
                    K = 10.0;
                    k = 0.006;
                    }
                    break;

                default:

                    std::cout << "ERROR: unknown segment type" << std::endl;
                    return false;
            }

            // Set constants
            network.template setProperty<double>(l, "K", K);
            network.template setProperty<double>(l, "k", k);
        }

        return true;
    }

    template <typename T>
    bool NetworkFlowSolver<T>::run() {
    
        // Get data handles        
        auto pointIndexes = network.getPointIndexes();
        auto lineStringIndexes = network.getLineStringIndexes();

        // Create auxillary vectors 
        std::size_t N = pointIndexes.size();
        std::vector<double> b0(N);         // Initial forcing vector
        std::vector<double> d(N);          // Diagonal vector
        std::set<std::size_t> terminators; // Termination points

        // Create vectors and matrices
        std::vector<Et> c;
        Eigen::SparseMatrix<double> A(N, N);
        Eigen::Matrix<double, Eigen::Dynamic, 1> b(N);  // Forcing vector
        Eigen::Matrix<double, Eigen::Dynamic, 1> h(N);  // Pressure vector
        Eigen::Matrix<double, Eigen::Dynamic, 1> dh(N); // Pressure update vector
        
        // Set data
        double inflow = 0.0;
        for (std::size_t k = 0; k < N; k++) {
            
            // Get point index
            auto &p = pointIndexes[k];

            // Set flow and pressure
            h(k) = 0.0;
            b0[k] = 0.0;
            if (network.template getProperty<int>(p, "type") == NetworkNodeType::Terminator) {

                // Get pressure from terminators
                h(k) = network.template getProperty<double>(p, "head");
                terminators.insert(k);

            } else {

                // Set default node property
                network.template setProperty<int>(p, "type", (int)NetworkNodeType::Junction);

                // Get inflow
                double flow = network.template getProperty<double>(p, "flow");

                // Update initial forcing vector
                b0[k] = flow;
                inflow += flow;
            }
            
        }

        // Iterate
        std::size_t iter = 0;
        double qval, dval;
        double eps = 1.0E-12;
        double dh_dot, dh0_dot = 1.0;
        do {

            // Clear data
            c.clear();
            for (std::size_t k = 0; k < N; k++) {

                // Set forcing
                b(k) = b0[k];

                // Reset diagonal
                d[k] = 0.0;
            }

            // Loop over links
            for (std::size_t k = 0; k < lineStringIndexes.size(); k++) {
                
                // Get link index
                auto &l = lineStringIndexes[k];

                // Get link parameters
                NetworkSegmentType::Type type = 
                    static_cast<NetworkSegmentType::Type>(network.template getProperty<int>(l, "type"));
                double lK = network.template getProperty<double>(l, "K");
                double lk = network.template getProperty<double>(l, "k");

                // Get indexes
                cl_uint i = linkPoints[k].first;
                cl_uint j = linkPoints[k].second;

                // Get h values from link
                double hi = h(i);
                double hj = h(j);
                double hval = fabs(hi-hj);
                double hsign = hi-hj > 0.0 ? 1.0 : -1.0;

                // Calculate matrix entries
                qval = 0.0;
                dval = 0.0;
                switch (type) {
                            
                    case NetworkSegmentType::HazenWilliams: {

                        qval = hsign*pow(hval*lK, lk);
                        dval = pow(lK, lk)*pow((hval+eps), (lk-1.0))*lk; 

                        } break;
                            
                    case NetworkSegmentType::ManningOpenChannel: {

                        qval = hsign*pow(hval*lK, lk);
                        dval = pow(lK, lk)*pow((hval+eps), (lk-1.0))*lk; 

                        } break;
                            
                    case NetworkSegmentType::Logarithmic: {
                                
                        qval = hsign*lK*log(lk*hval+1.0);
                        dval = lK*lk/(lk*hval+1.0); 

                        } break;
                            
                    case NetworkSegmentType::SqrtExp: {
                                
                        qval = hsign*sqrt(lK*hval+eps)*exp(-lk*lK*hval);
                        dval = 0.5*lK*exp(-lk*lK*hval)*(1.0-2.0*lk*lK*hval)/sqrt(lK*hval+eps);

                        } break;

                    default:
                        std::cout << "ERROR: unknown pipe type requested" << std::endl;
                        return false;
                }

                // Set matrix entries
                bool ti = terminators.find(i) == terminators.end();
                bool tj = terminators.find(j) == terminators.end();
                if (ti && tj) {
                    c.push_back(Et(i, j, -dval));
                    c.push_back(Et(j, i, -dval));
                }
                if (ti) {
                    d[i] += dval;
                    b(i) -= qval;
                }
                if (tj) {
                    d[j] += dval;
                    b(j) += qval;
                }
            }

            // Write to diagonal
            for (int i = 0; i < (int)d.size(); i++)
                c.push_back(Et(i, i, d[i] == 0.0 ? 1.0 : d[i]));
                    
            // Create matrix
            A.setFromTriplets(c.begin(), c.end());
            A.makeCompressed();

            // Solve matrix
            Eigen::SparseLU<Eigen::SparseMatrix<double> > solver(A);            
            solver.compute(A);

            if(solver.info() != Eigen::Success) {
                
                // Solver failed
                std::cout << "ERROR: Flow network solver failed" << std::endl;
                return false;
            }
            dh = solver.solve(b);

            // Update h
            h += dh;

            // Check convergence
            dh_dot = dh.dot(dh);
            if (iter == 0)
                dh0_dot = dh_dot;
            iter++;

            //std::cout << "Flow network convergence: " << dh_dot/dh0_dot << std::endl; // TEST

        } while(dh_dot/dh0_dot > 1.0E-3 && iter < 100);
                
        //std::cout << "Flow network took " << iter << " iterations" << std::endl; // TEST
        if (iter == 100) {
            std::cout << "ERROR: Flow network has not converged" << std::endl;
            return false;
        }
        
        // Store pressure
        for (std::size_t k = 0; k < N; k++)
            network.template setProperty<double>(pointIndexes[k], "head", h(k));
        
        // Store flow
        double outflow = 0.0;
        double Qmax = 0.0;
        for (std::size_t k = 0; k < lineStringIndexes.size(); k++) {
                
            // Get link index
            auto &l = lineStringIndexes[k];

            // Get indexes
            cl_uint i = linkPoints[k].first;
            cl_uint j = linkPoints[k].second;
            
            // Get link parameters
            NetworkSegmentType::Type type = 
                static_cast<NetworkSegmentType::Type>(network.template getProperty<int>(l, "type"));
            double lK = network.template getProperty<double>(l, "K");
            double lk = network.template getProperty<double>(l, "k");

            // Get h values from link
            double hi = h(i);
            double hj = h(j);

            // Calculate flow   
            double Q = 0.0;
            switch (type) {
                            
                case NetworkSegmentType::HazenWilliams: {
                        
                    if (hi > hj)
                        Q = pow((hi-hj)*lK, lk);
                    else
                        Q = -pow((hj-hi)*lK, lk);

                    } break;
                            
                case NetworkSegmentType::ManningOpenChannel: {
                        
                    if (hi > hj)
                        Q = pow((hi-hj)*lK, lk);
                    else
                        Q = -pow((hj-hi)*lK, lk);

                    } break;
                    
                case NetworkSegmentType::Logarithmic: {                                

                    Q = lK*log(lk*fabs(hi-hj)+1.0);
                    if (hi < hj)
                        Q = -Q;

                    } break;    

                case NetworkSegmentType::SqrtExp: {                                
                    
                    if (hi > hj)
                        Q = sqrt(lK*(hi-hj)+eps)*exp(-lk*lK*(hi-hj));
                    else
                        Q = -sqrt(lK*(hj-hi)+eps)*exp(-lk*lK*(hj-hi));

                    } break;

                default:
                    std::cout << "ERROR: unknown pipe type requested" << std::endl;
                    return false;
            }

            // Find maximum Q value
            Qmax = std::max(Qmax, fabs(Q));

            // Update network
            network.template setProperty<double>(l, "flow", Q);

            // Set flow at terminators
            bool ti = terminators.find(i) == terminators.end();
            bool tj = terminators.find(j) == terminators.end();
            if (!ti) {
                network.template setProperty<double>(pointIndexes[i], "flow", Q);
                outflow += Q;
            }
            else if (!tj) {
                network.template setProperty<double>(pointIndexes[i], "flow", -Q);
                outflow -= Q;
            }
        }

        // Check sum of flow, this should be relatively close to zero
        if (fabs(outflow+inflow)/Qmax > 1.0E-3)
            std::cout << "WARNING: Flow does not balance over network" << std::endl;

        return true;
    }

    // Float type definitions
    template bool NetworkFlowSolver<float>::init(Vector<float> &, std::string);
    template bool NetworkFlowSolver<float>::run();

    // Double type definitions
    template bool NetworkFlowSolver<double>::init(Vector<double> &, std::string);
    template bool NetworkFlowSolver<double>::run();
}
