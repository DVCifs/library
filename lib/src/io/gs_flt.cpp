/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>

#include "json11.hpp"

#include "gs_solver.h"
#include "gs_string.h"
#include "gs_flt.h"

using namespace json11;

namespace Geostack
{
    /**
    * Read ESRI flt to raster.
    * @param fileName input file name.
    * @return true if file is read, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool FltHandler<RTYPE, CTYPE>::read(std::string fileName, Raster<RTYPE, CTYPE> &r) {
        
        // Split filename 
        auto fileNameSplit = Geostack::Strings::splitPath(fileName);
      
        // Open files
        std::string fileHeaderName = fileNameSplit[0] + fileNameSplit[1] + ".hdr";
        std::ifstream fileHeaderStream(fileHeaderName);
        if (!fileHeaderStream) {
            std::cout << "ERROR: cannot open header '" << fileHeaderName << "' for reading" << std::endl;
            return false;
        }
        std::ifstream fileStream(fileName, std::ios::binary);
        if (!fileStream) {
            std::cout << "ERROR: cannot open '" << fileName << "' for reading" << std::endl;
            return false;
        }

        // Raster data
        uint32_t nx = 0;         // Number of cells in the x direction
        uint32_t ny = 0;         // Number of cells in the y direction     
        CTYPE h = 0.0;               // The spacing between each point in meters
        CTYPE ox = 0.0;              // The offset in the x direction in world co-ordinates        
        CTYPE oy = 0.0;              // The offset in the y direction in world co-ordinates
        std::string nodataValue; // The value representing unknown data   
        std::string endianness;  // String representing data endianness 

        // Read header
        std::string line;
        int nTokensFound = 0;
        try {
            while (std::getline(fileHeaderStream, line)) {

                // Split into pair
                auto splitLine = Strings::split(line, ' ');
                if (splitLine.size() == 2) {

                    // Check token
                    std::string token = splitLine[0];
                    std::string value = splitLine[1];
                    std::transform(token.begin(), token.end(), token.begin(), ::tolower);

                    if (token == "ncols") {
                        nx = std::stoi(value);
                        nTokensFound++;
                    } else if (token == "nrows") {
                        ny = std::stoi(value);
                        nTokensFound++;
                    } else if (token == "cellsize") {
                        h = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "xllcorner") {
                        ox = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "yllcorner") {
                        oy = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "nodata_value" || token == "nodata") {
                        nodataValue = value;
                        nTokensFound++;
                    } else if (token == "byteorder") {
                        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
                        endianness = value;
                        nTokensFound++;
                    }

                } else {
                    std::cout << "ERROR: incorrect header format" << std::endl;
                    return false;
                }

                if (nTokensFound == 7)
                    break;
            };
        } catch(std::invalid_argument &e) {
            std::cout << "ERROR: cannot convert header value to number; " << e.what() << std::endl;
            return false;
        }

        // Check token count
        if (nTokensFound != 7) {
            std::cout << "ERROR: missing header information" << std::endl;
            return false;
        }
        
        // Check values
        if (nx <= 0) {
            std::cout << "ERROR: ncols must be greater than zero" << std::endl;
            return false;
        }
        if (ny <= 0) {
            std::cout << "ERROR: nrows must be greater than zero" << std::endl;
            return false;
        }
        if (h <= (CTYPE)0.0) {
            std::cout << "ERROR: spacing must be greater than zero" << std::endl;
            return false;
        }
        if (endianness != "msbfirst" && endianness != "lsbfirst") {
            std::cout << "ERROR: endianness is not recognised '" << endianness << "'" << std::endl;
            return false;
        }

        // TODO handle endianness
        #if defined(ENDIAN_BIG)
        if (endianness != "msbfirst") {
            std::cout << "ERROR: only msbfirst endianness is supported" << std::endl;
            return false;
        }
        #elif defined(ENDIAN_LITTLE)
        if (endianness != "lsbfirst") {
            std::cout << "ERROR: only lsbfirst endianness is supported" << std::endl;
            return false;
        }
        #else
        #error "Endianness must be defined as ENDIAN_BIG or ENDIAN_LITTLE"
        #endif
           
        // Create raster
        r.init2D(nx, ny, h, h, ox, oy);

        // Create line buffer
        std::vector<float> dataLine;
        dataLine.resize(nx);

        // Write data
        float nullValueFlt = std::stof(nodataValue);
        for (uint32_t j = 0; j < ny; j++) {

            // Read line
            fileStream.read(reinterpret_cast<char *>(dataLine.data()), dataLine.size()*sizeof(float));

            if (fileStream) {
                for (uint32_t i = 0; i < nx; i++) {

                    // Set value
                    r.setCellValue(dataLine[i] == nullValueFlt ? Geostack::getNullValue<RTYPE>() : (RTYPE)dataLine[i], i, (ny-1)-j);
                }

            } else {

                // Error in reading file
                std::cout << "ERROR: unexpected end of flt file" << std::endl;
                return false;
            }
        }

        return true;
    }

    /**
    * Write raster to ESRI flt format.
    * @param fileName output file name.
    * @return true if file is written, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool FltHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // Check for data
        if (!r.hasData()) {
            std::cout << "ERROR: raster has no data to write to float file" << std::endl;
            return false;
        }

        // Set config defaults
        std::string nullValueString = "-9999.0";

        // Parse config
        if (!jsonConfig.empty()) {
        
            // Parse Json
            std::string err;
            auto config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                std::cout << "ERROR: " << err << std::endl;
                return false;
            }
              
            // Set compression type
            if (config["nullValue"].is_string()) {
                nullValueString = config["compression"].string_value();
            }
        }

        // Set null value
        float nullValue = std::stof(nullValueString);

        // Get raster dimensions
        auto dim = r.getRasterDimensions();

        // Check grid spacing is equal
        if (dim.d.hx != dim.d.hy) {
            std::cout << "ERROR: grid does not have equal x and y spacing" << std::endl;
            return false;
        }

        // Split filename 
        auto fileNameSplit = Geostack::Strings::splitPath(fileName);
      
        // Open files
        std::string fileHeaderName = fileNameSplit[0] + fileNameSplit[1] + ".hdr";
        std::ofstream fileHeaderStream(fileHeaderName);
        if (!fileHeaderStream) {
            std::cout << "ERROR: cannot open header '" << fileHeaderName << "' for writing" << std::endl;
            return false;
        }

        std::ofstream fileStream(fileName, std::ios::binary);
        if (!fileStream) {
            std::cout << "ERROR: cannot open '" << fileName << "' for writing" << std::endl;
            return false;
        }
        
        // Write header  
        fileHeaderStream << "ncols " << dim.d.nx << '\n';
        fileHeaderStream << "nrows " << dim.d.ny << '\n';
        fileHeaderStream << "cellsize " << dim.d.hx << '\n';
        fileHeaderStream << "nodata_value " << nullValueString << '\n';
        fileHeaderStream << "xllcorner " << dim.d.ox << '\n';
        fileHeaderStream << "yllcorner " << dim.d.oy << '\n';
        #if defined(ENDIAN_BIG)
        fileHeaderStream << "byteorder " << "MSBFIRST" << '\n';
        #elif defined(ENDIAN_LITTLE)
        fileHeaderStream << "byteorder " << "LSBFIRST" << '\n';
        #else
        #error "Endianness must be defined as ENDIAN_BIG or ENDIAN_LITTLE"
        #endif
        
        // Create line buffer
        std::vector<float> dataLine;
        dataLine.resize(dim.d.nx);

        // Write data
        for (uint32_t j = 0; j < dim.d.ny; j++) {
        
            // Map to float
            for (uint32_t i = 0; i < dim.d.nx; i++) {

                // Get value
                RTYPE v = r.getCellValue(i, dim.d.ny-1-j);
                if (v != v)
                    dataLine[i] = nullValue;
                else
                    dataLine[i] = (float)v;
            }

            // Write line
            fileStream.write(reinterpret_cast<const char *>(dataLine.data()), dataLine.size()*sizeof(float));
        }

        return true;
    }    
    
    // RTYPE float, CTYPE float definitions
    template class FltHandler<float, float>;
    
    // RTYPE uint, CTYPE float definitions
    template class FltHandler<uint32_t, float>;
    
    // RTYPE double, CTYPE double definitions
    template class FltHandler<double, double>;
    
    // RTYPE uint, CTYPE double definitions
    template class FltHandler<uint32_t, double>;
}
