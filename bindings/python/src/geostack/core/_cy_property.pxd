# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
# cython: language_level=3

from cython.operator import dereference as deref
from libcpp.string cimport string
from libcpp cimport bool
from libcpp.map cimport map as cpp_map
from libcpp.vector cimport vector
from libcpp.memory cimport shared_ptr, unique_ptr, make_shared
import numpy as np
cimport cython
cimport numpy as np

np.import_array()

cdef extern from "gs_property.h" namespace "Geostack::PropertyType":
    cdef enum PropertyType "Geostack::PropertyType::Type":
        Undefined = 0
        String = 1
        Integer = 2
        Double = 3
        Index = 4

cdef extern from "gs_property.h" namespace "Geostack":
    cdef cppclass Property:
        Property()
        Property(int v) const
        Property(double v) const
        Property(string v) const
        Property(size_t v) const
        Property(const Property &r) const
        Property& operator=(const Property &r)
        P get[P]() const
        void set[P](const P v)
        void clear()
        PropertyType getType()

    cdef cppclass PropertyHandler:
        void setProperty(string name, string v) const
        void setProperty(string name, int v) const
        void setProperty(string name, double v) const
        void setProperty(string name, size_t v) const
        P getProperty[P](string name) const
        bool hasProperty(string name) const
        cpp_map[string, P] getProperties[P]() const
        void removeProperty(string name)

# ctypedef enum _PropertyType:
#     Undefined = 0
#     String = 1
#     Integer = 2
#     Double = 3
#     Index = 4

ctypedef shared_ptr[Property] _PropertyPtr
# ctypedef _PropertyType _propertyType

cdef class _Property:
    cdef shared_ptr[Property] thisptr
    cdef void c_copy(self, _Property p)

cdef class _Property_i:
    cdef shared_ptr[Property] thisptr
    cdef void c_copy(self, _Property_i p)

cdef class _Property_d:
    cdef shared_ptr[Property] thisptr
    cdef void c_copy(self, _Property_d p)

cdef class _Property_s:
    cdef shared_ptr[Property] thisptr
    cdef void c_copy(self, _Property_s p)

cdef class _PropertyHandler:
    cdef PropertyHandler *thisptr
    cdef void setProperty_s(self, string name, string other)
    cdef void setProperty_i(self, string name, int other)
    cdef void setProperty_d(self, string name, double other)