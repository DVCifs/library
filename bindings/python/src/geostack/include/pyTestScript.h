/* This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <functional>

#include "gs_solver.h"
#include "gs_raster.h"

namespace Geostack {

    namespace pyGeostack {
        bool testSolver(bool verbosity);
    
    } // namespace pyGeostack
} // namespace Geostack
