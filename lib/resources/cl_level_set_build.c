/**
* Level set step build
*/
__kernel void build(
parameters _p,
__global REAL *_vars,
/*__ARGS__*/
) {

    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);

    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {

        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;

/*__VARS__*/

        //// Update start time
        //_start_time = _start_time_update;

        // Cell state is given by lower bit of classification buffer 
        uint _class_state = _class&1;

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (_class&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = _class>>24;

        // Initialise variables
        _rate = 0.0;
        REAL speed = 0.0;

        // Calculate derivative of level set for normed gradient
        REAL _dx = fmax(fmax(_distance-_distance_W, _distance-_distance_E)/_dim.hx, (REAL)0.0);
        REAL _dy = fmax(fmax(_distance-_distance_S, _distance-_distance_N)/_dim.hy, (REAL)0.0);
        REAL _mag = hypot(_dx, _dy);

        if (fabs(_distance) < _p.band_width && _mag > 0.0) {
    
            // Calculate upwind derivative of level set in wind direction
            if (_advect_x > 0.0) _dx = (_distance-_distance_W)/_dim.hx;
            else if (_advect_x < 0.0) _dx = (_distance_E-_distance)/_dim.hx;
            else _dx = 0.5*(_distance_E-_distance_W)/_dim.hx;
        
            if (_advect_y > 0.0) _dy = (_distance-_distance_S)/_dim.hy;
            else if (_advect_y < 0.0) _dy = (_distance_N-_distance)/_dim.hy;
            else _dy = 0.5*(_distance_N-_distance_S)/_dim.hx;
        
            // ---------------------------------
            // User defined code
/*__CODE__*/
            // ---------------------------------

            // Set state
            _class = (_class&~1)|(_class_state&1);

            // Ensure speed is positive
            speed = fmax(speed, (REAL)0.0);

            // Boundary conditions
            if (!(_class_W&1) && _distance > _distance_W)
                speed = 0.0;
            if (!(_class_E&1) && _distance > _distance_E)
                speed = 0.0;
            if (!(_class_S&1) && _distance > _distance_S)
                speed = 0.0;
            if (!(_class_N&1) && _distance > _distance_N)
                speed = 0.0;

        } else {
    
            // Set default speed outside narrow band to 50% of maximum speed
            REAL speed = 0.5*_p.max_speed; 
        }

        // Set speed to zero if state is un-burnable
        if (_class_state == 0)
            speed = 0.0;

        // Update forcing
        _rate = -speed*_mag;

        // Time integrate with Euler step
        _distance_update = _distance+_p.dt*_rate;

        // Store speed
        _speed = speed;

/*__POST__*/

    }

}
