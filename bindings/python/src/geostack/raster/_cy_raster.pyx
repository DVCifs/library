# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref, preincrement as preinc
from libcpp.string cimport string
from libcpp cimport nullptr
from libcpp.map cimport map as cpp_map
from libcpp.vector cimport vector
from libc.stdio cimport printf
from libc.stdint cimport uint32_t
from libc.string cimport memcpy
from libc.math cimport fmax
import numpy as np
cimport numpy as np
from ..vector._cy_vector cimport Vector
from ..core._cy_projection cimport _parsePROJ4_f, _parsePROJ4_d

np.import_array()

cdef class TileSpecifications:
    def __cinit__(self):
        pass

    @property
    def tileSize(self):
        return tileSize

    @property
    def tileSizePower(self):
        return tileSizePower

    @property
    def tileSizeSquared(self):
        return tileSizeSquared

    @property
    def tileSizeMask(self):
        return tileSizeMask

include "_cy_raster.pxi"

cdef class _cyRaster_f(_cyRasterBase_f):
    def __cinit__(self, other=None):
        cdef string c_string
        if other is not None:
            if isinstance(other, bytes):
                c_string = <string> other
            elif isinstance(other, str):
                c_string = <string> other.encode("utf-8")
            if type(self) is _cyRaster_f:
                self.sh_ptr.reset(new Raster[float, float](c_string))
                self.derivedptr = self.baseptr = self.thisptr = self.sh_ptr.get()

    cpdef _cyRaster_f region(self, _BoundingBox_f bounds):
        cdef string var_name
        cdef Raster[float, float] r_out
        var_name = self.getProperty_s("name".encode("utf-8"))
        out = _cyRaster_f(other=var_name)
        r_out = deref(self.derivedptr).region(deref(bounds.thisptr))
        out.c_rastercopy(r_out)
        return out

    cpdef bool resize2D(self, uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy):
        cdef bool out = False
        if self.hasData():
            out = deref(self.derivedptr).resize2D(nx, ny, tox, toy)
        return out

    cpdef bool getNeedsStatus(self) except+:
        return deref(self.derivedptr).getNeedsStatus()

    cpdef void setNeedsStatus(self, bool other) except+:
        deref(self.derivedptr).setNeedsStatus(other)

    cpdef bool getNeedsWrite(self) except+:
        return deref(self.derivedptr).getNeedsWrite()

    cpdef void setNeedsWrite(self, bool other) except+:
        deref(self.derivedptr).setNeedsWrite(other)

    cpdef ReductionType getReductionType(self) except+:
        return deref(self.derivedptr).getReductionType()

    cpdef void setReductionType(self, ReductionType other) except+:
        deref(self.derivedptr).setReductionType(other)

    cpdef NeighboursType getRequiredNeighbours(self) except+:
        return deref(self.derivedptr).getRequiredNeighbours()

    cpdef void setRequiredNeighbours(self, NeighboursType other) except+:
        deref(self.derivedptr).setRequiredNeighbours(other)

    cdef void init_with_raster_dimensions(self, _RasterDimensions_f d) except *:
        cdef bool out
        cdef Dimensions[float] dim
        dim = deref(d.thisptr).d
        out = deref(self.derivedptr).init(dim)
        if not out:
            RuntimeError("Unable to initialize raster")

    cdef void init_with_bbox(self, _BoundingBox_f b, float resolution) except *:
        cdef bool out
        out = deref(self.derivedptr).init(deref(b.thisptr), resolution)
        if not out:
            RuntimeError("Unable to initialize raster")

    def init(self, object other, object resolution=None):
        if resolution is not None:
            self.init_with_bbox(other, resolution)
        elif resolution is None:
            self.init_with_raster_dimensions(other)

    cpdef void init1D(self, uint32_t nx_, float hx_, float ox_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.hx_ = hx_
        self.ox_ = ox_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>0,
                <uint32_t>0,
                <float>self.hx_, 
                <float>1.0, 
                <float>1.0, 
                <float>self.ox_,
                <float>0.0,
                <float>0.0)
                
        if out == False:
            raise RuntimeError("Unable to initialize 1D raster")

    cpdef void init2D(self, uint32_t nx_, uint32_t ny_, float hx_,
            float hy_, float ox_=0.0, float oy_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.ny_ = ny_
        self.hx_ = hx_
        self.hy_ = hy_
        self.ox_ = ox_
        self.oy_ = oy_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>self.ny_,
                <uint32_t>0,
                <float>self.hx_, 
                <float>self.hy_, 
                <float>1.0, 
                <float>self.ox_,
                <float>self.oy_,
                <float>0.0)
                
        if out == False:
            raise RuntimeError("Unable to initialize 2D raster")

    cpdef void init3D(self, uint32_t nx_, uint32_t ny_, uint32_t nz_,
            float hx_, float hy_, float hz_, float ox_=0.0,
            float oy_=0.0, float oz_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.ny_ = ny_
        self.nz_ = nz_
        self.hx_ = hx_
        self.hy_ = hy_
        self.hz_ = hz_
        self.ox_ = ox_
        self.oy_ = oy_
        self.oz_ = oz_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>self.ny_,
                <uint32_t>self.nz_,
                <float>self.hx_, 
                <float>self.hy_, 
                <float>self.hz_, 
                <float>self.ox_,
                <float>self.oy_,
                <float>self.oz_)
                
        if out == False:
            raise RuntimeError("Unable to initialize 3D raster")

    @staticmethod
    def rastercopy(_cyRaster_f other):
        out = _cyRaster_f()
        out.c_rastercopy(deref(other.derivedptr))
        return out

    cdef void c_rastercopy(self, Raster[float, float] &other) except*:
        with nogil:
            self.sh_ptr.reset(new Raster[float, float](other))
            self.derivedptr = self.baseptr = self.thisptr = self.sh_ptr.get()

    cpdef bool mapVector(self, _Vector_f v, size_t parameters, string widthPropertyName) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.derivedptr).mapVector(deref(v.thisptr), parameters,
                widthPropertyName)
        return out

    cpdef bool rasterise(self, _Vector_f v, string propertyName, size_t parameters) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.derivedptr).rasterise(deref(v.thisptr), propertyName,
                parameters)
        return out

    cpdef _Vector_f vectorise(self, float[:] contourValue, object noDataValue=None) except+:
        out = _Vector_f()
        cdef Vector[float] _out
        cdef vector[float] contour_values
        cdef float null_value
        cdef int i, nvals
        nvals = contourValue.shape[0]
        for i in range(nvals):
            contour_values.push_back(contourValue[i])

        if noDataValue is None:
            null_value = getNullValue_flt()
        else:
            null_value = <float>noDataValue
        with nogil:
            _out = deref(self.derivedptr).vectorise(contour_values, null_value)
        out.c_copy(_out)
        return out

    def getData(self, ti=0, tj=0):
    
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        cdef int nz, rasterKind
        
        # Create buffers
        cdef vector[float] buf
        cdef Dimensions[float] d

        if self.hasData():
        
            rasterKind = self.getRasterKind()
            d = deref(self.derivedptr).getTileDimensions(_ti, _tj).d
            
            if rasterKind == RasterKind.Raster1D:    
                # Create 1D buffer
                self.data1D = np.full((tileSize,), getNullValue_flt(),
                    dtype=np.float32)
                buf.reserve(tileSize)
                buf.assign(&self.data1D[0], &self.data1D[0] + tileSize)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data1D[0], buf.data(), tileSize * sizeof(float))

                return np.asanyarray(self.data1D)[:d.nx]
            elif rasterKind == RasterKind.Raster2D:            
                # Create 2D buffer
                self.data2D = np.full((tileSize, tileSize), getNullValue_flt(),
                    dtype=np.float32)
                buf.reserve(tileSizeSquared)
                buf.assign(&self.data2D[0, 0], &self.data2D[0, 0] + tileSizeSquared)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data2D[0, 0], buf.data(), tileSizeSquared * sizeof(float))

                return np.asanyarray(self.data2D)[:d.ny, :d.nx]
            elif rasterKind == RasterKind.Raster3D:
                # Create 3D buffer
                nz = <int> fmax(d.nz, 1)
                self.data3D = np.full((nz, tileSize, tileSize), getNullValue_flt(),
                    dtype=np.float32)
                buf.reserve(nz*tileSizeSquared)
                buf.assign(&self.data3D[0, 0, 0], &self.data3D[0, 0, 0] + nz*tileSizeSquared)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data3D[0, 0, 0], buf.data(), nz*tileSizeSquared * sizeof(float))

                return np.asanyarray(self.data3D)[:, :d.ny, :d.nx]
            else:
            
                # Error if dimensions are incorrect
                raise RuntimeError("Unable to understand raster dimensionality")
        else:
            raise RuntimeError("Raster is not initialized")

    cpdef void set1D(self, float[:] inp, int ti=0, int tj=0):

        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        # Create buffers
        cdef vector[float] buf
        cdef bool out            
        if self.hasData():
            if self.getRasterKind() == RasterKind.Raster1D:
                with nogil:
                    buf.reserve(tileSize)
                    buf.assign(&inp[0], &inp[0] + tileSize)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 1D")

    cpdef void set2D(self, float[:, :] inp, int ti=0, int tj=0):

        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        # Create buffers
        cdef vector[float] buf
        cdef bool out

        if self.hasData():   
            if self.getRasterKind() == RasterKind.Raster2D:
                with nogil:
                    buf.resize(tileSizeSquared)
                    buf.assign(&inp[0, 0], &inp[0, 0] + tileSizeSquared)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 2D")

    cpdef void set3D(self, float[:, :, :] inp, int ti=0, int tj=0):
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        # Create buffers
        cdef vector[float] buf
        cdef bool out
        cdef int nz
        cdef Dimensions[float] d

        if self.hasData():
        
            # Get raster dimensions
            d = deref(self.derivedptr).getTileDimensions(_ti, _tj).d
            nz = <int> fmax(d.nz, 1)
            if self.getRasterKind() == RasterKind.Raster3D:
                with nogil:
                    buf.reserve(tileSizeSquared*nz)
                    buf.assign(&inp[0, 0, 0], &inp[0, 0, 0] + tileSizeSquared*nz)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 3D")

    cpdef float getCellValue(self, uint32_t i, uint32_t j=0, uint32_t k=0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).getCellValue(i, j, k)

    cpdef float getNearestValue(self, float x, float y=0.0, float z=0.0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).getNearestValue(x, y, z)

    cpdef float getBilinearValue(self, float x, float y=0.0, float z=0.0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).getBilinearValue(x, y, z)

    cpdef void setCellValue(self, float value, uint32_t i, uint32_t j=0, uint32_t k=0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialised")
        with nogil:
            deref(self.derivedptr).setCellValue(value, i, j, k)

    cpdef void setAllCellValues(self, float c) except+:
        if self.hasData():
            with nogil:
                deref(self.derivedptr).setAllCellValues(c)
        else:
            raise ValueError("Raster is not initialized")

    cpdef float maxVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).max()

    cpdef float minVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).min()

    cpdef float reduceVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).reduce()

    cpdef bool read(self, string fileName) except+:
        cdef bool out = False
        out = deref(self.derivedptr).read(fileName)
        return out

    cpdef bool write(self, string fileName, string jsonConfig) except+:
        cdef bool out
        if self.hasData():
            out = deref(self.derivedptr).write(fileName, jsonConfig)
            return out
        else:
            raise ValueError("raster is not yet initialized")

    cpdef bool hasData(self) except+:
        return deref(self.derivedptr).hasData()
        
    cpdef void deleteRasterData(self) except+:
        with nogil:
            deref(self.derivedptr).deleteRasterData()
            
    def __eq__(self, _cyRaster_f other):
        cdef bool out = False
        out = self.derivedptr == other.derivedptr
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        # if type(self) is _cyRaster_f and self.derivedptr != NULL:
        #     if self.hasData():
        #         del self.derivedptr
        if type(self) is _cyRaster_f and self.derivedptr != NULL:
            if self.sh_ptr != nullptr:
                self.sh_ptr.reset()

    def __exit__(self):
        self.__dealloc__()
        
        
cdef class _cyRaster_f_i(_cyRasterBase_f):
    def __cinit__(self, other=None):
        cdef string c_string
        if other is not None:
            if isinstance(other, bytes):
                c_string = <string> other
            elif isinstance(other, str):
                c_string = <string> other.encode("utf-8")
            if type(self) is _cyRaster_f_i:
                self.sh_ptr.reset(new Raster[uint32_t, float](c_string))
                self.derivedptr = self.baseptr = self.thisptr = self.sh_ptr.get()

    cpdef _cyRaster_f_i region(self, _BoundingBox_f bounds):
        cdef string var_name
        cdef Raster[uint32_t, float] r_out
        var_name = self.getProperty_s("name".encode("utf-8"))
        out = _cyRaster_f_i(other=var_name)
        r_out = deref(self.derivedptr).region(deref(bounds.thisptr))
        out.c_rastercopy(r_out)
        return out

    cpdef bool resize2D(self, uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy):
        cdef bool out = False
        if self.hasData():
            out = deref(self.derivedptr).resize2D(nx, ny, tox, toy)
        return out

    cpdef bool getNeedsStatus(self) except+:
        return deref(self.derivedptr).getNeedsStatus()

    cpdef void setNeedsStatus(self, bool other) except+:
        deref(self.derivedptr).setNeedsStatus(other)

    cpdef bool getNeedsWrite(self) except+:
        return deref(self.derivedptr).getNeedsWrite()

    cpdef void setNeedsWrite(self, bool other) except+:
        deref(self.derivedptr).setNeedsWrite(other)

    cpdef ReductionType getReductionType(self) except+:
        return deref(self.derivedptr).getReductionType()

    cpdef void setReductionType(self, ReductionType other) except+:
        deref(self.derivedptr).setReductionType(other)

    cpdef NeighboursType getRequiredNeighbours(self) except+:
        return deref(self.derivedptr).getRequiredNeighbours()

    cpdef void setRequiredNeighbours(self, NeighboursType other) except+:
        deref(self.derivedptr).setRequiredNeighbours(other)

    cdef void init_with_raster_dimensions(self, _RasterDimensions_f d) except *:
        cdef bool out
        cdef Dimensions[float] dim
        dim = deref(d.thisptr).d
        out = deref(self.derivedptr).init(dim)
        if not out:
            RuntimeError("Unable to initialize raster")

    cdef void init_with_bbox(self, _BoundingBox_f b, float resolution) except *:
        cdef bool out
        out = deref(self.derivedptr).init(deref(b.thisptr), resolution)
        if not out:
            RuntimeError("Unable to initialize raster")

    def init(self, object other, object resolution=None):
        if resolution is not None:
            self.init_with_bbox(other, resolution)
        elif resolution is None:
            self.init_with_raster_dimensions(other)

    cpdef void init1D(self, uint32_t nx_, float hx_, float ox_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.hx_ = hx_
        self.ox_ = ox_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>0,
                <uint32_t>0,
                <float>self.hx_, 
                <float>1.0, 
                <float>1.0, 
                <float>self.ox_,
                <float>0.0,
                <float>0.0)
        
        if out == False:
            raise RuntimeError("Unable to initialize 1D raster")

    cpdef void init2D(self, uint32_t nx_, uint32_t ny_, float hx_,
            float hy_, float ox_=0.0, float oy_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.ny_ = ny_
        self.hx_ = hx_
        self.hy_ = hy_
        self.ox_ = ox_
        self.oy_ = oy_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>self.ny_,
                <uint32_t>0,
                <float>self.hx_, 
                <float>self.hy_, 
                <float>1.0, 
                <float>self.ox_,
                <float>self.oy_,
                <float>0.0)
                
        if out == False:
            raise RuntimeError("Unable to initialize 2D raster")

    cpdef void init3D(self, uint32_t nx_, uint32_t ny_, uint32_t nz_,
            float hx_, float hy_, float hz_, float ox_=0.0,
            float oy_=0.0, float oz_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.ny_ = ny_
        self.nz_ = nz_
        self.hx_ = hx_
        self.hy_ = hy_
        self.hz_ = hz_
        self.ox_ = ox_
        self.oy_ = oy_
        self.oz_ = oz_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>self.ny_,
                <uint32_t>self.nz_,
                <float>self.hx_, 
                <float>self.hy_, 
                <float>self.hz_, 
                <float>self.ox_,
                <float>self.oy_,
                <float>self.oz_)
                
        if out == False:
            raise RuntimeError("Unable to initialize 3D raster")

    @staticmethod
    def rastercopy(_cyRaster_f_i other):
        out = _cyRaster_f_i()
        out.c_rastercopy(deref(other.derivedptr))
        return out

    cdef void c_rastercopy(self, Raster[uint32_t, float] &other) except*:
        with nogil:
            self.sh_ptr.reset(new Raster[uint32_t, float](other))
            self.derivedptr = self.baseptr = self.thisptr = self.sh_ptr.get()

    cpdef bool mapVector(self, _Vector_f v, size_t parameters, string widthPropertyName) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.derivedptr).mapVector(deref(v.thisptr), parameters,
                widthPropertyName)
        return out

    cpdef bool rasterise(self, _Vector_f v, string propertyName, size_t parameters) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.derivedptr).rasterise(deref(v.thisptr), propertyName,
                parameters)
        return out

    cpdef _Vector_f vectorise(self, uint32_t[:] contourValue, object noDataValue=None) except+:
        out = _Vector_f()
        cdef Vector[float] _out
        cdef vector[uint32_t] contour_values
        cdef uint32_t null_value
        cdef int i, nvals
        nvals = contourValue.shape[0]
        for i in range(nvals):
            contour_values.push_back(contourValue[i])

        if noDataValue is None:
            null_value = getNullValue_uint32()
        else:
            null_value = <uint32_t>noDataValue
        with nogil:
            _out = deref(self.derivedptr).vectorise(contour_values, null_value)
        out.c_copy(_out)
        return out

    def getData(self, ti=0, tj=0):

        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        cdef int nz, rasterKind

        # Create buffers
        cdef vector[uint32_t] buf
        cdef Dimensions[float] d

        if self.hasData():
            rasterKind = self.getRasterKind()
            # Get raster dimensions
            d = deref(self.derivedptr).getTileDimensions(_ti, _tj).d
            if rasterKind == RasterKind.Raster1D:
                # Create 1D buffer
                self.data1D = np.full((tileSize,), getNullValue_flt(),
                    dtype=np.uint32)
                buf.reserve(tileSize)
                buf.assign(&self.data1D[0], &self.data1D[0] + tileSize)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data1D[0], buf.data(), tileSize * sizeof(uint32_t))

                return np.asanyarray(self.data1D)[:d.nx]
            elif rasterKind == RasterKind.Raster2D:
                # Create 2D buffer
                self.data2D = np.full((tileSize, tileSize), getNullValue_flt(),
                    dtype=np.uint32)
                buf.reserve(tileSizeSquared)
                buf.assign(&self.data2D[0, 0], &self.data2D[0, 0] + tileSizeSquared)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data2D[0, 0], buf.data(), tileSizeSquared * sizeof(uint32_t))

                return np.asanyarray(self.data2D)[:d.ny, :d.nx]                
            elif rasterKind == RasterKind.Raster3D:
                # Create 3D buffer
                nz = <int> fmax(d.nz, 1)
                self.data3D = np.full((nz, tileSize, tileSize), getNullValue_flt(),
                    dtype=np.uint32)
                buf.reserve(nz*tileSizeSquared)
                buf.assign(&self.data3D[0, 0, 0], &self.data3D[0, 0, 0] + nz*tileSizeSquared)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data3D[0, 0, 0], buf.data(), nz*tileSizeSquared * sizeof(uint32_t))

                return np.asanyarray(self.data3D)[:, :d.ny, :d.nx]
            else:
                # Error if dimensions are incorrect
                raise RuntimeError("Unable to understand raster dimensionality")
        else:
            raise RuntimeError("Raster is not initialized")

    cpdef void set1D(self, uint32_t[:] inp, int ti=0, int tj=0):
        
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        # Create buffers
        cdef vector[uint32_t] buf
        cdef bool out            
    
        if self.hasData():
            if self.getRasterKind() == RasterKind.Raster1D:
                with nogil:
                    buf.reserve(tileSize)
                    buf.assign(&inp[0], &inp[0] + tileSize)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 1D")

    cpdef void set2D(self, uint32_t[:, :] inp, int ti=0, int tj=0):
        
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        # Create buffers
        cdef vector[uint32_t] buf
        cdef bool out

        if self.hasData():
            if self.getRasterKind() == RasterKind.Raster2D:
                with nogil:
                    buf.resize(tileSizeSquared)
                    buf.assign(&inp[0, 0], &inp[0, 0] + tileSizeSquared)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 2D")

    cpdef void set3D(self, uint32_t[:, :, :] inp, int ti=0, int tj=0):
        
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
    
        # Create buffers
        cdef vector[uint32_t] buf
        cdef bool out
        cdef int nz
        cdef Dimensions[float] d

        if self.hasData():
            # Get raster dimensions
            d = deref(self.derivedptr).getTileDimensions(_ti, _tj).d
            nz = <int> fmax(d.nz, 1)
            if self.getRasterKind() == RasterKind.Raster3D:
                with nogil:
                    buf.reserve(tileSizeSquared*nz)
                    buf.assign(&inp[0, 0, 0], &inp[0, 0, 0] + tileSizeSquared*nz)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 3D")

    cpdef uint32_t getCellValue(self, uint32_t i, uint32_t j=0, uint32_t k=0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).getCellValue(i, j, k)

    cpdef uint32_t getNearestValue(self, float x, float y=0.0, float z=0.0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).getNearestValue(x, y, z)

    #cpdef uint32_t getBilinearValue(self, float x, float y=0.0, float z=0.0) except+:
    #    if self.hasData() == False:
    #        raise ValueError("Raster is not initialized")
    #    with nogil:
    #        return deref(self.derivedptr).getBilinearValue(x, y, z)

    cpdef void setCellValue(self, uint32_t value, uint32_t i, uint32_t j=0, uint32_t k=0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialised")
        with nogil:
            deref(self.derivedptr).setCellValue(value, i, j, k)

    cpdef void setAllCellValues(self, uint32_t c) except+:
        if self.hasData():
            with nogil:
                deref(self.derivedptr).setAllCellValues(c)
        else:
            raise ValueError("Raster is not initialized")

    cpdef uint32_t maxVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).max()

    cpdef uint32_t minVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).min()

    cpdef uint32_t reduceVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).reduce()

    cpdef bool read(self, string fileName) except+:
        cdef bool out = False
        out = deref(self.derivedptr).read(fileName)
        return out

    cpdef bool write(self, string fileName, string jsonConfig) except+:
        cdef bool out
        if self.hasData():
            out = deref(self.derivedptr).write(fileName, jsonConfig)
            return out
        else:
            raise ValueError("raster is not yet initialized")

    cpdef bool hasData(self) except+:
        return deref(self.derivedptr).hasData()
        
    cpdef void deleteRasterData(self) except+:
        with nogil:
            deref(self.derivedptr).deleteRasterData()
            
    def __eq__(self, _cyRaster_f_i other):
        cdef bool out = False
        out = self.derivedptr == other.derivedptr
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        # if type(self) is _cyRaster_f_i and self.derivedptr != NULL:
        #     if self.hasData():
        #         del self.derivedptr
        if type(self) is _cyRaster_f_i and self.derivedptr != NULL:
            if self.sh_ptr != nullptr:
                self.sh_ptr.reset()

    def __exit__(self):
        self.__dealloc__()
        
        
cdef class _cyRaster_d(_cyRasterBase_d):
    def __cinit__(self, other=None):
        cdef string c_string
        if other is not None:
            if isinstance(other, bytes):
                c_string = <string> other
            elif isinstance(other, str):
                c_string = <string> other.encode("utf-8")
            if type(self) is _cyRaster_d:
                self.sh_ptr.reset(new Raster[double, double](c_string))
                self.derivedptr = self.baseptr = self.thisptr = self.sh_ptr.get()

    cpdef _cyRaster_d region(self, _BoundingBox_d bounds):
        cdef string var_name
        cdef Raster[double, double] r_out
        var_name = self.getProperty_s("name".encode("utf-8"))
        out = _cyRaster_d(other=var_name)
        r_out = deref(self.derivedptr).region(deref(bounds.thisptr))
        out.c_rastercopy(r_out)
        return out

    cpdef bool resize2D(self, uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy):
        cdef bool out = False
        if self.hasData():
            out = deref(self.derivedptr).resize2D(nx, ny, tox, toy)
        return out

    cpdef bool getNeedsStatus(self) except+:
        return deref(self.derivedptr).getNeedsStatus()

    cpdef void setNeedsStatus(self, bool other) except+:
        deref(self.derivedptr).setNeedsStatus(other)

    cpdef bool getNeedsWrite(self) except+:
        return deref(self.derivedptr).getNeedsWrite()

    cpdef void setNeedsWrite(self, bool other) except+:
        deref(self.derivedptr).setNeedsWrite(other)

    cpdef ReductionType getReductionType(self) except+:
        return deref(self.derivedptr).getReductionType()

    cpdef void setReductionType(self, ReductionType other) except+:
        deref(self.derivedptr).setReductionType(other)

    cpdef NeighboursType getRequiredNeighbours(self) except+:
        return deref(self.derivedptr).getRequiredNeighbours()

    cpdef void setRequiredNeighbours(self, NeighboursType other) except+:
        deref(self.derivedptr).setRequiredNeighbours(other)

    cdef void init_with_raster_dimensions(self, _RasterDimensions_d d) except *:
        cdef bool out
        cdef Dimensions[double] dim
        dim = deref(d.thisptr).d
        out = deref(self.derivedptr).init(dim)
        if not out:
            RuntimeError("Unable to initialize raster")

    cdef void init_with_bbox(self, _BoundingBox_d b, double resolution) except *:
        cdef bool out
        out = deref(self.derivedptr).init(deref(b.thisptr), resolution)
        if not out:
            RuntimeError("Unable to initialize raster")

    def init(self, object other, object resolution=None):
        if resolution is not None:
            self.init_with_bbox(other, resolution)
        elif resolution is None:
            self.init_with_raster_dimensions(other)

    cpdef void init1D(self, uint32_t nx_, double hx_, double ox_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.hx_ = hx_
        self.ox_ = ox_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>0,
                <uint32_t>0,
                <double>self.hx_, 
                <double>1.0, 
                <double>1.0, 
                <double>self.ox_,
                <double>0.0,
                <double>0.0)
                
        if out == False:
            raise RuntimeError("Unable to initialize 1D raster")

    cpdef void init2D(self, uint32_t nx_, uint32_t ny_, double hx_,
            double hy_, double ox_=0.0, double oy_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.ny_ = ny_
        self.hx_ = hx_
        self.hy_ = hy_
        self.ox_ = ox_
        self.oy_ = oy_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>self.ny_,
                <uint32_t>0,
                <double>self.hx_, 
                <double>self.hy_, 
                <double>1.0, 
                <double>self.ox_,
                <double>self.oy_,
                <double>0.0)
                
        if out == False:
            raise RuntimeError("Unable to initialize 2D raster")

    cpdef void init3D(self, uint32_t nx_, uint32_t ny_, uint32_t nz_,
            double hx_, double hy_, double hz_, double ox_=0.0,
            double oy_=0.0, double oz_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.ny_ = ny_
        self.nz_ = nz_
        self.hx_ = hx_
        self.hy_ = hy_
        self.hz_ = hz_
        self.ox_ = ox_
        self.oy_ = oy_
        self.oz_ = oz_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>self.ny_,
                <uint32_t>self.nz_,
                <double>self.hx_, 
                <double>self.hy_, 
                <double>self.hz_, 
                <double>self.ox_,
                <double>self.oy_,
                <double>self.oz_)
                
        if out == False:
            raise RuntimeError("Unable to initialize 3D raster")

    @staticmethod
    def rastercopy(_cyRaster_d other):
        out = _cyRaster_d()
        out.c_rastercopy(deref(other.derivedptr))
        return out

    cdef void c_rastercopy(self, Raster[double, double] &other) except*:
        with nogil:
            self.sh_ptr.reset(new Raster[double, double](other))
            self.derivedptr = self.baseptr = self.thisptr = self.sh_ptr.get()

    cpdef bool mapVector(self, _Vector_d v, size_t parameters, string widthPropertyName) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.derivedptr).mapVector(deref(v.thisptr), parameters,
                widthPropertyName)
        return out

    cpdef bool rasterise(self, _Vector_d v, string propertyName, size_t parameters) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.derivedptr).rasterise(deref(v.thisptr), propertyName,
                parameters)
        return out

    cpdef _Vector_d vectorise(self, double[:] contourValue, object noDataValue=None) except+:
        out = _Vector_d()
        cdef Vector[double] _out
        cdef vector[double] contour_values
        cdef double null_value
        cdef int i, nvals
        nvals = contourValue.shape[0]
        for i in range(nvals):
            contour_values.push_back(contourValue[i])

        if noDataValue is None:
            null_value = getNullValue_dbl()
        else:
            null_value = <double>noDataValue
        with nogil:
            _out = deref(self.derivedptr).vectorise(contour_values, null_value)
        out.c_copy(_out)
        return out

    def getData(self, ti=0, tj=0):
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        cdef int nz, rasterKind

        # Create buffers
        cdef vector[double] buf
        cdef Dimensions[double] d

        if self.hasData():
            rasterKind = self.getRasterKind()
            # Get raster dimensions
            d = deref(self.derivedptr).getTileDimensions(_ti, _tj).d

            if rasterKind == RasterKind.Raster1D:
                # Create 1D buffer
                self.data1D = np.full((tileSize,), getNullValue_dbl(),
                    dtype=np.float64)
                buf.reserve(tileSize)
                buf.assign(&self.data1D[0], &self.data1D[0] + tileSize)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data1D[0], buf.data(), tileSize * sizeof(double))

                return np.asanyarray(self.data1D)[:d.nx]
            elif rasterKind == RasterKind.Raster2D:
                # Create 2D buffer
                self.data2D = np.full((tileSize, tileSize), getNullValue_dbl(),
                    dtype=np.float64)
                buf.reserve(tileSizeSquared)
                buf.assign(&self.data2D[0, 0], &self.data2D[0, 0] + tileSizeSquared)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data2D[0, 0], buf.data(), tileSizeSquared * sizeof(double))

                return np.asanyarray(self.data2D)[:d.ny, :d.nx]
            elif rasterKind == RasterKind.Raster3D:
                # Create 3D buffer
                nz = <int> fmax(d.nz, 1)
                self.data3D = np.full((nz, tileSize, tileSize), getNullValue_dbl(),
                    dtype=np.float64)
                buf.reserve(nz*tileSizeSquared)
                buf.assign(&self.data3D[0, 0, 0], &self.data3D[0, 0, 0] + nz*tileSizeSquared)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data3D[0, 0, 0], buf.data(), nz*tileSizeSquared * sizeof(double))

                return np.asanyarray(self.data3D)[:, :d.ny, :d.nx]
            else:
                # Error if dimensions are incorrect
                raise RuntimeError("Unable to understand raster dimensionality")
        else:
            raise RuntimeError("Raster is not initialized")

    cpdef void set1D(self, double[:] inp, int ti=0, int tj=0):
        
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        # Create buffers
        cdef vector[double] buf
        cdef bool out            
    
        if self.hasData():
            if self.getRasterKind() == RasterKind.Raster1D:
                with nogil:
                    buf.reserve(tileSize)
                    buf.assign(&inp[0], &inp[0] + tileSize)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 1D")

    cpdef void set2D(self, double[:, :] inp, int ti=0, int tj=0):
        
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        # Create buffers
        cdef vector[double] buf
        cdef bool out

        if self.hasData():
            if self.getRasterKind() == RasterKind.Raster3D:
                with nogil:
                    buf.resize(tileSizeSquared)
                    buf.assign(&inp[0, 0], &inp[0, 0] + tileSizeSquared)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 2D")

    cpdef void set3D(self, double[:, :, :] inp, int ti=0, int tj=0):
        
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
    
        # Create buffers
        cdef vector[double] buf
        cdef bool out
        cdef int nz
        cdef Dimensions[double] d

        if self.hasData():
            # Get raster dimensions
            d = deref(self.derivedptr).getTileDimensions(_ti, _tj).d
            nz = <int> fmax(d.nz, 1)
            if self.getRasterKind() == RasterKind.Raster3D:
                with nogil:
                    buf.reserve(tileSizeSquared*nz)
                    buf.assign(&inp[0, 0, 0], &inp[0, 0, 0] + tileSizeSquared*nz)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 3D")

    cpdef double getCellValue(self, uint32_t i, uint32_t j=0, uint32_t k=0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).getCellValue(i, j, k)

    cpdef double getNearestValue(self, double x, double y=0.0, double z=0.0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).getNearestValue(x, y, z)

    cpdef double getBilinearValue(self, double x, double y=0.0, double z=0.0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).getBilinearValue(x, y, z)

    cpdef void setCellValue(self, double value, uint32_t i, uint32_t j=0, uint32_t k=0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialised")
        with nogil:
            deref(self.derivedptr).setCellValue(value, i, j, k)

    cpdef void setAllCellValues(self, double c) except+:
        if self.hasData():
            with nogil:
                deref(self.derivedptr).setAllCellValues(c)
        else:
            raise ValueError("Raster is not initialized")

    cpdef double maxVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).max()

    cpdef double minVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).min()

    cpdef double reduceVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).reduce()

    cpdef bool read(self, string fileName) except+:
        cdef bool out = False
        out = deref(self.derivedptr).read(fileName)
        return out

    cpdef bool write(self, string fileName, string jsonConfig) except+:
        cdef bool out
        if self.hasData():
            out = deref(self.derivedptr).write(fileName, jsonConfig)
            return out
        else:
            raise ValueError("raster is not yet initialized")

    cpdef bool hasData(self) except+:
        return deref(self.derivedptr).hasData()
        
    cpdef void deleteRasterData(self) except+:
        with nogil:
            deref(self.derivedptr).deleteRasterData()
            
    def __eq__(self, _cyRaster_d other):
        cdef bool out = False
        out = self.derivedptr == other.derivedptr
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        # if type(self) is _cyRaster_d and self.derivedptr != NULL:
        #     if self.hasData():
        #         del self.derivedptr
        if type(self) is _cyRaster_d and self.derivedptr != NULL:
            if self.sh_ptr != nullptr:
                self.sh_ptr.reset()

    def __exit__(self):
        self.__dealloc__()
        
        
cdef class _cyRaster_d_i(_cyRasterBase_d):
    def __cinit__(self, other=None):
        cdef string c_string
        if other is not None:
            if isinstance(other, bytes):
                c_string = <string> other
            elif isinstance(other, str):
                c_string = <string> other.encode("utf-8")
            if type(self) is _cyRaster_d_i:
                self.sh_ptr.reset(new Raster[uint32_t, double](c_string))
                self.derivedptr = self.baseptr = self.thisptr = self.sh_ptr.get()

    cpdef _cyRaster_d_i region(self, _BoundingBox_d bounds):
        cdef string var_name
        cdef Raster[uint32_t, double] r_out
        var_name = self.getProperty_s("name".encode("utf-8"))
        out = _cyRaster_d_i(other=var_name)
        r_out = deref(self.derivedptr).region(deref(bounds.thisptr))
        out.c_rastercopy(r_out)
        return out

    cpdef bool resize2D(self, uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy):
        cdef bool out = False
        if self.hasData():
            out = deref(self.derivedptr).resize2D(nx, ny, tox, toy)
        return out

    cpdef bool getNeedsStatus(self) except+:
        return deref(self.derivedptr).getNeedsStatus()

    cpdef void setNeedsStatus(self, bool other) except+:
        deref(self.derivedptr).setNeedsStatus(other)

    cpdef bool getNeedsWrite(self) except+:
        return deref(self.derivedptr).getNeedsWrite()

    cpdef void setNeedsWrite(self, bool other) except+:
        deref(self.derivedptr).setNeedsWrite(other)

    cpdef ReductionType getReductionType(self) except+:
        return deref(self.derivedptr).getReductionType()

    cpdef void setReductionType(self, ReductionType other) except+:
        deref(self.derivedptr).setReductionType(other)

    cpdef NeighboursType getRequiredNeighbours(self) except+:
        return deref(self.derivedptr).getRequiredNeighbours()

    cpdef void setRequiredNeighbours(self, NeighboursType other) except+:
        deref(self.derivedptr).setRequiredNeighbours(other)

    cdef void init_with_raster_dimensions(self, _RasterDimensions_d d) except *:
        cdef bool out
        cdef Dimensions[double] dim
        dim = deref(d.thisptr).d
        out = deref(self.derivedptr).init(dim)
        if not out:
            RuntimeError("Unable to initialize raster")

    cdef void init_with_bbox(self, _BoundingBox_d b, double resolution) except *:
        cdef bool out
        out = deref(self.derivedptr).init(deref(b.thisptr), resolution)
        if not out:
            RuntimeError("Unable to initialize raster")

    def init(self, object other, object resolution=None):
        if resolution is not None:
            self.init_with_bbox(other, resolution)
        elif resolution is None:
            self.init_with_raster_dimensions(other)

    cpdef void init1D(self, uint32_t nx_, double hx_, double ox_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.hx_ = hx_
        self.ox_ = ox_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>0,
                <uint32_t>0,
                <double>self.hx_, 
                <double>1.0, 
                <double>1.0, 
                <double>self.ox_,
                <double>0.0,
                <double>0.0)
                
        if out == False:
            raise RuntimeError("Unable to initialize 1D raster")

    cpdef void init2D(self, uint32_t nx_, uint32_t ny_, double hx_,
            double hy_, double ox_=0.0, double oy_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.ny_ = ny_
        self.hx_ = hx_
        self.hy_ = hy_
        self.ox_ = ox_
        self.oy_ = oy_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>self.ny_,
                <uint32_t>0,
                <double>self.hx_, 
                <double>self.hy_, 
                <double>1.0, 
                <double>self.ox_,
                <double>self.oy_,
                <double>0.0)
                
        if out == False:
            raise RuntimeError("Unable to initialize 2D raster")

    cpdef void init3D(self, uint32_t nx_, uint32_t ny_, uint32_t nz_,
            double hx_, double hy_, double hz_, double ox_=0.0,
            double oy_=0.0, double oz_=0.0) except *:
        cdef bool out = False
        self.nx_ = nx_
        self.ny_ = ny_
        self.nz_ = nz_
        self.hx_ = hx_
        self.hy_ = hy_
        self.hz_ = hz_
        self.ox_ = ox_
        self.oy_ = oy_
        self.oz_ = oz_
        with nogil:
            out = deref(self.derivedptr).init(
                <uint32_t>self.nx_,
                <uint32_t>self.ny_,
                <uint32_t>self.nz_,
                <double>self.hx_, 
                <double>self.hy_, 
                <double>self.hz_, 
                <double>self.ox_,
                <double>self.oy_,
                <double>self.oz_)
                
        if out == False:
            raise RuntimeError("Unable to initialize 3D raster")

    @staticmethod
    def rastercopy(_cyRaster_d_i other):
        out = _cyRaster_d_i()
        out.c_rastercopy(deref(other.derivedptr))
        return out

    cdef void c_rastercopy(self, Raster[uint32_t, double] &other) except*:
        with nogil:
            self.sh_ptr.reset(new Raster[uint32_t, double](other))
            self.derivedptr = self.baseptr = self.thisptr = self.sh_ptr.get()

    cpdef bool mapVector(self, _Vector_d v, size_t parameters, string widthPropertyName) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.derivedptr).mapVector(deref(v.thisptr), parameters,
                widthPropertyName)
        return out

    cpdef bool rasterise(self, _Vector_d v, string propertyName, size_t parameters) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.derivedptr).rasterise(deref(v.thisptr), propertyName,
                parameters)
        return out

    cpdef _Vector_d vectorise(self, uint32_t[:] contourValue, object noDataValue=None) except+:
        out = _Vector_d()
        cdef Vector[double] _out
        cdef vector[uint32_t] contour_values
        cdef uint32_t null_value
        cdef int i, nvals
        nvals = contourValue.shape[0]
        for i in range(nvals):
            contour_values.push_back(contourValue[i])

        if noDataValue is None:
            null_value = getNullValue_uint32()
        else:
            null_value = <uint32_t>noDataValue
        with nogil:
            _out = deref(self.derivedptr).vectorise(contour_values, null_value)
        out.c_copy(_out)
        return out

    def getData(self, ti=0, tj=0):
    
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        cdef int nz, rasterKind
        
        # Create buffers
        cdef vector[uint32_t] buf
        cdef Dimensions[double] d

        if self.hasData():
            # Get raster kind
            rasterKind = self.getRasterKind()
            # Get raster dimensions
            d = deref(self.derivedptr).getTileDimensions(_ti, _tj).d
            if rasterKind == RasterKind.Raster1D:
                # Create 1D buffer
                self.data1D = np.full((tileSize,), getNullValue_dbl(),
                    dtype=np.uint32)
                buf.reserve(tileSize)
                buf.assign(&self.data1D[0], &self.data1D[0] + tileSize)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data1D[0], buf.data(), tileSize * sizeof(uint32_t))

                return np.asanyarray(self.data1D)[:d.nx]
            elif rasterKind == RasterKind.Raster2D:
                # Create 2D buffer
                self.data2D = np.full((tileSize, tileSize), getNullValue_dbl(),
                    dtype=np.uint32)
                buf.reserve(tileSizeSquared)
                buf.assign(&self.data2D[0, 0], &self.data2D[0, 0] + tileSizeSquared)

                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data2D[0, 0], buf.data(), tileSizeSquared * sizeof(uint32_t))
                return np.asanyarray(self.data2D)[:d.ny, :d.nx]
            elif rasterKind == RasterKind.Raster3D:
                # Create 3D buffer
                nz = <int> fmax(d.nz, 1)
                self.data3D = np.full((nz, tileSize, tileSize), getNullValue_dbl(),
                    dtype=np.uint32)
                buf.reserve(nz*tileSizeSquared)
                buf.assign(&self.data3D[0, 0, 0], &self.data3D[0, 0, 0] + nz*tileSizeSquared)
                    
                # Get tile data
                deref(self.derivedptr).getTileData(_ti, _tj, buf)
                # Copy tile data
                memcpy(&self.data3D[0, 0, 0], buf.data(), nz*tileSizeSquared * sizeof(uint32_t))

                return np.asanyarray(self.data3D)[:, :d.ny, :d.nx]
            else:
                # Error if dimensions are incorrect
                raise RuntimeError("Unable to understand raster dimensionality")
        else:
            raise RuntimeError("Raster is not initialized")

    cpdef void set1D(self, uint32_t[:] inp, int ti=0, int tj=0):
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        # Create buffers
        cdef vector[uint32_t] buf
        cdef bool out            
    
        if self.hasData():
            if self.getRasterKind() == RasterKind.Raster1D:
                with nogil:
                    buf.reserve(tileSize)
                    buf.assign(&inp[0], &inp[0] + tileSize)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 1D")

    cpdef void set2D(self, uint32_t[:, :] inp, int ti=0, int tj=0):
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        # Create buffers
        cdef vector[uint32_t] buf
        cdef bool out

        if self.hasData():
            if self.getRasterKind() == RasterKind.Raster2D:
                with nogil:
                    buf.resize(tileSizeSquared)
                    buf.assign(&inp[0, 0], &inp[0, 0] + tileSizeSquared)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 2D")

    cpdef void set3D(self, uint32_t[:, :, :] inp, int ti=0, int tj=0):
        # Get tile indexes
        cdef uint32_t _ti = <uint32_t> ti
        cdef uint32_t _tj = <uint32_t> tj
        # Create buffers
        cdef vector[uint32_t] buf
        cdef bool out
        cdef int nz
        cdef Dimensions[double] d

        if self.hasData():
            # Get raster dimensions
            d = deref(self.derivedptr).getTileDimensions(_ti, _tj).d
            nz = <int> fmax(d.nz, 1)
            if self.getRasterKind() == RasterKind.Raster3D:
                with nogil:
                    buf.reserve(tileSizeSquared*nz)
                    buf.assign(&inp[0, 0, 0], &inp[0, 0, 0] + tileSizeSquared*nz)
                    out = deref(self.derivedptr).setTileData(_ti, _tj, buf)
                    buf.clear()
                if not out:
                    raise RuntimeError("Unable to set tile data")
            else:
                raise TypeError("Raster is not 3D")

    cpdef uint32_t getCellValue(self, uint32_t i, uint32_t j=0, uint32_t k=0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).getCellValue(i, j, k)

    cpdef uint32_t getNearestValue(self, double x, double y=0.0, double z=0.0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).getNearestValue(x, y, z)

    #cpdef uint32_t getBilinearValue(self, double x, double y=0.0, double z=0.0) except+:
    #    if self.hasData() == False:
    #        raise ValueError("Raster is not initialized")
    #    with nogil:
    #        return deref(self.derivedptr).getBilinearValue(x, y, z)

    cpdef void setCellValue(self, uint32_t value, uint32_t i, uint32_t j=0, uint32_t k=0) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialised")
        with nogil:
            deref(self.derivedptr).setCellValue(value, i, j, k)

    cpdef void setAllCellValues(self, uint32_t c) except+:
        if self.hasData():
            with nogil:
                deref(self.derivedptr).setAllCellValues(c)
        else:
            raise ValueError("Raster is not initialized")

    cpdef uint32_t maxVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).max()

    cpdef uint32_t minVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).min()

    cpdef uint32_t reduceVal(self) except+:
        if self.hasData() == False:
            raise ValueError("Raster is not initialized")
        with nogil:
            return deref(self.derivedptr).reduce()

    cpdef bool read(self, string fileName) except+:
        cdef bool out = False
        out = deref(self.derivedptr).read(fileName)
        return out

    cpdef bool write(self, string fileName, string jsonConfig) except+:
        cdef bool out
        if self.hasData():
            out = deref(self.derivedptr).write(fileName, jsonConfig)
            return out
        else:
            raise ValueError("raster is not yet initialized")

    cpdef bool hasData(self) except+:
        return deref(self.derivedptr).hasData()
        
    cpdef void deleteRasterData(self) except+:
        with nogil:
            deref(self.derivedptr).deleteRasterData()
            
    def __eq__(self, _cyRaster_d_i other):
        cdef bool out = False
        out = self.derivedptr == other.derivedptr
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        # if type(self) is _cyRaster_d_i and self.derivedptr != NULL:
        #     if self.hasData():
        #         del self.derivedptr
        if type(self) is _cyRaster_d_i and self.derivedptr != NULL:
            if self.sh_ptr != nullptr:
                self.sh_ptr.reset()

    def __exit__(self):
        self.__dealloc__()

cdef class DataFileHandler_f:

    def __cinit__(self, string name, object fileHandlerObj, object fileName):
        self.class_obj = fileHandlerObj(fileName, base_type=np.float32,
            data_type=np.float32)

        self._file_name = fileName
        self.cy_raster_obj = _cyRaster_f(other=name)
        self.cls_capsule = PyCapsule_New(<void*> self, "DataFileHandler", NULL)
        self.func_capsule = PyCapsule_New(<void*> self.setDataFunction, "setDataFunction", NULL)

        if fileName is None or not isinstance(fileName, str):
            if (PyCapsule_IsValid(self.func_capsule, "setDataFunction") and
                PyCapsule_IsValid(self.cls_capsule, "DataFileHandler")):
                self.thisptr.reset(new pyFileHandler[float, float](
                    <data_reader_f> PyCapsule_GetPointer(self.func_capsule, "setDataFunction"),
                    <void*> PyCapsule_GetPointer(self.cls_capsule, "DataFileHandler"),
                    <string> "".encode('utf-8')))
        else:
            if isinstance(fileName, str):
                if (PyCapsule_IsValid(self.func_capsule, "setDataFunction") and
                    PyCapsule_IsValid(self.cls_capsule, "DataFileHandler")):
                    self.thisptr.reset(new pyFileHandler[float, float](
                        <data_reader_f> PyCapsule_GetPointer(self.func_capsule, "setDataFunction"),
                        <void*> PyCapsule_GetPointer(self.cls_capsule, "DataFileHandler"),
                        <string> fileName.encode('utf-8')))
        if self.thisptr == nullptr:
            raise RuntimeError("Unable to initialise object instance")
        self.tidx = 0

    cpdef void setProjectionParameters(self, _ProjectionParameters_f src_prj):
        self.cy_raster_obj.setProjectionParameters(src_prj)

    cpdef _ProjectionParameters_f getProjectionParameters(self):
        return self.cy_raster_obj.getProjectionParameters()

    cdef pyException setDataFunction(self, TileDimensions[float] tdim,
            vector[float] &v) except+ with gil:
        cdef int buf_size
        cdef string varname
        cdef pyException outError

        varname = self.cy_raster_obj.getProperty_s("name".encode('utf-8'))
        buf_size = tileSize * tileSize

        try:           
            # Call data set function 
            raster_dimensions = self.cy_raster_obj.getRasterDimensions()
            self.buf_arr, ti, tj = self.class_obj.setter(tdim.ti, tdim.tj,
                raster_dimensions.tx, raster_dimensions.ty, varname, self.tidx)
        except RuntimeError as err:
            outError.rc = -1
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except TypeError as err:
            outError.rc = -2
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except ValueError as err:
            outError.rc = -3
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except IndexError as err:
            outError.rc = -4
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except AttributeError as err:
            outError.rc = -5
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except NameError as err:
            outError.rc = -6
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError

        if <int>v.size() == buf_size:
            v.reserve(buf_size)
            v.assign(&self.buf_arr[0, 0], &self.buf_arr[0, 0] + buf_size)
            outError.rc = 1
            outError.errMessage = <string>"".encode('utf-8')
            return outError
        else:
            outError.rc = -1
            outError.errMessage = <string>"Size of input vector is different from buffer size".encode('utf-8')
            return outError

    @staticmethod
    def readFile(readerFunction, fileName, thredds=False, use_pydap=False):
        file_handler = DataFileHandler_f(readerFunction, fileName)
        file_handler.read(thredds=<bool>thredds, use_pydap=<bool>use_pydap)
        return file_handler

    cpdef void setFileInputHandler(self):
        if self.thisptr == nullptr:
            raise RuntimeError("Object instance is a nullptr")
        else:
            deref(self.cy_raster_obj.derivedptr).setFileInputHandler(self.thisptr)

    cpdef void setFileOutputHandler(self):
        if self.thisptr == nullptr:
            raise RuntimeError("Object instance is a nullptr")
        else:
            deref(self.cy_raster_obj.derivedptr).setFileOutputHandler(self.thisptr)

    def getData(self, ti, tj, tidx):
        self.tidx = <int>tidx
        return self.cy_raster_obj.getData(ti=<int>ti, tj=<int>tj)

    cpdef void read(self, bool thredds=False, bool use_pydap=False,
        str modelProjection=None):
        cdef uint32_t nx, ny, tx, ty
        cdef float hx, hy, ox, oy
        cdef string projStr
        cdef _ProjectionParameters_f dst_proj

        # DataHandler reader function object
        nx, ny, hx, hy, ox, oy, projStr, self.time = self.class_obj.reader(
                thredds=<bool>thredds, use_pydap=<bool>use_pydap)

        if modelProjection is not None:
            if isinstance(modelProjection, str):
                dst_proj = _parsePROJ4_f(modelProjection.encode('utf-8'))
            elif isinstance(modelProjection, bytes):
                dst_proj = _parsePROJ4_f(modelProjection)
            self.cy_raster_obj.setProjectionParameters(dst_proj)
        elif <int> projStr.length() > 1:
            dst_proj = _parsePROJ4_f(projStr)
            self.cy_raster_obj.setProjectionParameters(dst_proj)

        self.cy_raster_obj.init2D(nx, ny, hx, hy, ox, oy)
        raster_dimensions = self.cy_raster_obj.getRasterDimensions()

    cpdef void write(self, object fileName, string jsonConfig):
        cdef object write_obj
        write_obj = self.class_obj.writer()

    cpdef void update_time(self, int tidx):
        # Set time index
        self.tidx = tidx
        # Set time
        self.time = self.class_obj.time(self.tidx)
        
    def __eq__(self, DataFileHandler_f other):
        cdef bool out = False
        out = self.cy_raster_obj.derivedptr == other.cy_raster_obj.derivedptr
        return out

    def __dealloc__(self):
        if type(self) is DataFileHandler_f:
            self.thisptr.reset()


cdef class DataFileHandler_f_i:

    def __cinit__(self, string name, object fileHandlerObj, object fileName):
        self.class_obj = fileHandlerObj(fileName, base_type=np.float32,
            data_type=np.uint32)

        self._file_name = fileName
        self.cy_raster_obj = _cyRaster_f_i(other=name)

        self.cls_capsule = PyCapsule_New(<void*> self, "DataFileHandler", NULL)
        self.func_capsule = PyCapsule_New(<void*> self.setDataFunction, "setDataFunction", NULL)

        if fileName is None or not isinstance(fileName, str):
            if (PyCapsule_IsValid(self.func_capsule, "setDataFunction") and
                PyCapsule_IsValid(self.cls_capsule, "DataFileHandler")):
                self.thisptr.reset(new pyFileHandler_i[float](
                    <data_reader_f_i> PyCapsule_GetPointer(self.func_capsule, "setDataFunction"),
                    <void*> PyCapsule_GetPointer(self.cls_capsule, "DataFileHandler"),
                    <string> "".encode('utf-8')))
        else:
            if isinstance(fileName, str):
                if (PyCapsule_IsValid(self.func_capsule, "setDataFunction") and
                    PyCapsule_IsValid(self.cls_capsule, "DataFileHandler")):
                    self.thisptr.reset(new pyFileHandler_i[float](
                        <data_reader_f_i> PyCapsule_GetPointer(self.func_capsule, "setDataFunction"),
                        <void*> PyCapsule_GetPointer(self.cls_capsule, "DataFileHandler"),
                        <string> fileName.encode('utf-8')))
        if self.thisptr == nullptr:
            raise RuntimeError("Unable to initialise object instance")
        self.tidx = 0

    cpdef void setProjectionParameters(self, _ProjectionParameters_f src_prj):
        self.cy_raster_obj.setProjectionParameters(src_prj)

    cpdef _ProjectionParameters_f getProjectionParameters(self):
        return self.cy_raster_obj.getProjectionParameters()

    cdef pyException setDataFunction(self, TileDimensions[float] tdim,
            vector[uint32_t] &v) except+ with gil:
        cdef int buf_size
        cdef string varname
        cdef pyException outError

        varname = self.cy_raster_obj.getProperty_s("name".encode('utf-8'))

        buf_size = tileSize * tileSize
        self.buf_arr = np.full((tileSize, tileSize), getNullValue_uint32(),
            dtype=np.uint32)

        try:
            raster_dimensions = self.cy_raster_obj.getRasterDimensions()
            self.buf_arr, ti, tj = self.class_obj.setter(tdim.ti, tdim.tj,
                raster_dimensions.tx, raster_dimensions.ty, varname, self.tidx)
        except RuntimeError as err:
            outError.rc = -1
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except TypeError as err:
            outError.rc = -2
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except ValueError as err:
            outError.rc = -3
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except IndexError as err:
            outError.rc = -4
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except AttributeError as err:
            outError.rc = -5
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except NameError as err:
            outError.rc = -6
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError

        if <int>v.size() == buf_size:
            v.reserve(buf_size)
            v.assign(&self.buf_arr[0, 0], &self.buf_arr[0, 0] + buf_size)
            outError.rc = 1
            outError.errMessage = <string>"".encode('utf-8')
            return outError
        else:
            outError.rc = -1
            outError.errMessage = <string>"Size of input vector is different from buffer size".encode('utf-8')
            return outError

    @staticmethod
    def readFile(readerFunction, fileName, thredds=False, use_pydap=False):
        file_handler = DataFileHandler_f_i(readerFunction, fileName)
        file_handler.read(thredds=<bool>thredds, use_pydap=<bool>use_pydap)
        return file_handler

    cpdef void setFileInputHandler(self):
        if self.thisptr == nullptr:
            raise RuntimeError("Object instance is a nullptr")
        else:
            deref(self.cy_raster_obj.derivedptr).setFileInputHandler(self.thisptr)

    cpdef void setFileOutputHandler(self):
        if self.thisptr == nullptr:
            raise RuntimeError("Object instance is a nullptr")
        else:
            deref(self.cy_raster_obj.derivedptr).setFileOutputHandler(self.thisptr)

    def getData(self, ti, tj, tidx):
        self.tidx = <int>tidx
        return self.cy_raster_obj.getData(ti=<int>ti, tj=<int>tj)

    cpdef void read(self, bool thredds=False, bool use_pydap=False,
        str modelProjection=None):
        cdef uint32_t nx, ny, tx, ty
        cdef float hx, hy, ox, oy
        cdef string projStr
        cdef _ProjectionParameters_f dst_proj

        # DataHandler reader function object
        nx, ny, hx, hy, ox, oy, projStr, self.time = self.class_obj.reader(
                thredds=<bool>thredds, use_pydap=<bool>use_pydap)

        if modelProjection is not None:
            if isinstance(modelProjection, str):
                dst_proj = _parsePROJ4_f(modelProjection.encode('utf-8'))
            elif isinstance(modelProjection, bytes):
                dst_proj = _parsePROJ4_f(modelProjection)
            self.cy_raster_obj.setProjectionParameters(dst_proj)
        elif <int> projStr.length() > 1:
            dst_proj = _parsePROJ4_f(projStr)
            self.cy_raster_obj.setProjectionParameters(dst_proj)

        self.cy_raster_obj.init2D(nx, ny, hx, hy, ox, oy)
        raster_dimensions = self.cy_raster_obj.getRasterDimensions()

    cpdef void write(self, string fileName, string jsonConfig):
        cdef object write_obj
        write_obj = self.class_obj.writer()

    cpdef void update_time(self, int tidx):

        # Set time index
        self.tidx = tidx
        # Set time
        self.time = self.class_obj.time(self.tidx)
        
    def __eq__(self, DataFileHandler_f_i other):
        cdef bool out = False
        out = self.cy_raster_obj.derivedptr == other.cy_raster_obj.derivedptr
        return out

    def __dealloc__(self):
        if type(self) is DataFileHandler_f_i:
            self.thisptr.reset()


cdef class DataFileHandler_d:

    def __cinit__(self, string name, object fileHandlerObj, object fileName):
        self.class_obj = fileHandlerObj(fileName, base_type=np.float64,
            data_type=np.float64)

        self._file_handler_obj = fileHandlerObj
        self.cy_raster_obj = _cyRaster_d(other=name)

        self.cls_capsule = PyCapsule_New(<void*> self, "DataFileHandler", NULL)
        self.func_capsule = PyCapsule_New(<void*> self.setDataFunction, "setDataFunction", NULL)

        if fileName is None or not isinstance(fileName, str):
            if (PyCapsule_IsValid(self.func_capsule, "setDataFunction") and
                PyCapsule_IsValid(self.cls_capsule, "DataFileHandler")):
                self.thisptr.reset(new pyFileHandler[double, double](
                    <data_reader_d> PyCapsule_GetPointer(self.func_capsule, "setDataFunction"),
                    <void*> PyCapsule_GetPointer(self.cls_capsule, "DataFileHandler"),
                    <string> "".encode('utf-8')))
        else:
            if isinstance(fileName, str):
                if (PyCapsule_IsValid(self.func_capsule, "setDataFunction") and
                    PyCapsule_IsValid(self.cls_capsule, "DataFileHandler")):
                    self.thisptr.reset(new pyFileHandler[double, double](
                        <data_reader_d> PyCapsule_GetPointer(self.func_capsule, "setDataFunction"),
                        <void*> PyCapsule_GetPointer(self.cls_capsule, "DataFileHandler"),
                        <string> fileName.encode('utf-8')))
        if self.thisptr == nullptr:
            raise RuntimeError("Unable to initialise object instance")
        self.tidx = 0

    cpdef void setProjectionParameters(self, _ProjectionParameters_d src_prj):
        self.cy_raster_obj.setProjectionParameters(src_prj)

    cpdef _ProjectionParameters_d getProjectionParameters(self):
        return self.cy_raster_obj.getProjectionParameters()

    cdef pyException setDataFunction(self, TileDimensions[double] tdim,
            vector[double] &v) except+ with gil:
        cdef int buf_size
        cdef string varname
        cdef pyException outError

        varname = self.cy_raster_obj.getProperty_s("name".encode('utf-8'))

        buf_size = tileSize * tileSize
        self.buf_arr = np.full((tileSize, tileSize), getNullValue_dbl(),
            dtype=np.float64)
        try:
            raster_dimensions = self.cy_raster_obj.getRasterDimensions()
            self.buf_arr, ti, tj = self.class_obj.setter(tdim.ti, tdim.tj,
                raster_dimensions.tx, raster_dimensions.ty, varname, self.tidx)
        except RuntimeError as err:
            outError.rc = -1
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except TypeError as err:
            outError.rc = -2
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except ValueError as err:
            outError.rc = -3
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except IndexError as err:
            outError.rc = -4
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except AttributeError as err:
            outError.rc = -5
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except NameError as err:
            outError.rc = -6
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError

        if buf_size == <int>v.size():
            v.reserve(buf_size)
            v.assign(&self.buf_arr[0, 0], &self.buf_arr[0, 0] + buf_size)
            outError.rc = 1
            outError.errMessage = <string> "".encode('utf-8')
            return outError
        else:
            outError.rc = -1
            outError.errMessage = <string> "Size of input vector is different from buffer size".encode('utf-8')
            return outError

    cpdef void setFileInputHandler(self):
        if self.thisptr == nullptr:
            raise RuntimeError("Object instance is a nullptr")
        else:
            deref(self.cy_raster_obj.derivedptr).setFileInputHandler(self.thisptr)

    cpdef void setFileOutputHandler(self):
        if self.thisptr == nullptr:
            raise RuntimeError("Object instance is a nullptr")
        else:
            deref(self.cy_raster_obj.derivedptr).setFileOutputHandler(self.thisptr)

    @staticmethod
    def readFile(readerFunction, fileName, thredds=False, use_pydap=False):
        file_handler = DataFileHandler_d(readerFunction, fileName)
        file_handler.read(thredds=<bool>thredds, use_pydap=<bool>use_pydap)
        return file_handler

    def getData(self, ti, tj, tidx):
        self.tidx = <int>tidx
        return self.cy_raster_obj.getData(ti=<int>ti, tj=<int>tj)
        
    cpdef void read(self, bool thredds=False, bool use_pydap=False,
        str modelProjection=None):
        cdef uint32_t nx, ny, tx, ty
        cdef double hx, hy, ox, oy

        cdef string projStr
        cdef _ProjectionParameters_d dst_proj

        # DataHandler reader function object
        nx, ny, hx, hy, ox, oy, projStr, self.time = self.class_obj.reader(
                thredds=<bool>thredds, use_pydap=<bool>use_pydap)

        if modelProjection is not None:
            if isinstance(modelProjection, str):
                dst_proj = _parsePROJ4_d(modelProjection.encode('utf-8'))
            elif isinstance(modelProjection, bytes):
                dst_proj = _parsePROJ4_d(modelProjection)
            self.cy_raster_obj.setProjectionParameters(dst_proj)
        elif <int> projStr.length() > 1:
            dst_proj = _parsePROJ4_d(projStr)
            self.cy_raster_obj.setProjectionParameters(dst_proj)

        self.cy_raster_obj.init2D(nx, ny, hx, hy, ox, oy)
        raster_dimensions = self.cy_raster_obj.getRasterDimensions()

    cpdef void write(self, string fileName, string jsonConfig):
        cdef object write_obj
        write_obj = self.class_obj.writer()

    cpdef void update_time(self, int tidx):
        # Set time index
        self.tidx = tidx
        # Set time
        self.time = self.class_obj.time(self.tidx)
        
    def __eq__(self, DataFileHandler_d other):
        cdef bool out = False
        out = self.cy_raster_obj.derivedptr == other.cy_raster_obj.derivedptr
        return out

    def __dealloc__(self):
        if type(self) is DataFileHandler_d:
            self.thisptr.reset()


cdef class DataFileHandler_d_i:

    def __cinit__(self, string name, object fileHandlerObj, object fileName):
        self.class_obj = fileHandlerObj(fileName, base_type=np.float64,
            data_type=np.uint32)

        self._file_handler_obj = fileHandlerObj
        self.cy_raster_obj = _cyRaster_d_i(other=name)

        self.cls_capsule = PyCapsule_New(<void*> self, "DataFileHandler", NULL)
        self.func_capsule = PyCapsule_New(<void*> self.setDataFunction, "setDataFunction", NULL)

        if fileName is None or not isinstance(fileName, str):
            if (PyCapsule_IsValid(self.func_capsule, "setDataFunction") and
                PyCapsule_IsValid(self.cls_capsule, "DataFileHandler")):
                self.thisptr.reset(new pyFileHandler_i[double](
                    <data_reader_d_i> PyCapsule_GetPointer(self.func_capsule, "setDataFunction"),
                    <void*> PyCapsule_GetPointer(self.cls_capsule, "DataFileHandler"),
                    <string> "".encode('utf-8')))
        else:
            if isinstance(fileName, str):
                if (PyCapsule_IsValid(self.func_capsule, "setDataFunction") and
                    PyCapsule_IsValid(self.cls_capsule, "DataFileHandler")):
                    self.thisptr.reset(new pyFileHandler_i[double](
                        <data_reader_d_i> PyCapsule_GetPointer(self.func_capsule, "setDataFunction"),
                        <void*> PyCapsule_GetPointer(self.cls_capsule, "DataFileHandler"),
                        <string> fileName.encode('utf-8')))
        if self.thisptr == nullptr:
            raise RuntimeError("Unable to initialise object instance")
        self.tidx = 0

    cpdef void setProjectionParameters(self, _ProjectionParameters_d src_prj):
        self.cy_raster_obj.setProjectionParameters(src_prj)

    cpdef _ProjectionParameters_d getProjectionParameters(self):
        return self.cy_raster_obj.getProjectionParameters()

    cdef pyException setDataFunction(self, TileDimensions[double] tdim,
            vector[uint32_t] &v) except+ with gil:
        cdef int buf_size
        cdef string varname
        cdef pyException outError

        varname = self.cy_raster_obj.getProperty_s("name".encode('utf-8'))

        buf_size = tileSize * tileSize
        self.buf_arr = np.full((tileSize, tileSize), getNullValue_uint32(),
            dtype=np.uint32)
        try:
            raster_dimensions = self.cy_raster_obj.getRasterDimensions()
            self.buf_arr, ti, tj = self.class_obj.setter(tdim.ti, tdim.tj,
                raster_dimensions.tx, raster_dimensions.ty, varname, self.tidx)
        except RuntimeError as err:
            outError.rc = -1
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except TypeError as err:
            outError.rc = -2
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except ValueError as err:
            outError.rc = -3
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except IndexError as err:
            outError.rc = -4
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except AttributeError as err:
            outError.rc = -5
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError
        except NameError as err:
            outError.rc = -6
            outError.errMessage = <string>err.args[0].encode('utf-8')
            return outError

        if buf_size == <int>v.size():
            v.reserve(buf_size)
            v.assign(&self.buf_arr[0, 0], &self.buf_arr[0, 0] + buf_size)
            outError.rc = 1
            outError.errMessage = <string>"".encode('utf-8')
            return outError
        else:
            outError.rc = -1
            outError.errMessage = <string>"Size of input vector is different from buffer size".encode('utf-8')
            return outError

    cpdef void setFileInputHandler(self):
        if self.thisptr == nullptr:
            raise RuntimeError("Object instance is a nullptr")
        else:
            deref(self.cy_raster_obj.derivedptr).setFileInputHandler(self.thisptr)

    cpdef void setFileOutputHandler(self):
        if self.thisptr == nullptr:
            raise RuntimeError("Object instance is a nullptr")
        else:
            deref(self.cy_raster_obj.derivedptr).setFileOutputHandler(self.thisptr)

    @staticmethod
    def readFile(readerFunction, fileName, thredds=False, use_pydap=False):
        file_handler = DataFileHandler_d_i(readerFunction, fileName)
        file_handler.read(thredds=<bool>thredds, use_pydap=<bool>use_pydap)
        return file_handler

    def getData(self, ti, tj, tidx):
        self.tidx = <int>tidx
        return self.cy_raster_obj.getData(ti=<int>ti, tj=<int>tj)

    cpdef void read(self, bool thredds=False, bool use_pydap=False,
        str modelProjection=None):
        cdef uint32_t nx, ny, tx, ty
        cdef double hx, hy, ox, oy
        cdef string projStr
        cdef _ProjectionParameters_d dst_proj

        # DataHandler reader function object
        nx, ny, hx, hy, ox, oy, projStr, self.time = self.class_obj.reader(
                thredds=<bool>thredds, use_pydap=<bool>use_pydap)

        if modelProjection is not None:
            if isinstance(modelProjection, str):
                dst_proj = _parsePROJ4_d(modelProjection.encode('utf-8'))
            elif isinstance(modelProjection, bytes):
                dst_proj = _parsePROJ4_d(modelProjection)
            self.cy_raster_obj.setProjectionParameters(dst_proj)
        elif <int> projStr.length() > 1:
            dst_proj = _parsePROJ4_d(projStr)
            self.cy_raster_obj.setProjectionParameters(dst_proj)

        self.cy_raster_obj.init2D(nx, ny, hx, hy, ox, oy)
        raster_dimensions = self.cy_raster_obj.getRasterDimensions()

    cpdef void write(self, string fileName, string jsonConfig):
        cdef object write_obj
        write_obj = self.class_obj.writer()

    cpdef void update_time(self, int tidx):

        # Set time index
        self.tidx = tidx
        # Set time
        self.time = self.class_obj.time(self.tidx)
        
    def __eq__(self, DataFileHandler_d_i other):
        cdef bool out = False
        out = self.cy_raster_obj.derivedptr == other.cy_raster_obj.derivedptr
        return out

    def __dealloc__(self):
        if type(self) is DataFileHandler_d_i:
            self.thisptr.reset()