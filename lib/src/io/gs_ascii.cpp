/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <algorithm>
#include <map>

#include "json11.hpp"

#include "gs_solver.h"
#include "gs_string.h"
#include "gs_ascii.h"

using namespace json11;

namespace Geostack
{
    /**
    * Read ESRI ascii to raster.
    * @param fileName input file name.
    * @return true if file is read, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool AsciiHandler<RTYPE, CTYPE>::read(std::string fileName, Raster<RTYPE, CTYPE> &r) {
        
        // Open file
        std::ifstream fileStream(fileName);
        if (!fileStream) {
            std::cout << "ERROR: cannot open '" << fileName << "' for reading" << std::endl;
            return false;
        }

        // Raster data
        uint32_t nx = 0;             // Number of cells in the x direction
        uint32_t ny = 0;             // Number of cells in the y direction     
        CTYPE h = 0.0;               // The spacing between each point in meters
        CTYPE ox = 0.0;              // The offset in the x direction in world co-ordinates        
        CTYPE oy = 0.0;              // The offset in the y direction in world co-ordinates
        std::string nullValueString; // The value representing unknown data   

        // Read header
        std::string line;
        int nTokensFound = 0;
        try {
            while (std::getline(fileStream, line)) {

                // Split into pair
                auto splitLine = Strings::split(line, ' ');
                if (splitLine.size() == 2) {

                    // Check token
                    std::string token = splitLine[0];
                    std::string value = splitLine[1];
                    std::transform(token.begin(), token.end(), token.begin(), ::tolower);

                    if (token == "ncols") {
                        nx = std::stoi(value);
                        nTokensFound++;
                    } else if (token == "nrows") {
                        ny = std::stoi(value);
                        nTokensFound++;
                    } else if (token == "cellsize") {
                        h = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "xllcorner") {
                        ox = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "yllcorner") {
                        oy = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "nodata_value" || token == "nodata") {
                        nullValueString = value;
                        nTokensFound++;
                    }

                } else {
                    std::cout << "ERROR: incorrect header format" << std::endl;
                    return false;
                }

                if (nTokensFound == 6)
                    break;
            };
        } catch(std::invalid_argument &e) {
            std::cout << "ERROR: cannot convert header value to number; " << e.what() << std::endl;
            return false;
        }

        // Check token count
        if (nTokensFound != 6) {
            std::cout << "ERROR: missing header information" << std::endl;
            return false;
        }
        
        // Check values
        if (nx <= 0) {
            std::cout << "ERROR: ncols must be greater than zero" << std::endl;
            return false;
        }
        if (ny <= 0) {
            std::cout << "ERROR: nrows must be greater than zero" << std::endl;
            return false;
        }
        if (h <= (CTYPE)0.0) {
            std::cout << "ERROR: spacing must be greater than zero" << std::endl;
            return false;
        }
              
        // Create raster
        r.init2D(nx, ny, h, h, ox, oy);

        // Parse remaining data
        uint32_t j = 0;
        try {
            while (std::getline(fileStream, line)) {
                
                // Split line
                auto splitLine = Strings::split(line, ' ');
                if (splitLine.size() != nx) {
                    std::cout << "ERROR: wrong number of entries in line" << std::endl;
                    return false;
                }

                // Set values
                for (uint32_t i = 0; i < nx; i++) {
                    RTYPE v = (splitLine[i] == nullValueString) ? 
                        Geostack::getNullValue<RTYPE>() : (RTYPE)std::stod(splitLine[i]);
                    r.setCellValue(v, i, (ny-1)-j);
                }
                
                // Increment and check vertical line counter
                if (j++ > ny) {
                    std::cout << "ERROR: unexpected data at end of ascii file" << std::endl;
                    return false;
                }
            };
        } catch(std::invalid_argument& e) {
            std::cout << "ERROR: cannot convert value to number; " << e.what() << std::endl;
            return false;
        }

        return true;
    }

    /**
    * Write raster to ESRI ascii format.
    * @param fileName output file name.
    * @return true if file is written, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool AsciiHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // Check for data
        if (!r.hasData()) {
            std::cout << "ERROR: raster has no data to write to ascii file" << std::endl;
            return false;
        }

        // Set config defaults
        std::string nullValueString = "-9999";

        // Parse config
        if (!jsonConfig.empty()) {
        
            // Parse Json
            std::string err;
            auto config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                std::cout << "ERROR: " << err << std::endl;
                return false;
            }
              
            // Set compression type
            if (config["nullValue"].is_string()) 
                nullValueString = config["compression"].string_value();
        }

        // Get raster dimensions
        auto dim = r.getRasterDimensions();

        // Check grid spacing is equal
        if (dim.d.hx != dim.d.hy) {
            std::cout << "ERROR: grid does not have equal x and y spacing" << std::endl;
            return false;
        }
      
        // Open file
        std::ofstream fileStream(fileName);
        if (!fileStream) {
            std::cout << "ERROR: cannot open '" << fileName << "' for writing" << std::endl;
            return false;
        }
        
        // Write header  
        fileStream << "ncols " << dim.d.nx << '\n';
        fileStream << "nrows " << dim.d.ny << '\n';
        fileStream << "cellsize " << dim.d.hx << '\n';
        fileStream << "nodata_value " << nullValueString << '\n';
        fileStream << "xllcorner " << dim.d.ox << '\n';
        fileStream << "yllcorner " << dim.d.oy << '\n';
        
        // Export data file
        RTYPE v;            
        for (uint32_t j = 0; j < dim.d.ny; j++) {
            for (uint32_t i = 0; i < dim.d.nx; i++) {
                v = r.getCellValue(i, dim.d.ny-1-j);
                if (Geostack::isValid(v))
                    fileStream << v << " ";
                else
                    fileStream << nullValueString << " ";
            }
            fileStream << '\n';
        }

        return true;
    }
    
    // RTYPE float, CTYPE float definitions
    template class AsciiHandler<float, float>;
    
    // RTYPE uint, CTYPE float definitions
    template class AsciiHandler<uint32_t, float>;
    
    // RTYPE double, CTYPE double definitions
    template class AsciiHandler<double, double>;
    
    // RTYPE uint, CTYPE double definitions
    template class AsciiHandler<uint32_t, double>;
}
