# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from enum import Enum, unique
import numbers

__all__ = ["GeometryType", "RasterCombinationType",
           "RasterResolutionType", "RasterNullValueType",
           "NeighboursType", "SeriesInterpolationType",
           "RasterInterpolationType", "ReductionType"]

class _baseEnum(Enum):
    def __and__(self, other):
        if isinstance(other, Enum):
            return self.value & other.value
        elif isinstance(other, numbers.Integral):
            return self.value & other
        else:
            raise TypeError("Invalid argument type")

    def __or__(self, other):
        if isinstance(other, Enum):
            return self.value | other.value
        elif isinstance(other, numbers.Integral):
            return self.value | other
        else:
            raise TypeError("Invalid argument type")

    def __xor__(self, other):
        if isinstance(other, Enum):
            return self.value ^ other.value
        elif isinstance(other, numbers.Integral):
            return self.value ^ other
        else:
            raise TypeError("Invalid argument type")

    def __rand__(self, other):
        if isinstance(other, numbers.Integral):
            return other & self.value
        else:
            raise TypeError("Invalid argument type")
    
    def __ror__(self, other):
        if isinstance(other, numbers.Integral):
            return other | self.value
        else:
            raise TypeError("Invalid argument type")
    
    def __rxor__(self, other):
        if isinstance(other, numbers.Integral):
            return other ^ self.value
        else:
            raise TypeError("Invalid argument type")

@unique
class SeriesInterpolationType(_baseEnum):
    Linear = 0
    MonotoneCubic = 1
    BoundedLinear = 2

@unique
class NeighboursType(_baseEnum):
    NoNeighbours = 0x00
    Rook = 0x01
    Queen = 0x03

@unique
class RasterCombinationType(_baseEnum):
    Union = 0
    Intersection = 1 << 0

@unique
class RasterResolutionType(_baseEnum):
    Minimum = 0
    Maximum = 1 << 2

@unique
class RasterInterpolationType(_baseEnum):
    Nearest = 0
    Bilinear = 1 << 4
    Bicubic = 2 << 4

@unique
class RasterNullValueType(_baseEnum):
    Null = 0
    Zero = 1 << 6
    One = 2 << 6

@unique
class GeometryType(_baseEnum):
    NoType = 0
    Point = 1
    LineString = 1 << 1
    Polygon = 1 << 2
    TileType = 1 << 3

@unique
class ReductionType(_baseEnum):
    NoReduction = 0
    Maximum = 1 << 8
    Minimum = 2 << 8
    Sum = 3 << 8
    Count = 4 << 8
    Mean = 5 << 8
