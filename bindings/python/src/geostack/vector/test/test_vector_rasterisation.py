# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import sys
import json
import numpy as np
from time import time
import pytest
from distutils import dir_util
sys.path.insert(0, os.path.realpath('../../../'))

from geostack.io import geoJsonToVector, vectorToGeoJson
from geostack.gs_enums import GeometryType
from geostack.vector import vector

@pytest.fixture
def test_geojson():
    _geo_json = """{"features": [
        {"geometry": {"coordinates": [0, 0.5], "type": "Point"},
            "properties": {"A": 1}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"A": 2}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"A": 3}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 4}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 5}, "type": "Feature"}
        ], "type": "FeatureCollection"}"""
    return _geo_json

@pytest.mark.xfail
def test_vec_rasterisation1(test_geojson):
    vec = geoJsonToVector(test_geojson)

    testRasterA = vec.rasterise(0.02)
    assert testRasterA.getCellValue(0, 25) == 1.0
    assert testRasterA.getCellValue(25, 25) == 1.0
    assert testRasterA.getCellValue(12, 12) == 1.0
    assert testRasterA.getCellValue(45, 46) == 1.0
    assert testRasterA.getCellValue(70, 51) == 1.0

@pytest.mark.xfail
def test_vec_rasterisation2(test_geojson):
    vec = geoJsonToVector(test_geojson)

    testRasterC = vec.rasterise(0.02, script="output = min(A, output);")
    assert testRasterC.getCellValue(0, 25) == 1.0
    assert testRasterC.getCellValue(25, 25) == 2.0
    assert testRasterC.getCellValue(12, 12) == 3.0
    assert testRasterC.getCellValue(45, 46) == 3.0
    assert testRasterC.getCellValue(70, 51) == 4.0

@pytest.mark.xfail
def test_vec_rasterisation3(test_geojson):
    vec = geoJsonToVector(test_geojson)

    testRasterD = vec.rasterise(0.02, script="output = max(A, output);")
    assert testRasterD.getCellValue(0, 25) == 1.0
    assert testRasterD.getCellValue(25, 25) == 2.0
    assert testRasterD.getCellValue(12, 12) == 3.0
    assert testRasterD.getCellValue(45, 46) == 5.0
    assert testRasterD.getCellValue(70, 51) == 5.0
