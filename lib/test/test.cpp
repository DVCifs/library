/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#define CATCH_CONFIG_MAIN
#define STRINGIFY(s) (#s)

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#include "catch.hpp"
#include "json11.hpp"

#include "gs_solver.h"
#include "gs_raster.h"
#include "gs_vector.h"
#include "gs_geojson.h"
#include "gs_projection.h"
#include "gs_series.h"
#include "gs_string.h"
#include "gs_ascii.h"
#include "gs_flt.h"
#include "gs_gsr.h"

using namespace Geostack;

TEST_CASE( "OpenCL initialisation", "[opencl]" ) {

    // Get solver singleton
    Solver &solver = Solver::getSolver();
    solver.setVerbose(true);
    solver.getContext();

    // Test initialisation
    REQUIRE( solver.openCLInitialised() == true );
}

TEST_CASE( "Raster initialisation 2D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.max() == (REAL)99.9 );
    REQUIRE( testRasterA.min() == (REAL)1.0 );
}

TEST_CASE( "Raster resize 2D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    // Resize
    testRasterA.resize2D(3, 3, 1, 1);

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(2, 2) == (REAL)99.9 );
    REQUIRE( testRasterA.max() == (REAL)99.9 );
    REQUIRE( testRasterA.min() == (REAL)1.0 );
}

TEST_CASE( "Raster variable script 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setVariableData<REAL>("varA", (REAL)99.9);
    testRasterA.setVariableData<REAL>("varB", (REAL)88.8);
    testRasterA.setVariableData<REAL>("varC", (REAL)77.7);

    // Run script
    runScript<REAL>("testRasterA = testRasterA::varA;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)99.9 );

    // Run script
    runScript<REAL>("testRasterA = testRasterA::varB;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)88.8 );

    // Run script
    runScript<REAL>("testRasterA = testRasterA::varC;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)77.7 );

    // Run script
    runScript<REAL>(
        "testRasterA = testRasterA::varA+testRasterA::varB+testRasterA::varC;",
        {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)99.9+(REAL)88.8+(REAL)77.7 );
}

TEST_CASE( "Raster float script 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(15, 15, 1.0, 1.0, -5, -5);
    testRasterB.setAllCellValues((REAL)2.0);

    makeRaster(testRasterC, REAL, REAL)
    testRasterC.init2D(15, 15, 1.0, 1.0, -5, -5);

    // Run script
    std::string script = "testRasterC = 100.0 + testRasterA * testRasterB;";
    runScript<REAL>(script, {testRasterC, testRasterA, testRasterB} );

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.max() == (REAL)299.8 );
    REQUIRE( testRasterC.min() == (REAL)102.0 );
    REQUIRE( testRasterC.getCellValue(7, 7) == (REAL)299.8 );
    REQUIRE( testRasterC.getNearestValue(2.5, 2.5) == (REAL)299.8 );
    REQUIRE( testRasterC.getBilinearValue(2.5, 2.5) == (REAL)299.8 );
}

TEST_CASE( "Raster float output script 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(15, 15, 1.0, 1.0, -5, -5);
    testRasterB.setAllCellValues((REAL)2.0);

    // Run script
    std::string script = "output = 100.0 + testRasterA * testRasterB;";
    auto testRasterC = runScript<REAL, REAL>(script, {testRasterA, testRasterB} );

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.max() == (REAL)299.8 );
    REQUIRE( testRasterC.min() == (REAL)102.0 );
    REQUIRE( testRasterC.getCellValue(7, 7) == (REAL)299.8 );
    REQUIRE( testRasterC.getNearestValue(2.5, 2.5) == (REAL)299.8 );
    REQUIRE( testRasterC.getBilinearValue(2.5, 2.5) == (REAL)299.8 );
}
 
TEST_CASE( "Raster integer output script 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, uint32_t, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues(1);
    testRasterA.setCellValue(99, 2, 2);

    makeRaster(testRasterB, uint32_t, REAL)
    testRasterB.init2D(15, 15, 1.0, 1.0, -5, -5);
    testRasterB.setAllCellValues(2);

    // Run script
    std::string script = "output = 100.0 + testRasterA * testRasterB;";
    auto testRasterC = runScript<uint32_t, REAL>(script, {testRasterA, testRasterB}, RasterNullValue::Zero);

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.max() == (uint32_t)298 );
    REQUIRE( testRasterC.min() == (uint32_t)100 );
    REQUIRE( testRasterC.getCellValue(7, 7) == (uint32_t)298 );
    REQUIRE( testRasterC.getNearestValue(2.5, 2.5) == (uint32_t)298 );
}
 
TEST_CASE( "Raster integer float output script 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(50, 50, (REAL)0.1, (REAL)0.1);
    testRasterA.setAllCellValues(0.0);
    
    makeRaster(testRasterB, uint32_t, REAL)
    testRasterB.init2D(50, 50, (REAL)0.1, (REAL)0.1);
    testRasterB.setAllCellValues(0);

    // Create gradient
    testRasterA = runScript<REAL, REAL>("output = x*y;", {testRasterA} );

    // Run script
    std::string script = "output = testRasterA;";
    auto testRasterC = runScript<uint32_t, REAL>(script, {testRasterB, testRasterA});

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getCellValue(26, 14) == (REAL)2.65*(REAL)1.45 );
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.getCellValue(26, 14) == (uint32_t)3 );
}

TEST_CASE( "Raster float area script 2D", "[raster]" ) {


    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(21, 21, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)0.0);
    testRasterA.setCellValue((REAL)99.9, 10, 10);

    // Run script
    std::string script = R"(
    
    // Average
    if (isValid_REAL(testRasterA)) { 
        output += testRasterA; 
        sum+=1.0;
    }
    
    )";
    auto output = runAreaScript<REAL, REAL>(script, testRasterA, 3);

    // Test values
    REQUIRE( output.hasData() == true );
    REQUIRE( output.getCellValue(10, 10) == ((REAL)99.9/(REAL)49.0) );
}

TEST_CASE( "Raster reduction script 2D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(500, 500, 1.0, 1.0);

    // Set values
    testRasterA.setAllCellValues((REAL)-99.9);
    testRasterA.setCellValue((REAL)99.9, 250, 250);
    
    // Test maximum reduction
    testRasterA.setReductionType(Reduction::Maximum);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)99.9);
    
    // Test minimum reduction
    testRasterA.setReductionType(Reduction::Minimum);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)-99.9);
    
    // Test maximum reduction
    testRasterA.setReductionType(Reduction::Maximum);
    runScript<REAL>("testRasterA = x*y;", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)249500.25);

    // Test count reduction
    testRasterA.setReductionType(Reduction::Count);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)500*500);

    // Test sum reduction
    testRasterA.setReductionType(Reduction::Sum);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)15625000000);

    // Test mean reduction
    testRasterA.setReductionType(Reduction::Mean);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)62500);
    
    // Test copy and maximum reduction
    auto testRasterB = runScript<REAL, REAL>("output = testRasterA;", { testRasterA }, Reduction::Maximum );
    REQUIRE(testRasterB.reduce() == (REAL)249500.25);
}

TEST_CASE( "Raster sampling 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(20, 20, 0.25, 0.25);

    // Run script
    //   This will default to use the minimum resolution of the two rasters
    std::string script = "output = testRasterA;";
    testRasterB = runScript<REAL, REAL>(script, {testRasterA, testRasterB} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(2.5, 2.5) == testRasterB.getNearestValue(2.5, 2.5) );
}

TEST_CASE( "Raster intersection 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(20, 20, 0.25, 0.25, -4, -4);
    testRasterB.setAllCellValues((REAL)-1.0);

    // Run script
    //   This will use zero as the null value and default 
    //   to use the minimum resolution of the two rasters
    std::string script = "output = testRasterA + testRasterB;";
    auto testRasterC = runScript<REAL, REAL>(script, {testRasterA, testRasterB}, RasterNullValue::Zero);

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.getCellValue(17, 17) == (REAL)0.0 );
}

TEST_CASE( "Raster properties 2D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);

    // Set properties
    testRasterA.setProperty("property0", 99);
    testRasterA.setProperty("property1", "rstr");

    // Test values
    REQUIRE( testRasterA.template getProperty<int>("property0") == 99 );
    REQUIRE( testRasterA.template getProperty<std::string>("property1") == "rstr" );
}

TEST_CASE( "Raster float script 3D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init(10, 10, 10, 1.0, 1.0, 1.0);
    runScript<REAL, REAL>("testRasterA = z*100000+y*1000+x*10;", { testRasterA } );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.max() == (REAL)959595 );
    REQUIRE( testRasterA.min() == (REAL)50505 );
    REQUIRE( testRasterA.getCellValue(5, 5, 5) == (REAL)555555 );

    // Create raster
    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init(12, 12, 12, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0);
    runScript<REAL>("testRasterB = testRasterA;", { testRasterB, testRasterA } );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.max() == (REAL)959595 );
    REQUIRE( testRasterB.min() == (REAL)50505 );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)252525 );
}

TEST_CASE( "Vector initialisation", "[vector]" ) {

    // Create vector data
    Vector<REAL> vector;
    Coordinate<REAL> c = { (REAL)144.9631, (REAL)-37.8136 };
    auto newPointIndex = vector.addPoint(c);
    vector.setProperty(newPointIndex, "newproperty", std::string("newstr"));

    // Test values
    REQUIRE( vector.getPointCoordinate(newPointIndex) == c );
    REQUIRE( vector.template getProperty<std::string>(newPointIndex, "newproperty") == "newstr" );
}

TEST_CASE( "Vector subregion", "[vector]" ) {

    // Create GeoJSON string
    std::string inputGeoJSON = R"({"features": [
        {"geometry": {"coordinates": [143, -36], "type": "Point"}, "properties": {}, "type": "Feature"},
        {"geometry": {"coordinates": [144, -37, 1], "type": "Point"}, "properties": {}, "type": "Feature"},
        {"geometry": {"coordinates": [145, -38, 1], "type": "Point"}, "properties": {"time": 10}, "type": "Feature"},
        {"geometry": {"coordinates": [146, -39, 2], "type": "Point"}, "properties": {"time": 10}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto vector = GeoJson<REAL>::geoJsonToVector(inputGeoJSON);

    // Test p, q slice
    auto r0 = vector.region({ {(REAL)143, (REAL)-36, (REAL)0}, {(REAL)143, (REAL)-36, (REAL)0}});
    auto &p0 = r0.getPointIndexes();
    REQUIRE( p0.size() == 1 );
    REQUIRE( r0.getPointCoordinate(p0[0]) == Coordinate<REAL>((REAL)143, (REAL)-36, (REAL)0) );
    
    // Test r slice
    auto r1 = vector.region({ {(REAL)143, (REAL)-36, (REAL)0.5}, {(REAL)146, (REAL)-39, (REAL)1.5}});
    auto &p1 = r1.getPointIndexes();
    REQUIRE( p1.size() == 1 );
    REQUIRE( r1.getPointCoordinate(p1[0]) == Coordinate<REAL>((REAL)144, (REAL)-37, (REAL)1, (REAL)0) );

    // Test s slice
    auto r2 = vector.region({ {(REAL)143, (REAL)-36, (REAL)0, (REAL)10}, {(REAL)146, (REAL)-39, (REAL)2, (REAL)10}});
    auto &p2 = r2.getPointIndexes();
    REQUIRE( p2.size() == 2 );
    REQUIRE( r2.getPointCoordinate(p2[0]) == Coordinate<REAL>((REAL)145, (REAL)-38, (REAL)1, (REAL)10) );
    REQUIRE( r2.getPointCoordinate(p2[1]) == Coordinate<REAL>((REAL)146, (REAL)-39, (REAL)2, (REAL)10) );
}

TEST_CASE( "Vector rasterisation", "[vector]" ) {

    // Create GeoJSON string
    std::string json = R"({"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"}, 
            "properties": {"A": 1}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"}, 
            "properties": {"A": 2}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"}, 
            "properties": {"A": 3}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"}, 
            "properties": {"A": 4}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"}, 
            "properties": {"A": 5}, "type": "Feature"}
        ], "type": "FeatureCollection"})";
    
    // Parse GeoJSON string
    auto v = GeoJson<REAL>::geoJsonToVector(json);

    // No script, default to mask of 1
    auto testRasterA = v.rasterise<REAL>(0.02);
    REQUIRE( testRasterA.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(25, 25) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(12, 12) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(45, 46) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(70, 51) == (REAL)1.0 );

    // No processing specified, A is order-specific
    auto testRasterB = v.rasterise<REAL>(0.02, "output = A;");
    REQUIRE( testRasterB.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterB.getCellValue(25, 25) == (REAL)2.0 );
    REQUIRE( testRasterB.getCellValue(12, 12) == (REAL)3.0 );
    REQUIRE( testRasterB.getCellValue(45, 46) == (REAL)5.0 );
    REQUIRE( testRasterB.getCellValue(70, 51) == (REAL)5.0 );
    
    // Minimum specified
    auto testRasterC = v.rasterise<REAL>(0.02, "output = min(A, output);");
    REQUIRE( testRasterC.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterC.getCellValue(25, 25) == (REAL)2.0 );
    REQUIRE( testRasterC.getCellValue(12, 12) == (REAL)3.0 );
    REQUIRE( testRasterC.getCellValue(45, 46) == (REAL)3.0 );
    REQUIRE( testRasterC.getCellValue(70, 51) == (REAL)4.0 );

    // Maximum specified
    auto testRasterD = v.rasterise<REAL>(0.02, "output = max(A, output);");
    REQUIRE( testRasterD.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterD.getCellValue(25, 25) == (REAL)2.0 );
    REQUIRE( testRasterD.getCellValue(12, 12) == (REAL)3.0 );
    REQUIRE( testRasterD.getCellValue(45, 46) == (REAL)5.0 );
    REQUIRE( testRasterD.getCellValue(70, 51) == (REAL)5.0 );
}

TEST_CASE( "Geohash", "[vector]" ) {

    // Create vector data
    Coordinate<double> c( { 144.9631, -37.8136 } );
    std::string geoHash = c.getGeoHash();

    // Test values
    REQUIRE( geoHash == "r1r0fsnzv41c" );
}

TEST_CASE( "GeoJSON", "[vector]" ) {

    // Create GeoJSON string
    std::string inputGeoJSON = R"({"features": [
        {"geometry": {"coordinates": [0, 0.5], "type": "Point"}, 
            "properties": {"p0": "pstr", "p1": 1, "p2": 1.1000000000000001}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"}, 
            "properties": {"l0": "lstr", "l1": 2, "l2": 2.2000000000000002}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"}, 
            "properties": {"y0": "ystr", "y1": 3, "y2": 3.2999999999999998}, "type": "Feature"}], "type": "FeatureCollection"})";
    
    // Parse GeoJSON string
    auto vector = GeoJson<REAL>::geoJsonToVector(inputGeoJSON);

    // Convert Vector to GeoJSON
    std::string outputGeoJSON = GeoJson<REAL>::vectorToGeoJson(vector);

    // Test strings
    REQUIRE( Strings::removeWhitespace(outputGeoJSON) == Strings::removeWhitespace(inputGeoJSON) );

    // Test properties
    auto &pointIndexes = vector.getPointIndexes();
    auto &lineStringIndexes = vector.getLineStringIndexes();
    auto &polygonIndexes = vector.getPolygonIndexes();
    REQUIRE( vector.template getProperty<std::string>(pointIndexes[0], "p0") == "pstr" );
    REQUIRE( vector.template getProperty<int>(lineStringIndexes[0], "l1") == 2 );
    REQUIRE( vector.template getProperty<double>(polygonIndexes[0], "y2") == (double)3.2999999999999998 );
}

TEST_CASE( "Vector projection from WKT", "[projection]" ) {

    // Create projections
    std::string projWKT_EPSG4326 = R"(GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]])";
    auto proj_EPSG4326 = Projection<double>::parseWKT(projWKT_EPSG4326);
    auto proj_EPSG4326_REAL = Projection<REAL>::parseWKT(projWKT_EPSG4326);

    std::string projWKT_EPSG3111 = R"(PROJCS["GDA94 / Vicgrid94",GEOGCS["GDA94",DATUM["Geocentric_Datum_of_Australia_1994",
        SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6283"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],
        PROJECTION["Lambert_Conformal_Conic_2SP"],PARAMETER["standard_parallel_1",-36],PARAMETER["standard_parallel_2",-38],PARAMETER["latitude_of_origin",-37],PARAMETER["central_meridian",145],PARAMETER["false_easting",2500000],PARAMETER["false_northing",2500000],AUTHORITY["EPSG","3111"],AXIS["Easting",EAST],AXIS["Northing",NORTH]])";
    auto proj_EPSG3111 = Projection<double>::parseWKT(projWKT_EPSG3111);
    auto proj_EPSG3111_REAL = Projection<REAL>::parseWKT(projWKT_EPSG3111);

    std::string projWKT_EPSG3577 = R"(PROJCS["GDA94 / Australian Albers",GEOGCS["GDA94",DATUM["Geocentric_Datum_of_Australia_1994",
        SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6283"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],
        PROJECTION["Albers_Conic_Equal_Area"],PARAMETER["standard_parallel_1",-18],PARAMETER["standard_parallel_2",-36],PARAMETER["latitude_of_center",0],PARAMETER["longitude_of_center",132],PARAMETER["false_easting",0],PARAMETER["false_northing",0],AUTHORITY["EPSG","3577"],AXIS["Easting",EAST],AXIS["Northing",NORTH]])";
    auto proj_EPSG3577 = Projection<double>::parseWKT(projWKT_EPSG3577);
    auto proj_EPSG3577_REAL = Projection<REAL>::parseWKT(projWKT_EPSG3577);

    std::string projWKT_EPSG28355 = R"(PROJCS["GDA94 / MGA zone 55",GEOGCS["GDA94",DATUM["Geocentric_Datum_of_Australia_1994",
        SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6283"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],
        PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",147],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",10000000],AUTHORITY["EPSG","28355"],AXIS["Easting",EAST],AXIS["Northing",NORTH]])";
    auto proj_EPSG28355 = Projection<double>::parseWKT(projWKT_EPSG28355);
    auto proj_EPSG28355_REAL = Projection<REAL>::parseWKT(projWKT_EPSG28355);

    // Test coordinate
    Coordinate<double> c_Elliptical = { 144.9631, -37.8136 };

    // Test Lambert conformal conic
    Coordinate<double> c_Lambert = c_Elliptical;
    Coordinate<double> c_Lambert_Target = { 2496750.96316546, 2409712.42995386 };
    Projection<double>::convert(c_Lambert, proj_EPSG3111, proj_EPSG4326);
    double delta_Lambert = std::hypot(c_Lambert.p-c_Lambert_Target.p, c_Lambert.q-c_Lambert_Target.q);
    REQUIRE( delta_Lambert < 1.0E-3 );

    Projection<double>::convert(c_Lambert, proj_EPSG4326, proj_EPSG3111);
    delta_Lambert = std::hypot(c_Lambert.p-c_Elliptical.p, c_Lambert.q-c_Elliptical.q);
    REQUIRE( delta_Lambert < 1.0E-6 );

    // Test Albers conic
    Coordinate<double> c_Albers = c_Elliptical;
    Coordinate<double> c_Albers_Target = { 1146469.071166, -4192119.31810244 };
    Projection<double>::convert(c_Albers, proj_EPSG3577, proj_EPSG4326);
    double delta_Albers = std::hypot(c_Albers.p-c_Albers_Target.p, c_Albers.q-c_Albers_Target.q);
    REQUIRE( delta_Albers < 1.0E-3 );

    Projection<double>::convert(c_Albers, proj_EPSG4326, proj_EPSG3577);
    delta_Albers = std::hypot(c_Albers.p-c_Elliptical.p, c_Albers.q-c_Elliptical.q);
    REQUIRE( delta_Albers < 1.0E-6 );

    // Test Mercator
    Coordinate<double> c_Transverse_Mercator = c_Elliptical;
    Coordinate<double> c_Transverse_Mercator_Target = { 320704.446321103, 5812911.69953155 };
    Projection<double>::convert(c_Transverse_Mercator, proj_EPSG28355, proj_EPSG4326);
    double delta_Transverse_Mercator = std::hypot(c_Transverse_Mercator.p-c_Transverse_Mercator_Target.p, c_Transverse_Mercator.q-c_Transverse_Mercator_Target.q);
    REQUIRE( delta_Transverse_Mercator < 1.0E-3 );

    Projection<double>::convert(c_Transverse_Mercator, proj_EPSG4326, proj_EPSG28355);
    delta_Transverse_Mercator = std::hypot(c_Transverse_Mercator.p-c_Elliptical.p, c_Transverse_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Transverse_Mercator < 1.0E-6 );

    // Test multi Lambert conformal conic 
    std::vector<Coordinate<REAL> > c_Lambert_multi;
    Coordinate<REAL> c_Lambert_Target_REAL = { (REAL)2496750.96316546, (REAL)2409712.42995386 };
    for (int i = 0; i < 10; i++) {
        c_Lambert_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection<REAL>::convert(c_Lambert_multi, proj_EPSG3111_REAL, proj_EPSG4326_REAL);
    REAL delta_Lambert_REAL = std::hypot(c_Lambert_multi[0].p-c_Lambert_Target_REAL.p, c_Lambert_multi[0].q-c_Lambert_Target_REAL.q);
    REQUIRE( delta_Lambert < 1.0 );
    
    // Test multi Albers
    std::vector<Coordinate<REAL> > c_Albers_multi;
    Coordinate<REAL> c_Albers_Target_REAL = { (REAL)1146469.071166, (REAL)-4192119.31810244 };
    for (int i = 0; i < 10; i++) {
        c_Albers_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection<REAL>::convert(c_Albers_multi, proj_EPSG3577_REAL, proj_EPSG4326_REAL);
    REAL delta_Albers_REAL = std::hypot(c_Albers_multi[0].p-c_Albers_Target_REAL.p, c_Albers_multi[0].q-c_Albers_Target_REAL.q);
    REQUIRE( delta_Albers < 10.0 );
    
    // Test multi Mercator
    std::vector<Coordinate<REAL> > c_Transverse_Mercator_multi;
    Coordinate<REAL> c_Transverse_Mercator_Target_REAL = { (REAL)320704.446321103, (REAL)5812911.69953155 };
    for (int i = 0; i < 10; i++) {
        c_Transverse_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection<REAL>::convert(c_Transverse_Mercator_multi, proj_EPSG28355_REAL, proj_EPSG4326_REAL);
    REAL delta_Transverse_Mercator_REAL = std::hypot(c_Transverse_Mercator_multi[0].p-c_Transverse_Mercator_Target_REAL.p, c_Transverse_Mercator_multi[0].q-c_Transverse_Mercator_Target_REAL.q);
    REQUIRE( delta_Transverse_Mercator < 1.0 );

}

TEST_CASE( "Vector projection from PROJ4", "[projection]" ) {

    // Create projections
    std::string projPROJ4_EPSG4326 = R"(+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs)";
    auto proj_EPSG4326 = Projection<double>::parsePROJ4(projPROJ4_EPSG4326);
    auto proj_EPSG4326_REAL = Projection<REAL>::parsePROJ4(projPROJ4_EPSG4326);

    std::string projPROJ4_EPSG3111 = R"(+proj=lcc +lat_1=-36 +lat_2=-38 +lat_0=-37 +lon_0=145 +x_0=2500000 +y_0=2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_EPSG3111 = Projection<double>::parsePROJ4(projPROJ4_EPSG3111);
    auto proj_EPSG3111_REAL = Projection<REAL>::parsePROJ4(projPROJ4_EPSG3111);

    std::string projPROJ4_EPSG3577 = R"(+proj=aea +lat_1=-18 +lat_2=-36 +lat_0=0 +lon_0=132 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_EPSG3577 = Projection<double>::parsePROJ4(projPROJ4_EPSG3577);
    auto proj_EPSG3577_REAL = Projection<REAL>::parsePROJ4(projPROJ4_EPSG3577);

    std::string projPROJ4_EPSG28355 = R"(+proj=utm +zone=55 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_EPSG28355 = Projection<double>::parsePROJ4(projPROJ4_EPSG28355);
    auto proj_EPSG28355_REAL = Projection<REAL>::parsePROJ4(projPROJ4_EPSG28355);

    std::string projPROJ4_EPSG3832 = R"(+proj=merc +lon_0=150 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs)";
    auto proj_EPSG3832 = Projection<double>::parsePROJ4(projPROJ4_EPSG3832);
    auto proj_EPSG3832_REAL = Projection<REAL>::parsePROJ4(projPROJ4_EPSG3832);

    std::string projPROJ4_EPSG3857 = R"(+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +no_defs)";
    auto proj_EPSG3857 = Projection<double>::parsePROJ4(projPROJ4_EPSG3857);
    auto proj_EPSG3857_REAL = Projection<REAL>::parsePROJ4(projPROJ4_EPSG3857);

    // Test coordinate
    Coordinate<double> c_Elliptical = { 144.9631, -37.8136 };

    // Test Lambert conformal conic
    Coordinate<double> c_Lambert = c_Elliptical;
    Coordinate<double> c_Lambert_Target = { 2496750.96316546, 2409712.42995386 };
    Projection<double>::convert(c_Lambert, proj_EPSG3111, proj_EPSG4326);
    double delta_Lambert = std::hypot(c_Lambert.p-c_Lambert_Target.p, c_Lambert.q-c_Lambert_Target.q);
    REQUIRE( delta_Lambert < 1.0E-3 );

    Projection<double>::convert(c_Lambert, proj_EPSG4326, proj_EPSG3111);
    delta_Lambert = std::hypot(c_Lambert.p-c_Elliptical.p, c_Lambert.q-c_Elliptical.q);
    REQUIRE( delta_Lambert < 1.0E-6 );

    // Test Albers conic
    Coordinate<double> c_Albers = c_Elliptical;
    Coordinate<double> c_Albers_Target = { 1146469.071166, -4192119.31810244 };
    Projection<double>::convert(c_Albers, proj_EPSG3577, proj_EPSG4326);
    double delta_Albers = std::hypot(c_Albers.p-c_Albers_Target.p, c_Albers.q-c_Albers_Target.q);
    REQUIRE( delta_Albers < 1.0E-3 );

    Projection<double>::convert(c_Albers, proj_EPSG4326, proj_EPSG3577);
    delta_Albers = std::hypot(c_Albers.p-c_Elliptical.p, c_Albers.q-c_Elliptical.q);
    REQUIRE( delta_Albers < 1.0E-6 );

    // Test Transverse Mercator
    Coordinate<double> c_Transverse_Mercator = c_Elliptical;
    Coordinate<double> c_Transverse_Mercator_Target = { 320704.446321103, 5812911.69953155 };
    Projection<double>::convert(c_Transverse_Mercator, proj_EPSG28355, proj_EPSG4326);
    double delta_Transverse_Mercator = std::hypot(c_Transverse_Mercator.p-c_Transverse_Mercator_Target.p, c_Transverse_Mercator.q-c_Transverse_Mercator_Target.q);
    REQUIRE( delta_Transverse_Mercator < 1.0E-3 );

    Projection<double>::convert(c_Transverse_Mercator, proj_EPSG4326, proj_EPSG28355);
    delta_Transverse_Mercator = std::hypot(c_Transverse_Mercator.p-c_Elliptical.p, c_Transverse_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Transverse_Mercator < 1.0E-6 );

    // Test Mercator
    Coordinate<double> c_Mercator = c_Elliptical;
    Coordinate<double> c_Mercator_Target = { -560705.14, -4526927.40 };
    Projection<double>::convert(c_Mercator, proj_EPSG3832, proj_EPSG4326);
    double delta_Mercator = std::hypot(c_Mercator.p-c_Mercator_Target.p, c_Mercator.q-c_Mercator_Target.q);
    REQUIRE( delta_Mercator < 1.0E-2 );

    Projection<double>::convert(c_Mercator, proj_EPSG4326, proj_EPSG3832);
    delta_Mercator = std::hypot(c_Mercator.p-c_Elliptical.p, c_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Mercator < 1.0E-6 );

    // Test Web Mercator
    Coordinate<double> c_Web_Mercator = c_Elliptical;
    Coordinate<double> c_Web_Mercator_Target = { 16137218.48, -4553127.11 };
    Projection<double>::convert(c_Web_Mercator, proj_EPSG3857, proj_EPSG4326);
    double delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Web_Mercator_Target.p, c_Web_Mercator.q-c_Web_Mercator_Target.q);
    REQUIRE( delta_Web_Mercator < 1.0E-2 );

    Projection<double>::convert(c_Web_Mercator, proj_EPSG4326, proj_EPSG3857);
    delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Elliptical.p, c_Web_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Web_Mercator < 1.0E-6 );

    // Test multi Lambert conformal conic 
    std::vector<Coordinate<REAL> > c_Lambert_multi;
    Coordinate<REAL> c_Lambert_Target_REAL = { (REAL)2496750.96316546, (REAL)2409712.42995386 };
    for (int i = 0; i < 10; i++) {
        c_Lambert_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection<REAL>::convert(c_Lambert_multi, proj_EPSG3111_REAL, proj_EPSG4326_REAL);
    REAL delta_Lambert_REAL = std::hypot(c_Lambert_multi[0].p-c_Lambert_Target_REAL.p, c_Lambert_multi[0].q-c_Lambert_Target_REAL.q);
    REQUIRE( delta_Lambert_REAL < 1.0 );
    
    // Test multi Albers
    std::vector<Coordinate<REAL> > c_Albers_multi;
    Coordinate<REAL> c_Albers_Target_REAL = { (REAL)1146469.071166, (REAL)-4192119.31810244 };
    for (int i = 0; i < 10; i++) {
        c_Albers_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection<REAL>::convert(c_Albers_multi, proj_EPSG3577_REAL, proj_EPSG4326_REAL);
    REAL delta_Albers_REAL = std::hypot(c_Albers_multi[0].p-c_Albers_Target_REAL.p, c_Albers_multi[0].q-c_Albers_Target_REAL.q);
    REQUIRE( delta_Albers_REAL < 10.0 );
    
    // Test multi Transverse Mercator
    std::vector<Coordinate<REAL> > c_Transverse_Mercator_multi;
    Coordinate<REAL> c_Transverse_Mercator_Target_REAL = { (REAL)320704.446321103, (REAL)5812911.69953155 };
    for (int i = 0; i < 10; i++) {
        c_Transverse_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection<REAL>::convert(c_Transverse_Mercator_multi, proj_EPSG28355_REAL, proj_EPSG4326_REAL);
    REAL delta_Transverse_Mercator_REAL = std::hypot(c_Transverse_Mercator_multi[0].p-c_Transverse_Mercator_Target_REAL.p, c_Transverse_Mercator_multi[0].q-c_Transverse_Mercator_Target_REAL.q);
    REQUIRE( delta_Transverse_Mercator_REAL < 1.0 );

    // Test multi Mercator
    std::vector<Coordinate<REAL> > c_Mercator_multi;
    Coordinate<REAL> c_Mercator_Target_REAL = { (REAL)-560705.14, (REAL)-4526927.40 };
    for (int i = 0; i < 10; i++) {
        c_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection<REAL>::convert(c_Mercator_multi, proj_EPSG3832_REAL, proj_EPSG4326_REAL);
    REAL delta_Mercator_REAL = std::hypot(c_Mercator_multi[0].p-c_Mercator_Target_REAL.p, c_Mercator_multi[0].q-c_Mercator_Target_REAL.q);
    REQUIRE( delta_Mercator_REAL < 10.0 );

    // Test multi Web Mercator
    std::vector<Coordinate<REAL> > c_Web_Mercator_multi;
    Coordinate<REAL> c_Web_Mercator_Target_REAL = { (REAL)16137218.48, (REAL)-4553127.11 };
    for (int i = 0; i < 10; i++) {
        c_Web_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection<REAL>::convert(c_Web_Mercator_multi, proj_EPSG3857_REAL, proj_EPSG4326_REAL);
    REAL delta_Web_Mercator_REAL = std::hypot(c_Web_Mercator_multi[0].p-c_Web_Mercator_Target_REAL.p, c_Web_Mercator_multi[0].q-c_Web_Mercator_Target_REAL.q);
    REQUIRE( delta_Web_Mercator < 10.0 );

}

TEST_CASE( "Time parsing", "[time]" ) {

    // Create vector data
    auto timeSinceEpoch = Strings::iso8601toEpoch("2010-01-10T08:05:10Z");
    auto failTest = Strings::iso8601toEpoch("FAIL_TEST");

    // Test values
    REQUIRE( timeSinceEpoch == 1263110710000 );
    REQUIRE( failTest == getNullValue<int64_t>() );
}

TEST_CASE( "Series", "[series]" ) {

    // Test constant series
    Series<double, double> s0;
    s0.addValue(1.0, 2.0);

    REQUIRE( s0.isInitialised() == true );
    REQUIRE( s0.isConstant() == true );
    REQUIRE( s0(1.0) == 2.0 );

    // Test series
    Series<double, double> s1;
    s1.addValues( { { 1.0, 20.0 }, { 0.0, 10.0 }, { 10.0, 0.0 }, { 2.0, 30.0 } } );

    REQUIRE( s1.isInitialised() == true );
    REQUIRE( s1.isConstant() == false );
    REQUIRE( s1.get_xMin() == 0.0 );
    REQUIRE( s1.get_xMax() == 10.0 );
    REQUIRE( s1.get_yMin() == 0.0 );
    REQUIRE( s1.get_yMax() == 30.0 );
    REQUIRE( s1(0.5) == 15.0 );
    REQUIRE( s1(1.5) == 25.0 );
    
    // Test series monotone interpolation
    s1.setInterpolation(SeriesInterpolation::MonotoneCubic);
    REQUIRE( s1(6.0) == 18.75 );
    
    // Test bounded linear interpolation
    s1.setInterpolation(SeriesInterpolation::BoundedLinear);
    s1.setBounds(0.0, 40.0);
    REQUIRE( s1(6.0) == 35.0 );

    // Test string-based series
    Series<double, double> s2;
    s2.addValues( { { "1.0", 20.0 }, { "0.0", 10.0 }, { "10.0", 0.0 }, { "2.0", 30.0 } } );

    REQUIRE( s2.isInitialised() == true );
    REQUIRE( s2.isConstant() == false );
    REQUIRE( s2.get_xMin() == 0.0 );
    REQUIRE( s2.get_xMax() == 10.0 );
    REQUIRE( s2.get_yMin() == 0.0 );
    REQUIRE( s2.get_yMax() == 30.0 );
    REQUIRE( s2(0.5) == 15.0 );
    REQUIRE( s2(1.5) == 25.0 );
    
    // Test long series
    Series<int64_t, double> s3;
    s3.addValues( { { 1, 20.0 }, { 0, 10.0 }, { 10, 0.0 }, { 2, 30.0 } } );

    REQUIRE( s3.isInitialised() == true );
    REQUIRE( s3.isConstant() == false );
    REQUIRE( s3.get_xMin() == 0.0 );
    REQUIRE( s3.get_xMax() == 10.0 );
    REQUIRE( s3.get_yMin() == 0.0 );
    REQUIRE( s3.get_yMax() == 30.0 );
    REQUIRE( s3(6) == 15.0 );
}

TEST_CASE( "Time series", "[series]" ) {

    // Test string-based series
    Series<int64_t, double> s0;
    s0.addValues( { 
        { "2010-01-10T08:06:10Z", 20.0 }, 
        { "2010-01-10T08:05:10Z", 10.0 }, 
        { "2010-01-10T08:15:10Z", 0.0 }, 
        { "2010-01-10T08:07:10Z", 30.0 }
    } );

    REQUIRE( s0.isInitialised() == true );
    REQUIRE( s0.isConstant() == false );
    REQUIRE( s0.get_xMin() == Strings::iso8601toEpoch("2010-01-10T08:05:10Z") );
    REQUIRE( s0.get_xMax() == Strings::iso8601toEpoch("2010-01-10T08:15:10Z") );
    REQUIRE( s0.get_yMin() == 0.0 );
    REQUIRE( s0.get_yMax() == 30.0 );
    REQUIRE( s0(Strings::iso8601toEpoch("2010-01-10T08:05:40Z")) == 15.0 );
    REQUIRE( s0(Strings::iso8601toEpoch("2010-01-10T08:06:40Z")) == 25.0 );
}
