# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref
from libcpp.memory cimport unique_ptr, shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport nullptr_t, nullptr
from libcpp cimport bool
import numpy as np
cimport numpy as np
from ..vector._cy_vector cimport Vector
from ..core._cy_json cimport Json
from ..vector._cy_vector cimport _Vector_d, _Vector_f

np.import_array()

cdef extern from "gs_network_flow.h" namespace "Geostack":
    ctypedef enum NetworkNodeType "NetworkNodeType::Type":
        Junction
        Terminator
    
    ctypedef enum NetworkSegmentType "NetworkSegmentType::Type":
        Undefined
        HazenWilliams
        MannigOpenChannel
        Logarithmic
        SqrtExp

    cdef cppclass NetworkFlowSolver[T]:
        bool init(Vector[T] &network, string jsonConfig)
        bool run()
        Vector[T]& getNetwork()


cdef class cyNetworkFlowSolver_d:
    cdef unique_ptr[NetworkFlowSolver[double]] thisptr

    def __cinit__(self):
        self.thisptr.reset(new NetworkFlowSolver[double]())

    cdef bool init(self, Vector[double]& network, string jsonConfig):
        cdef bool out
        out = deref(self.thisptr).init(network, jsonConfig)
        return out

    cpdef bool init_solver(self, _Vector_d input_network, string jsonConfig):
        cdef bool out
        out = self.init(deref(input_network.thisptr), jsonConfig)
        return out

    cpdef _Vector_d getNetwork(self):
        out = _Vector_d()
        cdef Vector[double] _out
        _out = deref(self.thisptr).getNetwork()
        out.c_copy(_out)
        return out

    cpdef bool run(self):
        cdef bool out
        out = deref(self.thisptr).run()
        return out
    
    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if self.thisptr != nullptr:
            if type(self) is cyNetworkFlowSolver_d:
                self.thisptr.reset()

    def __exit__(self):
        self.__dealloc__()


cdef class cyNetworkFlowSolver_f:
    cdef unique_ptr[NetworkFlowSolver[float]] thisptr
    def __cinit__(self):
        self.thisptr.reset(new NetworkFlowSolver[float]())
    
    cdef bool init(self, Vector[float]& network, string jsonConfig):
        cdef bool out
        out = deref(self.thisptr).init(network, jsonConfig)
        return out

    cpdef bool init_solver(self, _Vector_f input_network, string jsonConfig):
        cdef bool out
        out = self.init(deref(input_network.thisptr), jsonConfig)
        return out

    cpdef _Vector_f getNetwork(self):
        out = _Vector_f()
        cdef Vector[float] _out
        _out = deref(self.thisptr).getNetwork()
        out.c_copy(_out)
        return out

    cpdef bool run(self):
        cdef bool out
        out = deref(self.thisptr).run()
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if self.thisptr != nullptr:
            if type(self) is cyNetworkFlowSolver_f:
                self.thisptr.reset()

    def __exit__(self):
        self.__dealloc__()