typedef struct parameters_struct {

    REAL time;            // Current time
    REAL dt;              // Current time step
    REAL dt_last;         // Last time step
    REAL max_speed;       // Maximum speed in domain
    REAL area;            // Area within perimeter
    REAL band_width;      // Width of narrow band in world units
    REAL Julian_date;     // Current Julian date
    REAL Julian_fraction; // Julian fraction for current day

} __attribute__ ((aligned (8))) parameters;
