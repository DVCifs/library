/**
* Level set step update
*/
__kernel void update(
parameters _p,
__global REAL *_vars,
/*__ARGS__*/
) {

    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);

/*__REDUCE__
// Initialise reduction
size_t lid = get_local_id(0)+get_local_id(1)*get_local_size(0);
size_t _lsize = get_local_size(0)*get_local_size(1);
__REDUCE_HEAD__
__REDUCE__*/

    // Local atomic variables
    __local uint _cell_count;
    __local uint _tile_boundary;
    if (lid == 0) {
        _cell_count = 0;
        _tile_boundary = 0;
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {

        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;

/*__VARS__*/

        // Cell state is given by lower bit of classification buffer 
        uint _class_state = _class&1;

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (_class&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = _class>>24;

        // Get speed - set to zero outside narrow band
        REAL speed = fabs(_distance_update) < _p.band_width ? _speed : 0.0;
    
        // Update forcing
        REAL _dx = fmax(fmax(_distance_update-_distance_update_W, _distance_update-_distance_update_E)/_dim.hx, (REAL)0.0);
        REAL _dy = fmax(fmax(_distance_update-_distance_update_S, _distance_update-_distance_update_N)/_dim.hy, (REAL)0.0);
        REAL _r2 = -speed*hypot(_dx, _dy);
    
        // Time integrate with Runge-Kutta 2-step method
        _rate = 0.5*(_rate+_r2);
        _distance+=_p.dt*_rate;

        // Update arrival time
        if (_distance <= 0.0 && !isValid_REAL(_arrival)) {

            // Calculate corrected arrival time from level set and local rate
            if (_rate != 0.0) {
                REAL _tcorrect = fmin(_distance/_rate, _p.dt);
                _arrival = fmax(_p.time-_tcorrect, (REAL)0.0); 
            } else {
                _arrival = _p.time; 
            }

            //// Update start time
            //_start_time_update = _arrival;
            //if (isValid_REAL(_start_time_N))
            //    _start_time_update = min(_start_time_update, _start_time_N);
            //if (isValid_REAL(_start_time_NE))
            //    _start_time_update = min(_start_time_update, _start_time_NE);
            //if (isValid_REAL(_start_time_E))
            //    _start_time_update = min(_start_time_update, _start_time_E);
            //if (isValid_REAL(_start_time_SE))
            //    _start_time_update = min(_start_time_update, _start_time_SE);
            //if (isValid_REAL(_start_time_S))
            //    _start_time_update = min(_start_time_update, _start_time_S);
            //if (isValid_REAL(_start_time_SW))
            //    _start_time_update = min(_start_time_update, _start_time_SW);
            //if (isValid_REAL(_start_time_W))
            //    _start_time_update = min(_start_time_update, _start_time_W);
            //if (isValid_REAL(_start_time_NW))
            //    _start_time_update = min(_start_time_update, _start_time_NW);
        }
    
        // Run user code
        if (isValid_REAL(_arrival)) {
    
            // ---------------------------------
            // User defined code
/*__CODE__*/
            // ---------------------------------

            // Update burnt cell count
            atomic_inc(&_cell_count);
        
            // Set state
            _class = (_class&~1)|(_class_state&1);
        }

        // Update boundary values to identify if a new tile needs to be created
        if (_distance < _p.band_width) {

            // Set bits to represent interior cell
            _tile_boundary |= 0x10;

            // Set bits to represent boundary approach
            if (_j == _dim.ny-1) _tile_boundary |= 0x01; // Interface is near north boundary
            if (_i == _dim.nx-1) _tile_boundary |= 0x02; // Interface is near east boundary
            if (_j == 0        ) _tile_boundary |= 0x04; // Interface is near south boundary
            if (_i == 0        ) _tile_boundary |= 0x08; // Interface is near west boundary
        }

/*__POST__*/

    }

    // Update atomic variables
    barrier(CLK_LOCAL_MEM_FENCE);
    if (lid == 0) {
        atomic_add(__arrival_status, _cell_count);
        atomic_or(__distance_status, _tile_boundary);
    }

/*__REDUCE__
// Calculate reduction
barrier(CLK_LOCAL_MEM_FENCE);
for (size_t _step = _lsize>>1; _step >= 1; _step>>=1) {
    if (lid < _step) {
        __REDUCE_CODE__
    }
    barrier(CLK_LOCAL_MEM_FENCE);
}
if (lid == 0) {
    size_t gid = get_group_id(0)+get_num_groups(0)*(get_group_id(1)+get_num_groups(1)*get_group_id(2));
    __REDUCE_POST__
}
__REDUCE__*/

}
