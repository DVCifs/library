#include "pyTestScript.h"

namespace Geostack
{
namespace pyGeostack
{
bool testSolver(bool verbosity)
{
    // Get solver singleton
    Solver &solver = Solver::getSolver();
    solver.setVerbose(verbosity);

    // Get context from solver
    solver.getContext();
    if (!solver.openCLInitialised())
    {
        std::cout << "ERROR: OpenCL not initialised" << std::endl;
        return false;
    }
    else
    {
        return true;
    }
    solver.setVerbose(false);
}

} // namespace pyGeostack

} // namespace Geostack