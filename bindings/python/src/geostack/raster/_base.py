# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from enum import Enum, unique
import warnings
import numbers
from collections import Counter
from typing import Union, Iterable
import numpy as np
from .. import core
from .. import gs_enums
from . import raster
from ..vector import vector, _cy_vector
from ._cy_raster import (DataFileHandler_f, DataFileHandler_d,
    DataFileHandler_d_i, DataFileHandler_f_i, TileSpecifications)
from ._cy_raster import (_cyRaster_d, _cyRaster_f, _cyRaster_d_i,
    _cyRaster_f_i)
from ..runner import runScript, runAreaScript

@unique
class RasterKind(Enum):
    Raster1D = 1
    Raster2D = 2
    Raster3D = 3

    def __eq__(self, other):
        if isinstance(other, numbers.Integral):
            return self.value == other
        else:
            return self == other

    def __req__(self, other):
        if isinstance(other, numbers.Integral):
            return other == self.value
        else:
            return other == self

class _RasterBase(core.PropertyHandler):
    def __init__(self, base_type, data_type):
        self._dtype = None
        self._handle = None
        self._has_input_handler = False
        self.tileNum = 0
        self.base_type = base_type
        self.data_type = data_type
        self._properties = {}
        self._tmp_script = []
        self._tmp_var_count = None
        self._tmp_var_vector = []

    def getVariableData(self, name):
    
        # Set variable data
        _name = name
        if isinstance(_name, str):
            _name = _name.encode('utf-8')
            
        # Get handle
        if isinstance(self, raster.RasterFile):
            return self._handle.cy_raster_obj.getVariableData(_name)
        else:
            return self._handle.getVariableData(_name)
        return None

    def setVariableData(self, name, value):

        # Set variable data
        _name = name
        if isinstance(_name, str):
            _name = _name.encode('utf-8')
            
        # Get handle
        if isinstance(self, raster.RasterFile):
            self._handle.cy_raster_obj.setVariableData(_name, self.data_type(value))
        else:
            self._handle.setVariableData(_name, self.data_type(value))
            

    @property
    def name(self):
        out = self.getProperty('name')
        return out

    @name.setter
    def name(self, other):
        self.setProperty("name", other)

    @property
    def nullValue(self):
        return raster.getNullValue(self.data_type)

    def setProjectionParameters(self, other):
        """Set projection parameters for the raster.

        Parameters
        ----------
        other : ProjectionParameters object
            An instance of projection parameters obtained from a wkt or proj4.

        Returns
        -------
        Nil
        """
        if not isinstance(other, core.ProjectionParameters):
            raise TypeError("Argument should be an instance of ProjectionParameters")
        if isinstance(self, raster.Raster):
            self._handle.setProjectionParameters(other._handle)
        elif isinstance(self, raster.RasterFile):
            self._handle.cy_raster_obj.setProjectionParameters(other._handle)

    def getProjectionParameters(self):
        """Get projection parameters from the raster.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : ProjectionParameters
            Returns Raster projection parameters as an instance of ProjectionParameters
        """
        if isinstance(self, raster.Raster):
            out = core.ProjectionParameters.from_proj_param(
                self._handle.getProjectionParameters())
        elif isinstance(self, raster.RasterFile):
            out = core.ProjectionParameters.from_proj_param(
                self._handle.cy_raster_obj.getProjectionParameters())
        return out

    def region(self, other):
        if not isinstance(other, (vector.BoundingBox, _cy_vector._BoundingBox_d,
            _cy_vector._BoundingBox_f)):
            raise TypeError("Input argument should be an instance of BoundingBox")
        if self.base_type != other._dtype:
            raise TypeError("Type mismatch between Raster instance and input BoundingBox")
        if isinstance(self, raster.Raster):
            out = raster.Raster.copy("", self._handle.region(other._handle))
        elif isinstance(self, raster.RasterFile):
            if self._has_input_handler:
                temp = self._handle.cy_raster_obj.region(other._handle)
                out = raster.Raster.copy("", temp)
                return out
        return out

    def clip(self, other, projParams=None):
        if isinstance(other, (raster.Raster, raster.RasterFile)):
            bbox = other.getBounds()
            if self.getProjectionParameters() != other.getProjectionParameters():
                _bbox = bbox.convert(self.getProjectionParameters(),
                    other.getProjectionParameters())
            else:
                _bbox = bbox
        elif isinstance(other, vector.BoundingBox):
            if projParams is None:
                raise ValueError("ProjectionParameters must be provided for BoundingBox")
            if not isinstance(projParams, core.ProjectionParameters):
                raise TypeError("projParams should be an instance of ProjectionParameters")
            if self.getProjectionParameters() != projParams:
                _bbox = other.convert(self.getProjectionParameters(), projParams)
            else:
                _bbox = other
        else:
            raise TypeError("Argument 'other' should be an instance of Raster/RasterFile/BoundingBox")

        out = self.region(_bbox)
        out.setProperty("name", self.getProperty("name"))
        out.setProjectionParameters(self.getProjectionParameters())
        return out

    def get_tile_idx_bounds(self, idx):
        tileSize = TileSpecifications().tileSize
        ti = idx % self.raster_dim.tx
        tj = idx // self.raster_dim.tx
        idx_s = ti * tileSize
        idx_e = min((ti + 1) * tileSize, self.raster_dim.nx)
        jdx_s = tj * tileSize
        jdx_e = min((tj + 1) * tileSize, self.raster_dim.ny)
        return idx_s, idx_e, jdx_s, jdx_e

    def get_full_data(self):
        if self._handle is None:
            return
        elif isinstance(self, raster.RasterFile):
            if self._handle.cy_raster_obj.hasData() == False:
                raise RuntimeError("RasterFile object is not initialized")
        if not hasattr(self, "raster_dim"):
            self.raster_dim = self.getDimensions()
        
        # Get raster kind
        if isinstance(self, raster.RasterFile):
            raster_kind = self._handle.cy_raster_obj.getRasterKind()
        else:
            raster_kind = self._handle.getRasterKind()
            
        if raster_kind == RasterKind.Raster2D:
            out_data = np.full((self.raster_dim.ny,
                                self.raster_dim.nx),
                                self.nullValue,
                                dtype=self.data_type)
        elif raster_kind == RasterKind.Raster3D:
            out_data = np.full((self.raster_dim.nz,
                                self.raster_dim.ny,
                                self.raster_dim.nx),
                                self.nullValue,
                                dtype=self.data_type)
        else:
            raise NotImplementedError("only 2D and 3D rasters are handled")
        
        numTiles = self.raster_dim.tx * self.raster_dim.ty
        for idx in range(numTiles):
            idx_s, idx_e, jdx_s, jdx_e = self.get_tile_idx_bounds(idx)
            if raster_kind == RasterKind.Raster2D:
                out_data[jdx_s:jdx_e, idx_s:idx_e] = self.get_tile(idx)
            elif raster_kind == RasterKind.Raster3D:
                out_data[:, jdx_s:jdx_e, idx_s:idx_e] = self.get_tile(idx)
        return out_data

    def resize2D(self, nx, ny, tox, toy):
        if isinstance(self, raster.Raster):
            out = self._handle.resize2D(np.uint32(nx), np.uint32(ny),
                np.uint32(tox), np.uint32(toy))
        elif isinstance(self, raster.RasterFile):
            out = self._handle.cy_raster_obj.resize2D(np.uint32(nx), np.uint32(ny),
                np.uint32(tox), np.uint32(toy))
        return out

    def __getitem__(self, val):
        if not hasattr(self, "raster_dim"):
            self.raster_dim = self.getDimensions()
        num_tiles = self.raster_dim.tx * self.raster_dim.ty

        if not isinstance(val, (list, tuple)):
            if isinstance(val, int):
                return self.get_tile(val)
            elif isinstance(val, slice):
                start, stop, step = val
                _start = 1 if start is None else start
                _stop = num_tiles if stop is None else stop
                _step = 1 if step is None else step
            elif val is Ellipsis:
                return self.get_full_data()
        else:
            if len(val) > 2:
                raise NotImplementedError("Only Raster with 2D tiles has been implemented")
            else:
                first_arg, second_arg = val
                if isinstance(first_arg, int) and isinstance(second_arg, int):
                    return self.get_tile(first_arg * second_arg)
                else:
                    raise NotImplementedError("Only integer indexing supported for multiple args")

    def get_next_tile(self):
        if not hasattr(self, "raster_dim"):
            self.raster_dim = self.getDimensions()
        num_tiles = self.raster_dim.tx * self.raster_dim.ty
        self.tileNum += 1
        if self.tileNum < num_tiles:
            return self.get_tile(self.tileNum)
        else:
            raise StopIteration()

    def get_tile(self, tileNum):
        if not hasattr(self, "raster_dim"):
            self.raster_dim = self.getDimensions()
        num_tiles = self.raster_dim.tx * self.raster_dim.ty
        if tileNum > num_tiles:
            raise ValueError("Request tile number of out of bounds")
        ti = tileNum % self.raster_dim.tx
        tj = tileNum // self.raster_dim.tx
        if isinstance(self, raster.Raster):
            return self.writeData(ti=ti, tj=tj)
        elif isinstance(self, raster.RasterFile):
            return self.getData(ti=ti, tj=tj)

    def getReductionType(self):
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return gs_enums.ReductionType(self._handle.getReductionType())
            elif isinstance(self, raster.RasterFile):
                return gs_enums.ReductionType(self._handle.cy_raster_obj.getReductionType())
        else:
            raise RuntimeError("Raster is not yet initialized")

    def setReductionType(self, other):
        if self._handle is not None and isinstance(other, gs_enums.ReductionType):
            if isinstance(self, raster.Raster):
                self._handle.setReductionType(other.value)
            elif isinstance(self, raster.RasterFile):
                self._handle.cy_raster_obj.setReductionType(other.value)
        else:
            raise RuntimeError("Raster is not yet initialized")

    def getRequiredNeighbours(self):
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return gs_enums.NeighboursType(self._handle.getRequiredNeighbours())
            elif isinstance(self, raster.RasterFile):
                return gs_enums.NeighboursType(self._handle.cy_raster_obj.getRequiredNeighbours())
        else:
            raise RuntimeError("Raster is not yet initialized")

    def setRequiredNeighbours(self, other):
        if self._handle is not None:
            if not isinstance(other, gs_enums.NeighboursType):
                raise TypeError("input argument should be of NeighboursType")
            if isinstance(self, raster.Raster):
                self._handle.setRequiredNeighbours(other.value)
            elif isinstance(self, raster.RasterFile):
                self._handle.cy_raster_obj.setRequiredNeighbours(other.value)
        else:
            raise RuntimeError("Raster is not yet initialized")

    def max(self):
        """Get maximum value of the Raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : int/float32/float64
            return the maximum value from the Raster.
        """
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return self._handle.maxVal()
            elif isinstance(self, raster.RasterFile):
                if self._handle.cy_raster_obj.hasData() == False:
                    raise RuntimeError("RasterFile object is not initialized")
                return self._handle.cy_raster_obj.maxVal()

    def min(self):
        """Get minimum value of the Raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : int/float32/float64
            return the minimum value from the Raster.
        """
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return self._handle.minVal()
            elif isinstance(self, raster.RasterFile):
                if self._handle.cy_raster_obj.hasData() == False:
                    raise RuntimeError("RasterFile object is not initialized")
                return self._handle.cy_raster_obj.minVal()

    def getCellValue(self, i, j=0.0, k=0.0):
        """Get cell value for a location within the Raster.

        Parameters
        ----------
        i : int
            grid cell index along x-axis
        j : int (optional)
            grid cell index along y-axis
        k : int (optional)
            grid cell index along z-axis

        Returns
        -------
        out : float32/uint32/float64
            Raster value at a given index within the raster.
        """
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return self._handle.getCellValue(i, j=j, k=k)
            elif isinstance(self, raster.RasterFile):
                return self._handle.cy_raster_obj.getCellValue(i, j=j, k=k)
        else:
            raise RuntimeError("Raster is not yet initialized")

    def getNearestValue(self, x, y=0.0, z=0.0):
        """Get a nearest value for a location within the spatial extent of Raster.

        Parameters
        ----------
        x : int/float
            Spatial location along x-axis
        y : int/float (optional)
            Spatial location along y-axis
        z : int/float (optional)
            Spatial location along z-axis

        Returns
        -------
        out : float32/uint32/float64
            Raster value a given location within the spatial extent of the raster.
        """
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return self._handle.getNearestValue(x, y=y, z=z)
            elif isinstance(self, raster.RasterFile):
                return self._handle.cy_raster_obj.getNearestValue(x, y=y, z=z)
        else:
            raise RuntimeError("Raster is not yet initialized")

    def getBilinearValue(self, x, y=0.0, z=0.0):
        """Get a bilinearly interpolated value for a location within the spatial extent of Raster.

        Parameters
        ----------
        x : int/float
            Spatial location along x-axis
        y : int/float (optional)
            Spatial location along y-axis
        z : int/float (optional)
            Spatial location along z-axis

        Returns
        -------
        out : float32/uint32/float64
            Raster value a given location within the spatial extent of the raster.
        """
        if isinstance(self._handle, (_cyRaster_d_i, _cyRaster_f_i,
                                DataFileHandler_d_i, DataFileHandler_f_i)):
            raise NotImplementedError("Bilinear value cannot be computed for Raster of datatype uint32 ")
        else:
            if self._handle is not None:
                if isinstance(self, raster.Raster):
                    return self._handle.getBilinearValue(x, y=y, z=z)
                elif isinstance(self, raster.RasterFile):
                    return self._handle.cy_raster_obj.getBilinearValue(x, y=y, z=z)
            else:
                raise RuntimeError("Raster is not yet initialized")

    def getBounds(self):
        """Get bounding box of the raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : BoundingBox object
            Return bounding box of the raster object.
        """
        if self._handle is not None:
            if isinstance(self, raster.RasterFile):
                if self._handle.cy_raster_obj.hasData() == False:
                    raise RuntimeError("RasterFile object is not initialized")
                return vector.BoundingBox.from_bbox(self._handle.cy_raster_obj.getBounds())
            elif isinstance(self, raster.Raster):
                return vector.BoundingBox.from_bbox(self._handle.getBounds())
        else:
            return

    def getDimensions(self):
        """Get dimensions of the raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : RasterDimensions object
            Return dimensions of the raster object.
        """
        return self.getRasterDimensions()

    def getRasterDimensions(self):
        """Get raster dimensions of the raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : RasterDimensions object
            An instance of RasterDimensions containing dimensions of the Raster object.
        """
        if self._handle is None: return
        
        if isinstance(self, raster.Raster):
            return raster.RasterDimensions.copy(self._handle.getRasterDimensions())
        elif isinstance(self, raster.RasterFile):
            if self._handle.cy_raster_obj.hasData() == False:
                raise RuntimeError("RasterFile object is not initialized")
            return raster.RasterDimensions.copy(self._handle.cy_raster_obj.getRasterDimensions())

    def vectorise(self, contourValue, noDataValue=None):
        """Get a vector from the Raster object.

        Parameters
        ----------
        contourValue : int/float/list(int/float),np.ndarray
            A constant value or a list of values to compute contour from the Raster object.

        Returns
        -------
        out : Vector object
            Return a vector object obtained from the raster for a given contour value.
        """

        if (not isinstance(contourValue, (float, list, np.ndarray)) and
            not isinstance(contourValue, numbers.Real)):
            raise TypeError("contourValue should be of numeric type or list/ndarray of numeric types")
        
        if isinstance(self, raster.Raster):
            _raster_handle = self._handle
        elif isinstance(self, raster.RasterFile):
            _raster_handle = self._handle.cy_raster_obj

        if not isinstance(contourValue, np.ndarray):
            if isinstance(contourValue, list):
                _contour_value = contourValue
            else:
                _contour_value = [contourValue]
            _out = _raster_handle.vectorise(np.array(_contour_value, dtype=self.data_type),
                noDataValue)
        elif isinstance(contourValue, np.ndarray):
            _out = _raster_handle.vectorise(contourValue.astype(self.data_type), noDataValue)
        out = vector.Vector._from_vector(_out)
        return out

    def mapVector(self, inp_vector: vector.Vector,
                  geom_type: Union[numbers.Integral, gs_enums.GeometryType]=7,
                  widthPropertyName: str=""):
        """Convert an input vector to a raster

        Parameters
        ----------
        inp_vector : vector.Vector
            A vector object to be converted to a raster object
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of the vector geometries
        widthPropertyName : str, optional
            vector property used for mapping, by default ""

        Returns
        -------
        bool
            True if vector was mapped False otherwise

        Raises
        ------
        TypeError
            inp_vector should be an instance of Vector class
        TypeError
            geom_type should be int/ GeometryType
        """
        if not isinstance(inp_vector, (vector.Vector,
            vector._Vector_d, vector._Vector_f)):
            raise TypeError("inp_vector should be an instance of Vector class")

        if isinstance(inp_vector, vector.Vector):
            assert self.data_type == inp_vector._dtype
            _inp_vector = inp_vector._handle
        else:
            if isinstance(inp_vector, vector._Vector_d):
                assert self.data_type == np.float64
            elif isinstance(inp_vector, vector._Vector_f):
                assert self.data_type == np.float32
            _inp_vector = inp_vector

        if isinstance(widthPropertyName, str):
            _prop = widthPropertyName.encode("UTF-8")
        elif isinstance(widthPropertyName, bytes):
            _prop = widthPropertyName

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ GeometryType")
        
        if isinstance(geom_type, gs_enums.GeometryType):
            _geom_type = geom_type.value
        else:
            _geom_type = geom_type

        out = self._handle.mapVector(_inp_vector, _geom_type, _prop)
        return out

    def rasterise(self, inp_vector: vector.Vector, propertyName: str,
                  geom_type: Union[numbers.Integral, gs_enums.GeometryType]=7):
        """Convert an input vector to a raster.

        Parameters
        ----------
        inp_vector : vector.Vector
            A vector object to be converted to a raster object
        propertyName : str
            vector property used to assign values to raster cells.
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of the vector geometries

        Returns
        -------
        bool
            True if vector was rasterised False otherwise

        Raises
        ------
        TypeError
            inp_vector should be an instance of Vector class
        AssertionError
            datatype mismatch
        TypeError
            geom_type should be int/ GeometryType
        """
        if not isinstance(inp_vector, (vector.Vector,
            vector._Vector_d, vector._Vector_f)):
            raise TypeError("inp_vector should be an instance of Vector class")

        if isinstance(inp_vector, vector.Vector):
            _inp_vector = inp_vector._handle
        else:
            _inp_vector = inp_vector

        if isinstance(inp_vector, vector._Vector_d):
            assert self.data_type == np.float64, "datatype mismatch"
        elif isinstance(inp_vector, vector._Vector_f):
            assert self.data_type == np.float32, "datatype mismatch"

        if isinstance(propertyName, str):
            _prop = propertyName.encode("UTF-8")
        elif isinstance(propertyName, bytes):
            _prop = propertyName

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ GeometryType")
        
        if isinstance(geom_type, gs_enums.GeometryType):
            _geom_type = geom_type.value
        else:
            _geom_type = geom_type

        out = self._handle.rasterise(_inp_vector, _prop, _geom_type)
        return out

    def hasData(self):
        """Check if raster object has data.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : bool
            Return true if raster object has data else false.
        """
        if isinstance(self, raster.Raster):
            return self._handle.hasData()
        elif isinstance(self, raster.RasterFile):
            return self._handle.cy_raster_obj.hasData()

    @staticmethod
    def _check_inputs(inp):
        if not isinstance(inp, (raster.Raster, raster.RasterFile)):
            if not isinstance(inp, numbers.Real):
                warnings.warn("Input argument should be numeric", RuntimeWarning)
                return False
            else:
                return True
        if not(len(inp.name) >= 1):
            warnings.warn("Length of Input raster name should be greater than 1", RuntimeWarning)
            return False
        else:
            return True

    def _check_input_args(self, other):
        assert self.getDimensions() == other.getDimensions(), "Raster size should be same"
        assert self.name != other.name, "Input raster can't have same name"

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__


class _Raster_list:
    def __init__(self, dtype, handle=None):
        self._dtype = dtype
        self._handle = handle
        self._obj_list = []

    def _from_list(self, other):
        if not isinstance(other, list):
            raise TypeError('Input argument should be a list')
        else:
            self._from_iterable(other)
            self._obj_list += other

    def _from_tuple(self, other):
        if not isinstance(other, tuple):
            raise TypeError('Input argument should be a tuple')
        else:
            self._from_iterable(other)
            self._obj_list += list(other)

    def _from_iterable(self, other):
        n_items = len(other)
        n_rasters = 0
        n_df_handler = 0
        for item in other:
            if isinstance(item, raster.Raster):
                n_rasters += 1
            elif isinstance(item, raster.RasterFile):
                n_df_handler += 1
        if n_rasters > 0:
            if n_rasters != n_items:
                raise ValueError("All element of tuple should be instances of Raster")
            else:
                for i, item in enumerate(other, 0):
                    if item.base_type != self._dtype:
                        raise TypeError("Mismatch between Raster datatype and class instance")
                    self._add_raster(item)
        elif n_df_handler > 0:
            if n_df_handler != n_items:
                raise ValueError("All element of tuple should be instances of DataFileHandler")
            else:
                for i, item in enumerate(other, 0):
                    if item._dtype != self._dtype:
                        raise TypeError("Mismatch between RasterFile datatype and class instance")
                    self._add_data_handler(item)

    def _append(self, other):
        if isinstance(other, raster.RasterFile):
            self._add_data_handler(other)
        elif isinstance(other, raster.Raster):
            self._add_raster(other)
        self._obj_list.append(other)

    def _add_raster(self, other):
        if isinstance(other, raster.Raster):
            if self._dtype != other.base_type:
                raise TypeError("mismatch between datatype of input raster and class instance")
            self._add_raster(other._handle)
        elif isinstance(other, (_cyRaster_d, _cyRaster_d_i)):
            if self._dtype != np.float64:
                raise TypeError("Cannot add input raster of double type to class instance of single precision")
            if isinstance(other, _cyRaster_d):
                self._handle.add_dbl_raster(other)
            elif isinstance(other, _cyRaster_d_i):
                self._handle.add_int_raster(other)
        elif isinstance(other, (_cyRaster_f, _cyRaster_f_i)):
            if self._dtype != np.float32:
                raise TypeError("Cannot add input raster of single type to class instance of double precision")
            if isinstance(other, _cyRaster_f):
                self._handle.add_flt_raster(other)
            elif isinstance(other, _cyRaster_f_i):
                self._handle.add_int_raster(other)
        else:
            raise TypeError("input argument should be an instance of Raster")

    def _add_data_handler(self, other):
        if isinstance(other, raster.RasterFile):
            if self._dtype != other.base_type:
                raise TypeError("mismatch between datatype of input RasterFile and class instance")
            self._add_data_handler(other._handle)
        elif isinstance(other, (DataFileHandler_d, DataFileHandler_d_i)):
            if self._dtype != np.float64:
                raise TypeError("Cannot add input RasterFile of double type to class instance of single precision")
            if isinstance(other, DataFileHandler_d):
                self._handle.add_dbl_df_handler(other)
            if isinstance(other, DataFileHandler_d_i):
                self._handle.add_int_df_handler(other)
        elif isinstance(other, (DataFileHandler_f, DataFileHandler_f_i)):
            if self._dtype != np.float32:
                raise TypeError("Cannot add input RasterFile of single type to class instance of double precision")
            if isinstance(other, DataFileHandler_f):
                self._handle.add_flt_df_handler(other)
            elif isinstance(other, DataFileHandler_f_i):
                self._handle.add_int_df_handler(other)
        else:
            raise TypeError("input argument should be an instance of RasterFile")

    @property
    def _size(self):
        if self._handle is not None:
            return self._handle.get_number_of_rasters()

    def __len__(self):
        if self._handle is not None:
            return self._size
    
    def __getitem__(self, other):
        if isinstance(other, int):
            if other < self._size:
                return self._obj_list[other]

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__
