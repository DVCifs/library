# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import importlib

global HAS_PYDAP, HAS_GDAL, HAS_NCDF, HAS_XARRAY, HAS_RASTERIO
global HAS_PYSHP, HAS_GPD, HAS_FIONA, SUPPORTED_LIBS

__all__ = ["HAS_GDAL", "HAS_NCDF", "HAS_XARRAY", "HAS_RASTERIO",
    "HAS_PYDAP", "HAS_PYSHP", "HAS_GPD", "HAS_FIONA", "RequireLib"]

SUPPORTED_LIBS = {"gdal":"gdal",
                  "netcdf":"netCDF4",
                  "rasterio":"rasterio",
                  "xarray":"xarray",
                  "pydap":"pydap",
                  "pyshp":"shapefile",
                  "geopandas":"geopandas",
                  "fiona":"fiona"}

def import_or_skip(*args, **kwargs):
    try:
        out = importlib.import_module(*args, **kwargs)
    except ModuleNotFoundError:
        return None, False
    return out, True

class RequireLib:
    def __init__(self, libname):
        self.libname = libname

    def _dummy_function(self):
        raise ModuleNotFoundError(f"library {self.libname} is not installed")

    def __call__(self, input_function, *args, **kwargs):

        def inner_func(*args, **kwargs):
            if self.libname in SUPPORTED_LIBS:
                if self.libname not in ["geopandas", "netcdf"]:
                    if globals()[f'has_{self.libname}'.upper()]:
                        return input_function(*args, **kwargs)
                    else:
                        return self._dummy_function()
                else:
                    if self.libname == "netcdf":
                        if HAS_NCDF:
                            return input_function(*args, **kwargs)
                        else:
                            return self._dummy_function()
                    elif self.libname == "geopandas":
                        if HAS_GPD:
                            return input_function(*args, **kwargs)
                        else:
                            return self._dummy_function()
        return inner_func

for libname in SUPPORTED_LIBS:
    if libname not in ["geopandas", "netcdf", "pyshp"]:
        _, globals()[f'has_{libname}'.upper()] = import_or_skip(SUPPORTED_LIBS[libname])
    else:
        if libname == "netcdf":
            _, HAS_NCDF = import_or_skip(SUPPORTED_LIBS[libname])
        elif libname == "geopandas":
            _, HAS_GPD = import_or_skip(SUPPORTED_LIBS[libname])
        elif libname == "pyshp":
            _, HAS_PYSHP = import_or_skip(SUPPORTED_LIBS[libname])