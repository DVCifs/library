# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import sys
sys.path.insert(0, os.path.realpath('../../../'))

import numpy as np
from geostack.raster import Raster, equalSpatialMetrics, RasterBaseList
from geostack.runner import runScript, getSolver
from geostack.gs_enums import RasterCombinationType, RasterNullValueType, RasterResolutionType

def test_raster1D_script():

    testRasterA = Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(5, 1.0)
    testRasterA.setAllCellValues(1.0)
    testRasterA.setCellValue(99.9, 2)

    testRasterB = Raster(name="testRasterB", base_type=np.float32,
        data_type=np.float32)
    testRasterB.init(15, 1.0, ox=-5.0)
    testRasterB.setAllCellValues(2.0)

    raster_list = RasterBaseList(dtype=np.float32)
    raster_list.add_raster(testRasterA)
    raster_list.add_raster(testRasterB)

    script = "output = 100.0 + testRasterA * testRasterB;"
    testRasterC = runScript(script, raster_list, output_type=np.float32,
        parameter=RasterCombinationType.Union)

    print(testRasterC.getDimensions().to_dict())

    assert testRasterC.hasData() == True
    assert np.round(testRasterC.max(), 1) == 299.8
    assert testRasterC.min() == 102.0
    assert np.round(testRasterC.getCellValue(7), 1) == 299.8
    assert np.round(testRasterC.getNearestValue(2.5), 1) == 299.8
    assert np.round(testRasterC.getBilinearValue(2.5), 1) == 299.8


def test_raster1d_raster2d_script():
    
    testRasterA = Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(5, 1.0)
    testRasterA.setAllCellValues(1.0)

    testRasterB = Raster(name="testRasterB", base_type=np.float32,
        data_type=np.float32)
    testRasterB.init(15, 1.0, ny=15, hy=1.0, ox=-5.0, oy=-5.0)
    testRasterB.setAllCellValues(2.0)
    
    script = "output = testRasterA + testRasterB;"
    testRasterC = runScript(script, [testRasterA, testRasterB],
        output_type=np.float32, parameter=RasterCombinationType.Union)

    assert testRasterC.hasData() == True
    assert testRasterC.max() == 3.0
    assert testRasterC.getCellValue(5, 5) == 3.0
    assert testRasterC.getNearestValue(0.0, 0.0) == 3.0 


def test_raster2d_float_script():

    testRasterA = Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
    testRasterA.setAllCellValues(1.0)
    testRasterA.setCellValue(99.9, i=2, j=2)

    testRasterB = Raster(name="testRasterB", base_type=np.float32,
        data_type=np.float32)
    testRasterB.init(nx=15, ny=15, hx=1.0, hy=1.0, ox=-5.0, oy=-5.0)
    testRasterB.setAllCellValues(2.0)
    #
    script = "output = 100.0 + testRasterA * testRasterB;"
    testRasterC = runScript(script, [testRasterA, testRasterB],
        output_type=np.float32, parameter=RasterCombinationType.Union)
    
    assert testRasterC.hasData() == True
    assert np.round(testRasterC.max(), 1) == 299.8
    assert np.round(testRasterC.min(), 1) == 102.0
    assert np.round(testRasterC.getCellValue(7, 7), 1) == 299.8
    assert np.round(testRasterC.getNearestValue(2.5, 2.5), 1) == 299.8
    assert np.round(testRasterC.getBilinearValue(2.5, 2.5), 1) == 299.8


def test_raster_resize2D():
    testRasterA = Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
    testRasterA.setAllCellValues(1.0)
    testRasterA.setCellValue(99.9, i=2, j=2)

    testRasterA.resize2D(3, 3, 1, 1)

    assert testRasterA.hasData() == True
    assert np.round(testRasterA.getNearestValue(2, 2), 1) == 99.9
    assert np.round(testRasterA.max(), 1) == 99.9
    assert np.round(testRasterA.min(), 1) == 1.0

def test_raster2d_int_script():
    testRasterA = Raster(name="testRasterA", base_type=np.float32,
        data_type=np.uint32)
    testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
    testRasterA.setAllCellValues(1)
    testRasterA.setCellValue(99, i=2, j=2)

    testRasterB = Raster(name="testRasterB", base_type=np.float32,
        data_type=np.uint32)
    testRasterB.init(nx=15, ny=15, hx=1.0, hy=1.0, ox=-5.0, oy=-5.0)
    testRasterB.setAllCellValues(2)
    #
    script = "output = 100.0 + testRasterA * testRasterB;"
    testRasterC = runScript(script, [testRasterA, testRasterB],
        output_type=np.uint32, parameter=RasterNullValueType.Zero)    

    assert testRasterC.hasData() == True
    assert testRasterC.max() == 298
    assert testRasterC.min() == 100
    assert testRasterC.getCellValue(7, 7) == 298
    assert testRasterC.getNearestValue(2.5, 2.5) == 298


def test_raster_int_float_script():

    testRasterA = Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(nx=50, ny=50, hx=0.1, hy=0.1)
    testRasterA.setAllCellValues(0.0)

    testRasterB = Raster(name="testRasterB", base_type=np.float32,
        data_type=np.uint32)
    testRasterB.init(nx=50, ny=50, hx=0.1, hy=0.1)
    testRasterB.setAllCellValues(0)
    #
    script = "output = x*y;"
    testRasterA = runScript(script, [testRasterA],
        output_type=np.float32, parameter=RasterCombinationType.Union)
    testRasterA.setProperty("name", "testRasterA")

    script = "output = testRasterA;"
    testRasterC = runScript(script, [testRasterB, testRasterA],
        output_type=np.uint32, parameter=RasterCombinationType.Union)
    testRasterC.setProperty("name", "testRasterC")

    assert testRasterA.hasData() == True
    assert np.round(testRasterA.getCellValue(26, 14), 4) == (2.65 * 1.45)
    assert testRasterC.hasData() == True
    assert testRasterC.getCellValue(26, 14) == 3


def test_sampling_2d():
    testRasterA = Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
    testRasterA.setAllCellValues(1.0)
    testRasterA.setCellValue(99.9, i=2, j=2)

    testRasterB = Raster(name="testRasterB", base_type=np.float32,
        data_type=np.float32)
    testRasterB.init(nx=20, ny=20, hx=0.25, hy=0.25)

    script = "output = testRasterA;"
    testRasterB = runScript(script, [testRasterA, testRasterB],
        output_type=np.float32, parameter=RasterResolutionType.Minimum)

    assert testRasterA.hasData() == True
    assert testRasterA.getNearestValue(2.5, 2.5) == testRasterB.getNearestValue(2.5, 2.5)


def test_intersection():
    testRasterA = Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
    testRasterA.setAllCellValues(1.0)
    testRasterA.setCellValue(99.9, i=2, j=2)

    testRasterB = Raster(name="testRasterB", base_type=np.float32,
        data_type=np.float32)
    testRasterB.init(nx=20, ny=20, hx=0.25, hy=0.25, ox=-4.0, oy=-4.0)
    testRasterB.setAllCellValues(-1.0)

    script = "output = testRasterA + testRasterB;"
    testRasterC = runScript(script, [testRasterA, testRasterB],
        parameter=RasterResolutionType.Minimum, output_type=np.float32)

    assert testRasterC.hasData() == True
    assert testRasterC.getCellValue(17, 17) == 0.0
