# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import sys
import os.path as pth
import numpy as np

from ..dataset import (HAS_GDAL, HAS_GPD, HAS_PYSHP, HAS_FIONA,
    RequireLib, gdal_dtype_to_numpy, numpy_to_gdal_dtype)
import warnings

if HAS_GDAL:
    from osgeo import ogr
    os.environ['GDAL_CACHEMAX'] = "100"

if HAS_GPD:
    import geopandas as gpd
    import pandas as pd

if HAS_PYSHP:
    import shapefile

if HAS_FIONA:
    import fiona

from .. import core, io, vector
import time


__all__ = ['from_geopandas', "from_shapefile", "from_ogr", "from_fiona"]


@RequireLib("geopandas")
def get_column_names(this):
    if isinstance(this, pd.DataFrame):
        column_names = this.columns
    elif isinstance(this, pd.Series):
        column_names = this.keys()
    return column_names


@RequireLib("fiona")
def from_fiona(file_path, dtype=np.float32):
    """Create Vector object using fiona.
    """
    if isinstance(file_path, str):
        datain = fiona.open(file_path, mode="r")
    elif isinstance(file_path, fiona.Collection):
        datain = file_path

    vec = vector.Vector(dtype=dtype)

    vec.setProjectionParameters(
        core.ProjectionParameters.from_wkt(datain.crs_wkt, dtype=dtype))

    for item in datain.keys():
        geom = item['geometry']
        props = item['properties']
        if geom['type'] == 'LineString':
            idx = vec.addLineString(geom['coordinates'])
            for prop in props:
                if props[prop] is None:
                    vec.setProperty(idx, prop, "")
                else:
                    vec.setProperty(idx, prop, props[prop])
        elif geom['type'] == "Point":
            idx = vec.addPoint(geom['coordinates'])
            for prop in props:
                if props[prop] is None:
                    vec.setProperty(idx, prop, "")
                else:
                    vec.setProperty(idx, prop, props[prop])
        elif geom['type'] == "Polygon":
            idx = vec.addPolygon(np.array(geom['coordinates']).squeeze())
            for prop in props:
                if props[prop] is None:
                    vec.setProperty(idx, prop, "")
                else:
                    vec.setProperty(idx, prop, props[prop])

    if isinstance(file_path, str):
        datain.close()
    return vec


@RequireLib("geopandas")
def from_geopandas(file_path, dtype=np.float32):
    """Create Vector object using geopandas.
    """
    if isinstance(file_path, str):
        datain = gpd.read_file(file_path)
    elif isinstance(file_path, gpd.GeoDataFrame):
        datain = file_path

    nrows = datain.shape[0]
    ncols = datain.shape[1]

    vec = vector.Vector(dtype=dtype)

    for i in range(nrows):
        _obj = datain.iloc[i]
        props = {'string':{}, 'int':{}, 'double':{}}
        for item in get_column_names(_obj):
            if item != "geometry":
                if isinstance(getattr(_obj, item), str):
                    if _obj.get(item) is not None:
                        props['string'][item] = _obj.get(item)
                    else:
                        props['string'][item] = ""
                elif isinstance(getattr(_obj, item), int):
                    if _obj.get(item) is not None:
                        props['int'][item] = _obj.get(item)
                    else:
                        props['string'][item] = ""
                elif isinstance(getattr(_obj, item), float):
                    if _obj.get(item) is not None:
                        props['double'][item] = _obj.get(item)
                    else:
                        props['string'][item] = ""
        _geom = getattr(_obj, 'geometry')
        _geo_obj = _geom.__geo_interface__['coordinates']
        if _geom.type == 'MultiPolygon':
            for j in range(len(_geo_obj)):
                _coords = np.squeeze(np.array(_geo_obj[j]))
                if len(_coords.shape) == 1:
                    for k in range(len(_coords)):
                        __coords = np.squeeze(np.array(_coords[k]))
                        poly_idx = vec.addPolygon([__coords])
                        for _item in props:
                            for _prop in props[_item]:
                                vec.setProperty(poly_idx, _prop, props[_item][_prop])
                else:
                    poly_idx = vec.addPolygon([_coords])
                    for _item in props:
                        if len(props[_item]) > 0:
                            for _prop in props[_item]:
                                vec.setProperty(poly_idx, _prop, props[_item][_prop])
        elif _geom.type == 'Polygon':
            for j in range(len(_geo_obj)):
                _coords = np.squeeze(np.array(_geo_obj[j]))
                poly_idx = vec.addPolygon([_coords])
                for _item in props:
                    if len(props[_item]) > 0:
                        for _prop in props[_item]:
                            vec.setProperty(poly_idx, _prop, props[_item][_prop])
        elif _geom.type == "Point":
            idx = vec.addPoint([_geom.x, _geom.y])
            for _item in props:
                if len(props[_item]) > 0:
                    for _prop in props[_item]:
                        vec.setProperty(idx, _prop, props[_item][_prop])
        elif _geom.type == "LineString":
            idx = vec.addLineString(list(_geo_obj))
            for _item in props:
                if len(props[_item]) > 0:
                    for _prop in props[_item]:
                        vec.setProperty(idx, _prop, props[_item][_prop])
    return vec


@RequireLib("gdal")
def from_ogr(file_path, dtype=np.float32):
    """Create Vector object using GDAL's OGR.
    """
    if isinstance(file_path, str):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        datain = driver.Open(file_path)
    elif isinstance(file_path, ogr.DataSource):
        datain = file_path
    elif isinstance(file_path, ogr.Layer):
        datain = None
    
    if datain is not None:
        layer_count = datain.GetLayerCount()
    else:
        layer_count = 1

    #this part converts the vector information into geostack Vector object
    vec = vector.Vector(dtype=dtype)

    for i in range(layer_count):
        if datain is not None:
            data_layer = datain.GetLayerByIndex(i)
        elif datain is None and isinstance(file_path, ogr.Layer):
            data_layer = file_path
        layer_type = ogr.GeometryTypeToName(data_layer.GetGeomType())
        spatial_ref = data_layer.GetSpatialRef()
        feature_count = data_layer.GetFeatureCount()
        for j in range(feature_count):
            data_feature = data_layer.GetFeature(j)
            field_count = data_feature.GetFieldCount()
            props = {'string':{}, 'int':{}, 'double':{}}
            for k in range(field_count):
                field_def = data_feature.GetFieldDefnRef(k)
                if ogr.GetFieldTypeName(field_def.GetType()) == 'String':
                    if data_feature.GetField(k) is not None:
                        props['string'][field_def.GetName()] = data_feature.GetField(k)
                    else:
                        props['string'][field_def.GetName()] = ""
                elif ogr.GetFieldTypeName(field_def.GetType()) == 'Integer':
                    if data_feature.GetField(k) is not None:
                        props['int'][field_def.GetName()] = data_feature.GetField(k)
                    else:
                        props['string'][field_def.GetName()] = ""
                elif ogr.GetFieldTypeName(field_def.GetType()) == 'Real':
                    if data_feature.GetField(k) is not None:
                        props['double'][field_def.GetName()] = data_feature.GetField(k)
                    else:
                        props['string'][field_def.GetName()] = ""
            geom = data_feature.GetGeometryRef()
            if layer_type == "Point":
                idx = vec.addPoint(geom.GetPoint_2D())
                for _item in props:
                    if len(props[_item]) > 0:
                        for _prop in props[_item]:
                            vec.setProperty(idx, _prop, props[_item][_prop])
            elif layer_type == "Line String":
                idx = vec.addLineString(geom.GetPoints())
                for _item in props:
                    if len(props[_item]) > 0:
                        for _prop in props[_item]:
                            vec.setProperty(idx, _prop, props[_item][_prop])
            elif layer_type == "Polygon":
                geom_count = geom.GetGeometryCount()
                poly = []
                for k in range(geom_count):
                    _geom = geom.GetGeometryRef(k)
                    _geom_count = _geom.GetGeometryCount()
                    if _geom_count > 0:
                        _poly = []
                        for n in range(_geom_count):
                            __geom = _geom.GetGeometryRef(n)
                            points = np.array(__geom.GetPoints())
                            _poly.append(points)
                        poly.append(_poly)
                    else:
                        points = np.array(_geom.GetPoints())
                        poly.append(points)

                if isinstance(poly[0], list):
                    for k in range(len(poly)):
                        for item in poly[k]:
                            poly_idx = vec.addPolygon([item])
                            for _item in props:
                                if len(props[_item]) > 0:
                                    for _prop in props[_item]:
                                        vec.setProperty(poly_idx, _prop, props[_item][_prop])
                elif isinstance(poly[0], np.ndarray):
                    for item in poly:
                        poly_idx = vec.addPolygon([item])
                        for _item in props:
                            if len(props[_item]) > 0:
                                for _prop in props[_item]:
                                    vec.setProperty(poly_idx, _prop, props[_item][_prop])

    vec.setProjectionParameters(
        core.ProjectionParameters.from_wkt(spatial_ref.ExportToWkt(),
        dtype=dtype))
    if isinstance(file_path, str):
        del(datain)
        datain = None
    return vec


@RequireLib("pyshp")
def from_shapefile(file_path, dtype=np.float32):
    """Create Vector object using pyshp's shapefile.
    """
    if isinstance(file_path, str):
        datain = shapefile.Reader(pth.realpath(file_path))
    elif isinstance(file_path, shapefile.Reader):
        datain = file_path

    if pth.exists(f"{datain.shapeName}.prj"):
        with open(f"{datain.shapeName}.prj", "r") as inp:
            proj_param = inp.read()
        shape_proj = core.ProjectionParameters.from_wkt(proj_param,
            dtype=dtype)
    else:
        shape_proj = None

    n_shapes = datain.numRecords
    data_fields = datain.fields

    vec = vector.Vector(dtype=dtype)

    for i in range(n_shapes):
        props = {'string':{}, 'int':{}, 'double':{}}
        for j, item in enumerate(data_fields[1:], 0):
            if item[1] == 'C':
                props['string'][item[0]] = datain.record(i=i)[j]
            elif item[1] == "N":
                props['int'][item[0]] = datain.record(i=i)[j]
            elif item[1] == 'F':
                props['double'][item[0]] = datain.record(i=i)[j]
        geom = datain.shape(i=i)
        geom_points = geom.points
        if datain.shapeTypeName == "POINT":
            idx = vec.addPoint(geom_points[0])
            for _item in props:
                if len(props[_item]) > 0:
                    for _prop in props[_item]:
                        vec.setProperty(idx, _prop, props[_item][_prop])
        elif datain.shapeTypeName == "POLYLINE":
            idx = vec.addLineString(geom_points)
            for _item in props:
                if len(props[_item]) > 0:
                    for _prop in props[_item]:
                        vec.setProperty(idx, _prop, props[_item][_prop])
        elif datain.shapeTypeName == "POLYGON":
            geom_parts = geom.parts
            for k in range(len(geom_parts)):
                if k == len(geom_parts)-1:
                    _coords = np.squeeze(np.array(geom_points[geom_parts[k]:len(geom_points)]))
                else:
                    _coords = np.squeeze(np.array(geom_points[geom_parts[k]:geom_parts[k+1]]))
                poly_idx = vec.addPolygon([_coords])
                for _item in props:
                    if len(props[_item]) > 0:
                        for _prop in props[_item]:
                            vec.setProperty(poly_idx, _prop, props[_item][_prop])

    if shape_proj is not None:
        vec.setProjectionParameters(shape_proj)
    if isinstance(file_path, str):
        datain.close()
    return vec


if __name__ == "__main__":

    file_path = "./aus-states-09072015.shp"
    start = time.time()
    vec = from_geopandas(file_path)
    end = time.time()
    print("Time taken by geopandas to geostack = %f" % (end - start))

    start = time.time()
    vec = from_ogr(file_path)
    end = time.time()
    print("Time taken by ogr to geostack = %f" % (end - start))

    start = time.time()
    vec = from_shapefile(file_path)
    end = time.time()
    print("Time taken by ogr to geostack = %f" % (end - start))

    out = io.vectorToGeoJson(vec)
    core.Json11.load(out).dumps("./test2.geojson")

    print("Time taken to write to geojson = %f" % (time.time() - end))
