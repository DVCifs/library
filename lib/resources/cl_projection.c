/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

 __kernel void convert_Transverse_Mercator_to_Ellipsoid(__global Coordinate *c, uint Nc, ProjectionParameters proj) {

    if (get_global_id(0) >= Nc) 
        return;
   
   c += get_global_id(0);
   *c = fn_Transverse_Mercator_to_Ellipsoid(*c, proj);
}

__kernel void convert_Ellipsoid_to_Transverse_Mercator(__global Coordinate *c, uint Nc, ProjectionParameters proj) {

    if (get_global_id(0) >= Nc) 
        return;
   
   c += get_global_id(0);
   *c = fn_Ellipsoid_to_Transverse_Mercator(*c, proj);
}

__kernel void convert_Lambert_to_Ellipsoid(__global Coordinate *c, uint Nc, ProjectionParameters proj) {

    if (get_global_id(0) >= Nc) 
        return;
   
   c += get_global_id(0);
   *c = fn_Lambert_to_Ellipsoid(*c, proj);
}

__kernel void convert_Ellipsoid_to_Lambert(__global Coordinate *c, uint Nc, ProjectionParameters proj) {

    if (get_global_id(0) >= Nc) 
        return;
   
   c += get_global_id(0);
   *c = fn_Ellipsoid_to_Lambert(*c, proj);
}

__kernel void convert_Albers_to_Ellipsoid(__global Coordinate *c, uint Nc, ProjectionParameters proj) {

    if (get_global_id(0) >= Nc) 
        return;
   
   c += get_global_id(0);
   *c = fn_Albers_to_Ellipsoid(*c, proj);
}

__kernel void convert_Ellipsoid_to_Albers(__global Coordinate *c, uint Nc, ProjectionParameters proj) {

    if (get_global_id(0) >= Nc) 
        return;
   
   c += get_global_id(0);
   *c = fn_Ellipsoid_to_Albers(*c, proj);
}

__kernel void convert_Ellipsoid_to_Mercator(__global Coordinate *c, uint Nc, ProjectionParameters proj) {

    if (get_global_id(0) >= Nc) 
        return;
   
   c += get_global_id(0);
   *c = fn_Ellipsoid_to_Mercator(*c, proj);
}

__kernel void convert_Mercator_to_Ellipsoid(__global Coordinate *c, uint Nc, ProjectionParameters proj) {

    if (get_global_id(0) >= Nc) 
        return;
   
   c += get_global_id(0);
   *c = fn_Mercator_to_Ellipsoid(*c, proj);
}

