# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref, preincrement as preinc
from libcpp cimport nullptr
from libcpp.string cimport string
from libcpp.map cimport map as cpp_map
from libcpp.vector cimport vector
import numpy as np
cimport numpy as np

np.import_array()

include "_cy_property.pxi"

cdef class _Property:
    def __cinit__(self):
        self.thisptr.reset(new Property())

    cdef void c_copy(self, _Property p):
        self.thisptr.reset(new Property(deref(p.thisptr)))

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if self.thisptr != nullptr:
            self.thisptr.reset()

cdef class _PropertyHandler:
    def __cinit__(self):
        if type(self) is _PropertyHandler:
            self.thisptr = new PropertyHandler()
        # self.thisptr.reset(new PropertyHandler())

    cdef void setProperty_s(self, string name, string other):
        deref(self.thisptr).setProperty(name, other)

    cdef void setProperty_i(self, string name, int other):
        deref(self.thisptr).setProperty(name, other)

    cdef void setProperty_d(self, string name, double other):
        deref(self.thisptr).setProperty(name, other)

    def setProperty(self, bytes name, other):
        if isinstance(other, bytes):
            self.setProperty_s(<string> name, <string> other)
        elif isinstance(other, int):
            self.setProperty_i(<string> name, <int> other)
        elif isinstance(other, np.float64):
            self.setProperty_d(<string> name, <double> other)

    def getProperty_i(self, bytes name):
        cdef int _out
        cdef string _name
        _name = <string> name
        _out = deref(self.thisptr).getProperty[int](_name)
        return int(_out)

    def getProperty_d(self, bytes name):
        cdef double _out
        cdef string _name
        _name = <string> name
        _out = deref(self.thisptr).getProperty[double](_name)
        return float(_out)

    def getProperty_s(self, bytes name):
        cdef string _out
        cdef string _name
        _name = <string> name
        _out = deref(self.thisptr).getProperty[string](_name)
        return _out

    def hasProperty(self, bytes name):
        cdef string _name
        _name = <string> name
        return deref(self.thisptr).hasProperty(_name)

    def getProperties_s(self):
        cdef cpp_map[string, string] _out
        cdef cpp_map[string, string].iterator end
        cdef cpp_map[string, string].iterator it
        out = {}
        _out = deref(self.thisptr).getProperties[string]()
        end = _out.end()
        it = _out.begin()
        while it != end:
            out[deref(it).first] = deref(it).second
            preinc(it)
        return out

    def getProperties_i(self):
        cdef cpp_map[string, int] _out
        cdef cpp_map[string, int].iterator end
        cdef cpp_map[string, int].iterator it
        _out = deref(self.thisptr).getProperties[int]()
        end = _out.end()
        it = _out.begin()
        out = {}
        while it != end:
            out[deref(it).first] = deref(it).second
            preinc(it)

        return out

    def getProperties_d(self):
        cdef cpp_map[string, double] _out
        cdef cpp_map[string, double].iterator end
        cdef cpp_map[string, double].iterator it
        _out = deref(self.thisptr).getProperties[double]()
        end = _out.end()
        it = _out.begin()
        out = {}
        while it != end:
            out[deref(it).first] = deref(it).second
            preinc(it)
        return out

    def removeProperty(self, bytes name):
        cdef string _name
        _name = <string> name
        deref(self.thisptr).removeProperty(_name)

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        # if self.thisptr != nullptr:
        if type(self) is _PropertyHandler:
            #self.thisptr.reset()
            del self.thisptr