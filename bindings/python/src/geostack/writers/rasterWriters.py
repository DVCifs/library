# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import sys
from pprint import pprint
import numpy as np
import json
from ..dataset import (HAS_NCDF, HAS_GDAL, HAS_RASTERIO, HAS_XARRAY,
    HAS_PYDAP, RequireLib, gdal_dtype_to_numpy, numpy_to_gdal_dtype)
import warnings

if HAS_GDAL:
    from osgeo import gdal, gdalconst
    os.environ['GDAL_CACHEMAX'] = "100"

if HAS_NCDF:
    import netCDF4 as nc
    if HAS_PYDAP:
        from ..readers.ncutils import Pydap2NC

if HAS_XARRAY:
    import xarray as xr

if HAS_RASTERIO:
    import rasterio as rio
    from rasterio.windows import Window

from ..raster import raster, _cy_raster
from ..readers import rasterReaders


__all__ = ["create_output_file_gdal", "write_data_to_gdal_buffer"]


def create_grid_from_corner(ulx, uly, delx, dely, nx, ny):
    '''
    generate grid for the raster using the values of the corners
    '''
    xcoords = np.linspace(0, nx, nx + 1) * delx + ulx
    ycoords = np.linspace(0, ny, ny + 1) * dely + uly
    return xcoords, ycoords


def grid_vertices_to_centers(lonInp, latInp):
    '''
    convert grid vertices to an array of corners of cells
    '''
    if len(lonInp.shape) == 1 and len(latInp.shape) == 1:
        lon_centers = (lonInp[1:] + lonInp[:-1]) * 0.5
        lat_centers = (latInp[1:] + latInp[:-1]) * 0.5
        return lon_centers, lat_centers
    else:
        raise TypeError("only 1D numpy array should be used")


@RequireLib("gdal")
def create_output_file_gdal(file_path, inputRaster, bands_count=1, compress=True,
    out_driver="GTiff"):
    """Create output file for GDAL IO using inputRaster.
    """
    if not isinstance(inputRaster, raster.Raster):
        raise TypeError("input raster should be an instance of Raster")

    if not isinstance(file_path, str):
        raise TypeError("Path of output file 'file_path' should be of string type")
    if pth.exists(file_path):
        raise FileExistsError("file %s exist, choose different name")

    _file_dir = pth.dirname(file_path).strip()
    if len(_file_dir) > 0:
        if not pth.isdir(_file_dir):
            raise NotADirectoryError("Path %s is invalid" % _file_dir)
    
    file_ext = pth.basename(file_path).split('.')[-1]
    if 'tif' in file_ext.lower():
        _out_driver = "GTiff"
    else:
        _out_driver = out_driver
    
    raster_dimensions = inputRaster.getDimensions()
    nx = raster_dimensions.nx
    ny = raster_dimensions.ny
    out_type = numpy_to_gdal_dtype(inputRaster.data_type)
    out_geotransform = rasterReaders.get_gdal_geotransform(raster_dimensions)
    nbands = max(1, bands_count)

    driver = gdal.GetDriverByName(_out_driver)
    if driver is not None:
        if compress:
            out_file = driver.Create(file_path, xsize=nx,
                ysize=ny, bands=nbands, eType=out_type,
                options=["COMPRESS=DEFLATE"])
        else:
            out_file = driver.Create(file_path, xsize=nx,
                ysize=ny, bands=nbands, eType=out_type)
    else:
        raise ValueError("Driver %s is not a valid gdal driver" % _out_driver)

    out_file.SetGeoTransform(out_geotransform)

    return out_file


@RequireLib("gdal")
def write_data_to_gdal_buffer(fileHandle, inputRaster, raster_band=1, invert_y=True):
    """Write raster data from inputRaster to fileHandle.
    """
    if not isinstance(inputRaster, raster.Raster):
        raise TypeError("input raster should be an instance of Raster")
    if not isinstance(fileHandle, (gdal.Dataset, str)):
        raise TypeError("fileHandle should an instance of gdal.Dataset or file name")

    if raster_band > fileHandle.RasterCount:
        raise ValueError("raster band is more than available bands in file")
    if raster_band == 0:
        raise ValueError("raster band should be greater than 0")

    out_band = fileHandle.GetRasterBand(raster_band)
    raster_dims = inputRaster.getDimensions()
    tileSize = raster.TileSpecifications().tileSize
    for i in range(raster_dims.tx):
        for j in range(raster_dims.ty):
            idx_s = i * tileSize
            jdx_s = j * tileSize
            out_data = inputRaster.writeData(i, j)
            if invert_y:
                out_band.WriteArray(out_data[::-1, :], xoff=idx_s, yoff=jdx_s)
            else:
                out_band.WriteArray(out_data[:, :], xoff=idx_s, yoff=jdx_s)
            out_band.FlushCache()


@RequireLib("gdal")
def writeRaster(out_file_name, in_file, num_bands, dtype=None, compress=False,
    build_overview=False):
    '''
    write output raster using the structure of the input satellite image with the
    number of bands specified using num_bands
    '''

    if not HAS_GDAL:
        raise RuntimeError("GDAL is not installed in current python environment")

    if isinstance(in_file, str):
        _in_file_handle = gdal.Open(in_file, 0)
    elif isinstance(in_file, gdal.Dataset):
        _in_file_handle = in_file
    driver = gdal.GetDriverByName("GTiff")
    if build_overview:
        gdal.SetConfigOption('TIFF_USE_OVR', 'YES')
    if isinstance(out_file_name, str):
        if pth.exists(out_file_name):
            raise ValueError("File %s exists, delete it before continuing" % out_file_name)
    else:
        raise TypeError("out_file_name should be of type string")
    if dtype is None:
        if compress:
            out_file_handle = driver.Create(out_file_name, _in_file_handle.RasterXSize,
                _in_file_handle.RasterYSize, num_bands, _in_file_handle.GetRasterBand(1).DataType,
                options=['COMPRESS=DEFLATE'])
        else:
            out_file_handle = driver.Create(out_file_name, _in_file_handle.RasterXSize,
                _in_file_handle.RasterYSize, num_bands, _in_file_handle.GetRasterBand(1).DataType)
    else:
        if compress:
            out_file_handle = driver.Create(out_file_name, _in_file_handle.RasterXSize,
                _in_file_handle.RasterYSize, num_bands, dtype, options=['COMPRESS=DEFLATE'])
        else:
            out_file_handle = driver.Create(out_file_name, _in_file_handle.RasterXSize,
                _in_file_handle.RasterYSize, num_bands, dtype)

    out_file_handle.SetGeoTransform(_in_file_handle.GetGeoTransform())
    if len(_in_file_handle.GetProjection()) > 0:
        out_file_handle.SetProjection(_in_file_handle.GetProjection())
    if isinstance(in_file, str):
        del _in_file_handle
        _in_file_handle = None
    return out_file_handle