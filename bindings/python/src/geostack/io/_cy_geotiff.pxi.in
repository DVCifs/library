"""
Template for each `dtype` helper function for series

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# name, x_name, y_name

rtypes = [('d', 'double'),
         ('f', 'float'),]
ctypes = {}
ctypes['d'] = [('d', 'double'),
         ('i', 'uint32_t'),]
ctypes['f'] = [('f', 'float'),
         ('i', 'uint32_t'),]
}}

{{for x_name, x_type in rtypes}}

{{for y_name, y_type in ctypes[x_name]}}

cdef class cyGeoTIFF_{{x_name}}_{{y_name}}:
    def __cinit__(self):
        self.thisptr.reset(new GeoTIFFHandler[{{y_type}}, {{x_type}}]())

    cdef void setFileInputHandler(self, Raster[{{y_type}}, {{x_type}}] &r,
            shared_ptr[RasterFileHandler[{{y_type}}, {{x_type}}]] rf):
        r.setFileInputHandler(rf)

    {{if x_name == y_name}}
    cpdef bool read(self, string fileName, _cyRaster_{{x_name}} r):
        cdef bool out
        self.setFileInputHandler(deref(r.derivedptr),
                <shared_ptr[RasterFileHandler[{{y_type}}, {{x_type}}]]> self.thisptr)

        out = deref(self.thisptr).read(fileName, deref(r.derivedptr))
        return out

    cpdef bool write(self, string fileName, _cyRaster_{{x_name}} r, string jsonConfig):
        cdef bool out
        out = deref(self.thisptr).write(fileName,
                deref(r.derivedptr), jsonConfig)
        return out
    {{else}}
    cpdef bool read(self, string fileName, _cyRaster_{{x_name}}_{{y_name}} r):
        cdef bool out
        self.setFileInputHandler(deref(r.derivedptr),
                <shared_ptr[RasterFileHandler[{{y_type}}, {{x_type}}]]> self.thisptr)

        out = deref(self.thisptr).read(fileName, deref(r.derivedptr))
        return out

    cpdef bool write(self, string fileName, _cyRaster_{{x_name}}_{{y_name}} r, string jsonConfig):
        cdef bool out
        out = deref(self.thisptr).write(fileName,
                deref(r.derivedptr), jsonConfig)
        return out
    {{endif}}

    def __dealloc__(self):
        if self.thisptr != nullptr:
            if type(self) is cyGeoTIFF_{{x_name}}_{{y_name}}:
                self.thisptr.reset()

    def __repr__(self):
        return self.__class__.__name__

    def __exit__(self):
        self.__dealloc__()

{{endfor}}

{{endfor}}