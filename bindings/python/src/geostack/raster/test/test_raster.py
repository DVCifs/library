# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import os
import sys

sys.path.insert(0, os.path.realpath('../../../'))

import numpy as np
from geostack import raster


def test_raster_1D():
    testRasterA = raster.Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(5, 1.0)
    testRasterA.setAllCellValues(1.0)
    testRasterA.setCellValue(99.9, 2)
    
    assert np.round(testRasterA.max(), 1) == 99.9
    assert testRasterA.min() == 1.0


def test_raster2d_resize():
    testRasterA = raster.Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(5, 1.0, ny=5, hy=1.0)
    testRasterA.setAllCellValues(1.0)
    testRasterA.setCellValue(99.9, i=2, j=2)

    assert testRasterA.hasData() == True
    assert np.round(testRasterA.getNearestValue(2, 2), 1) == 99.9
    assert np.round(testRasterA.max(), 1) == 99.9
    assert testRasterA.min() == 1.0


def test_raster_max():

    testRasterA = raster.Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
    testRasterA.setAllCellValues(0.0)
    testRasterA.setCellValue(99.9, i=2, j=2)


    assert np.round(testRasterA.max(), 1) == 99.9

def test_raster_min():
    testRasterA = raster.Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
    testRasterA.setAllCellValues(0.0)
    testRasterA.setCellValue(99.9, i=2, j=2)

    assert np.round(testRasterA.min(), 1) == 0.0


def test_raster_copy():
    testRasterA = raster.Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
    testRasterA.setAllCellValues(0.0)
    testRasterA.setCellValue(99.9, i=2, j=2)

    testRasterC = raster.Raster.copy("testRasterC", testRasterA)

    assert testRasterC.name == "testRasterC"

def test_raster_data():

    testRasterA = raster.Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(nx=5, ny=5, hx=1.0, hy=1.0)
    testRasterA.setAllCellValues(0.0)
    testRasterA.setCellValue(99.9, i=2, j=2)

    testRasterC = raster.Raster.copy("testRasterC", testRasterA)
    assert np.allclose(testRasterC.writeData(), testRasterA.writeData()) == True


def test_raster2d_properties():
    testRasterA = raster.Raster(name="testRasterA", base_type=np.float32,
        data_type=np.float32)
    testRasterA.init(5, 1.0, ny=5, hy=1.0)

    testRasterA.setProperty("property0", 99)
    testRasterA.setProperty("property1", "rstr")

    assert int(testRasterA.getProperty("property0")) == 99
    assert testRasterA.getProperty("property1") == "rstr"