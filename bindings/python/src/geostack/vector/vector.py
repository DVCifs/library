# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import json
import numpy as np
from copy import deepcopy
import functools
from typing import Union
import numbers
from ._cy_vector import (_Coordinate_d, _Coordinate_f,
                        _VertexCollection_d, _VertexCollection_f,
                        _Vector_d, _Vector_f)
from ._cy_vector import IndexList as _IndexList
from ._cy_vector import _BoundingBox_d, _BoundingBox_f
from .. import io
from .. import core
from .. import gs_enums
from ..raster import raster

__all__ = ["Coordinate", "VertexCollection",
            "Vector", "BoundingBox", "IndexList"]

# __all__ = ["Coordinate", "Vertex", "Point", "LineString",
#            "Polygon", 'MultiPoint', "MultiLineString", 
#            "MultiPolygon", "Vector", "BoundingBox"]

class IndexList:
    def __init__(self, index_list):
        if not isinstance(index_list, _IndexList):
            raise TypeError("index_list should be an instance of cython index list")
        self._handle = index_list

    @property
    def size(self):
        return self._handle.size

    def as_array(self):
        return self._handle.as_array()

    def __iter__(self):
        iter(self._handle)

    def __next__(self):
        next(self._handle)

    def __getitem__(self, index):
        return self._handle[index]

    def __len__(self):
        return len(self._handle)

    def __repr__(self):
        return "<class 'geostack.%s.%s'>" % (self.__module__,
                                     self.__class__.__name__)

class BoundingBox:
    """BoundingBox class for cython wrapper to c++ object
    """
    def __init__(self, dtype=np.float32):
        """BoundingBox constructor.

        Parameters
        ----------
        dtype: np.dtype (np.float32/np.float64)
            data type of bounding box.

        Returns
        -------
        out: BoundingBox
            An instance of BoundingBox object.

        Examples
        --------
        >>> bbox = BoundingBox(dtype=np.float32)
        """
        self._handle = None
        self._dtype = None
        if dtype == np.float32:
            self._handle = _BoundingBox_f()
            self._dtype = np.float32
        elif dtype == np.float64:
            self._handle = _BoundingBox_d()
            self._dtype = np.float64

    @staticmethod
    def from_bbox(input_bbox):
        """Intantiate BoundingBox from cython instance.

        A static method to generate a bounding box from cython instances
        of BoundingBox. It is provided as a convenience function and should
        not be needed for most of purposes.

        Parameters
        ----------
        input_bbox: _BoundingBox_d/_BoundingBox_f
            An instances of boundingbox classes implemented in cython
        """
        out = BoundingBox()
        if isinstance(input_bbox, BoundingBox):
            out._handle = input_bbox._handle
            out.dtype = input_bbox._dtype
        else:
            out._handle = input_bbox
            out._dtype = np.float32 if isinstance(input_bbox, _BoundingBox_f) else np.float64
        return out

    @staticmethod
    def from_list(input_bbox, dtype=np.float32):
        """Instantiate a BoundingBox from a list of list of coordinate bounds.

        A static method to instantiate a bounding box from a list of list
        containing coordinate bounds of a box. This function is provided
        for convenience to generate a C++ instance of bounding box from
        python list.

        For instances, a bounding box can be *[[lower_left_x, lower_left_y],
        [upper_right_x, upper_right_y]]*, where *(lower_left_x, lower_left_y)*
        are the **x** and **y** coordinates of the lower left corner of the
        bounding box while *(upper_right_x, upper_right_y)* are the **x**, **y**
        coordinates of the upper right corner.


        Parameters
        ----------
        input_bbox: list
            input bounding box as a list of list

        dtype: np.dtype (np.float32/np.float64)
            Data type of bounding box
        """
        _input_bbox = deepcopy(input_bbox)
        for item in _input_bbox:
            item.extend([0] * (4 - len(item)))
        if dtype is None or dtype == np.float32:
            out = BoundingBox.from_bbox(_BoundingBox_f.from_list(_input_bbox))
        elif dtype == np.float64:
            out = BoundingBox.from_bbox(_BoundingBox_d.from_list(_input_bbox))
        return out

    def convert(self, dst_proj, src_proj):
        if not isinstance(src_proj, core.ProjectionParameters):
            raise TypeError("src_proj should be an instance of ProjectionParameters")
        if not isinstance(dst_proj, core.ProjectionParameters):
            raise TypeError("dst_proj should be an instance of ProjectionParameters")
        out = BoundingBox(dtype=self._dtype)
        out._handle = self._handle.convert(dst_proj._handle, src_proj._handle)
        return out

    def centroidDistanceSqr(self, other):
        """Compute minimumdistanceSqr from the centroid of input BoundingBox.

        Parameters
        ----------
        other: BoundingBox
            An instance of BoundingBox class with a same dtype.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        if not isinstance(other, (BoundingBox, _BoundingBox_d, _BoundingBox_f)):
            raise TypeError("Input bounding box should be an instance of BoundingBox class")
        _other = other._handle if isinstance(other, BoundingBox) else other
        if isinstance(_other, _BoundingBox_d):
            assert isinstance(self._handle, _BoundingBox_d), "datatype mismatch"
        elif isinstance(_other, _BoundingBox_f):
            assert isinstance(self._handle, _BoundingBox_f), "datatype mismatch"
        out = self._handle.centroidDistanceSqr(_other)
        return out

    def minimumDistanceSqr(self, other):
        """Compute distanceSqr between the boundingBox to the input boundingBox.

        Parameters
        ----------
        other: BoundingBox
            An instance of BoundingBox class with a same dtype.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        if not isinstance(other, (BoundingBox, _BoundingBox_d, _BoundingBox_f)):
            raise TypeError("Input bounding box should be an instance of BoundingBox class")
        _other = other._handle if isinstance(other, BoundingBox) else other
        if isinstance(_other, _BoundingBox_d):
            assert isinstance(self._handle, _BoundingBox_d), "datatype mismatch"
        elif isinstance(_other, _BoundingBox_f):
            assert isinstance(self._handle, _BoundingBox_f), "datatype mismatch"
        out = self._handle.minimumDistanceSqr(_other)
        return out

    def extend(self, other):
        """Extend the bounding box using input argument.

        Parameters
        ----------
        other: BoundingBox/float/Coordinate
            Input argument for extending the bounding box.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        if isinstance(other, (Coordinate, _Coordinate_d, _Coordinate_f)):
            _other = other._handle if isinstance(other, Coordinate) else other
            if isinstance(_other, _Coordinate_d):
                assert self._dtype == np.float64, "datatype mismatch"
            elif isinstance(_other, _Coordinate_f):
                assert self._dtype == np.float32, "datatype mismatch"
            self._handle.extend_with_coordinate(_other)
        elif isinstance(other, numbers.Real):
            self._handle.extend_with_value(float(other))
        elif isinstance(other, (BoundingBox, _BoundingBox_d, _BoundingBox_f)):
            _other = other._handle if isinstance(other, BoundingBox) else other
            if isinstance(_other, _BoundingBox_d):
                assert self._dtype == np.float64, "datatype mismatch"
            elif isinstance(_other, _BoundingBox_f):
                assert self._dtype == np.float32, "datatype mismatch"
            self._handle.extend_with_bbox(_other)
        else:
            raise TypeError("other should be a float or an instance of Coordinate or BoundingBox")

    @property
    def centroid(self):
        """Get the centroid of the bounding box.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        return Coordinate._from_coordinate(self._handle.centroid())

    @property
    def extent(self):
        """Get the extent of the bounding box.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        return Coordinate._from_coordinate(self._handle.extent())

    @property
    def min(self):
        assert self._handle is not None, "BoundingBox is not instantiated"
        return Coordinate._from_coordinate(self._handle.min_c)

    @property
    def max(self):
        assert self._handle is not None, "BoundingBox is not instantiated"
        return Coordinate._from_coordinate(self._handle.max_c)

    def reset(self):
        """Reset the instantiation of the bounding box.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        self._handle.reset()

    def to_list(self):
        """Convert the bounding box to list of list of coordinates.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        return self._handle.to_list()

    def contains(self, other):
        if not isinstance(other, Coordinate):
            raise TypeError("input argument should be a Coordinate")
        if self._dtype != other._dtype:
            raise TypeError("Type mismatch between BoundingBox and Coordinate")
        return self._handle.contains(other._handle)

    @staticmethod
    def boundingBoxIntersects(this, other):
        if not isinstance(this, BoundingBox) or isinstance(other, BoundingBox):
            raise TypeError("input arguments should be a BoundingBox")
        assert this._dtype == other._dtype, "Type mismatch between input argument"
        if this._dtype == np.float32:
            return _BoundingBox_f.boundingBoxIntersects(this._handle,
                other._handle)
        elif this._dtype == np.float64:
            return _BoundingBox_d.boundingBoxIntersects(this._handle,
                other._handle)

    @staticmethod
    def boundingBoxContains(this, other):
        if not isinstance(this, BoundingBox):
            raise TypeError("first argument should be a BoundingBox")
        if not isinstance(other, (Coordinate, BoundingBox)):
            raise TypeError("second argument should be a Coordinate/BoundingBox")
        assert this._dtype == other._dtype, "Type mismatch between input argument"
        if this._dtype == np.float32:
            if isinstance(other, Coordinate):
                return _BoundingBox_f.bbox_contains_coordinate(this._handle,
                    other._handle)
            else:
                return _BoundingBox_f.bbox_contains_bbox(this._handle,
                    other._handle)
        elif this._dtype == np.float64:
            if isinstance(other, Coordinate):
                return _BoundingBox_d.bbox_contains_coordinate(this._handle,
                    other._handle)
            else:
                return _BoundingBox_d.bbox_contains_bbox(this._handle,
                    other._handle)

    def __getitem__(self, idx):
        if idx > 1:
            raise IndexError(f"Index {idx} should be in range [0,1]")
        return Coordinate._from_coordinate(self._handle[idx])

    def __repr__(self):
        return "<class 'geostack.%s.%s'>" % (self.__module__,
                                     self.__class__.__name__)


class Coordinate:
    '''Coordinate class wrapper for c++ object.

    Parameters
    ----------
    lon: longitude of coordinate, float value in degrees east
    lat: latitude of coordinate, float value in degrees north

    Returns
    -------
    Instance of coordinate object


    Examples
    --------
    >>> c = Coordinate(lon=130.0, lat=-34.0, dtype=np.float32)
    '''

    def __init__(self, lon=None, lat=None, lev=None, tstep=None, dtype=np.float32):
        if lon is None and lat is None:
            self._handle = None
            self._dtype = None
        else:
            if dtype not in [float, np.float32, np.float64]:
                raise TypeError("dtype should be np.float32/np.float64")
            self._dtype = np.float32 if dtype in [float, np.float32] else np.float64
            if dtype == np.float64:
                self._handle = _Coordinate_d(
                    np.float64(lon), np.float64(lat))
            elif dtype == np.float32 or dtype == float:
                self._handle = _Coordinate_f(
                    np.float32(lon), np.float32(lat))

    @staticmethod
    def _from_coordinate(inp_coordinate):
        """Instantiate Coordinate object from a Cython object.

        Parameters
        ----------
        inp_coordinate : _Coordinate_f/ _Coordinate_d
            An instance of float or double type Coordinate object.

        Returns
        -------
        out: Coordinate
            An instance of Python Coordinate object
        """
        if isinstance(inp_coordinate, _Coordinate_d):
            out = Coordinate()
            out._dtype = np.float64
            out._handle = inp_coordinate
        elif isinstance(inp_coordinate, _Coordinate_f):
            out = Coordinate()
            out._dtype = np.float32
            out._handle = inp_coordinate
        return out

    @staticmethod
    def from_list(other, dtype=np.float32):
        if not isinstance(other, list):
            raise TypeError("input argument should be a list")
        if len(other) < 2:
            raise ValueError("length of input list should be atleast 2")
        _other = deepcopy(other)
        _other.extend([0] * (4 - len(other)))
        if dtype == np.float32:
            return Coordinate._from_coordinate(_Coordinate_f.from_list(_other))
        elif dtype == np.float64:
            return Coordinate._from_coordinate(_Coordinate_d.from_list(_other))

    def getCoordinate(self):
        '''get the coordinates of the object

        Parameters
        ----------
        Nil

        Returns
        -------
        out: tuple of coordinate (longitude, latitude)

        Examples
        --------
        >>> c = Coordinate(lon=130.0, lat=-34.0)
        >>> c.getCoordinate()
        (130.0, -34.0)
        '''
        if hasattr(self, "_handle"):
            return self._handle.getCoordinate()
        else:
            raise AttributeError("Coordinate class is not yet initialized")

    def maxCoordinate(self, this, other):
        """Instantiate Coordinate object with the maximum of two input coordinates.

        Parameters
        ----------
        this : Coordinate
            An instance of Coordinate object
        other : Coordinate
            An instance of Coordinate object

        Returns
        -------
        out : Coordinate
            A Coordinate instance with the maximum of the input Coordinates

        Raises
        ------
        TypeError
            Type mismatch between two input coordinates
        RuntimeError
            Input Coordinate not initialized
        TypeError
            Input arguments should be instance of coordinates
        """
        assert self._handle is not None, "Coordinate is not instantiated"
        if isinstance(this, Coordinate) and isinstance(other, Coordinate):
            if this._handle is not None or other._handle is not None:
                raise RuntimeError("input coordinates are not initialized")
            assert self._dtype == this._dtype == other._dtype, "data type mismatch"
            return Coordinate._from_coordinate(
                self._handle.maxCoordinate(this._handle, other._handle))
        else:
            raise TypeError("Input argument should be instance of Coordinate")

    def minCoordinate(self, this, other):
        """Instantiate Coordinate object with the minimum of two input coordinates.

        Parameters
        ----------
        this : Coordinate
            An instance of Coordinate object
        other : Coordinate
            An instance of Coordinate object

        Returns
        -------
        out : Coordinate
            A Coordinate instance with the minimum of the input Coordinates

        Raises
        ------
        TypeError
            Type mismatch between two input coordinates
        RuntimeError
            Input Coordinate not initialized
        TypeError
            Input arguments should be instance of coordinates
        """
        assert self._handle is not None, "Coordinate is not instantiated"
        if isinstance(this, Coordinate) and isinstance(other, Coordinate):
            if this._handle is not None or other._handle is not None:
                raise RuntimeError("input coordinates are not initialized")
            assert self._dtype == this._dtype == other._dtype, "datatype mismatch"
            return Coordinate._from_coordinate(
                self._handle.minCoordinate(this._handle, other._handle))
        else:
            raise TypeError("Input argument should be instance of Coordinate")

    def centroid(self, this, other):
        """Instantiate Coordinate object with the centroid of two input coordinates

        Parameters
        ----------
        this : Coordinate
            An instance of Coordinate object
        other : Coordinate
            An instance of Coordinate object

        Returns
        -------
        out : Coordinate
            A Coordinate instance with the centroid of the input Coordinates

        Raises
        ------
        TypeError
            Type mismatch between two input coordinates
        RuntimeError
            Input Coordinate not initialized
        TypeError
            Input arguments should be instance of coordinates
        """
        assert self._handle is not None, "Coordinate is not instantiated"
        if isinstance(this, Coordinate) and isinstance(other, Coordinate):
            if this._handle is not None or other._handle is not None:
                raise RuntimeError("input coordinates are not initialized")
            assert self._dtype == this._dtype == other._dtype, "datype mismatch"
            return Coordinate._from_coordinate(
                self._handle.centroid(this._handle, other._handle))
        else:
            raise TypeError("Input argument should be instance of Coordinate")

    def magnitudeSquared(self):
        if self._handle is not None:
            return self._handle.magnitudeSquared()

    @property
    def latitude(self):
        assert hasattr(self, "_handle"), "Coordinate is not yet initialized"
        return self._handle.getLatitude()

    @property
    def longitude(self):
        assert hasattr(self, "_handle"), "Coordinate is not yet initialized"
        return self._handle.getLongitude()

    @property
    def level(self):
        assert hasattr(self, "_handle"), "Coordinate is not yet initialized"
        return self._handle.getLevel()

    @property
    def timeStep(self):
        assert hasattr(self, "_handle"), "Coordinate is not yet initialized"
        return self._handle.getTimeStep()

    def to_list(self):
        return self._handle.to_list()

    def _check_type(self, other):
        assert type(self._handle) == type(other._handle), "datatype mismatch"

    def getGeoHash(self):
        return self._handle.getGeoHash()

    def __eq__(self, other):
        self._check_type(other)
        out = self._handle == other._handle
        return out

    def __ne__(self, other):
        self._check_type(other)
        out = self._handle != other._handle
        return out

    def __add__(self, other):
        self._check_type(other)
        out = self._handle + other._handle
        return Coordinate._from_coordinate(out)

    def __sub__(self, other):
        self._check_type(other)
        out = self._handle - other._handle
        return Coordinate._from_coordinate(out)

    def __getitem__(self, idx):
        if idx > 3:
            raise IndexError(f"Index {idx} should be in range[0, 3]")
        return self._handle[idx]

    def __repr__(self):
        return "<class 'geostack.%s.%s'>" % (self.__module__,
                                     self.__class__.__name__)


class VertexCollection:
    '''VertexCollection class wrapper for c++ vertex class

    Examples
    --------
    >>> v = VertexCollection()
    '''

    def __init__(self, dtype=np.float32):
        if dtype == np.float64:
            self._handle = _VertexCollection_d()
        elif dtype == np.float32:
            self._handle = _VertexCollection_f()
        self._dtype = dtype

    def getCoordinates(self):
        '''get the coordinates of the vertex collection

        Parameters
        ----------
        None

        Returns
        -------
        out: array of coordinates of the vertex collection

        Examples
        --------
        >>> v = VertexCollection()
        >>> coords = v.getCoordinates()
        '''
        if not hasattr(self, "_handle"):
            raise AttributeError("Vertex class is not yet initialized")
        return np.asanyarray(self._handle.getCoordinates())

    def clear(self):
        self._handle.clear()
    
    @property
    def size(self):
        return self._handle.size()

    def __repr__(self):
        return "<class 'geostack.%s.%s'>" % (self.__module__,
            self.__class__.__name__)


# class Point:
#     '''Point class wrapper of c++ point class

#     Parameters
#     ----------
#     data_type : data type for the point, np.float32 or np.float64

#     Returns
#     -------
#     Instance of Point object


#     Examples
#     --------
#     >>> p = Point(data_type=np.float32)
#     '''

#     def __init__(self, data_type=np.float32):
#         self._properties = {}
#         self._propNames = {}
#         self._property_count = 0
#         self._dtype = None
#         self._handle = None

#     @staticmethod
#     def _from_point(point_object):
#         out = Point()
#         if isinstance(point_object, _Point_f):
#             out._dtype = np.float32
#             out._handle = point_object
#         elif isinstance(point_object, _Point_d):
#             out._dtype = np.float64
#             out._handle = point_object
#         elif isinstance(point_object, Point):
#             out._dtype = point_object._dtype
#             out._handle = point_object._handle
#             raise TypeError(
#                 "point_obj should an instance of Point class")
#         out.getPropertyNames()
#         return out

#     @property
#     def bounds(self):
#         return self.getBounds()

#     def getBounds(self):
#         if self._handle is not None:
#             return BoundingBox.from_bbox(self._handle.getBounds())

#     def hasProperty(self, other):
#         if isinstance(other, str):
#             _propname = other
#         elif isinstance(other, bytes):
#             _propname = other.decode()
#         else:
#             raise TypeError("property name should be str or bytes type")

#         if self._handle is not None:
#             if self._property_count > 0:
#                 for item in self._propNames:
#                     if _propname in self._propNames[item]:
#                         return True
#             else:
#                 raise RuntimeWarning("No properties are defined for the \
#                     Point")
#         else:
#             raise RuntimeWarning("Point class has not been initialized")

#         return False

#     def getProperty(self, other):

#         if not isinstance(other, str):
#             raise TypeError("property name should be of string type")

#         if self._handle is not None:
#             if not self._property_count > 0:
#                 raise RuntimeWarning("No properties are defined for the \
#                     Point")
#             else:
#                 if self.hasProperty(other):
#                     for item in self._propNames:
#                         if other in self._propNames[item]:
#                             if item == "int":
#                                 return self._handle.getProperty(
#                                         other.encode('UTF-8'), int)
#                             elif item == 'double':
#                                 return self._handle.getProperty(
#                                         other.encode('UTF-8'), float)
#                             elif item == 'string':
#                                 return self._handle.getProperty(
#                                         other.encode('UTF-8'), str)
#                 else:
#                     raise ValueError("property %s is not defined for the \
#                         Point object" % other)
#         else:
#             raise RuntimeWarning("Point class has not been initialized")

#     @property
#     def properties(self):
#         return self.getProperties()

#     def getProperties(self):
#         if self._handle is not None:
#             if not self._property_count > 0:
#                 raise RuntimeWarning("No properties are defined for the \
#                     Point")
#             else:
#                 for item in self._propNames:
#                     if item == "int":
#                         self._properties[item] = self._handle.getProperties_i()
#                     elif item == "double":
#                         self._properties[item] = self._handle.getProperties_d()
#                     elif item == "string":
#                         self._properties[item] = self._handle.getProperties_s()
#                 return self._properties
#         else:
#             raise RuntimeWarning("Point class has not been initialized")


#     def getPropertyNames(self):
#         if self._handle is not None:
#             self._propNames = self._handle.getPropertyNames()
#             for item in self._propNames:
#                 self._property_count += len(self._propNames[item])
#         return self._propNames

#     @property
#     def coordinate(self):
#         return self.getCoordinate()

#     def getCoordinate(self):
#         if self._handle is not None:
#             return self._handle.getCoordinate()

#     def __repr__(self):
#         return "<class 'geostack.%s.%s'>" % (self.__module__,
#             self.__class__.__name__)


# class LineString:
#     def __init__(self):
#         self._properties = {}
#         self._propNames = {}
#         self._property_count = 0
#         self._handle = None
#         self._dtype = None

#     @staticmethod
#     def _from_line_string(line_string_obj):
#         out = LineString()
#         if isinstance(line_string_obj, _LineString_f):
#             out._handle = line_string_obj
#             out._dtype = np.float32
#         elif isinstance(line_string_obj, _LineString_d):
#             out._handle = line_string_obj
#             out._dtype = np.float64
#         elif isinstance(line_string_obj, LineString):
#             out._handle = line_string_obj._handle
#             out._dtype = line_string_obj._dtype
#         else:
#             raise TypeError(
#                 "line_string_obj should an instance of LineString class")
#         out.getPropertyNames()
#         return out

#     @property
#     def bounds(self):
#         return self.getBounds()

#     def getBounds(self):
#         if self._handle is not None:
#             return BoundingBox.from_bbox(self._handle.getBounds())

#     def hasProperty(self, other):
#         if isinstance(other, str):
#             _propname = other
#         elif isinstance(other, bytes):
#             _propname = other.decode()
#         else:
#             raise TypeError("property name should be str or bytes type")

#         if self._handle is not None:
#             if self._property_count > 0:
#                 for item in self._propNames:
#                     if _propname in self._propNames[item]:
#                         return True
#             else:
#                 raise RuntimeWarning("No properties are defined for the \
#                     LineString")
#         else:
#             raise RuntimeWarning("LineString class has not been initialized")

#         return False

#     def getProperty(self, other):

#         if not isinstance(other, str):
#             raise TypeError("property name should be of string type")

#         if self._handle is not None:
#             if not self._property_count > 0:
#                 raise RuntimeWarning("No properties are defined for the \
#                     LineString")
#             else:
#                 if self.hasProperty(other):
#                     for item in self._propNames:
#                         if other in self._propNames[item]:
#                             if item == "int":
#                                 return self._handle.getProperty(
#                                         other.encode('UTF-8'), int)
#                             elif item == 'double':
#                                 return self._handle.getProperty(
#                                         other.encode('UTF-8'), float)
#                             elif item == 'string':
#                                 return self._handle.getProperty(
#                                         other.encode('UTF-8'), str)
#                 else:
#                     raise ValueError("property %s is not defined for the \
#                         LineString object" % other)
#         else:
#             raise RuntimeWarning("LineString class has not been initialized")

#     @property
#     def properties(self):
#         return self.getProperties()

#     def getProperties(self):
#         if self._handle is not None:
#             if not self._property_count > 0:
#                 raise RuntimeWarning("No properties are defined for the \
#                     LineString")
#             else:
#                 for item in self._propNames:
#                     if item == "int":
#                         self._properties[item] = self._handle.getProperties_i()
#                     elif item == "double":
#                         self._properties[item] = self._handle.getProperties_d()
#                     elif item == "string":
#                         self._properties[item] = self._handle.getProperties_s()
#                 return self._properties
#         else:
#             raise RuntimeWarning("LineString class has not been initialized")

#     def getPropertyNames(self):
#         if self._handle is not None:
#             self._propNames = self._handle.getPropertyNames()
#             for item in self._propNames:
#                 self._property_count += len(self._propNames[item])
#         return self._propNames

#     def getCoordinates(self):
#         if self._handle is not None:
#             return self._handle.getCoordinates()

#     def __repr__(self):
#         return "<class 'geostack.%s.%s'>" % (self.__module__,
#             self.__class__.__name__)


# class Polygon:
#     def __init__(self):
#         self._properties = {}
#         self._propNames = {}
#         self._property_count = 0
#         self._handle = None
#         self._dtype = None

#     @staticmethod
#     def _from_polygon(polygon_obj):
#         out = Polygon()
#         if isinstance(polygon_obj, _Polygon_f):
#             out._handle = polygon_obj
#             out._dtype = np.float32
#         elif isinstance(polygon_obj, _Polygon_d):
#             out._handle = polygon_obj
#             out._dtype = np.float64
#         elif isinstance(polygon_obj, Polygon):
#             out._handle = polygon_obj._handle
#             out._dtype = polygon_obj._dtype
#         else:
#             raise TypeError(
#                 "polygon_obj should an instance of Polygon class")
#         out.getPropertyNames()
#         return out

#     @property
#     def bounds(self):
#         return self.getBounds()

#     def getBounds(self):
#         if self._handle is not None:
#             return BoundingBox.from_bbox(self._handle.getBounds())

#     def hasProperty(self, other):
#         if isinstance(other, str):
#             _propname = other
#         elif isinstance(other, bytes):
#             _propname = other.decode()
#         else:
#             raise TypeError("property name should be str or bytes type")
#         if self._handle is not None:
#             if self._property_count > 0:
#                 for item in self._propNames:
#                     if _propname in self._propNames[item]:
#                         return True
#             else:
#                 raise RuntimeWarning("No properties are defined for the \
#                     Polygon")
#         else:
#             raise RuntimeWarning("Polygon class has not been initialized")

#         return False

#     def getProperty(self, other):
#         if not isinstance(other, str):
#             raise TypeError("property name should be of string type")

#         if self._handle is not None:
#             if not self._property_count > 0:
#                 raise RuntimeWarning("No properties are defined for the \
#                     Polygon")
#             else:
#                 if self.hasProperty(other):
#                     for item in self._propNames:
#                         if other in self._propNames[item]:
#                             if item == "int":
#                                 return self._handle.getProperty(
#                                         other.encode('UTF-8'), int)
#                             elif item == 'double':
#                                 return self._handle.getProperty(
#                                         other.encode('UTF-8'), float)
#                             elif item == 'string':
#                                 return self._handle.getProperty(
#                                         other.encode('UTF-8'), str)
#                 else:
#                     raise ValueError("property %s is not defined for the \
#                         Polygon object" % other)
#         else:
#             raise RuntimeWarning("Polygon class has not been initialized")

#     @property
#     def properties(self):
#         return self.getProperties()

#     def getProperties(self):
#         if self._handle is not None:
#             if not self._property_count > 0:
#                 raise RuntimeWarning("No properties are defined for the \
#                     Polygon")
#             else:
#                 for item in self._propNames:
#                     if item == "int":
#                         self._properties[item] = self._handle.getProperties_i()
#                     elif item == "double":
#                         self._properties[item] = self._handle.getProperties_d()
#                     elif item == "string":
#                         self._properties[item] = self._handle.getProperties_s()
#                 return self._properties
#         else:
#             raise RuntimeWarning("Polygon class has not been initialized")

#     def getPropertyNames(self):
#         if self._handle is not None:
#             self._propNames = self._handle.getPropertyNames()
#             for item in self._propNames:
#                 self._property_count += len(self._propNames[item])
#         return self._propNames

#     def getCoordinates(self):
#         if self._handle is not None:
#             return self._handle.getCoordinates()

#     def __repr__(self):
#         return "<class 'geostack.%s.%s'>" % (self.__module__,
#             self.__class__.__name__)


# class MultiPoint:
#     def __init__(self):
#         self._dtype = None
#         self._handle = None

#     @staticmethod
#     def _from_list_of_points(list_of_points):
#         out = MultiPoint()
#         if isinstance(list_of_points, _ListOfPoints_d):
#             out._dtype = np.float64
#             out._handle = list_of_points
#         elif isinstance(list_of_points, _ListOfPoints_f):
#             out._dtype = np.float32
#             out._handle = list_of_points
#         elif isinstance(list_of_points, MultiPoint):
#             out._handle = list_of_points._handle
#             out._dtype = list_of_points._dtype
#         else:
#             raise TypeError(
#                 "list_of_points should be an instance of cython objects")
#         return out

#     @property
#     def count(self):
#         if self._handle is not None:
#             return self._handle.count

#     def to_array(self):
#         if self._handle is not None:
#             return self._handle.to_array()
#         else:
#             raise RuntimeError("MultiPoint class has not been initialized")

#     def getBounds(self, idx):
#         if not isinstance(idx, int):
#             raise TypeError("index should be of integer type")
#         if self._handle is not None:
#             return BoundingBox.from_bbox(self._handle.getBounds(idx))

#     def getProperty(self, idx, propName, propType):
#         if isinstance(propName, str):
#             _propName = propName.encode('UTF-8')
#         if propType not in [str, int, float]:
#             raise ValueError("propType should be either int, float or str")
#         return self._handle.getProperty(int(idx), _propName, propType)

#     def __getitem__(self, idx):
#         if self._handle is not None:
#             if idx >= 0 and idx < self.count:
#                 return Point._from_point(self._handle.__getitem__(idx))
#             else:
#                 raise IndexError(f"Index {idx} is out of bounds")

#     def __iter__(self):
#         return self

#     def __next__(self):
#         if self._handle is not None:
#             return Point._from_point(next(self._handle))

#     def __repr__(self):
#         return "<class 'geostack.%s.%s'>" % (self.__module__,
#             self.__class__.__name__)


# class MultiLineString:
#     def __init__(self):
#         self._dtype = None
#         self._handle = None

#     @staticmethod
#     def _from_list_of_linestrings(list_of_linestrings):
#         out = MultiLineString()
#         if isinstance(list_of_linestrings, _ListOfLines_d):
#             out._dtype = np.float64
#             out._handle = list_of_linestrings
#         elif isinstance(list_of_linestrings, _ListOfLines_f):
#             out._dtype = np.float32
#             out._handle = list_of_linestrings
#         elif isinstance(list_of_linestrings, MultiLineString):
#             out._dtype = list_of_linestrings._dtype
#             out._handle = list_of_linestrings._handle
#         else:
#             raise TypeError(
#                 "list_of_linestrings should be an instance of cython objects")
#         return out

#     @property
#     def count(self):
#         if self._handle is not None:
#             return self._handle.count

#     def to_array(self):
#         if self._handle is not None:
#             return self._handle.to_array()
#         else:
#             raise RuntimeError("MultiLineString class has not been initialized")

#     def getBounds(self, idx):
#         if not isinstance(idx):
#             raise TypeError("index should be of integer type")
#         if self._handle is not None:
#             return BoundingBox.from_bbox(self._handle.getBounds(idx))

#     def getProperty(self, idx, propName, propType):
#         if isinstance(propName, str):
#             _propName = propName.encode('UTF-8')
#         if propType not in [str, int, float]:
#             raise ValueError("propType should be either int, float or str")
#         return self._handle.getProperty(int(idx), _propName, propType)

#     def __getitem__(self, idx):
#         if self._handle is not None:
#             if idx >= 0 and idx < self.count:
#                 return LineString._from_line_string(self._handle.__getitem__(idx))
#             else:
#                 raise IndexError(f"Index {idx} is out of bounds")

#     def __iter__(self):
#         return self

#     def __next__(self):
#         if self._handle is not None:
#             return LineString._from_line_string(next(self._handle))

#     def __repr__(self):
#         return "<class 'geostack.%s.%s'>" % (self.__module__,
#             self.__class__.__name__)


# class MultiPolygon:
#     def __init__(self):
#         self._dtype = None
#         self._handle = None

#     @staticmethod
#     def _from_list_of_polygons(list_of_polygons):
#         out = MultiPolygon()
#         if isinstance(list_of_polygons, _ListOfPolygons_d):
#             out._dtype = np.float64
#             out._handle = list_of_polygons
#         elif isinstance(list_of_polygons, _ListOfPolygons_f):
#             out._dtype = np.float32
#             out._handle = list_of_polygons
#         elif isinstance(list_of_polygons, MultiPolygon):
#             out._dtype = list_of_polygons._dtype
#             out._handle = list_of_polygons._handle
#         else:
#             raise TypeError(
#                 "list_of_polygons should be an instance of cython objects")
#         return out

#     @property
#     def count(self):
#         if self._handle is not None:
#             return self._handle.count

#     def to_array(self):
#         if self._handle is not None:
#             return self._handle.to_array()
#         else:
#             raise RuntimeError("MultiPolygon class has not been initialized")

#     def getBounds(self, idx):
#         if not isinstance(idx, int):
#             raise TypeError("index should be integer type")
#         if self._handle is not None:
#             return BoundingBox.from_bbox(self._handle.getBounds(idx))

#     def getProperty(self, idx, propName, propType):
#         if isinstance(propName, str):
#             _propName = propName.encode('UTF-8')
#         if propType not in [str, int, float]:
#             raise ValueError("propType should be either int, float or str")
#         return self._handle.getProperty(int(idx), _propName, propType)

#     def __getitem__(self, idx):
#         if self._handle is not None:
#             if idx >= 0 and idx < self.count:
#                 return Polygon._from_polygon(self._handle.__getitem__(idx))
#             else:
#                 raise IndexError(f"Index {idx} is out of bounds")

#     def __iter__(self):
#         return self

#     def __next__(self):
#         if self._handle is not None:
#             return Polygon._from_polygon(next(self._handle))

#     def __repr__(self):
#         return "<class 'geostack.%s.%s'>" % (self.__module__,
#             self.__class__.__name__)


class Vector:
    """Vector class python object around C++ Vector.

    Parameters
    ----------
    dtype : numpy.dtype
        Data type for instantiating Cython Vector object, (np.float32/np.float64).

    Attributes
    ----------
    _dtype : np.dtype
        Data type of Vector class object.

    _handle : Cython object
        Handle to Cython Vector object.

    Methods
    -------
    _from_vector(vector_object)
        Instantiate Vector object from existing vector object.
    clear()
        Clear Vector object.
    hasData()
        Check whether Vector object has data.
    addPoint((lon,lat))
        Add point to the Vector object.
    addLineString(list_of_coordinates)
        Add a list of points representing a line string to the Vector object.
    addPolygon(list_of_coordinates)
        Add a list of coordinates representing a closed polygon the Vector object.
    getPoint(index)
        Get a point from the Vector object at an index.
    getLineString(index)
        Get a line string from the Vector object at an index.
    getPolygon(index)
        Get a polygon from the Vector object at an index.
    getPoints()
        Get all of the points from the Vector object.
    getLineStrings()
        Get all of the line strings from the Vector object.
    getPolygons()
        Get all of the polygons from the Vector object.
    getBounds()
        Get bounds of the Vector object.
    mapDistance(resolution, parameter=gs_enums.GeometryType, bounds=BoundingBox)
        Create a distance raster from a Vector object for a given resolution 
        and a given bounding box.
    setPointProperty(idx, propName, propValue)
        Set property propName to a value propValue of a point at a given index.
    setLineStringProperty(idx, propName, propValue)
        Set property propName to a value propValue of a LineString at a given index.
    setPolygonProperty(idx, propName, propValue)
        Set property propName to a value propValue of a Polygon at a given index.
    deduplicateVertices()
    region(other, parameter=gs_enums.GeometryType)
    nearest(other, parameter=gs_enums.GeometryType)
    pointSample(other=raster.Raster)
    """
    def __init__(self, dtype=np.float32):
        self._properties = {}
        self._dtype = None
        self._handle = None
        if dtype not in [float, np.float32, np.float64]:
            raise TypeError("dtype should be np.float32/ np.float64")
        self._dtype = np.float32 if dtype in [float, np.float32] else np.float64
        if self._dtype == np.float32:
            self._handle = _Vector_f()
        elif self._dtype == np.float64:
            self._handle = _Vector_d()

    @classmethod
    def _from_vector(cls, vec_obj):
        out = cls()
        _vec_obj = vec_obj._handle if isinstance(vec_obj, cls) else vec_obj
        if isinstance(_vec_obj, _Vector_d):
            out._dtype = np.float64
            out._handle = _vec_obj
        elif isinstance(_vec_obj, _Vector_f):
            out._dtype = np.float32
            out._handle = _vec_obj
        else:
            raise TypeError("vector_object can be an instance of Vector")
        return out

    def addPoint(self, other):
        if self._dtype is None or self._handle is None:
            raise RuntimeWarning('Vector class is not yet initialized')
        if isinstance(other, (list, tuple)):
            if len(other) != 2:
                raise TypeError("point should be a list or tuple of \
                        length 2")
            else:
                if not isinstance(other[0], numbers.Real):
                    raise TypeError("values in point should be of \
                        float type")
                else:
                    out = self._handle.addPoint(Coordinate.from_list(other,
                        dtype=self._dtype)._handle)
                    return out
        elif isinstance(other, Coordinate):
            out = self._handle.addPoint(other._handle)
            return out
        else:
            raise TypeError("point to be added should be a list or \
                    tuple")

    def addLineString(self, other):
        if self._dtype is None or self._handle is None:
            raise RuntimeWarning("Vector class is not yet initialized")
        if isinstance(other, list):
            if not isinstance(other[0], (list, tuple)):
                raise TypeError("If a list is provided, it should be a list \
                    of list/ or tuple")
            if len(other[0]) < 2:
                raise TypeError('If a list is provided, it should contain \
                    points as list of length atleast 2')
            _other = np.array(other).astype(self._dtype)
            for _ in range(4 - _other.shape[1]):
                _other = np.vstack((_other, np.zeros(_other.shape[0])))
            out = self._handle.addLineString(_other)
        elif isinstance(other, np.ndarray):
            if len(other.shape) == 2 and other.shape[1] >= 2:
                for _ in range(4 - _other.shape[1]):
                    _other = np.vstack((_other, np.zeros(_other.shape[0])))
                out = self._handle.addLineString(_other)
            else:
                raise TypeError("If a numpy array if provided, it should be \
                        of shape npoints x 2 is expected")
        return out

    def addPolygon(self, other):
        if self._dtype is None or self._handle is None:
            raise RuntimeWarning("Vector class is not yet initialized")
        if not isinstance(other, list):
            raise TypeError("Polygon object should be a list of \
                numpy array or list")
        elif not isinstance(other[0], (list, np.ndarray)):
            raise TypeError("Polygon object should be a list of \
                numpy array or list")

        if isinstance(other[0], list):
            if len(other[0][0]) < 2:
                raise TypeError('If a list is provided, it should contain \
                    points as list of length atleast 2')
        elif isinstance(other[0], np.ndarray):
            if len(other[0].shape) != 2 and other[0].shape[1] >= 2:
                raise TypeError("If a numpy array if provided, it should be \
                        of shape npoints x 2 is expected")

        _poly_item = []
        if isinstance(other[0], list):
            for item in other:
                _other = np.array(other[0]).astype(self._dtype)
                for _ in range(4 - _other.shape[1]):
                    _other = np.append(_other, np.zeros(shape=(_other.shape[0],1)), axis=1)
                _poly_item.append(_other)
        elif isinstance(other[0], np.ndarray):
            for item in other:
                _other = item.copy()
                for _ in range(4 - item.shape[1]):
                    _other = np.append(_other, np.zeros(shape=(_other.shape[0],1)), axis=1)
                _poly_item.append(_other)
        out = self._handle.addPolygon(_poly_item)
        return out

    def updatePointIndex(self, index):
        self._handle.updatePointIndex(index)

    def updateLineStringIndex(self, index):
        self._handle.updateLineStringIndex(index)
    
    def updatePolygonIndex(self, index):
        self._handle.updatePolygonIndex(index)

    def clear(self):
        self._handle.clear()

    def getGeometryIndexes(self):
        return IndexList(self._handle.getGeometryIndexes())

    def getPointIndexes(self):
        return IndexList(self._handle.getPointIndexes())

    def getLineStringIndexes(self):
        return IndexList(self._handle.getLineStringIndexes())

    def getPolygonIndexes(self):
        return IndexList(self._handle.getPolygonIndexes())

    def getPointCoordinate(self, idx):
        if self._handle is not None:
            return Coordinate._from_coordinate(self._handle.getPointCoordinate(int(idx)))

    def getLineStringCoordinates(self, idx):
        if self._handle is not None:
            out = self._handle.getLineStringCoordinates(int(idx))
            return np.asanyarray(out)

    def getPolygonCoordinates(self, idx):
        if self._handle is not None:
            out = self._handle.getPolygonCoordinates(int(idx))
            return np.asanyarray(out)

    def setProperty(self, idx, propName, propValue):
        if not isinstance(propValue,  (str, numbers.Real)):
            raise ValueError("property value should be either of \
                int, float and string")
        if not isinstance(propName, str):
            raise TypeError('Property name should be of string type')
        if self._handle is not None:
            self._handle.setProperty(idx,
                propName.encode('UTF-8'), propValue)
        else:
            raise RuntimeWarning("Vector class is not yet initialized")

    def getProperty(self, idx, propName, propType):
        if propType not in [int, str, float]:
            raise ValueError("propType should be int/str/float")
        if self._handle is not None:
            out = self._handle.getProperty(idx,
                propName.encode('UTF-8'), propType)
            return out
        else:
            raise RuntimeWarning("Vector class is not yet initialized")

    def deleteProperty(self, idx, propName):
        if self._handle is not None:
            out = self._handle.deleteProperty(idx, propName.encode('UTF-8'))
            return out
        else:
            raise RuntimeWarning("Vector class is not yet initialized")

    def setProjectionParameters(self, other):
        if not isinstance(other, core.ProjectionParameters):
            raise TypeError("dst_proj should be an instance of ProjectionParameters")
        if self._dtype != other.dtype:
            raise TypeError("mismatch between dtype of class instance and ProjectionParameters")
        self._handle.setProjectionParameters(other._handle)

    def getProjectionParameters(self):
        return core.ProjectionParameters.from_proj_param(self._handle.getProjectionParameters())

    def convert(self, dst_proj):
        if not isinstance(dst_proj, core.ProjectionParameters):
            raise TypeError("dst_proj should be an instance of ProjectionParameters")
        return Vector._from_vector(self._handle.convert(dst_proj._handle))

    # def getPoints(self):
    #     if self._handle is not None:
    #         return MultiPoint._from_list_of_points(self._handle.getPoints())

    # def getLineStrings(self):
    #     if self._handle is not None:
    #         return MultiLineString._from_list_of_linestrings(
    #             self._handle.getLineStrings())

    # def getPolygons(self):
    #     if self._handle is not None:
    #         return MultiPolygon._from_list_of_polygons(
    #             self._handle.getPolygons())

    def region(self, other: BoundingBox,
               geom_type: Union[numbers.Integral, gs_enums.GeometryType]=7):
        """Return vector geometries within a region.

        Parameters
        ----------
        other : BoundingBox
            input region of type BoundingBox
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of vector geometry to return

        Returns
        -------
        Vector
            Vector object containing vector geometries within the
            given bounding box.

        Raises
        ------
        TypeError
            input argument should be an instance of BoundingBox
        TypeError
            geom_type should be int/ gs_enums.GeometryType
        """
        if not isinstance(other, (BoundingBox, _BoundingBox_d, _BoundingBox_f)):
            raise TypeError("other should be an instance of BoundingBox")
        
        _bbox = other._handle if isinstance(other, BoundingBox) else other
        if isinstance(_bbox, _BoundingBox_d):
            assert self._dtype == np.float64
        else:
            assert self._dtype == np.float32
        
        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ gs_enums.GeometryType")
        
        if isinstance(geom_type, gs_enums.GeometryType):
            _geom_type = geom_type.value
        else:
            _geom_type = geom_type
        out = Vector._from_vector(self._handle.region(_bbox, _geom_type))
        return out

    def nearest(self, other: BoundingBox,
                geom_type: Union[numbers.Integral, gs_enums.GeometryType]=7):
        """Return vector geometries nearest to a region.

        Parameters
        ----------
        other : BoundingBox
            input region of type BoundingBox
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of vector geometry to return

        Returns
        -------
        Vector
            Vector object containing vector geometries within the
            given bounding box.

        Raises
        ------
        TypeError
            input argument should be an instance of BoundingBox
        TypeError
            geom_type should be int/ gs_enums.GeometryType
        """
        if not isinstance(other, (BoundingBox, _BoundingBox_d, _BoundingBox_f)):
            raise TypeError("other should be an instance of BoundingBox")
        _bbox = other._handle if isinstance(other, BoundingBox) else other
        if isinstance(_bbox, _BoundingBox_d):
            assert self._dtype == np.float64
        else:
            assert self._dtype == np.float32

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ gs_enums.GeometryType")

        if isinstance(geom_type, gs_enums.GeometryType):
            geom_type = geom_type.value
        else:
            geom_type = geom_type

        out = Vector._from_vector(self._handle.nearest(_bbox, geom_type))
        return out

    def attached(self, inp_coordinate: Coordinate,
                 geom_type: Union[numbers.Integral, gs_enums.GeometryType]=7):
        """Return vector geometries attached to the input coordinate.

        Parameters
        ----------
        inp_coordinate : Coordinate
            Input coordinate to obtain the attached vector geometry
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of vector geometry to return

        Returns
        -------
        Vector
            Vector object containing vector geometries attached to the
            input coordinate

        Raises
        ------
        TypeError
            inp_coordinate should be of type Coordinate
        TypeError
            geom_type should be int/ gs_enums.GeometryType
        AssertionError
            Datatype mismatch b/w Vector and input coordinate
        """
        if not isinstance(inp_coordinate, Coordinate):
            raise TypeError("inp_coordinate should be Coordinate")
        assert self._dtype == inp_coordinate._dtype, "DataType mismatch"
        if isinstance(geom_type, gs_enums.GeometryType):
            _geom_type = geom_type.value
        elif isinstance(_geom_type, numbers.Integral):
            _geom_type = geom_type
        else:
            raise TypeError("geom_type should be int/ gs_enums.GeometryType")
        out = Vector._from_vector(self._handle.attached(inp_coordinate,
            _geom_type))
        return out

    def deduplicateVertices(self):
        assert self._handle is not None, "Vector is not instantiated"
        self._handle.deduplicateVertices()

    def mapDistance(self, resolution: numbers.Real,
                    geom_type: Union[numbers.Integral, gs_enums.GeometryType]=7,
                    bounds: BoundingBox=None):
        """Return a distance map for the vector object.

        Parameters
        ----------
        resolution : numbers.Real
            resolution of the output raster.
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of geometry use for distance map.
        bounds : BoundingBox, optional
            bounding box to subset vector before creating
            distance map, by default None

        Returns
        -------
        raster.Raster
            distance map raster

        Raises
        ------
        AssertionError
            Vector is not instantiated
        TypeError
            resolution should be numeric
        TypeError
            bounds should be an instance of boundingbox
        TypeError
            geom_type should be int/ gs_enums.GeometryType
        """
        assert self._handle is not None, "Vector is not instantiated"
        if not isinstance(resolution, numbers.Real):
            raise TypeError("resolution should be numeric")
        _resolution = self._dtype(resolution)

        if bounds is None:
            _bounds = BoundingBox()
        else:
            if not isinstance(bounds, BoundingBox):
                raise TypeError("bounds should be an instance of boundingbox")
            _bounds = bounds

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ gs_enums.GeometryType")

        if isinstance(geom_type, gs_enums.GeometryType):
            _out = self._handle.mapDistance(_resolution, geom_type.value,
                _bounds._handle)
        else:
            _out = self._handle.mapDistance(_resolution, geom_type,
                _bounds._handle)
        out = raster.Raster.copy("rasterised", _out)
        out.setProjectionParameters(self.getProjectionParameters())
        return out

    def rasterise(self, resolution: numbers.Real,
                        script: str="",
                        geom_type: Union[numbers.Integral, gs_enums.GeometryType]=7,
                        bounds: BoundingBox=None):
        """Return a raster after rasterising the vector object.

        Parameters
        ----------
        resolution : numbers.Real
            resolution of the output raster
        script : str
            script to use for rasterise operation
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of geometry use for rasterise operation
        bounds : BoundingBox, optional
            bounding box to subset vector before creating
            distance map, by default None

        Returns
        -------
        raster.Raster
            raster generated from rasterise operation.

        Raises
        ------
        AssertionError
            Vector is not instantiated
        TypeError
            bounds should be an instance of boundingbox
        TypeError
            geom_type should be int/ gs_enums.GeometryType
        """
        assert self._handle is not None, "Vector is not instantiated"
        _resolution = self._dtype(resolution)

        if bounds is None:
            _bounds = BoundingBox(dtype=np.float32)
        else:
            if not isinstance(bounds, BoundingBox):
                raise TypeError("bounds should be an instance of boundingbox")
            _bounds = bounds

        if isinstance(script, str):
            _script = script.encode("UTF-8")
        elif isinstance(script, bytes):
            _script = script

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ gs_enums.GeometryType")

        if isinstance(geom_type, gs_enums.GeometryType):
            _out = self._handle.rasterise(_resolution,
                _script, geom_type.value, _bounds._handle)
        else:
            _out = self._handle.rasterise(_resolution,
                _script, geom_type, _bounds._handle)
        out = raster.Raster.copy("rasterised", _out)
        out.setProjectionParameters(self.getProjectionParameters())
        return out

    def pointSample(self, other):
        """Sample raster at the location of vector geometries.

        Parameters
        ----------
        other : raster.Raster
            input raster dataset for sampling.

        Returns
        -------
        bool
            True if raster was sampled False otherwise

        Raises
        ------
        TypeError
            Input argument should be of type Raster
        AssertionError
            Datatype mismatch
        """
        if not isinstance(other, (raster._cyRaster_d,
                                  raster._cyRaster_f,
                                  raster.Raster)):
            raise TypeError("Input argument should be an instance of Raster class")
        _bbox = other._handle if isinstance(other, raster.Raster) else other
        
        if isinstance(_bbox, raster._cyRaster_d):
            assert self._dtype == np.float64, "Datatype mismatch"
        else:
            assert self._dtype == np.float32, "Datatype mismatch"
        return self._handle.pointSample(_bbox)

    def getBounds(self):
        if self._handle is not None:
            return BoundingBox.from_bbox(self._handle.getBounds())

    def hasData(self):
        return self._handle.hasData()

    @staticmethod
    def from_geojson(fileName, dtype=np.float32):
        return io.geoJsonToVector(fileName, dtype=dtype)

    def to_geojson(self, fileName=None):
        if fileName is None:
            out = json.loads(io.vectorToGeoJson(self))
            return out
        else:
            with open(fileName, 'w') as out:
                out.write(io.vectorToGeoJson(self))

    @staticmethod
    def from_shapefile(fileName, dtype=np.float32):
        return io.shapefileToVector(fileName, dtype=dtype)

    def to_shapefile(self, fileName, geom_type=None):
        out = io.vectorToShapefile(self, fileName, geom_type=geom_type)
        return out

    @property
    def __geo_interface__(self):
        return io.vectorToGeoJson(self)

    def __eq__(self, other):
        if not isinstance(other, Vector):
            raise TypeError("input argument should be a Vector")
        assert self._dtype == other._dtype, "Type mismatch for Vectors"
        return self._handle == other._handle

    def __ne__(self, other):
        if not isinstance(other, Vector):
            raise TypeError("input argument should be a Vector")
        assert self._dtype == other._dtype, "Type mismatch for Vectors"
        return self._handle != other._handle

    def __repr__(self):
        return "<class 'geostack.%s.%s'>" % (self.__module__,
            self.__class__.__name__)
