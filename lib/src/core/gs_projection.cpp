/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <cmath>
#include <sstream>

#include "gs_opencl.h"
#include "gs_geometry.h"
#include "gs_string.h"
#include "gs_vector.h"
#include "gs_projection.h"

// Include code for projection
#include "cl_projection_head.c"

using namespace json11;

namespace Geostack
{
    /**
    * Singleton instance of %ProjectionTable.
    */
    ProjectionTable &ProjectionTable::instance() {

        static ProjectionTable projectionOperator;
        return projectionOperator;
    }

    /**
    * %ProjectionTable initialisation
    */
    void ProjectionTable::initialise() {
    
        if (!initialised) {

            // Build datums
            std::string datum;
            std::string datumCSV = std::string(R_PROJ4_datum_csv);
            std::istringstream datumCSVStream(datumCSV);

            // Skip header
            std::getline(datumCSVStream, datum);

            // Parse data
            while (std::getline(datumCSVStream, datum)) {

                // Clear whitespace
                datum = Strings::removeWhitespace(datum);

                // Get parts
                auto datumParts = Strings::split(datum, ',');

                // Check data
                if (datumParts.size() != 2) {
                    throw std::runtime_error("Invalid number of entries in PROJ4_datum csv file");
                } else {

                    // Add to table
                    datumTable[Strings::toUpper(datumParts[0])] = Strings::toUpper(datumParts[1]);
                }
            }

            // Build ellipsoids
            std::string ellipsoid;
            std::string ellipsoidCSV = std::string(R_PROJ4_ellipsoid_csv);
            std::istringstream ellipsoidCSVStream(ellipsoidCSV);

            // Skip header
            std::getline(ellipsoidCSVStream, ellipsoid);

            // Parse data
            while (std::getline(ellipsoidCSVStream, ellipsoid)) {

                // Clear whitespace
                ellipsoid = Strings::removeWhitespace(ellipsoid);

                // Get parts
                auto ellipsoidParts = Strings::split(ellipsoid, ',');

                // Check data
                if (ellipsoidParts.size() != 3) {
                    throw std::runtime_error("Invalid number of entries in PROJ4_ellipsoid csv file");
                } else {

                    // Check values
                    if (!Strings::isNumber(ellipsoidParts[1]) || !Strings::isNumber(ellipsoidParts[2])) {
                        throw std::runtime_error("Invalid numeric data in PROJ4_ellipsoid csv file");
                    }

                    // Add to table
                    std::vector<double> values = { 
                        (double)Strings::toNumber<double>(ellipsoidParts[1]),   // Equatorial radius
                        (double)Strings::toNumber<double>(ellipsoidParts[2]) }; // Inverse flattening
                    ellipsoidTable[Strings::toUpper(ellipsoidParts[0])] = values;
                }
            }

            // Set flag
            initialised = true;
        }
    }

    /**
    * Get datum ellipsoid from %ProjectionTable
    * @param datumName PROJ4 name of datum
    * @param ellipsoid refernce to pass ellipsoid name
    * @return true if datum is found, false otherwise
    */
    bool ProjectionTable::getDatum(std::string datumName, std::string &ellipsoidName) {

        // Ensure tables are initialised
        if (!initialised)
            initialise();

        // Get value
        auto itDatum = datumTable.find(Strings::toUpper(datumName));
        if (itDatum == datumTable.end())
            return false;

        // Populate value
        ellipsoidName = itDatum->second;

        return true;
    }

    /**
    * Get ellipsoid value from %ProjectionTable
    * @param ellipsoidName PROJ4 name of ellipsoid
    * @param equatorialRadius equatorial radius for ellipsoid
    * @param flattening flattening for ellipsoid
    * @return true if ellipsoid is found, false otherwise
    */
    bool ProjectionTable::getEllipsoid(std::string ellipsoidName, double &equatorialRadius, double &inverseFlattening) {

        // Ensure tables are initialised
        if (!initialised)
            initialise();

        // Get value
        auto itEllipsoid = ellipsoidTable.find(Strings::toUpper(ellipsoidName));
        if (itEllipsoid == ellipsoidTable.end())
            return false;

        // Populate values
        auto &values = itEllipsoid->second;
        equatorialRadius = values[0];
        inverseFlattening = values[1];

        return true;
    }

    /**
    * Get ellipsoid name from %ProjectionTable
    * @param equatorialRadius equatorial radius for ellipsoid
    * @param flattening flattening for ellipsoid
    * @return PROJ4 name of ellipsoid, blank if not found
    */
    template <typename CTYPE>
    std::string ProjectionTable::getEllipsoidName(CTYPE equatorialRadius, CTYPE flattening) {

        // Ensure tables are initialised
        if (!initialised)
            initialise();

        // Scan table
        for (auto eit : ellipsoidTable) {

            auto &values = eit.second;
            if (equatorialRadius == (CTYPE)values[0] && flattening == (CTYPE)(1.0/values[1]))
                return eit.first;
        }

        return std::string();
    }

    /**
    * Singleton instance of %ProjectionOperator.
    */
    ProjectionOperator &ProjectionOperator::instance() {

        static ProjectionOperator projectionOperator;
        return projectionOperator;
    }

    /**
    * %ProjectionOperator initialisation
    */
    void ProjectionOperator::initialise(Geostack::Solver &solver) {
    
        if (!initialised) {

            // Build program
            std::string script = 
                std::string(R_cl_projection_head_c) + 
                std::string(R_cl_projection_c);
            Solver::processScript(script);
            clProjectionHash = solver.buildProgram(script);
            if (clProjectionHash == solver.getNullHash())
                throw std::runtime_error("Cannot build program");
            
            // Set flag
            initialised = true;
        }
    }

    /**
    * %ProjectionOperator kernels
    * @return %ProjectionOperator kernel
    */
    cl::Kernel ProjectionOperator::getKernel(std::string kernel) {

        // Get solver handle
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        
        // Ensure program is built and kernel is initialised
        if (!initialised)
            initialise(solver);
        
        // Return kernel
        return solver.getKernel(clProjectionHash, kernel);
    }

    namespace ProjectionProcessing {
        
        // Ignored data
        const std::set<std::string> ignoreList = { "TOWGS84" };
        
        // Translations from OGC WKT to ESRI WKT style
        const std::map<std::string, std::string> translations = { 
            {"LATITUDE_OF_CENTER", "LATITUDE_OF_ORIGIN"}, 
            {"LONGITUDE_OF_CENTER", "CENTRAL_MERIDIAN"}
        };

        // Translation of specific parameters in WKT
        const std::map<std::pair<std::string, int>, std::string> parameterTranslations = { 
            { { "UNIT", 0 }, "CONVERSION_FACTOR" }, 
            { { "PRIMEM", 0 }, "LONGITUDE" }, 
            { { "SPHEROID", 0 }, "EQUATORIAL_RADIUS" }, 
            { { "SPHEROID", 1 }, "INVERSE_FLATTENING"}
        };

        /*
        * Clean string and return uppercase substring
        */
        std::string getFormattedSubString(std::string s, std::size_t start, std::size_t length) {

            // Clip and clean string
            std::string r = Strings::removeCharacter(s.substr(start, length), '"');

            // Convert to uppercase
            r = Strings::toUpper(r);

            return r;
        }

        /*
        * Check and get value from map
        */
        bool getMapValue(std::string name, double &v, std::map<std::string, std::string> &projMap) {

            // Find value
            auto it = projMap.find(Strings::toUpper(name));
            if (it == projMap.end())
                return false;

            // Check value
            std::string &val = it->second;
            if (!Strings::isNumber(val))
                return false;

            // Set value
            v = Strings::toNumber<double>(val);

            return true;
        }

        /*
        * Recursive parsing for WKT string, expects form of Name["Value", ...] 
        * @param string to parse
        * @return Json object
        */
        Json::object parseProjectionRecurse(std::string s) {

            // Skip blank strings
            if (!s.empty()) {

                // Break string
                unsigned level = 0;
                unsigned maxLevel = 0;
                std::vector<std::size_t> delPos;
                for (std::size_t pos = 0; pos < s.size(); pos++) {
                    if (s[pos] == '[') {
                        if (level == 0)
                            delPos.push_back(pos);
                        level++;
                    }
                    if (level == 1 && (s[pos] == ',' || s[pos] == ']'))
                        delPos.push_back(pos);
                    if (s[pos] == ']')
                        level--;
                }

                // Parse sub-strings
                if (delPos.size() == 2) {
                
                    // Get name and value
                    std::string name = getFormattedSubString(s, 0, delPos[0]);
                    std::string value = getFormattedSubString(s, delPos[0]+1, delPos[1]-(delPos[0]+1));

                    // Create object
                    if (Strings::isNumber(value)) {
                        return Json::object { { name, std::stod(value) } };
                    } else {
                        return Json::object { { name, value } };
                    }

                } else {

                    // Get name and value
                    std::string name = getFormattedSubString(s, 0, delPos[0]);
                    std::string value = getFormattedSubString(s, delPos[0]+1, delPos[1]-(delPos[0]+1));
                    
                    // Ignore certain OGC strings
                    if (ignoreList.find(name) != ignoreList.end()) 
                        return Json::object();

                    // Check for parameters
                    if (name == "PARAMETER" && delPos.size() == 3) {
                        std::string paramValue = getFormattedSubString(s, delPos[1]+1, delPos[2]-(delPos[1]+1));

                        // Specialised translations
                        auto it = translations.find(value);
                        if (it != translations.end())
                            value = it->second;

                        // Create object
                        if (Strings::isNumber(paramValue)) {
                            return Json::object { { value, std::stod(paramValue) } };
                        } else {
                            return Json::object { { value, paramValue } };
                        }
                    }

                    // Create object and array
                    Json::object jsonObj { { "NAME", value } };

                    // Recursively parse
                    unsigned param = 0;
                    for (std::size_t pos = 1; pos < delPos.size()-1; pos++) {

                        // Get substring
                        std::string ss = getFormattedSubString(s, delPos[pos]+1, delPos[pos+1]-(delPos[pos]+1));

                        // Search for groups in substring
                        if (ss.find('[') != std::string::npos) {
                            Json::object p = parseProjectionRecurse(ss);
                            jsonObj.insert(p.begin(), p.end());
                        } else {

                            // Search for special parameter translations
                            std::string parameterName = "PARAMETER" + std::to_string(param);
                            auto it = parameterTranslations.find( {name, param} );
                            if (it != parameterTranslations.end()) {
                                parameterName = it->second;
                            }

                            // Create object
                            if (Strings::isNumber(ss)) {
                                jsonObj[parameterName] = std::stod(ss);
                            } else {
                                jsonObj[parameterName] = ss;
                            }

                            // Increment parameter counter
                            param++;
                        }
                    }

                    return Json::object { { name , jsonObj } };
                }
            }

            return Json::object();
        }
    }
        
    /*
    * Parse OGC WKT string into %Projection object
    * @param WKT well-known text string
    * @return %Projection object
    */
    template <typename CTYPE>
    ProjectionParameters<CTYPE> Projection<CTYPE>::parseWKT(const std::string WKT) {
        
        // Parse projection string into tree
        if (WKT.length() > 0) {
        
            // Parse projection into searchable JSON object
            Json json;
            std::string cleanWKT = Geostack::Strings::removeWhitespace(WKT);
            json = ProjectionProcessing::parseProjectionRecurse(cleanWKT);
            if (json.object_items().size() == 0) {
                std::cout << "ERROR: Unknown projection type '" + json.string_value() + "'" << std::endl;
                return ProjectionParameters<CTYPE>();
            }

            // Create projection
            ProjectionParameters<CTYPE> p = ProjectionParameters<CTYPE>();
            auto jsonObj = json.object_items();
            std::string topLevelName = jsonObj.begin()->first;
            if (topLevelName == "GEOGCS") {
            
                // Check geographic coordinate system
                if (jsonObj["GEOGCS"]["DATUM"].is_null()) {
                    std::cout << "ERROR: Geographic coordinate system has no DATUM definition" << std::endl;
                    return ProjectionParameters<CTYPE>();
                }

                if (jsonObj["GEOGCS"]["DATUM"]["SPHEROID"].is_null()) {
                    std::cout << "ERROR: Geographic coordinate system has no SPHEROID definition" << std::endl;
                    return ProjectionParameters<CTYPE>();
                }
                
                // Get projection objects
                auto &spheriodObj = jsonObj["GEOGCS"]["DATUM"]["SPHEROID"];

                // Set projection parameters
                p.type = 2;
                p.cttype = 0;
                p.a = (CTYPE)spheriodObj["EQUATORIAL_RADIUS"].number_value();
                p.f = (CTYPE)(1.0/spheriodObj["INVERSE_FLATTENING"].number_value());

            } else if (topLevelName == "PROJCS") {

                // Check for geographic coordinate system
                auto GEOGCSjson = jsonObj["PROJCS"]["GEOGCS"];
                if (GEOGCSjson.is_null()) {
                    std::cout << "ERROR: Projected system goes not contain does not contain geographic projection" << std::endl;
                    return ProjectionParameters<CTYPE>();
                }

                // Check projected coordinate system
                auto projTypeName = jsonObj["PROJCS"]["PROJECTION"].string_value();

                // Lambert conformal conic
                if (projTypeName== "LAMBERT_CONFORMAL_CONIC_2SP" || projTypeName == "LAMBERT_CONFORMAL_CONIC") {

                    // Lambert conformal conic
                    std::vector<std::string> requiredParameters {
                        "CENTRAL_MERIDIAN",
                        "FALSE_EASTING",
                        "FALSE_NORTHING",
                        "LATITUDE_OF_ORIGIN",
                        "STANDARD_PARALLEL_1",
                        "STANDARD_PARALLEL_2",
                    };
                    
                    for (auto &r : requiredParameters) {
                        if (jsonObj["PROJCS"][r].is_null()) {
                            std::cout << "ERROR: Lambert conformal conic projection does not contain '" << r << "' parameter" << std::endl;
                            return ProjectionParameters<CTYPE>();
                        }
                    }

                    // Get projection objects
                    auto &projcsObj = jsonObj["PROJCS"];
                    auto &spheriodObj = projcsObj["GEOGCS"]["DATUM"]["SPHEROID"];

                    // Set projection parameters
                    p.type = 1;
                    p.cttype = 8;
                    p.a = (CTYPE)spheriodObj["EQUATORIAL_RADIUS"].number_value();
                    p.f = (CTYPE)(1.0/spheriodObj["INVERSE_FLATTENING"].number_value());
                    p.x0 = (CTYPE)projcsObj["CENTRAL_MERIDIAN"].number_value();
                    p.k0 = (CTYPE)0.0;
                    p.fe = (CTYPE)projcsObj["FALSE_EASTING"].number_value();
                    p.fn = (CTYPE)projcsObj["FALSE_NORTHING"].number_value(); 
                    p.phi_0 = (CTYPE)projcsObj["LATITUDE_OF_ORIGIN"].number_value();
                    p.phi_1 = (CTYPE)projcsObj["STANDARD_PARALLEL_1"].number_value();
                    p.phi_2 = (CTYPE)projcsObj["STANDARD_PARALLEL_2"].number_value();

                } else if (projTypeName== "TRANSVERSE_MERCATOR") {

                    // Transverse Mercator
                    std::vector<std::string> requiredParameters {
                        "CENTRAL_MERIDIAN",
                        "FALSE_EASTING",
                        "FALSE_NORTHING",
                        "LATITUDE_OF_ORIGIN",
                        "SCALE_FACTOR",
                    };
                    
                    for (auto &r : requiredParameters) {
                        if (jsonObj["PROJCS"][r].is_null()) {
                            std::cout << "ERROR: Transverse Mercator projection does not contain '" << r << "' parameter" << std::endl;
                            return ProjectionParameters<CTYPE>();
                        }
                    }
                    
                    // Get projection objects
                    auto &projcsObj = jsonObj["PROJCS"];
                    auto &spheriodObj = projcsObj["GEOGCS"]["DATUM"]["SPHEROID"];

                    // Set projection parameters
                    p.type = 1;
                    p.cttype = 1;
                    p.a = (CTYPE)spheriodObj["EQUATORIAL_RADIUS"].number_value();
                    p.f = (CTYPE)(1.0/spheriodObj["INVERSE_FLATTENING"].number_value());
                    p.x0 = (CTYPE)projcsObj["CENTRAL_MERIDIAN"].number_value();
                    p.k0 = (CTYPE)projcsObj["SCALE_FACTOR"].number_value();
                    p.fe = (CTYPE)projcsObj["FALSE_EASTING"].number_value();
                    p.fn = (CTYPE)projcsObj["FALSE_NORTHING"].number_value(); 
                    p.phi_0 = (CTYPE)projcsObj["LATITUDE_OF_ORIGIN"].number_value();

                } else if (projTypeName== "ALBERS_CONIC_EQUAL_AREA") {

                    // Albers conic
                    std::vector<std::string> requiredParameters {
                        "CENTRAL_MERIDIAN",
                        "FALSE_EASTING",
                        "FALSE_NORTHING",
                        "LATITUDE_OF_ORIGIN",
                        "STANDARD_PARALLEL_1",
                        "STANDARD_PARALLEL_2",
                    };
                    
                    for (auto &r : requiredParameters) {
                        if (jsonObj["PROJCS"][r].is_null()) {
                            std::cout << "ERROR: Albers conic projection does not contain '" << r << "' parameter" << std::endl;
                            return ProjectionParameters<CTYPE>();
                        }
                    }

                    // Get projection objects
                    auto &projcsObj = jsonObj["PROJCS"];
                    auto &spheriodObj = projcsObj["GEOGCS"]["DATUM"]["SPHEROID"];

                    // Set projection parameters
                    p.type = 1;
                    p.cttype = 11;
                    p.a = (CTYPE)spheriodObj["EQUATORIAL_RADIUS"].number_value();
                    p.f = (CTYPE)(1.0/spheriodObj["INVERSE_FLATTENING"].number_value());
                    p.x0 = (CTYPE)projcsObj["CENTRAL_MERIDIAN"].number_value();
                    p.k0 = (CTYPE)0.0;
                    p.fe = (CTYPE)projcsObj["FALSE_EASTING"].number_value();
                    p.fn = (CTYPE)projcsObj["FALSE_NORTHING"].number_value(); 
                    p.phi_0 = (CTYPE)projcsObj["LATITUDE_OF_ORIGIN"].number_value();
                    p.phi_1 = (CTYPE)projcsObj["STANDARD_PARALLEL_1"].number_value();
                    p.phi_2 = (CTYPE)projcsObj["STANDARD_PARALLEL_2"].number_value();

                } else if (projTypeName== "MERCATOR_1SP" || projTypeName== "MERCATOR") {

                    // Web mercator

                } else {
                    std::cout << "ERROR: Unknown projection type '" << projTypeName << "'" << std::endl;
                    return ProjectionParameters<CTYPE>();
                }

            } else {

                std::cout << "ERROR: Unknown projection type '" + json.string_value() + "'" << std::endl;
                return ProjectionParameters<CTYPE>();
            }
            
            // Return projection
            return p;

        }

        // Return empty projection
        return ProjectionParameters<CTYPE>();
    }
  
    /*
    * Parse PROJ4 string into %Projection object
    * @param PROJ4 string
    * @return %Projection object
    */
    template <typename CTYPE>
    ProjectionParameters<CTYPE> Projection<CTYPE>::parsePROJ4(const std::string PROJ4) {

        // Remove whitespace
        std::string cleanPROJ4 = Strings::removeWhitespace(PROJ4);

        // Split string into parts
        auto PROJ4params = Strings::split(cleanPROJ4, '+');
        if (PROJ4params.size() > 0) {

            // Create map
            std::map<std::string, std::string> projMap;
            for (auto param : PROJ4params) {

                auto part = Strings::split(param, '=');
                if (part.size() > 0)
                    projMap[Strings::toUpper(part[0])] = part.size() > 1 ? part[1] : "";
            }            
            
            // Get ellipsoid parameters
            double a, f;
            auto itDatum = projMap.find("DATUM");
            if (itDatum != projMap.end()) {

                // Search for ellipse
                auto datum = itDatum->second;
                std::string ellipsoid;
                if (ProjectionTable::instance().getDatum(datum, ellipsoid)) {

                    // Get ellipsoid parameters
                    double fi;
                    if (!ProjectionTable::instance().getEllipsoid(ellipsoid, a, fi)) {
                        std::cout << "ERROR: Ellipsoid '" << ellipsoid << "' not found in table" << std::endl;
                        return ProjectionParameters<CTYPE>();
                    }

                    // Set flattening
                    f = fi == 0.0 ? 1.0 : 1.0/fi;

                } else {
                    std::cout << "ERROR: No datum or ellipsoid found in PROJ4 string" << std::endl;
                    return ProjectionParameters<CTYPE>();
                }

            } else {

                // Search for ellipse
                auto itEllipse = projMap.find("ELLPS");
                if (itEllipse != projMap.end()) {

                    // Get ellipsoid parameters
                    double fi;
                    std::string ellipsoid = itEllipse->second;
                    if (!ProjectionTable::instance().getEllipsoid(ellipsoid, a, fi)) {
                        std::cout << "ERROR: Ellipsoid '" << ellipsoid << "' not found in table" << std::endl;
                        return ProjectionParameters<CTYPE>();
                    }

                    // Set flattening
                    f = fi == 0.0 ? 1.0 : 1.0/fi;

                } else {

                    // Get semimajor radius
                    if (!ProjectionProcessing::getMapValue("a", a, projMap)) {
                        std::cout << "ERROR: No datum, ellipsoid or radius found in PROJ4 string" << std::endl;
                        return ProjectionParameters<CTYPE>();
                    }
                    
                    // Get semiminor radius
                    double b;
                    if (ProjectionProcessing::getMapValue("b", b, projMap)) {

                        // Set flattening
                        f = (a-b)/a;

                    } else {

                        // Find flattening
                        if (!ProjectionProcessing::getMapValue("f", f, projMap)) {
                            std::cout << "ERROR: No semiminor radius or flattening found in PROJ4 string" << std::endl;
                            return ProjectionParameters<CTYPE>();
                        }
                    }
                }
            }

            // Parse projection types
            auto itProj = projMap.find("PROJ");
            if (itProj != projMap.end()) {
                    
                // Get projection
                auto projName = Strings::toUpper(itProj->second);

                // Parse projection types
                if (projName == "LONGLAT") {
                    
                    // Geographic projection
                    ProjectionParameters<CTYPE> p = ProjectionParameters<CTYPE>();
                    p.type = 2;
                    p.cttype = 0;
                    p.a = (CTYPE)a;
                    p.f = (CTYPE)f;

                    // Return projection
                    return p;

                } else if (projName == "UTM") {

                    // Universal transverse Mercator 
                    double zone;
                    if (!ProjectionProcessing::getMapValue("zone", zone, projMap)) {
                        std::cout << "ERROR: No zone found for Universal transverse Mercator in PROJ4 string '" << projName << "'" << std::endl;
                        return ProjectionParameters<CTYPE>();
                    }
                            
                    // Return projection
                    ProjectionParameters<CTYPE> p = ProjectionParameters<CTYPE>();
                    p.type = 1;
                    p.cttype = 1;
                    p.a = (CTYPE)a;
                    p.f = (CTYPE)f;
                    p.x0 = (CTYPE)(-183.0+zone*6.0);
                    p.k0 = (CTYPE)0.9996;
                    p.fe = (CTYPE)500000;
                    p.fn = projMap.find("SOUTH") != projMap.end() ? (CTYPE)10000000.0 : (CTYPE)0.0;

                    // Return projection
                    return p;

                } else if (projName == "MERC") {

                    // Mercator
                    double x0, fe, fn, k;
                    bool success = true;
                    success &= ProjectionProcessing::getMapValue("lon_0", x0, projMap);
                    success &= ProjectionProcessing::getMapValue("x_0", fe, projMap);
                    success &= ProjectionProcessing::getMapValue("y_0", fn, projMap);
                    if (!ProjectionProcessing::getMapValue("k_0", k, projMap))
                        success &= (ProjectionProcessing::getMapValue("k", k, projMap));
                    if (!success) {
                        std::cout << "ERROR: Insufficient parameters for Mercator PROJ4 type '" << projName << "'" << std::endl;
                        return ProjectionParameters<CTYPE>();
                    }
                            
                    // Return projection
                    ProjectionParameters<CTYPE> p = ProjectionParameters<CTYPE>();
                    p.type = 1;
                    p.cttype = 7;
                    p.a = (CTYPE)a;
                    p.f = (CTYPE)f;
                    p.x0 = (CTYPE)x0;
                    p.k0 = (CTYPE)k;
                    p.fe = (CTYPE)fe;
                    p.fn = (CTYPE)fn; 

                    // Return projection
                    return p;

                } else if (projName == "LCC") {
                        
                    // Lambert conformal conic
                    double x0, fe, fn, phi_0, phi_1, phi_2;
                    bool success = true;
                    success &= ProjectionProcessing::getMapValue("lon_0", x0, projMap);
                    success &= ProjectionProcessing::getMapValue("x_0", fe, projMap);
                    success &= ProjectionProcessing::getMapValue("y_0", fn, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_0", phi_0, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_1", phi_1, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_2", phi_2, projMap);
                    if (!success) {
                        std::cout << "ERROR: Insufficient parameters for Lambert conformal conic PROJ4 type '" << projName << "'" << std::endl;
                        return ProjectionParameters<CTYPE>();
                    }

                    // Return projection
                    ProjectionParameters<CTYPE> p = ProjectionParameters<CTYPE>();
                    p.type = 1;
                    p.cttype = 8;
                    p.a = (CTYPE)a;
                    p.f = (CTYPE)f;
                    p.x0 = (CTYPE)x0;
                    p.k0 = (CTYPE)0.0;
                    p.fe = (CTYPE)fe;
                    p.fn = (CTYPE)fn; 
                    p.phi_0 = (CTYPE)phi_0;
                    p.phi_1 = (CTYPE)phi_1;
                    p.phi_2 = (CTYPE)phi_2;
                    return p;

                } else if (projName == "AEA") {

                    // Albers conic
                    double x0, fe, fn, phi_0, phi_1, phi_2;
                    bool success = true;
                    success &= ProjectionProcessing::getMapValue("lon_0", x0, projMap);
                    success &= ProjectionProcessing::getMapValue("x_0", fe, projMap);
                    success &= ProjectionProcessing::getMapValue("y_0", fn, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_0", phi_0, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_1", phi_1, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_2", phi_2, projMap);
                    if (!success) {
                        std::cout << "ERROR: Insufficient parameters for Albers conic PROJ4 type '" << projName << "'" << std::endl;
                        return ProjectionParameters<CTYPE>();
                    }

                    // Return projection
                    ProjectionParameters<CTYPE> p = ProjectionParameters<CTYPE>();
                    p.type = 1;
                    p.cttype = 11;
                    p.a = (CTYPE)a;
                    p.f = (CTYPE)f;
                    p.x0 = (CTYPE)x0;
                    p.k0 = (CTYPE)0.0;
                    p.fe = (CTYPE)fe;
                    p.fn = (CTYPE)fn; 
                    p.phi_0 = (CTYPE)phi_0;
                    p.phi_1 = (CTYPE)phi_1;
                    p.phi_2 = (CTYPE)phi_2;
                    return p;

                } else {
                    std::cout << "ERROR: Projection type '" << projName << "' is not currently supported" << std::endl;
                    return ProjectionParameters<CTYPE>();
                }

            } else {
                std::cout << "ERROR: No projection name found in PROJ4 string '" << PROJ4 << "'" << std::endl;
                return ProjectionParameters<CTYPE>();
            }

        } else {
            std::cout << "ERROR: No parameters found in PROJ4 string '" << PROJ4 << "'" << std::endl;
            return ProjectionParameters<CTYPE>();
        }
    }
    
    /*
    * Convert %Projection object into PROJ4 string
    * @param %Projection object
    * @return PROJ4 string
    */
    template <typename CTYPE>
    std::string Projection<CTYPE>::toPROJ4(const ProjectionParameters<CTYPE> proj) {

        // Create output stream
        std::ostringstream projStr;
        projStr.precision(10);

        // Add projection type
        switch (proj.type) {
            case 1:
                switch (proj.cttype) {

                    case 1: {
                        projStr << "+proj=utm";
                        int zone = (int)((proj.x0+183.0)/6.0);
                        projStr << " +zone=" << zone;
                        if (proj.fn > 0.0) projStr << " +south";
                        } break;

                    case 7:
                        projStr << "+proj=merc";
                        projStr << " +lon_0=" << proj.x0;
                        projStr << " +x_0=" << proj.fe;
                        projStr << " +y_0=" << proj.fn;
                        projStr << " +k=" << proj.k0;
                        break;

                    case 8:
                        projStr << "+proj=lcc";
                        projStr << " +lon_0=" << proj.x0;
                        projStr << " +x_0=" << proj.fe;
                        projStr << " +y_0=" << proj.fn;
                        projStr << " +lat_0=" << proj.phi_0;
                        projStr << " +lat_1=" << proj.phi_1;
                        projStr << " +lat_2=" << proj.phi_2;
                        break;

                    case 11:
                        projStr << "+proj=aea";
                        projStr << " +lon_0=" << proj.x0;
                        projStr << " +x_0=" << proj.fe;
                        projStr << " +y_0=" << proj.fn;
                        projStr << " +lat_0=" << proj.phi_0;
                        projStr << " +lat_1=" << proj.phi_1;
                        projStr << " +lat_2=" << proj.phi_2;
                        break;

                    default:
                        std::cout << "ERROR: projection type not supported" << std::endl;
                        return std::string();
                }
                break;

            case 2:
                projStr << "+proj=longlat";
                break;

            default:
                std::cout << "ERROR: projection has no type" << std::endl;
                return std::string();
        }

        // Scan for ellipsoid
        auto ellipsoidName = ProjectionTable::instance().getEllipsoidName<CTYPE>(proj.a, proj.f);
        if (ellipsoidName.size() != 0) {
            projStr << " +ellps=" + ellipsoidName;
        } else {
            projStr << " +a=" << proj.a;
            projStr << " +b=" << proj.a*(1-proj.f);
        }

        // Add no default
        projStr << " +no_defs";

        return projStr.str();
    }

    /**
    * Convert single %Coordinate between projections
    * @param c @Coordinate.
    * @param to projection to convert @Coordinate to.
    * @param from projection of the @Coordinate.
    */
    template <typename CTYPE>
    bool Projection<CTYPE>::convert(Coordinate<CTYPE> &c, ProjectionParameters<CTYPE> to, ProjectionParameters<CTYPE> from) {
    
        // Check for same projection
        if (to == from)
            return true;

        // Projected to geographical transformation
        if (from.type == 0) {
        
            std::cout << "ERROR: No projection type set to convert from" << std::endl;
            return false;
            
        } else if (from.type == 1) {
        
            switch (from.cttype) {

                case 1:

                    // Transverse Mercator
                    c = ProjectionCalculation::fn_Transverse_Mercator_to_Ellipsoid<CTYPE>(c, from);
                    break;

                case 7:

                    // Mercator
                    c = ProjectionCalculation::fn_Mercator_to_Ellipsoid<CTYPE>(c, from);
                    break;

                case 8:

                    // Lambert conformal conic (2 point)
                    c = ProjectionCalculation::fn_Lambert_to_Ellipsoid<CTYPE>(c, from);
                    break;

                case 11:

                    // Albers equal area 
                    c = ProjectionCalculation::fn_Albers_to_Ellipsoid<CTYPE>(c, from);
                    break;

                default:

                    // Unsupported transform 
                    std::cout << "ERROR: Unsupported projection transform '" << from.cttype << "'" << std::endl;
                    return false;
                    
            }

        } else if (from.type == 2) {

            // No operation
        
        } else {

            std::cout << "ERROR: Unsupported projection type '" << from.type << "'" << std::endl;
            return false;
        }
        
        // Geographical to projected transformation
        if (to.type == 0) {
        
            std::cout << "ERROR: No projection type set to convert to" << std::endl;
            return false;
            
        } else if (to.type == 1) {
        
            switch (to.cttype) {

                case 1:

                    // Transverse Mercator
                    c = ProjectionCalculation::fn_Ellipsoid_to_Transverse_Mercator<CTYPE>(c, to);
                    break;

                case 7:

                    // Mercator
                    c = ProjectionCalculation::fn_Ellipsoid_to_Mercator<CTYPE>(c, to);
                    break;

                case 8:

                    // Lambert conformal conic (2 point)
                    c = ProjectionCalculation::fn_Ellipsoid_to_Lambert<CTYPE>(c, to);
                    break;

                case 11:

                    // Albers equal area 
                    c = ProjectionCalculation::fn_Ellipsoid_to_Albers<CTYPE>(c, to);
                    break;

                default:

                    // Unsupported transform 
                    std::cout << "ERROR: Unsupported projection transform '" << to.cttype << "'" << std::endl;
                    return false;
                    
            }

        } else if (to.type == 2) {

            // No operation
        
        } else {

            std::cout << "ERROR: Unsupported projection type '" << to.type << "'" << std::endl;
            return false;
        }

        return true;
    }

    /**
    * Convert multiple %Coordinates between projections
    * @param c @Coordinate.
    * @param to projection to convert @Coordinate to.
    * @param from projection of the @Coordinate.
    */
    template <typename CTYPE>
    bool Projection<CTYPE>::convert(std::vector<Coordinate<CTYPE> > &c, ProjectionParameters<CTYPE> to, ProjectionParameters<CTYPE> from) {

        // Create VertexCollection
        VertexCollection<CTYPE> v;

        // Copy data to VertexCollection
        v.getCoordinates() = c;

        // Convert projection
        if (Projection<CTYPE>::convert(v, to, from)) {

            // Copy data from VertexCollection
            c = v.getCoordinates();
            return true;
        }
        return false;
    }

    /**
    * Convert %VertexCollection between projections
    * @param v @VertexCollection.
    * @param to projection to convert @Coordinate to.
    * @param from projection of the @Coordinate.
    */
    template <typename CTYPE>
    bool Projection<CTYPE>::convert(VertexCollection<CTYPE> &v, ProjectionParameters<CTYPE> to, ProjectionParameters<CTYPE> from) {
    
        // Check for same projection
        if (to == from)
            return true;
        
        // Get OpenCL solver handle
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            try {

                // Create buffer
                cl_int err = CL_SUCCESS;
            
                // Create local OpenCL variables
                cl::Kernel projectionToEllipseKernel, projectionFromEllipseKernel;
                size_t clWorkgroupSize, clPaddedWorkgroupSize;

                // Projected to geographical transformation
                if (from.type == 0) {
        
                    std::cout << "ERROR: No projection type set to convert from" << std::endl;
                    return false;
            
                } if (from.type == 1) {
        
                    switch (from.cttype) {

                        case 1:
                        
                            // Transverse Mercator
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Transverse_Mercator_to_Ellipsoid");
                            break;

                        case 7:
                        
                            // Mercator
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Mercator_to_Ellipsoid");
                            break;

                        case 8:

                            // Lambert conformal conic (2 point)
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Lambert_to_Ellipsoid");
                            break;

                        case 11:

                            // Albers equal area 
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Albers_to_Ellipsoid");
                            break;

                        default:

                            // Unsupported transform 
                            std::cout << "ERROR: Unsupported projection transform '" << from.cttype << "'" << std::endl;
                            return false;
                    
                    }

                    // Set up kernel
                    projectionToEllipseKernel.setArg(0, v.getVertexBuffer());
                    projectionToEllipseKernel.setArg(1, (cl_uint)v.size());
                    projectionToEllipseKernel.setArg(2, from);

                    // Execute kernel
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(projectionToEllipseKernel);
                    clPaddedWorkgroupSize = (size_t)(v.size()+((clWorkgroupSize-(v.size()%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(projectionToEllipseKernel, cl::NullRange, 
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                } else if (from.type == 2) {

                    // No operation
        
                } else {

                    std::cout << "ERROR: Unsupported projection type '" << from.type << "'" << std::endl;
                    return false;
                }
        
                // Geographical to projected transformation
                if (to.type == 0) {
        
                    std::cout << "ERROR: No projection type set to convert to" << std::endl;
                    return false;
            
                } else if (to.type == 1) {
        
                    switch (to.cttype) {

                        case 1:

                            // Transverse Mercator
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Ellipsoid_to_Transverse_Mercator");
                            break;

                        case 7:

                            // Mercator
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Ellipsoid_to_Mercator");
                            break;

                        case 8:

                            // Lambert conformal conic (2 point)
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Ellipsoid_to_Lambert");
                            break;

                        case 11:

                            // Albers equal area 
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Ellipsoid_to_Albers");
                            break;

                        default:

                            // Unsupported transform 
                            std::cout << "ERROR: Unsupported projection transform '" << to.cttype << "'" << std::endl;
                            return false;
                    
                    }

                    // Set up kernel
                    projectionToEllipseKernel.setArg(0, v.getVertexBuffer());
                    projectionToEllipseKernel.setArg(1, (cl_uint)v.size());
                    projectionToEllipseKernel.setArg(2, to);

                    // Execute kernel
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(projectionToEllipseKernel);
                    clPaddedWorkgroupSize = (size_t)(v.size()+((clWorkgroupSize-(v.size()%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(projectionToEllipseKernel, cl::NullRange, 
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                } else if (to.type == 2) {

                    // No operation
        
                } else {

                    std::cout << "ERROR: Unsupported projection type '" << to.type << "'" << std::endl;
                    return false;
                }

            } catch (cl::Error& e) {
                std::cout << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
                return false;
            }
        }

        return true;
    }

    
    /**
    * Check whether projection type is supported.
    * @param proj projection to check.
    */    
    template <typename CTYPE>
    bool Projection<CTYPE>::checkProjection(const ProjectionParameters<CTYPE> &proj) {


        // Projected type
        if (proj.type == 1 && (
            proj.cttype == 1 || 
            proj.cttype == 7 || 
            proj.cttype == 8 || 
            proj.cttype == 11)) {
            return true;
        }

        // Geographic type
        if (proj.type == 2 && proj.cttype == 0) {
            return true;
        }

        // Invalid projection
        return false;
    }

    /**
    * %ProjectionParameters equal operator
    * @param l left-hand projection to compare.
    * @param r right-hand projection to compare.
    */
    template <typename CTYPE>
    bool operator==(const ProjectionParameters<CTYPE> &l, const ProjectionParameters<CTYPE> &r) { 

        // Compare objects
        if (&l == &r)
            return true;

        if (l.type == r.type &&
            l.cttype == r.cttype &&
            l.a == r.a &&
            l.f == r.f &&
            l.x0 == r.x0 &&
            l.k0 == r.k0 &&
            l.fe == r.fe &&
            l.fn == r.fn &&
            l.phi_0 == r.phi_0 &&
            l.phi_1 == r.phi_1 &&
            l.phi_2 == r.phi_2)
            return true;

        return false; 
    }

    /**
    * %ProjectionParameters not equal operator
    * @param l left-hand projection to compare.
    * @param r right-hand projection to compare.
    */
    template <typename CTYPE>
    bool operator!=(const ProjectionParameters<CTYPE> &l, const ProjectionParameters<CTYPE> &r) { 
        return !operator==(l, r);
    }

    // CTYPE float definitions
    template class Projection<float>;
    template bool operator==(const ProjectionParameters<float> &, const ProjectionParameters<float> &);
    template bool operator!=(const ProjectionParameters<float> &, const ProjectionParameters<float> &);

    // CTYPE double definitions
    template class Projection<double>;
    template bool operator==(const ProjectionParameters<double> &, const ProjectionParameters<double> &);
    template bool operator!=(const ProjectionParameters<double> &, const ProjectionParameters<double> &);
}
