/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
// Unaligned access
#define _val2D(R, i, j) (*((R)+(i)+(j)*_dim##R.mx))
#define _val3D(R, i, j, k) (*((R)+(i)+((j)+(k)*_dim##R.my)*_dim##R.mx))

// Aligned access
#define _val2D_a(R, i, j) (*((R)+(i)+(j)*_dim.mx))
#define _val3D_a(R, i, j, k) (*((R)+(i)+((j)+(k)*_dim.my)*_dim.mx))

typedef struct DimensionsStruct {

    uint nx; ///< Number of cells in x dimension
    uint ny; ///< Number of cells in y dimension
    uint nz; ///< Number of cells in z dimension
    REAL hx; ///< Spacing in x dimension
    REAL hy; ///< Spacing in y dimension
    REAL hz; ///< Spacing in z dimension
    REAL ox; ///< Start coordinate in x dimension
    REAL oy; ///< Start coordinate in y dimension
    REAL oz; ///< Start coordinate in z dimension
    uint mx; ///< Number of cells stored in memory for x dimension
    uint my; ///< Number of cells stored in memory for y dimension

} __attribute__ ((aligned (8))) Dimensions;

/**
* Map distance from point to raster.
* @param _u %Tile data.
* @param _dim_u %Dimensions structure.
* @param _c Array of coordinates.
* @param _r Point width array.
* @param _n Number of coordinates.
*/
__kernel void map2DPointDistance(
    __global REAL *_u,
    const Dimensions _dim_u,
    __global REALVEC4 *_c, 
    __global REAL *_r,
    const uint _n/*__ARGS__*/
) {

    const size_t i = get_global_id(0);
    const size_t j = get_global_id(1);

    // Check limits
    if (i >= _dim_u.nx || j >= _dim_u.ny) {
        _val2D(_u, i, j) = NAN;
        return;
    }
    
    // Cell centred position
    const REALVEC2 p = (REALVEC2)(
        ((REAL)i+0.5)*_dim_u.hx+_dim_u.ox, 
        ((REAL)j+0.5)*_dim_u.hy+_dim_u.oy);

/*__PROJECT__

    // Convert to vector projection
    convert(&p, _vproj, _rproj);

__PROJECT__*/
        
    // Read initial value
    REAL umin = _val2D(_u, i, j);

    // Replace nodata with large value
    if (umin != umin)
        umin = MAXFLOAT;
            
    // Loop over points
    for (uint k = 0; k < _n; k++) {

        // Calculate displacement vector
        REALVEC2 d = p-_c[k].xy;

        // Update minimum
        umin = fmin(umin, length(d)-_r[0]);

        // TODO distance is a very rough approximation for GCS, user Haversine great circle formula
    }

/*__PROJECT__

    // If the one projection is a GCS and the other is a PCS
    // *approximate* the distance using arc formula with mean earth radius.
    if (_vproj.type == 1 && _rproj.type == 2) {
        umin = degrees(3.0*umin/((3.0-_rproj.f)*_rproj.a));
    } else if (_vproj.type == 2 && _rproj.type == 1) {
        umin = _vproj.a*radians(umin)*(3.0-_vproj.f)/3.0;
    }

__PROJECT__*/

    // Write to raster
    _val2D(_u, i, j) = umin;
}

/**
* Map distance from line strings to raster.
* @param _u %Tile data.
* @param _dim_u %Dimensions structure.
* @param _c Array of coordinates.
* @param _r Line width array.
* @param _o Array of offsets denoting the start of each line string.
* @param _n Length of offset array.
*/
__kernel void map2DLineStringDistance(
    __global REAL *_u,
    const Dimensions _dim_u,
    __global REALVEC4 *_c, 
    __global REAL *_r,
    __global uint *_o,
    const uint _n/*__ARGS__*/
) {
       
    const size_t i = get_global_id(0);
    const size_t j = get_global_id(1);

    // Check limits
    if (i >= _dim_u.nx || j >= _dim_u.ny) {
        _val2D(_u, i, j) = NAN;
        return;
    }
    
    // Cell centred position
    const REALVEC2 p = (REALVEC2)(
        ((REAL)i+0.5)*_dim_u.hx+_dim_u.ox, 
        ((REAL)j+0.5)*_dim_u.hy+_dim_u.oy);

/*__PROJECT__

    // Convert to vector projection
    convert(&p, _vproj, _rproj);

__PROJECT__*/

    // Read initial value
    REAL umin = _val2D(_u, i, j);
        
    // Replace nodata with large value
    if (umin != umin)
        umin = MAXFLOAT;
        
    // Loop over lines
    uint lastoff = 0; 
    for (uint line = 0; line < _n; line++) {

        for (uint k = lastoff; k < _o[line]; k++) {

            // Get segment
            REALVEC2 l = _c[k+1].xy-_c[k].xy;
            REALVEC2 v = p-_c[k].xy;

            // Calculate Gram-Schmidt projection
            REAL dlv = dot(l, v);
            REAL dll = dot(l, l);
            if (dlv < 0.0 || dlv > dll) {

                // Projection onto line is outside line segment, use end points
                umin = fmin(umin, length(v)-_r[line]);
                v = p-_c[k+1].xy;
                umin = fmin(umin, length(v)-_r[line]);

                // TODO distance is a very rough approximation for GCS, user Haversine great circle formula

            } else {

                // Projection onto line is inside line segment, use distance to line
                REALVEC2 d = v-(dlv/dll)*l;
                umin = fmin(umin, length(d)-_r[line]);

                // TODO distance is a very rough approximation for GCS, user Haversine great circle formula
            }
        }

        // Update last offset
        lastoff = _o[line]+1;
    }

/*__PROJECT__

    // If the one projection is a GCS and the other is a PCS
    // *approximate* the distance using arc formula with mean earth radius.
    if (_vproj.type == 1 && _rproj.type == 2) {
        umin = degrees(3.0*umin/((3.0-_rproj.f)*_rproj.a));
    } else if (_vproj.type == 2 && _rproj.type == 1) {
        umin = _vproj.a*radians(umin)*(3.0-_vproj.f)/3.0;
    }

__PROJECT__*/

    // Write to raster
    _val2D(_u, i, j) = umin;
}

/**
* Map distance from polygons to raster.
* @param _u %Tile data.
* @param _dim_u %Dimensions structure.
* @param _c Array of coordinates.
* @param _o Array of offsets denoting the start of each polygon.
* @param _b Array of polygon bounding boxes as coordinates.
* @param _n Length of offset array.
*/
__kernel void map2DPolygonDistance(
    __global REAL *_u,
    const Dimensions _dim_u,
    __global REALVEC4 *_c,
    __global uint *_o,
    __global REALVEC4 *_b,
    const uint _n/*__ARGS__*/
) {
      
    const size_t i = get_global_id(0);
    const size_t j = get_global_id(1);

    // Check limits
    if (i >= _dim_u.nx || j >= _dim_u.ny) {
        _val2D(_u, i, j) = NAN;
        return;
    }
        
    // Cell centred position
    const REALVEC2 p = (REALVEC2)(
        ((REAL)i+0.5)*_dim_u.hx+_dim_u.ox, 
        ((REAL)j+0.5)*_dim_u.hy+_dim_u.oy);

/*__PROJECT__

    // Convert to vector projection
    convert(&p, _vproj, _rproj);

__PROJECT__*/

    // Read initial value
    REAL umin = _val2D(_u, i, j);
        
    // Replace nodata with large value
    REAL sign = 1.0;
    if (umin != umin) {
        umin = MAXFLOAT;
    } else {
        sign = umin < 0.0 ? -1.0 : 1.0;
        umin = umin*umin;
    }
        
    // Loop over polygons
    uint lastoff = 0;  
    for (uint poly = 0; poly < _n; poly++) {
        
        // Process if point is within polygon bounding box, 
        // or minimum distance to bounding box is less than current distance
        REALVEC2 c = p-clamp(p, _b[poly*2].lo, _b[poly*2+1].lo);
        if (dot(c, c) < umin) {

            // Map polygon
            int winding = 0;
            for (uint k = lastoff; k < _o[poly]; k++) {

                // Get segment
                REALVEC2 l = _c[k+1].xy-_c[k].xy;
                REALVEC2 v = p-_c[k].xy;

                // Check winding (http://geomalgorithms.com/a03-_inclusion.html)
                if (_c[k].y <= p.y) {
                    if (_c[k+1].y > p.y && (l.x*v.y-v.x*l.y) > 0.0)
                        winding++;
                } else {
                    if (_c[k+1].y <= p.y && (l.x*v.y-v.x*l.y) < 0.0)
                        winding--;
                }

                // Calculate Gram-Schmidt projection
                REAL dlv = dot(l, v);
                REAL dll = dot(l, l);
                if (dlv < 0.0 || dlv > dll) {

                    // Projection onto line is outside line segment, use end points
                    umin = fmin(umin, dot(v, v));
                    v = p-_c[k+1].xy;
                    umin = fmin(umin, dot(v, v));

                    // TODO distance is a very rough approximation for GCS, user Haversine great circle formula

                } else {

                    // Projection onto line is inside line segment, use distance to line
                    REALVEC2 d = v-(dlv/dll)*l;
                    umin = fmin(umin, dot(d, d));

                    // TODO distance is a very rough approximation for GCS, user Haversine great circle formula
                }
            }

            // If the point is inside any polygon, flip the sign
            if (winding != 0)
                sign = -sign;
        }

        // Update last offset
        lastoff = _o[poly]+1;
    }

/*__PROJECT__

    // If the one projection is a GCS and the other is a PCS
    // *approximate* the distance using arc formula with mean earth radius.
    if (_vproj.type == 1 && _rproj.type == 2) {
        umin = degrees(3.0*umin/((3.0-_rproj.f)*_rproj.a));
    } else if (_vproj.type == 2 && _rproj.type == 1) {
        umin = _vproj.a*radians(umin)*(3.0-_vproj.f)/3.0;
    }

__PROJECT__*/

    // Write to raster
    _val2D(_u, i, j) = sign*sqrt(umin);
}
