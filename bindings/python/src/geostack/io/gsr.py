# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import numpy as np
from ._cy_gsr import cyGsr_d_d, cyGsr_f_f, cyGsr_d_i, cyGsr_f_i
from .. import raster


class GsrHandler:
    def __init__(self, dtype=np.float32):
        self._handle = None
        self.dtype = dtype
        if self.dtype == np.float64:
            self._handle = cyGsr_d_d()
        elif self.dtype == np.float32:
            self._handle = cyGsr_f_f()

##read gsr to a raster
# @file_name: name of the file (string type) to be read
# @dtype: optional argument, data type of raster class instance
#
    def read(self, fileName, input_raster=None):
        '''write gsr to raster file

        Parameters
        ----------
        file_name: str
            Name of the file to be read.
        dtype: np.float32/np.float64 (optional)
            Type of raster instance

        Returns
        -------
        out: Raster object
            An instance of raster class

        Examples
        --------
        >>> out = GsrHandler()
        >>> out.read("testRasterA.gsr")
        '''
        if input_raster is None:
            self._raster_handler = raster.Raster(base_type=self.dtype)
        else:
            if isinstance(input_raster, raster.Raster):
                if input_raster.base_type != self.dtype:
                    raise TypeError("Type mismatch between input raster and GsrHandler")
                self._raster_handler = input_raster
            else:
                raise TypeError("input_raster should be an instance of raster.Raster")

        out = self._handle.read(fileName, self._raster_handler._handle)
        if not out:
            raise IOError("Unable to read file")
        return out

##write raster to gsr file
# @this: an instance of raster class
# @file_name: name of the file (string type) to be written
#
    def write(self, fileName, jsonConfig, input_raster=None):
        '''write raster to gsr file

        Parameters
        ----------
        this: Raster object
            An instance of raster class to be written out
        file_name: str
            Name of the file for writing raster

        Returns
        -------
        out: integer value

        Examples
        --------
        >>> this = Raster("testRasterA", np.float32)
        >>> this.init(nx=5, ny=5, hx=1.0, hy=1.0)
        >>> file_name = "testRasterA.gsr"
        >>> gsr = GsrHandler(input_raster=this)
        >>> gsr.write(file_name)
        '''
        if input_raster is not None:
            if not isinstance(input_raster, raster.Raster):
                raise TypeError("input_raster should be an instance of raster.Raster")
            out = self._handle.write(fileName, input_raster._handle, jsonConfig)
        else:
            if hasattr(self, "_raster_handler"):
                out = self._handle.write(fileName, self.raster._handle,
                        jsonConfig)
            else:
                raise RuntimeError("Need input_raster or a raster object in GsrHandler")
        if not out:
            raise IOError("Unable to write file")
        return out

    @property
    def raster(self):
        if hasattr(self, "_raster_handler"):
            return self._raster_handler

    @raster.setter
    def raster(self, input_raster):
        if not isinstance(input_raster, raster.Raster):
            raise TypeError("input_raster should be an instance of raster.Raster")
        if self.dtype != input_raster.base_type:
            raise TypeError("Mismatch between data type of GsrHandler and input_raster")
        self._raster_handler = input_raster

    @staticmethod
    def to_raster(fileName, dtype=np.float32):
        """
        Examples
        --------
        >>> test = Raster()
        >>> GsrHandler.to_raster(test, "testRasterA.gsr")
        """
        gsr_handler = GsrHandler(dtype=dtype)
        out = gsr_handler.read(fileName)
        return gsr_handler

    @staticmethod
    def from_raster(output_raster, fileName, jsonConfig):
        """
        Examples
        --------
        >>> test = Raster(name="testRasterA")
        >>> test.init(nx=5, ny=5, hx=1.0, hy=1.0)
        >>> file_name = "testRasterA.gsr"
        >>> GsrHandler.from_raster(test, file_name, "")
        """
        if isinstance(output_raster, raster.Raster):
            gsr_handler = GsrHandler(dtype=output_raster.base_type)
            out = gsr_handler.write(fileName, jsonConfig,
                    input_raster=output_raster)
            return out
        else:
            raise TypeError("output_raster should be an instance of raster.Raster class")

    @staticmethod
    def readGsrFile(fileName, dtype=np.float32):
        """
        Examples
        --------
        >>> file_name = "testRasterA.gsr"
        >>> out = GsrHandler.readGsrFile(file_name)
        """
        gsr_handler = GsrHandler(dtype=dtype)
        out = gsr_handler.read(fileName)
        return gsr_handler

    def __repr__(self):
        return self.__class__.__name__
