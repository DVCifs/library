/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <fstream>
#include <cstring>

#include "miniz.h"

#include "gs_geojson.h"

using namespace json11;

namespace Geostack
{
    /**
    * GeoJSON parser.
    * Converts a GeoJSON string to a %Vector object
    * @param geoJsonStr GeoJSON string.
    * @return Geostack %Vector object.
    */
    template <typename T>
    Geostack::Vector<T> GeoJson<T>::geoJsonFileToVector(std::string geoJsonFile) {

        // Create return Vector
        Vector<T> v;

        std::ifstream f(geoJsonFile, std::ios::in | std::ios::binary);
        if (f) {

            // Read first character
            char c;
            f.get(c);

            if (c == '{') {

                // Read file into string
                std::string geoJsonStr;
                f.seekg(0, std::ios::end);
                geoJsonStr.resize(f.tellg());
                f.seekg(0, std::ios::beg);
                f.read(&geoJsonStr[0], geoJsonStr.size());
                f.close();

                // Parse string
                if (!parseString(v, geoJsonStr))
                    v.clear();

            } else {

                // Try and open as zip file
                mz_zip_archive zGeoJson;
                std::memset(&zGeoJson, 0, sizeof(zGeoJson));
                if (mz_zip_reader_init_file(&zGeoJson, geoJsonFile.c_str(), 0)) {

                    // Loop through files
                    std::size_t size;
                    for (mz_uint i = 0; i < (mz_uint)mz_zip_reader_get_num_files(&zGeoJson); i++) {

                        mz_zip_archive_file_stat zStat;
                        if (mz_zip_reader_file_stat(&zGeoJson, i, &zStat)) {

                            // Skip directory entries
                            if (!mz_zip_reader_is_file_a_directory(&zGeoJson, i)) {

                                // Read data
                                void *data = mz_zip_reader_extract_file_to_heap(&zGeoJson, zStat.m_filename, &size, 0);

                                // Copy to string
                                std::string geoJsonStr;
                                geoJsonStr.resize(size);
                                std::memcpy(&geoJsonStr[0], data, size);

                                // Free data
                                mz_free(data);

                                // Check data and uncompressed size
                                if (data && zStat.m_uncomp_size == size) {

                                    // Parse string
                                    if (!parseString(v, geoJsonStr))
                                        v.clear();

                                } else {
                                    std::cout << "ERROR: zip uncompressioned failed" << std::endl;
                                }
                            }
                        }
                    }

                }
            }

        } else {
            std::cout << "ERROR: cannot open '" << geoJsonFile << "'" << std::endl;
        }

        // Set projection
        setProjection(v);
        
        // Return Vector
        return v;
    }

    /**
    * GeoJSON parser.
    * Converts a GeoJSON string to a %Vector object
    * @param geoJsonStr GeoJSON string.
    * @return Geostack %Vector object.
    */
    template <typename T>
    Geostack::Vector<T> GeoJson<T>::geoJsonToVector(std::string geoJsonStr) {

        // Create return Vector
        Vector<T> v;

        // Parse string
        if (!parseString(v, geoJsonStr)) {

            // Reset vector
            v.clear();

        } else {

            // Set projection
            setProjection(v);
        }
        
        // Return Vector
        return v;
    }
    
    /**
    * GeoJSON coordinate handler.
    * Converts %PropertyHandler into GeoJSON properties.
    * @param p %PropertyHandler value.
    * @return GeoJSON properties object.
    */
    template <typename T>
    Coordinate<T> GeoJson<T>::parseCoordinate(const std::vector<json11::Json> &coordinates, const std::map<std::string, json11::Json> &properties) {

        // Search for time property
        auto it = properties.find("time");
        if (it != properties.end()) {
            
            // Get time
            T time = (T)0.0;
            if (it->second.is_number())
                time = (T)it->second.number_value();
                
            // Add coordinates with time
            if (coordinates.size() < 3) {
                return Coordinate<T>( { (T)coordinates[0].number_value(), (T)coordinates[1].number_value(), (T)0.0, time } );
            } else {
                return Coordinate<T>( { (T)coordinates[0].number_value(), (T)coordinates[1].number_value(), (T)coordinates[2].number_value(), time } );
            }

        } else {
            
            // Add coordinates
            if (coordinates.size() < 3) {
                return Coordinate<T>( { (T)coordinates[0].number_value(), (T)coordinates[1].number_value() } );
            } else {
                return Coordinate<T>( { (T)coordinates[0].number_value(), (T)coordinates[1].number_value(), (T)coordinates[2].number_value() } );
            }
        }
    }

    /**
    * GeoJSON parser.
    * Converts a GeoJSON string to a %Vector object
    * @param geoJsonStr GeoJSON string.
    * @return Geostack %Vector object.
    */
    template <typename T>
    bool GeoJson<T>::parseString(Geostack::Vector<T> &v, std::string geoJsonStr) {

        // Create Json object
        std::string err;
        const auto geoJson = Json::parse(geoJsonStr, err);

        // Check for correct type
        if (geoJson["type"].string_value() == "FeatureCollection") {

            // List of coordinates and new geometry
            CoordinateList<T> cs;
            std::vector<cl_uint> newIndexes;
        
            // Check for feature array
            if (geoJson["features"].is_array()) {
                for (auto &feature : geoJson["features"].array_items()) {

                    // Check for correct feature type
                    if (feature["type"].string_value() == "Feature") {

                        // Read properties
                        auto properties = feature["properties"].object_items();

                        // Parse geometry
                        auto &geometry = feature["geometry"];
                        std::string geometryType = geometry["type"].string_value();
                        if (geometryType == "Point" || geometryType == "MultiPoint") {

                            // Parse point data
                            if (geometry["coordinates"].is_array()) {

                                newIndexes.clear();
                                if (geometryType == "Point") {

                                    // Add point
                                    newIndexes.push_back(v.addPoint(parseCoordinate(geometry["coordinates"].array_items(), properties)));

                                } else {

                                    // Add multiple points
                                    for (auto &coordinates : geometry["coordinates"].array_items())
                                        newIndexes.push_back(v.addPoint(parseCoordinate(coordinates.array_items(), properties)));
                                }

                                // Add properties
                                for (auto &property : properties) {

                                    // Skip time property
                                    if (property.first == "time")
                                        continue;

                                    // Parse values
                                    if (property.second.is_string()) {

                                        // Set string property
                                        for (auto &index : newIndexes)
                                            v.setProperty(index, property.first, property.second.string_value());

                                    } else if (property.second.is_number()) {

                                        // Set number property
                                        for (auto &index : newIndexes)
                                            v.setProperty(index, property.first, property.second.number_value());

                                    } else {

                                        // Unsupported types
                                    }
                                }

                            } else {
                                std::cout << "ERROR: point coordinates not found" << std::endl;
                                return false;
                            }
                        } else if (geometryType == "LineString" || geometryType == "MultiLineString") {
                        
                            // Parse line string data
                            if (geometry["coordinates"].is_array()) {
                                
                                newIndexes.clear();
                                if (geometryType == "LineString") {

                                    // Add line string
                                    cs.clear();
                                    for (auto &coordinates : geometry["coordinates"].array_items())
                                        cs.push_back(parseCoordinate(coordinates.array_items(), properties));
                                    newIndexes.push_back(v.addLineString(cs));

                                } else {

                                    // Add multiple line strings
                                    for (auto &line : geometry["coordinates"].array_items()) {
                                        cs.clear();
                                        for (auto &coordinates : line.array_items())
                                            cs.push_back(parseCoordinate(coordinates.array_items(), properties));
                                        newIndexes.push_back(v.addLineString(cs));
                                    }
                                }

                                // Add properties
                                for (auto &property : properties) {

                                    // Skip time property
                                    if (property.first == "time")
                                        continue;

                                    // Parse values
                                    if (property.second.is_string()) {

                                        // Set string property
                                        for (auto &index : newIndexes)
                                            v.setProperty(index, property.first, property.second.string_value());

                                    } else if (property.second.is_number()) {

                                        // Set number property
                                        for (auto &index : newIndexes)
                                            v.setProperty(index, property.first, property.second.number_value());

                                    } else {

                                        // Unsupported types
                                    }
                                }

                            } else {
                                std::cout << "ERROR: line string coordinates not found" << std::endl;
                                return false;
                            }

                        } else if (geometryType == "Polygon" || geometryType == "MultiPolygon") {
                        
                            // Parse polygon data
                            if (geometry["coordinates"].is_array()) {
                            
                                newIndexes.clear();
                                if (geometryType == "Polygon") {

                                    // Add polygons
                                    std::vector<CoordinateList<T> > pcs;
                                    for (auto &polygon : geometry["coordinates"].array_items()) {

                                        // Add coordinates
                                        cs.clear();
                                        for (auto &coordinates : polygon.array_items())
                                            cs.push_back(parseCoordinate(coordinates.array_items(), properties));

                                        // Check first coordinate is the same as the last
                                        if (cs.front() != cs.back()) {
                                            std::cout << "ERROR: last coordinate in polygon must be same as the first" << std::endl;
                                            return false;
                                        }

                                        // Update polygon list
                                        pcs.push_back(cs);
                                    }
                                    newIndexes.push_back(v.addPolygon(pcs));

                                } else {

                                    // Add multiple polygons
                                    std::vector<CoordinateList<T> > pcs;
                                    for (auto &multiPolygon : geometry["coordinates"].array_items()) {
                                        pcs.clear();
                                        for (auto &polygon : multiPolygon.array_items()) {

                                            // Add coordinates
                                            cs.clear();
                                            for (auto &coordinates : polygon.array_items())
                                                cs.push_back(parseCoordinate(coordinates.array_items(), properties));

                                            // Check first coordinate is the same as the last
                                            if (cs.front() != cs.back()) {
                                                std::cout << "ERROR: last coordinate in polygon must be same as the first" << std::endl;
                                                return false;
                                            }

                                            // Update polygon list
                                            pcs.push_back(cs);
                                        }
                                        newIndexes.push_back(v.addPolygon(pcs));
                                    }

                                }

                                // Add properties
                                for (auto &property : properties) {

                                    // Skip time property
                                    if (property.first == "time")
                                        continue;

                                    // Parse values
                                    if (property.second.is_string()) {

                                        // Set string property
                                        for (auto &index : newIndexes)
                                            v.setProperty(index, property.first, property.second.string_value());

                                    } else if (property.second.is_number()) {

                                        // Set number property
                                        for (auto &index : newIndexes)
                                            v.setProperty(index, property.first, property.second.number_value());

                                    } else {

                                        // Unsupported types
                                    }
                                }

                            } else {
                                std::cout << "ERROR: polygon coordinates not found" << std::endl;
                                return false;
                            }

                        } else {
                            std::cout << "ERROR: cannot parse geometry type '" << geometryType << "' in GeoJSON" << std::endl;
                            return false;
                        }

                    } else {
                        std::cout << "ERROR: cannot find feature in GeoJSON" << std::endl;
                        return false;
                    }
                }
            } else {
                std::cout << "ERROR: cannot find feature array in GeoJSON" << std::endl;
                return false;
            }
        } else {
            std::cout << "ERROR: string is not GeoJSON" << std::endl;
            return false;
        }

        return true;
    }

    /**
    * GeoJSON parser.
    * Converts a %Vector object to a GeoJSON string
    * @param v %Vector object.
    * @return GeoJSON string.
    */
    template <typename T>
    std::string GeoJson<T>::vectorToGeoJson(Geostack::Vector<T> &v) {
        
        Json::array jsonFeatures;

        // Get geometry and properties
        auto &geometryIndexes = v.getGeometryIndexes();
        auto &properties = v.getProperties();

        for (const auto &index : geometryIndexes) {

            // Check type
            auto &g = v.getGeometry(index);
            if (!g->isType(GeometryType::Point | GeometryType::LineString | GeometryType::Polygon))
                continue;
        
            // Create feature
            Json::object feature = Json::object { { "type", "Feature" } };

            // Process type
            if (g->isType(GeometryType::Point)) {
            
                // Create geometry
                auto p = std::static_pointer_cast<Point<T> >(g);
                auto c = v.getPointCoordinate(index);
                feature["geometry"] = Json::object {
                    { "type", "Point" },
                    { "coordinates", Json::array { c.p, c.q } }
                };

            } else if (g->isType(GeometryType::LineString)) {
            
                // Parse coordinates
                auto l = std::static_pointer_cast<LineString<T> >(g);
                Json::array jsonLineGeometry = Json::array();
                for (auto &c : v.getLineStringCoordinates(index)) {
                    jsonLineGeometry.push_back( Json::array { c.p, c.q });
                }
            
                // Create geometry
                feature["geometry"] = Json::object {
                    { "type", "LineString" },
                    { "coordinates", jsonLineGeometry }
                };
            
            } else if (g->isType(GeometryType::Polygon)) {
        
                // Create arrays
                Json::array jsonPolygonGeometry = Json::array();
                Json::array jsonSubPolygonGeometry = Json::array();
            
                // Parse coordinates
                auto p = std::static_pointer_cast<Polygon<T> >(g);
                auto subIndexes = p->getSubIndexes();
                cl_uint currentIndex = 0;
                for (auto &c : v.getPolygonCoordinates(index)) {

                    // Add point to sub-geometry
                    jsonSubPolygonGeometry.push_back( Json::array { c.p, c.q });

                    // Create new entry for each sub-polygon
                    if (--subIndexes[currentIndex] == 0) {

                        // Move to next index
                        currentIndex++;

                        // Add to geometry
                        jsonPolygonGeometry.push_back(jsonSubPolygonGeometry);
                        jsonSubPolygonGeometry.clear();
                    }
                }
            
                // Create geometry
                feature["geometry"] = Json::object {
                    { "type", "Polygon" },
                    { "coordinates", jsonPolygonGeometry }
                };
            }

            // Get properties
            Json::object jsonProperties = Json::object();
            for (const auto &p : properties) {

                // Check for property
                if (p.second.size() > 0) {

                    // Get property
                    auto &pv = p.second[g->getID()];
                
                    // Parse property
                    switch (pv.getType()) {

                        case(PropertyType::String):
                                
                            // Add string field
                            jsonProperties[p.first] = pv.template get<std::string>();
                            break;

                        case(PropertyType::Integer):
                        case(PropertyType::Index):
                                
                            // Add integer field
                            jsonProperties[p.first] = pv.template get<int>();
                            break;

                        case(PropertyType::Double):
                                
                            // Add floating point field
                            jsonProperties[p.first] = pv.template get<double>();
                            break;
                    }
                 }
            }
            feature["properties"] = jsonProperties;

            // Add to features
            jsonFeatures.push_back(feature);
        }

        // Create object
        Json geoJson = Json::object {
            { "type", "FeatureCollection" }, 
            { "features", jsonFeatures }
        };

        // Convert to string
        return geoJson.dump();
    }
    
    /**
    * Set GeoJSON projection and check bounds
    * @param v %Vector object.
    */
    template <typename T>
    void GeoJson<T>::setProjection(Vector<T> &v) {

        // Set projection to EPSG:4326, as per the standard
        std::string projPROJ4_EPSG4326 = R"(+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs)";
        auto proj_EPSG4326 = Projection<T>::parsePROJ4(projPROJ4_EPSG4326);
        v.setProjectionParameters(proj_EPSG4326);

        // Check bounds
        auto bounds = v.getBounds();
        if (bounds.min.p < -180.0 || bounds.max.p > 180.0 || bounds.min.q < -90.0 || bounds.max.q > 90.0) {
            std::cout << "WARNING: GeoJSON data out of bounds, " << 
                bounds.min.p << "-" << bounds.max.p << ", " <<
                bounds.min.q << "-" << bounds.max.q << std::endl;
        }
    }

    // Float type definitions
    template Geostack::Vector<float> GeoJson<float>::geoJsonFileToVector(std::string);
    template Geostack::Vector<float> GeoJson<float>::geoJsonToVector(std::string);
    template std::string GeoJson<float>::vectorToGeoJson(Geostack::Vector<float> &);
        
    // Double type definitions
    template Geostack::Vector<double> GeoJson<double>::geoJsonFileToVector(std::string);
    template Geostack::Vector<double> GeoJson<double>::geoJsonToVector(std::string);
    template std::string GeoJson<double>::vectorToGeoJson(Geostack::Vector<double> &);
}