# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3


from cython.operator import dereference as deref
from libcpp.string cimport string
from libcpp cimport bool
from libcpp.map cimport map as cpp_map
from libcpp.pair cimport pair as cpp_pair
from libcpp.vector cimport vector
from libc.stdint cimport uint32_t, int32_t, uint64_t, int64_t
from libcpp.iterator cimport iterator
import numpy as np
cimport cython
cimport numpy as np
from libcpp.memory cimport shared_ptr, unique_ptr, make_shared

np.import_array()

ctypedef cython.floating floating
ctypedef cython.integral integral

cdef extern from "gs_series.h" namespace "Geostack::SeriesInterpolation":
    cdef enum SeriesInterpolationType:
        Linear = 0
        MonotoneCubic = 1
        BoundedLinear = 2

cdef extern from "gs_series.h" namespace "Geostack":
    cdef cppclass SeriesItem[XTYPE, YTYPE]:
        XTYPE x
        YTYPE y
    
    cdef cppclass Series[X, Y]:
        Series() except +
        Series(Series &) except +
        Series& operator=(Series &) except +
        void clear() except +
        void addValue(X xval, Y yval) except +
        void addValues(vector[SeriesItem[X, Y]] newValues) except +
        void addValues(vector[cpp_pair[string, Y]] newValues) except +
        void update() except +
        void updateLimits() except +
        void setBounds(Y lowerLimit, Y upperLimit) except +

        Y operator()(X) except +
        
        X get_xMax() except +
        X get_xMin() except +
        Y get_yMax() except +
        Y get_yMin() except +

        bool inRange(X) except +
        void setName(string &name) except +
        string getName() except +
        bool isInitialised() except +
        bool isConstant() except +
        void setInterpolation(SeriesInterpolationType interp_type) except +

cdef class Series_dbl_flt:
    cdef shared_ptr[Series[double, float]] thisptr
    cpdef void clear(self)
    cpdef double get_xMax(self)
    cpdef double get_xMin(self)
    cpdef float get_yMax(self)
    cpdef float get_yMin(self)
    cpdef bool isInitialised(self)
    cpdef bool isConstant(self)
    cpdef string getName(self)
    cpdef void setName(self, string name)
    cpdef bool inRange(self, double x)
    cpdef void update(self)
    cpdef void updateLimits(self)
    cpdef void setBounds(self, float lowerLimit, float upperLimit)
    cdef float get(self, double x)
    cpdef void from_series(self, Series_dbl_flt other)
    cpdef void setInterpolation(self, int interp_type)
    cpdef void add_value(self, double xval, float yval)
    cpdef void add_values(self, double[:] xvals, float[:] yvals)

cdef class Series_dbl_dbl:
    cdef shared_ptr[Series[double, double]] thisptr
    cpdef void clear(self)
    cpdef double get_xMax(self)
    cpdef double get_xMin(self)
    cpdef double get_yMax(self)
    cpdef double get_yMin(self)
    cpdef bool isInitialised(self)
    cpdef bool isConstant(self)
    cpdef string getName(self)
    cpdef void setName(self, string name)
    cpdef bool inRange(self, double x)
    cpdef void update(self)
    cpdef void updateLimits(self)
    cpdef void setBounds(self, double lowerLimit, double upperLimit)
    cdef double get(self, double x)
    cpdef void from_series(self, Series_dbl_dbl other)
    cpdef void setInterpolation(self, int interp_type)
    cpdef void add_value(self, double xval, double yval)
    cpdef void add_values(self, double[:] xvals, double[:] yvals)

cdef class Series_int_dbl:
    cdef shared_ptr[Series[int64_t, double]] thisptr
    cpdef void clear(self)
    cpdef int64_t get_xMax(self)
    cpdef int64_t get_xMin(self)
    cpdef double get_yMax(self)
    cpdef double get_yMin(self)
    cpdef bool isInitialised(self)
    cpdef bool isConstant(self)
    cpdef string getName(self)
    cpdef void setName(self, string name)
    cpdef bool inRange(self, int64_t x)
    cpdef void update(self)
    cpdef void updateLimits(self)
    cpdef void setBounds(self, double lowerLimit, double upperLimit)
    cdef double get(self, int64_t x)
    cpdef void from_series(self, Series_int_dbl other)
    cpdef void setInterpolation(self, int interp_type)
    cpdef void add_value(self, int64_t xval, double yval)
    cpdef void add_values(self, int64_t[:] xvals, double[:] yvals)

cdef class Series_int_flt:
    cdef shared_ptr[Series[int64_t, float]] thisptr
    cpdef void clear(self)
    cpdef int64_t get_xMax(self)
    cpdef int64_t get_xMin(self)
    cpdef float get_yMax(self)
    cpdef float get_yMin(self)
    cpdef bool isInitialised(self)
    cpdef bool isConstant(self)
    cpdef string getName(self)
    cpdef void setName(self, string name)
    cpdef bool inRange(self, int64_t x)
    cpdef void update(self)
    cpdef void updateLimits(self)
    cpdef void setBounds(self, float lowerLimit, float upperLimit)
    cdef float get(self, int64_t x)
    cpdef void from_series(self, Series_int_flt other)
    cpdef void setInterpolation(self, int interp_type)
    cpdef void add_value(self, int64_t xval, float yval)
    cpdef void add_values(self, int64_t[:] xvals, float[:] yvals)

ctypedef SeriesItem[double, floating] series_dbl
ctypedef SeriesItem[int64_t, floating] series_int

cdef vector[cpp_pair[string, floating]] vec_of_pair(self, char[:, :] xval, floating[:] yval)
cdef vector[series_dbl] vec_of_series_dbl(self, double[:] xval, floating[:] yval)
cdef vector[series_int] vec_of_series_int(self, int64_t[:] xval, floating[:] yval)
cdef series_dbl get_series_item_dbl(double x, floating y)
cdef series_int get_series_item_int(int64_t x, floating y)