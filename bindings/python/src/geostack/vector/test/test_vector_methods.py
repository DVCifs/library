# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import sys
import json
import numpy as np
from time import time
import pytest
from distutils import dir_util
sys.path.insert(0, os.path.realpath('../../../'))

from geostack.io import geoJsonToVector, vectorToGeoJson
from geostack.gs_enums import GeometryType
from geostack.core import ProjectionParameters

@pytest.fixture
def datadir(tmpdir, request):
    '''
    Fixture responsible for searching a folder with the same name of test
    module and, if available, moving all contents to a temporary directory so
    tests can use them freely.

    ref: https://stackoverflow.com/questions/29627341/pytest-where-to-store-expected-data
    '''
    filename = request.module.__file__
    test_dir, _ = os.path.splitext(filename)

    if os.path.isdir(test_dir):
        dir_util.copy_tree(test_dir, str(tmpdir))

    return tmpdir

@pytest.mark.xfail
def test_vec_methods(datadir):

    proj_EPSG3111 = "(+proj=lcc +lat_1=-36 +lat_2=-38 +lat_0=-37 +lon_0=145 +x_0=2500000 +y_0=2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)"
    proj_EPSG3111_REAL = ProjectionParameters.from_proj4(proj_EPSG3111)
    projPROJ4_EPSG4326 = "(+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs)"
    proj_EPSG4326_REAL = ProjectionParameters.from_proj4(projPROJ4_EPSG4326)

    start = time()
    file_path = datadir.join("test_data_2.geojson")
    fileVector = geoJsonToVector(file_path.strpath, dtype=np.float32)
    end = time()
    print("Time taken to process file %f" % (end - start))

    nearestGeoJson = """{"features": [{"geometry": {"coordinates": [144.07501, -37.28393], "type": "Point"}, "properties": {"radius": 0.05}, "type": "Feature"}], "type": "FeatureCollection"}"""

    nearestPointVector = geoJsonToVector(nearestGeoJson)
    nearestVector = fileVector.nearest(nearestPointVector.getBounds())

    with open(datadir.join("out_test_data_nearest.geojson").strpath, "w") as out:
        out.write(vectorToGeoJson(nearestVector))

    with open(datadir.join("out_test_data_nearest_point.geojson").strpath, "w") as out:
        out.write(vectorToGeoJson(nearestPointVector))

    start = time()
    fileVector.deduplicateVertices()
    end = time()
    print("Time taken to deduplicate %f" % (end - start))

    boundsGeoJson = """{"features": [{"geometry": {"coordinates": [[[143.73701, -37.46474], [143.73701, -37.13560], [144.41891, -37.13560], [144.41891, -37.46474], [143.73701, -37.46474]]], "type": "Polygon"}, "properties": {}, "type": "Feature"}], "type": "FeatureCollection"}"""
    boundsVector = geoJsonToVector(boundsGeoJson)

    regionVector = fileVector.region(boundsVector.getBounds())
    with open(datadir.join("out_test_data_region.geojson").strpath, "w") as out:
        out.write(vectorToGeoJson(regionVector))

    with open(datadir.join("out_test_data_bounds.geojson").strpath, "w") as out:
        out.write(vectorToGeoJson(boundsVector))

    fileVector = fileVector.convert(proj_EPSG3111_REAL)
    nearestPointVector = nearestPointVector.convert(proj_EPSG3111_REAL)
    testRasterise = fileVector.mapDistance(50.0, geom_type=GeometryType.LineString)
    testRasterise.setProjectionParameters(proj_EPSG3111_REAL)
    testRasterise.mapVector(nearestPointVector, widthPropertyName="radius")
    testRasterise.write(datadir.join("out_test_data_distance.tif").strpath)

    contourVector = testRasterise.vectorise([1000.0, 2000.0])
    contourVector = contourVector.convert(proj_EPSG4326_REAL)

    with open(datadir.join("out_test_data_contour.geojson").strpath, "w") as out:
        out.write(vectorToGeoJson(contourVector))