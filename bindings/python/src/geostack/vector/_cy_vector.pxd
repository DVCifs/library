# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
# cython: language_level=3

from cython.operator import dereference as deref
from libcpp.string cimport string
from libc.stdint cimport uint16_t, uint32_t, uint64_t
from libcpp cimport bool
from libcpp.map cimport map as cpp_map
from libcpp.pair cimport pair
from libcpp.vector cimport vector
from libcpp.memory cimport shared_ptr, make_shared, unique_ptr
from libcpp.list cimport list as cpp_list
import numpy as np
cimport cython
cimport numpy as np
from ..core._cy_property cimport *
from ..raster._cy_raster cimport _cyRaster_d, _cyRaster_f, GeometryType
from ..core._cy_projection cimport _ProjectionParameters_f, _ProjectionParameters_d

np.import_array()

ctypedef uint16_t cl_uint16
ctypedef uint32_t cl_uint

cdef extern from "utils.h":
    void cy_copy[T](T& a, T& b)

cdef extern from "gs_raster.h" namespace "Geostack":
    cdef cppclass Raster[R, C]:
        pass

cdef extern from "gs_projection.h" namespace "Geostack":
    cdef cppclass ProjectionParameters[C]:
        pass

cdef extern from "gs_vector.h" namespace "Geostack":
    cdef cppclass VectorGeometryProperty:
        string name
        GeometryType geometryType
        PropertyType dataType

    cdef cppclass Coordinate[T]:
        Coordinate() except +
        Coordinate(Coordinate[T] &c) except +
        Coordinate(T p, T q, T r, T s) except +
        T magnitudeSquared() except +
        Coordinate[T] max_c "max"(Coordinate[T] &a, Coordinate[T] &b) except +
        Coordinate[T] min_c "min"(Coordinate[T] &a, Coordinate[T] &b) except +
        Coordinate[T] centroid(Coordinate[T] &a, Coordinate[T] &b) except +
        Coordinate[T]& operator=(Coordinate[T] &c) except +
        T p, q, r, s
        string geoHashEnc32
        string getGeoHash()
    
    bool operator==[T](Coordinate[T] &a, Coordinate[T] &b) except +
    bool operator!=[T](Coordinate[T] &a, Coordinate[T] &b) except +
    Coordinate[T]& operator+[T](Coordinate[T] &a, Coordinate[T] &b) except +
    Coordinate[T]& operator-[T](Coordinate[T] &a, Coordinate[T] &b) except +

    cdef cppclass BoundingBox[T]:
        BoundingBox() except +
        BoundingBox(BoundingBox[T] &b) except +
        BoundingBox(Coordinate[T], Coordinate[T]) except +
        Coordinate[T] min_c "min"
        Coordinate[T] max_c "max"
        void reset()
        void extend(T)
        void extend(Coordinate[T]) except +
        void extend(const BoundingBox[T] &b) except +
        T minimumDistanceSqr(BoundingBox[T] &b) except +
        T centroidDistanceSqr(const BoundingBox[T] &b) except +
        Coordinate[T] centroid()
        Coordinate[T] extent()
        uint64_t createZIndex(Coordinate[T] c)
        BoundingBox[T] convert(ProjectionParameters[T] this, ProjectionParameters[T] other)
        bool contains(const Coordinate[T] c)
        @staticmethod
        bool bbox_contains_coordinate "boundingBoxContains"(BoundingBox[T] A, Coordinate[T] c)
        @staticmethod
        bool bbox_contains_bbox "boundingBoxContains"(BoundingBox[T] A, BoundingBox[T] B)
        @staticmethod
        bool boundingBoxIntersects(BoundingBox[T] A, BoundingBox[T] B)
        const BoundingBox[T] geoHashBounds

    cdef cppclass VertexCollection[T]:
        VertexCollection() except +
        vector[Coordinate[T]]& getCoordinates()
        void clear()
        size_t size()

    cdef cppclass GeometryBase[T]:
        BoundingBox[T] getBounds()
        bool isContainer()

    cdef cppclass Vector[T]:
        Vector() except+
        Vector(const Vector &v) except+
        bool operator==(Vector &v)

        cl_uint addPoint(Coordinate[T] c_)
        cl_uint addLineString(vector[Coordinate[T]] cs_)
        cl_uint addPolygon(vector[vector[Coordinate[T]]] pcs_)

        void updatePointIndex(cl_uint index)
        void updateLineStringIndex(cl_uint index)
        void updatePolygonIndex(cl_uint index)
        void clear()

        Coordinate[T]& getCoordinate(cl_uint index)
        cpp_map[string, vector[Property]]& getProperties()

        vector[cl_uint]& getGeometryIndexes() nogil except +IndexError
        vector[cl_uint]& getPointIndexes() nogil except +IndexError
        vector[cl_uint]& getLineStringIndexes() nogil except +IndexError
        vector[cl_uint]& getPolygonIndexes() nogil except +IndexError

        Coordinate[T] getPointCoordinate(cl_uint index)
        vector[Coordinate[T]] getLineStringCoordinates(cl_uint index)
        vector[Coordinate[T]] getPolygonCoordinates(cl_uint index)
        vector[cl_uint]& getPolygonVertexIndexes(cl_uint index) nogil except +IndexError
        vector[cl_uint]& getPolygonSubIndexes(cl_uint index) nogil except +IndexError
        vector[BoundingBox[T]]& getPolygonSubBounds(cl_uint index) nogil except +IndexError

        void setProperty[P](cl_uint index, string name, P v) except +IndexError
        P getProperty[P](cl_uint index, string name) except +IndexError
        void deleteProperty(cl_uint index, string name) except +IndexError

        void setProjectionParameters(ProjectionParameters[T] proj_)
        ProjectionParameters[T] getProjectionParameters()
        Vector[T] convert(ProjectionParameters[T] proj_to)

        Vector[T] region(BoundingBox[T] bounds, size_t geometryTypes)
        Vector[T] nearest(BoundingBox[T] bounds, size_t geometryTypes)
        vector[shared_ptr[GeometryBase[T]]] attached(Coordinate[T] coord, size_t geometryTypes)
        void deduplicateVertices()
        Raster[R, T] mapDistance[R](T resolution, size_t geometryTypes,
            BoundingBox[T] bounds) nogil except+
        Raster[R, T] rasterise[R](T resolution, string script, size_t geometryTypes,
            BoundingBox[T] bounds) nogil except+
        bool pointSample[R](Raster[R, T] &r)
        BoundingBox[T] getBounds()
        bool hasData()

cdef extern from "<utility>" namespace "std" nogil:
    cdef shared_ptr[Vector[double]] move(shared_ptr[Vector[double]])
    cdef shared_ptr[Vector[float]] move(shared_ptr[Vector[float]])
    cdef shared_ptr[BoundingBox[double]] move(shared_ptr[BoundingBox[double]] bb)
    cdef shared_ptr[BoundingBox[float]] move(shared_ptr[BoundingBox[float]] bb)
    cdef vector[size_t]& move(vector[size_t]&)

ctypedef Coordinate[double] _coordinate_d
ctypedef Coordinate[float] _coordinate_f
ctypedef pair[_coordinate_d, _coordinate_d] _coordinatePair_d
ctypedef pair[_coordinate_f, _coordinate_f] _coordinatePair_f
ctypedef BoundingBox[double] _boundingBox_d
ctypedef BoundingBox[float] _boundingBox_f
ctypedef shared_ptr[_boundingBox_d] _bbox_ptr_d
ctypedef shared_ptr[_boundingBox_f] _bbox_ptr_f
ctypedef vector[_boundingBox_d] _bbox_list_d
ctypedef vector[_boundingBox_f] _bbox_list_f
ctypedef vector[cl_uint] _index_list

cdef class IndexList:
    cdef vector[cl_uint] *thisptr
    cdef public int index
    cdef void c_copy(self, _index_list other)
    @staticmethod
    cdef IndexList from_index_list(_index_list this)

cdef class _Coordinate_d:
    cdef Coordinate[double] *thisptr
    cdef double lng
    cdef double lat
    cdef double lev
    cdef double tstep
    cdef void c_copy(self, Coordinate[double] c)
    cpdef _Coordinate_d maxCoordinate(self, _Coordinate_d this, _Coordinate_d other)
    cpdef _Coordinate_d minCoordinate(self, _Coordinate_d this, _Coordinate_d other)
    cpdef _Coordinate_d centroid(self, _Coordinate_d this, _Coordinate_d other)
    cpdef double magnitudeSquared(self)
    cpdef string getGeoHash(self)

cdef class _Coordinate_f:
    cdef Coordinate[float] *thisptr
    cdef float lng
    cdef float lat
    cdef float lev
    cdef float tstep
    cdef void c_copy(self, Coordinate[float] c)
    cpdef _Coordinate_f maxCoordinate(self, _Coordinate_f this, _Coordinate_f other)
    cpdef _Coordinate_f minCoordinate(self, _Coordinate_f this, _Coordinate_f other)
    cpdef _Coordinate_f centroid(self, _Coordinate_f this, _Coordinate_f other)
    cpdef float magnitudeSquared(self)
    cpdef string getGeoHash(self)

cdef class _BoundingBox_d:
    cdef _bbox_ptr_d thisptr
    cdef void c_copy(self, _boundingBox_d other)
    cpdef _Coordinate_d centroid(self)
    cpdef _Coordinate_d extent(self)
    cpdef void extend_with_value(self, double other)
    cpdef void extend_with_coordinate(self, _Coordinate_d other)
    cpdef void extend_with_bbox(self, _BoundingBox_d other)
    cpdef double minimumDistanceSqr(self, _BoundingBox_d other)
    cpdef double centroidDistanceSqr(self, _BoundingBox_d other)
    cpdef void reset(self)
    cpdef _BoundingBox_d convert(self, _ProjectionParameters_d this,
        _ProjectionParameters_d other)
    cpdef bool contains(self, _Coordinate_d c)
    @staticmethod
    cdef bool _bbox_contains_coordinate(BoundingBox[double] A, Coordinate[double] c)
    @staticmethod
    cdef bool _bbox_contains_bbox(BoundingBox[double] A, BoundingBox[double] B)
    @staticmethod
    cdef bool _boundingBoxIntersects(BoundingBox[double] A, BoundingBox[double] B)

cdef class _BoundingBox_f:
    cdef _bbox_ptr_f thisptr
    cdef void c_copy(self, _boundingBox_f other)
    cpdef _Coordinate_f centroid(self)
    cpdef _Coordinate_f extent(self)
    cpdef void extend_with_value(self, float other)
    cpdef void extend_with_coordinate(self, _Coordinate_f other)
    cpdef void extend_with_bbox(self, _BoundingBox_f other)
    cpdef float minimumDistanceSqr(self, _BoundingBox_f other)
    cpdef float centroidDistanceSqr(self, _BoundingBox_f other)
    cpdef void reset(self)
    cpdef _BoundingBox_f convert(self, _ProjectionParameters_f this,
        _ProjectionParameters_f other)
    cpdef bool contains(self, _Coordinate_f c)
    @staticmethod
    cdef bool _bbox_contains_coordinate(BoundingBox[float] A, Coordinate[float] c)
    @staticmethod
    cdef bool _bbox_contains_bbox(BoundingBox[float] A, BoundingBox[float] B)
    @staticmethod
    cdef bool _boundingBoxIntersects(BoundingBox[float] A, BoundingBox[float] B)

cdef class _VertexCollection_d:
    cdef VertexCollection[double] *thisptr
    cpdef double[:, :] getCoordinates(self)
    cpdef size_t size(self)
    cpdef void clear(self)

cdef class _VertexCollection_f:
    cdef VertexCollection[float] *thisptr
    cpdef float[:, :] getCoordinates(self)
    cpdef size_t size(self)
    cpdef void clear(self)

cdef class _Vector_d:
    cdef shared_ptr[Vector[double]] thisptr
    cdef void c_copy(self, Vector[double] v)
    cdef size_t _add_point(self, Coordinate[double] c_)
    cdef size_t _add_line_string(self, vector[Coordinate[double]] cs_)
    cdef size_t _add_polygon(self, vector[vector[Coordinate[double]]] pcs_)
    cpdef _Vector_d region(self, _BoundingBox_d other, size_t parameters)
    cpdef _Vector_d nearest(self, _BoundingBox_d other, size_t parameters)
    cpdef _Vector_d attached(self, _Coordinate_d other, size_t parameters)
    cpdef _BoundingBox_d getBounds(self)
    cpdef _cyRaster_d mapDistance(self, double resolution, 
        size_t geom_type, _BoundingBox_d bounds) except+
    cpdef _cyRaster_d rasterise(self, double resolution, string script,
        size_t geom_type, _BoundingBox_d bounds) except+
    cpdef bool pointSample(self, _cyRaster_d r)
    cpdef _Vector_d convert(self, _ProjectionParameters_d other)
    cpdef void setProjectionParameters(self, _ProjectionParameters_d proj_)
    cpdef _ProjectionParameters_d getProjectionParameters(self)
    cpdef void updatePointIndex(self, size_t index)
    cpdef void updateLineStringIndex(self, size_t index)
    cpdef void updatePolygonIndex(self, size_t index)
    cpdef void clear(self)
    cpdef IndexList getGeometryIndexes(self)
    cpdef IndexList getPointIndexes(self)
    cpdef IndexList getLineStringIndexes(self)
    cpdef IndexList getPolygonIndexes(self)
    cpdef IndexList getPolygonSubIndexes(self, size_t index)
    cpdef _Coordinate_d getPointCoordinate(self, size_t index)
    cpdef double[:, :] getLineStringCoordinates(self, size_t index)
    cpdef double[:, :] getPolygonCoordinates(self, size_t index)

cdef class _Vector_f:
    cdef shared_ptr[Vector[float]] thisptr
    cdef void c_copy(self, Vector[float] v)
    cdef size_t _add_point(self, Coordinate[float] c_)
    cdef size_t _add_line_string(self, vector[Coordinate[float]] cs_)
    cdef size_t _add_polygon(self, vector[vector[Coordinate[float]]] pcs_)
    cpdef _Vector_f region(self, _BoundingBox_f other, size_t parameters)
    cpdef _Vector_f nearest(self, _BoundingBox_f other, size_t parameters)
    cpdef _Vector_f attached(self, _Coordinate_f other, size_t parameters)
    cpdef _BoundingBox_f getBounds(self)
    cpdef _cyRaster_f mapDistance(self, float resolution,
        size_t parameters, _BoundingBox_f bounds) except+
    cpdef _cyRaster_f rasterise(self, float resolution, string script,
        size_t parameters, _BoundingBox_f bounds) except+
    cpdef bool pointSample(self, _cyRaster_f r)
    cpdef _Vector_f convert(self, _ProjectionParameters_f other)
    cpdef void setProjectionParameters(self, _ProjectionParameters_f proj_)
    cpdef _ProjectionParameters_f getProjectionParameters(self)
    cpdef void updatePointIndex(self, size_t index)
    cpdef void updateLineStringIndex(self, size_t index)
    cpdef void updatePolygonIndex(self, size_t index)
    cpdef void clear(self)
    cpdef IndexList getGeometryIndexes(self)
    cpdef IndexList getPointIndexes(self)
    cpdef IndexList getLineStringIndexes(self)
    cpdef IndexList getPolygonIndexes(self)
    cpdef IndexList getPolygonSubIndexes(self, size_t index)
    cpdef _Coordinate_f getPointCoordinate(self, size_t index)
    cpdef float[:, :] getLineStringCoordinates(self, size_t index)
    cpdef float[:, :] getPolygonCoordinates(self, size_t index)