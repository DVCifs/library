# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import os
import json
import numbers
import os.path as pth
from collections import OrderedDict
import numpy as np
from ._cy_shapefile import (shapefile_d, shapefile_f)
from ..vector._cy_vector import _Vector_d, _Vector_f
from .. import raster
from .. import vector
from .. import gs_enums

__all__ = ["shapefileToVector", "vectorToShapefile"]

def shapefileToVector(filename, dtype=np.float32):
    if isinstance(filename, str):
        if dtype is not None:
            if dtype != np.float32 and dtype != np.float64:
                raise ValueError(
                    "dtype can be either numpy.float32 or numpy.float64")
        if dtype is None:
                out = shapefile_f.shapefileToVector(filename.encode('UTF-8'))
        elif dtype == np.float32:
                out = shapefile_f.shapefileToVector(filename.encode('UTF-8'))
        elif dtype == np.float64:
                out = shapefile_d.shapefileToVector(filename.encode('UTF-8'))
    else:
        raise TypeError("%s is not an acceptable type of argument" %
                        type(filename).__name__)
    return vector.Vector._from_vector(out)


def vectorToShapefile(vectorInp, filename,
                     geom_type: gs_enums.GeometryType=None):
    if geom_type is None:
        raise ValueError("No geometry type is specified")
    elif not isinstance(geom_type, gs_enums.GeometryType):
        raise TypeError("'geom_type' should be an instance of gs_enums.GeometryType")

    if isinstance(vectorInp, (vector.Vector, _Vector_d, _Vector_f)):
        if isinstance(vectorInp, vector.Vector):
            if vectorInp._dtype == np.float32:
                out = shapefile_f.vectorToShapefile(vectorInp._handle,
                    filename.encode("UTF-8"), geom_type.value)
            elif vectorInp._dtype == np.float64:
                out = shapefile_d.vectorToShapefile(vectorInp._handle,
                    filename.encode("UTF-8"), geom_type.value)
        elif isinstance(vectorInp, _Vector_d):
            out = shapefile_d.vectorToShapefile(vectorInp,
                filename.encode("UTF-8"), geom_type.value)
        elif isinstance(vectorInp, _Vector_f):
            out = shapefile_f.vectorToShapefile(vectorInp,
                filename.encode("UTF-8"), geom_type.value)
    else:
        raise TypeError(
            "Incorrect input type, only instance of vector python or cython class")
    return out
