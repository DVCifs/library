# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import os.path as pth
import calendar
import numbers
import numpy as np
from datetime import datetime, timedelta
from functools import partial
from ._cy_series import (
    Series_dbl_flt,
    Series_int_flt,
    Series_dbl_dbl,
    Series_int_dbl) #noqa
from .. import gs_enums
from ..dataset import import_or_skip

global HAS_PYTZ

pytz, HAS_PYTZ = import_or_skip('pytz')

__all__ = ["Series"]

def parse_date_string(inp_str, timezone=None, dt_format="%Y-%m-%dT%H:%M:%SZ"):
    time_zone = None

    if HAS_PYTZ:
        if timezone is None:
            time_zone = pytz.UTC
        else:
            if isinstance(timezone, pytz.BaseTzInfo):
                time_zone = timezone
            else:
                raise TypeError("timezone should be an instance of pytz.BaseTzInfo")
        
        try:
            if time_zone is pytz.UTC:
                out_date = datetime.strptime(inp_str, dt_format)
            else:
                out_date = datetime.strptime(inp_str, dt_format)
        except ValueError:
            try:
                if time_zone is pytz.UTC:
                    out_date = datetime.strptime(inp_str, dt_format)
                else:
                    out_date = datetime.strptime(inp_str, dt_format)
            except ValueError:
                raise ValueError("Incorrect datetime format")
        out_date = out_date.replace(tzinfo=time_zone)
    else:
        out_date = datetime.strptime(inp_str, dt_format)
    return out_date

class Series:
    def __init__(self, *args, **kwargs):
        self.obj_types = {"index_type":np.int64, "value_type":np.float32}

        if args:
            self._parse_args(args)
        if kwargs:
            self._parse_kwargs(kwargs)
        
        if self.obj_types['value_type'] == np.float32:
            if self.obj_types['index_type'] == np.int64:
                self._handle = Series_int_flt()
            else:
                self._handle = Series_dbl_flt()
        else:
            if self.obj_types['index_type'] == np.int64:
                self._handle = Series_int_dbl()
            else:
                self._handle = Series_dbl_dbl()
        
    def _parse_args(self, args):
        arg_len = min(len(args), 2)
        for item, arg in zip(['index_type', 'value_type'], range(arg_len)):
            if item == "index_type":
                if args[arg] in [int, np.int, np.int64]:
                    self.obj_types[item] = np.int64
                elif args[arg] in [float, np.float, np.float64]:
                    self.obj_types[item] = np.float64
                else:
                    raise ValueError(f"{item} type can be only int64/float64")
            else:
                if args[arg] in [float, np.float, np.float32]:
                    self.obj_types[item] = np.float32
                elif args[arg] in [np.float64]:
                    self.obj_types[item] = np.float64
                else:
                    raise ValueError(f"{item} type can be only float32/float64")

    def _parse_kwargs(self, kwargs):
        arg_len = min(len(kwargs), 2)
        for item in ['index_type', 'value_type']:
            if item == "index_type":
                if kwargs.get(item) in [int, np.int, np.int64]:
                    self.obj_types[item] = np.int64
                elif kwargs.get(item) in [float, np.float, np.float64]:
                    self.obj_types[item] = np.float64
                else:
                    raise ValueError("Index type can be only int64/float64")
            else:
                if kwargs.get(item) in [float, np.float, np.float32]:
                    self.obj_types[item] = np.float32
                elif kwargs.get(item) in [np.float64]:
                    self.obj_types[item] = np.float64
                else:
                    raise ValueError("Value type can be only float32/float64")

    @staticmethod
    def read_csv_file(filepath, **kwargs):
        if 'index_type' in kwargs:
            x_type = kwargs.get("index_type")
            kwargs.pop('index_type')
        else:
            x_type = np.int64
        if 'value_type' in kwargs:
            y_type = kwargs.get("value_type")
            kwargs.pop('value_type')
        else:
            x_type = np.float32
        out = Series(index_type=x_type, value_type=y_type)
        out.from_csvfile(filepath, **kwargs)
        return out

    def from_csvfile(self, other, index_col=0, usecols=None,
            parse_date=False, dt_format=None, time_factor=None,
            timezone=None, **kwargs):
        if not isinstance(other, str):
            raise TypeError("input argument should be of str type")
        if not pth.exists(other):
            raise FileNotFoundError(f"{other} is not present")

        if parse_date:
            if dt_format is None:
                datetime_format = "%Y-%m-%dT%H:%M:%SZ"
            else:
                datetime_format = dt_format

            if time_factor is None:
                _time_factor = 1.0
            else:
                _time_factor = time_factor
        
        if usecols is not None:
            assert len(usecols) == 2, "Series object only support two columns"
        else:
            usecols = [index_col, 1]
        
        if 'dtype' is not kwargs:
            if not parse_date:
                if self.obj_types['index_type'] == np.int64:
                    x_type = 'i8'
                elif self.obj_types['index_type'] == np.float64:
                    x_type = 'f8'
            else:
                x_type = "U20"
            if self.obj_types['value_type'] == np.float32:
                y_type = 'f4'
            elif self.obj_types['value_type'] == np.float64:
                y_type = 'f8'
            dtype = f'{x_type},{y_type}'
        else:
            dtype = kwargs.get('dtype')
            kwargs.pop('dtype')
        
        if 'names' in kwargs:
            if isinstance(kwargs['names'], list):
                col_names = kwargs['names']
                kwargs.pop('names')
            else:
                if kwargs['names']:
                    with open(other, 'r') as inp:
                        row = inp.readline()
                    col_names = [row.split(',')[item] for item in usecols]
        else:
            with open(other, 'r') as inp:
                row = inp.readline()
            col_names = [row.split(',')[item] for item in usecols]

        datainp = np.recfromcsv(other, dtype=dtype, usecols=usecols,
            names=col_names, **kwargs)

        if 'names' in kwargs:
            var_names = kwargs.get('names')
        else:
            var_names = datainp.dtype.names
        if index_col not in usecols:
            raise ValueError("index column should be in usecols")
        for item in usecols:
            if item != index_col:
                var_col = item
        nx = len(datainp)
        if index_col < var_col:
            var_name = var_names[1]
            index_name = var_names[0]
        else:
            var_name = var_names[0]
            index_name = var_names[1]
        
        if parse_date:
            date_parser = partial(parse_date_string, timezone=timezone,
                dt_format=datetime_format)
            dt_stamp = [date_parser(item).timestamp() for item in datainp[var_names[index_col]]]
            dt_stamp = np.array(dt_stamp, dtype=self.obj_types['index_type']) * _time_factor
        else:
            dt_stamp = datainp[index_name]
        self.add_values(dt_stamp, datainp[var_name])
        self.setName(var_name)

    def from_list(self, other):
        assert len(other[0]) == 2, "input argument should be list of list"
        self.from_ndarray(np.array(other))

    def from_ndarray(self, other):
        assert isinstance(other, np.ndarray), "input argument should be np.ndarray"
        if isinstance(other, np.recarray):
            self.from_recarray(other)
        else:
            assert other.shape[1] == 2, "np.ndarray should be (N,2) shape"
            self.add_values(other[:, 0].astype(self.obj_types['index_type']),
                other[:, 1].astype(self.obj_types['var_type']))

    def from_recarray(self, other):
        assert isinstance(other, np.recarray), "input argument should be np.recarray"
        assert len(other.dtype) == 2, "np.recarray should have two records"
        var_names = other.dtype.names
        index_name = var_names[0]
        var_name = var_names[1]
        self.setName(var_name)
        self.add_values(other[index_name].astype(self.obj_types['index_type']),
            other[var_name].astype(self.obj_types['var_type']))

    def add_value(self, x, y):
        x_type = self.obj_types['index_type']
        y_type = self.obj_types['value_type']
        assert x_type is not None and y_type is not None, "index/ value type not set"
        self._handle.add_value(x_type(x), y_type(y))

    def add_values(self, x, y):
        assert isinstance(x, np.ndarray) and isinstance(y, np.ndarray), "input should be np.ndarray"
        x_type = self.obj_types['index_type']
        y_type = self.obj_types['value_type']
        assert x_type is not None and y_type is not None, "index/ value type not set"
        self._handle.add_values(x.astype(x_type), y.astype(y_type))

    def setInterpolation(self, interp_type):
        if not isinstance(interp_type, (gs_enums.SeriesInterpolationType, numbers.Integral)):
            raise TypeError("series interpolation type should be int/ SeriesInterpolationType")
        if isinstance(interp_type, numbers.Integral):
            self._handle.setInterpolation(interp_type)
        elif isinstance(interp_type, gs_enums.SeriesInterpolationType):
            self._handle.setInterpolation(interp_type.value)
    
    def setName(self, other):
        if isinstance(other, str):
            self._handle.setName(other.encode('utf-8'))
        elif isinstance(other, bytes):
            self._handle.setName(other)
        else:
            raise TypeError("name should be of str/ bytes type")

    def setBounds(self, lowerLimit, upperLimit):
        y_type = self.obj_types['value_type']
        if y_type is not None:
            self._handle.setBounds(y_type(lowerLimit), y_type(upperLimit))
        else:
            raise RuntimeError("value type is not set")

    def get(self, x):
        x_type = self.obj_types['index_type']
        if x_type is not None:
            return self._handle(x_type(x))
        else:
            raise RuntimeError("index type is not set")

    @classmethod
    def from_series(cls, other):
        if isinstance(other, cls):
            out = cls(**other.obj_type)
            out._handle.from_series(other._handle)
        else:
            obj_type = {}
            if isinstance(other, Series_dbl_dbl):
                obj_type['index_type'] = np.float64
                obj_type['value_type'] = np.float64
            elif isinstance(other, Series_int_dbl):
                obj_type['index_type'] = np.int64
                obj_type['value_type'] = np.float64
            elif isinstance(other, Series_dbl_flt):
                obj_type['index_type'] = np.float64
                obj_type['value_type'] = np.float32
            elif isinstance(other, Series_int_flt):
                obj_type['index_type'] = np.int64
                obj_type['value_type'] = np.float32
            else:
                raise TypeError("input object should be of Series type")
            out = cls(**obj_type)
            out._handle.from_series(other)
        return out

    def getName(self):
        return self._handle.getName()

    def clear(self):
        self._handle.clear()
    
    def get_xMax(self):
        return self._handle.get_xMax()
    
    def get_xMin(self):
        return self._handle.get_xMin()
    
    def get_yMax(self):
        return self._handle.get_yMax()
    
    def get_yMin(self):
        return self._handle.get_yMin()
    
    def isInitialised(self):
        return self._handle.isInitialised()
    
    def isConstant(self):
        return self._handle.isConstant()

    def inRange(self, x):
        x_type = self.obj_types['index_type']
        if x_type is not None:
            return self._handle.inRange(x_type(x))
        else:
            raise RuntimeError("index type is not specified")

    def update(self):
        self._handle.update()

    def updateLimits(self):
        self._handle.updateLimits()

    def __call__(self, x):
        x_type = self.obj_types['index_type']
        return self._handle[x_type(x)]

    def __repr__(self):
        return "<class 'geostack.series.%s'>" % self.__class__.__name__

